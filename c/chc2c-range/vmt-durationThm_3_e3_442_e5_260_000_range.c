// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: vmt-chc-benchmarks/vmt-durationThm_3_e3_442_e5_260_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "vmt-durationThm_3_e3_442_e5_260_000_range.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int state_0;
    int state_1;
    _Bool state_2;
    _Bool state_3;
    int state_4;
    int state_5;
    int state_6;
    int state_7;
    _Bool state_8;
    int state_9;
    int state_10;
    int state_11;
    int state_12;
    _Bool state_13;
    int state_14;
    int state_15;
    _Bool state_16;
    _Bool state_17;
    int state_18;
    int state_19;
    int state_20;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    _Bool E_0;
    int F_0;
    int G_0;
    int H_0;
    int I_0;
    int J_0;
    int K_0;
    _Bool L_0;
    _Bool M_0;
    int N_0;
    _Bool O_0;
    _Bool P_0;
    _Bool Q_0;
    int R_0;
    int S_0;
    int T_0;
    int U_0;
    int A_1;
    int B_1;
    int C_1;
    int D_1;
    _Bool E_1;
    int F_1;
    int G_1;
    _Bool H_1;
    int I_1;
    int J_1;
    int K_1;
    int L_1;
    int M_1;
    int N_1;
    _Bool O_1;
    _Bool P_1;
    int Q_1;
    _Bool R_1;
    int S_1;
    int T_1;
    int U_1;
    _Bool V_1;
    int W_1;
    int X_1;
    int Y_1;
    int Z_1;
    _Bool A1_1;
    int B1_1;
    int C1_1;
    int D1_1;
    int E1_1;
    _Bool F1_1;
    int G1_1;
    _Bool H1_1;
    _Bool I1_1;
    int J1_1;
    _Bool K1_1;
    _Bool L1_1;
    int M1_1;
    int N1_1;
    int O1_1;
    int P1_1;
    int A_2;
    int B_2;
    int C_2;
    int D_2;
    _Bool E_2;
    int F_2;
    int G_2;
    int H_2;
    int I_2;
    int J_2;
    int K_2;
    _Bool L_2;
    _Bool M_2;
    int N_2;
    _Bool O_2;
    _Bool P_2;
    _Bool Q_2;
    int R_2;
    int S_2;
    int T_2;
    int U_2;

    if (((state_0 <= -1000000000) || (state_0 >= 1000000000))
        || ((state_1 <= -1000000000) || (state_1 >= 1000000000))
        || ((state_4 <= -1000000000) || (state_4 >= 1000000000))
        || ((state_5 <= -1000000000) || (state_5 >= 1000000000))
        || ((state_6 <= -1000000000) || (state_6 >= 1000000000))
        || ((state_7 <= -1000000000) || (state_7 >= 1000000000))
        || ((state_9 <= -1000000000) || (state_9 >= 1000000000))
        || ((state_10 <= -1000000000) || (state_10 >= 1000000000))
        || ((state_11 <= -1000000000) || (state_11 >= 1000000000))
        || ((state_12 <= -1000000000) || (state_12 >= 1000000000))
        || ((state_14 <= -1000000000) || (state_14 >= 1000000000))
        || ((state_15 <= -1000000000) || (state_15 >= 1000000000))
        || ((state_18 <= -1000000000) || (state_18 >= 1000000000))
        || ((state_19 <= -1000000000) || (state_19 >= 1000000000))
        || ((state_20 <= -1000000000) || (state_20 >= 1000000000))
        || ((A_0 <= -1000000000) || (A_0 >= 1000000000))
        || ((B_0 <= -1000000000) || (B_0 >= 1000000000))
        || ((C_0 <= -1000000000) || (C_0 >= 1000000000))
        || ((D_0 <= -1000000000) || (D_0 >= 1000000000))
        || ((F_0 <= -1000000000) || (F_0 >= 1000000000))
        || ((G_0 <= -1000000000) || (G_0 >= 1000000000))
        || ((H_0 <= -1000000000) || (H_0 >= 1000000000))
        || ((I_0 <= -1000000000) || (I_0 >= 1000000000))
        || ((J_0 <= -1000000000) || (J_0 >= 1000000000))
        || ((K_0 <= -1000000000) || (K_0 >= 1000000000))
        || ((N_0 <= -1000000000) || (N_0 >= 1000000000))
        || ((R_0 <= -1000000000) || (R_0 >= 1000000000))
        || ((S_0 <= -1000000000) || (S_0 >= 1000000000))
        || ((T_0 <= -1000000000) || (T_0 >= 1000000000))
        || ((U_0 <= -1000000000) || (U_0 >= 1000000000))
        || ((A_1 <= -1000000000) || (A_1 >= 1000000000))
        || ((B_1 <= -1000000000) || (B_1 >= 1000000000))
        || ((C_1 <= -1000000000) || (C_1 >= 1000000000))
        || ((D_1 <= -1000000000) || (D_1 >= 1000000000))
        || ((F_1 <= -1000000000) || (F_1 >= 1000000000))
        || ((G_1 <= -1000000000) || (G_1 >= 1000000000))
        || ((I_1 <= -1000000000) || (I_1 >= 1000000000))
        || ((J_1 <= -1000000000) || (J_1 >= 1000000000))
        || ((K_1 <= -1000000000) || (K_1 >= 1000000000))
        || ((L_1 <= -1000000000) || (L_1 >= 1000000000))
        || ((M_1 <= -1000000000) || (M_1 >= 1000000000))
        || ((N_1 <= -1000000000) || (N_1 >= 1000000000))
        || ((Q_1 <= -1000000000) || (Q_1 >= 1000000000))
        || ((S_1 <= -1000000000) || (S_1 >= 1000000000))
        || ((T_1 <= -1000000000) || (T_1 >= 1000000000))
        || ((U_1 <= -1000000000) || (U_1 >= 1000000000))
        || ((W_1 <= -1000000000) || (W_1 >= 1000000000))
        || ((X_1 <= -1000000000) || (X_1 >= 1000000000))
        || ((Y_1 <= -1000000000) || (Y_1 >= 1000000000))
        || ((Z_1 <= -1000000000) || (Z_1 >= 1000000000))
        || ((B1_1 <= -1000000000) || (B1_1 >= 1000000000))
        || ((C1_1 <= -1000000000) || (C1_1 >= 1000000000))
        || ((D1_1 <= -1000000000) || (D1_1 >= 1000000000))
        || ((E1_1 <= -1000000000) || (E1_1 >= 1000000000))
        || ((G1_1 <= -1000000000) || (G1_1 >= 1000000000))
        || ((J1_1 <= -1000000000) || (J1_1 >= 1000000000))
        || ((M1_1 <= -1000000000) || (M1_1 >= 1000000000))
        || ((N1_1 <= -1000000000) || (N1_1 >= 1000000000))
        || ((O1_1 <= -1000000000) || (O1_1 >= 1000000000))
        || ((P1_1 <= -1000000000) || (P1_1 >= 1000000000))
        || ((A_2 <= -1000000000) || (A_2 >= 1000000000))
        || ((B_2 <= -1000000000) || (B_2 >= 1000000000))
        || ((C_2 <= -1000000000) || (C_2 >= 1000000000))
        || ((D_2 <= -1000000000) || (D_2 >= 1000000000))
        || ((F_2 <= -1000000000) || (F_2 >= 1000000000))
        || ((G_2 <= -1000000000) || (G_2 >= 1000000000))
        || ((H_2 <= -1000000000) || (H_2 >= 1000000000))
        || ((I_2 <= -1000000000) || (I_2 >= 1000000000))
        || ((J_2 <= -1000000000) || (J_2 >= 1000000000))
        || ((K_2 <= -1000000000) || (K_2 >= 1000000000))
        || ((N_2 <= -1000000000) || (N_2 >= 1000000000))
        || ((R_2 <= -1000000000) || (R_2 >= 1000000000))
        || ((S_2 <= -1000000000) || (S_2 >= 1000000000))
        || ((T_2 <= -1000000000) || (T_2 >= 1000000000))
        || ((U_2 <= -1000000000) || (U_2 >= 1000000000)))
        abort ();

    // main logic
    goto main_init;

  main_init:
    if (!
        ((K_0 == N_0) && (F_0 == A_0) && (F_0 == S_0) && (H_0 == T_0)
         && (H_0 == G_0) && (I_0 == 0) && (I_0 == R_0) && (J_0 == 0)
         && (J_0 == U_0)
         && (((!M_0) || ((N_0 + (-1 * T_0) + (-1 * S_0)) <= 0)) == L_0)
         && (O_0 == M_0)
         && (E_0 ==
             ((Q_0 || (!(S_0 <= R_0))) && (U_0 <= T_0) && (1 <= S_0)
              && (1 <= T_0))) && (E_0 == O_0) && (K_0 == 0)))
        abort ();
    state_0 = K_0;
    state_1 = N_0;
    state_2 = E_0;
    state_3 = O_0;
    state_4 = J_0;
    state_5 = U_0;
    state_6 = I_0;
    state_7 = R_0;
    state_8 = M_0;
    state_9 = H_0;
    state_10 = T_0;
    state_11 = F_0;
    state_12 = S_0;
    state_13 = L_0;
    state_14 = G_0;
    state_15 = A_0;
    state_16 = Q_0;
    state_17 = P_0;
    state_18 = B_0;
    state_19 = C_0;
    state_20 = D_0;
    E_1 = __VERIFIER_nondet__Bool ();
    F_1 = __VERIFIER_nondet_int ();
    if (((F_1 <= -1000000000) || (F_1 >= 1000000000)))
        abort ();
    K1_1 = __VERIFIER_nondet__Bool ();
    G_1 = __VERIFIER_nondet_int ();
    if (((G_1 <= -1000000000) || (G_1 >= 1000000000)))
        abort ();
    I1_1 = __VERIFIER_nondet__Bool ();
    G1_1 = __VERIFIER_nondet_int ();
    if (((G1_1 <= -1000000000) || (G1_1 >= 1000000000)))
        abort ();
    E1_1 = __VERIFIER_nondet_int ();
    if (((E1_1 <= -1000000000) || (E1_1 >= 1000000000)))
        abort ();
    C1_1 = __VERIFIER_nondet_int ();
    if (((C1_1 <= -1000000000) || (C1_1 >= 1000000000)))
        abort ();
    A1_1 = __VERIFIER_nondet__Bool ();
    S_1 = __VERIFIER_nondet_int ();
    if (((S_1 <= -1000000000) || (S_1 >= 1000000000)))
        abort ();
    T_1 = __VERIFIER_nondet_int ();
    if (((T_1 <= -1000000000) || (T_1 >= 1000000000)))
        abort ();
    U_1 = __VERIFIER_nondet_int ();
    if (((U_1 <= -1000000000) || (U_1 >= 1000000000)))
        abort ();
    W_1 = __VERIFIER_nondet_int ();
    if (((W_1 <= -1000000000) || (W_1 >= 1000000000)))
        abort ();
    X_1 = __VERIFIER_nondet_int ();
    if (((X_1 <= -1000000000) || (X_1 >= 1000000000)))
        abort ();
    Y_1 = __VERIFIER_nondet_int ();
    if (((Y_1 <= -1000000000) || (Y_1 >= 1000000000)))
        abort ();
    Z_1 = __VERIFIER_nondet_int ();
    if (((Z_1 <= -1000000000) || (Z_1 >= 1000000000)))
        abort ();
    J1_1 = __VERIFIER_nondet_int ();
    if (((J1_1 <= -1000000000) || (J1_1 >= 1000000000)))
        abort ();
    H1_1 = __VERIFIER_nondet__Bool ();
    F1_1 = __VERIFIER_nondet__Bool ();
    D1_1 = __VERIFIER_nondet_int ();
    if (((D1_1 <= -1000000000) || (D1_1 >= 1000000000)))
        abort ();
    B1_1 = __VERIFIER_nondet_int ();
    if (((B1_1 <= -1000000000) || (B1_1 >= 1000000000)))
        abort ();
    N_1 = state_0;
    Q_1 = state_1;
    H_1 = state_2;
    R_1 = state_3;
    M_1 = state_4;
    P1_1 = state_5;
    L_1 = state_6;
    M1_1 = state_7;
    P_1 = state_8;
    K_1 = state_9;
    O1_1 = state_10;
    I_1 = state_11;
    N1_1 = state_12;
    O_1 = state_13;
    J_1 = state_14;
    A_1 = state_15;
    L1_1 = state_16;
    V_1 = state_17;
    B_1 = state_18;
    C_1 = state_19;
    D_1 = state_20;
    if (!
        ((N1_1 == S_1) && (Y_1 == U_1) && (Z_1 == X_1) && (B1_1 == Y_1)
         && (C1_1 == S_1) && (D1_1 == T_1) && (E1_1 == Z_1) && (G1_1 == W_1)
         && (N_1 == Q_1) && (M_1 == P1_1) && (L_1 == M1_1) && (K_1 == O1_1)
         && (I_1 == N1_1) && (J1_1 == G1_1)
         && (((!P_1) || ((Q_1 + (-1 * O1_1) + (-1 * N1_1)) <= 0)) == O_1)
         && (((!I1_1) || ((J1_1 + (-1 * D1_1) + (-1 * C1_1)) <= 0)) == H1_1)
         && (F1_1 ==
             (R_1 && (A1_1 || (!(C1_1 <= B1_1))) && (E1_1 <= D1_1)
              && (1 <= D1_1) && (1 <= C1_1))) && (R_1 == P_1) && (H_1 == R_1)
         && (K1_1 == F1_1) && (K1_1 == I1_1) && ((!L1_1)
                                                 || ((P1_1 + (-1 * X_1)) ==
                                                     2)) && (L1_1
                                                             || (X_1 == 0))
         && ((!V_1) || ((M1_1 + (-1 * U_1)) == 2)) && ((!V_1)
                                                       || ((Q_1 + (-1 * W_1))
                                                           == 2)) && (V_1
                                                                      || (U_1
                                                                          ==
                                                                          0))
         && (V_1 || (W_1 == 0)) && (O1_1 == T_1)))
        abort ();
    state_0 = G1_1;
    state_1 = J1_1;
    state_2 = F1_1;
    state_3 = K1_1;
    state_4 = Z_1;
    state_5 = E1_1;
    state_6 = Y_1;
    state_7 = B1_1;
    state_8 = I1_1;
    state_9 = T_1;
    state_10 = D1_1;
    state_11 = S_1;
    state_12 = C1_1;
    state_13 = H1_1;
    state_14 = G_1;
    state_15 = F_1;
    state_16 = A1_1;
    state_17 = E_1;
    state_18 = U_1;
    state_19 = X_1;
    state_20 = W_1;
    goto state_0;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  state_0:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          K_2 = state_0;
          N_2 = state_1;
          E_2 = state_2;
          O_2 = state_3;
          J_2 = state_4;
          U_2 = state_5;
          I_2 = state_6;
          R_2 = state_7;
          M_2 = state_8;
          H_2 = state_9;
          T_2 = state_10;
          F_2 = state_11;
          S_2 = state_12;
          L_2 = state_13;
          G_2 = state_14;
          A_2 = state_15;
          Q_2 = state_16;
          P_2 = state_17;
          B_2 = state_18;
          C_2 = state_19;
          D_2 = state_20;
          if (!(!L_2))
              abort ();
          goto main_error;

      case 1:
          E_1 = __VERIFIER_nondet__Bool ();
          F_1 = __VERIFIER_nondet_int ();
          if (((F_1 <= -1000000000) || (F_1 >= 1000000000)))
              abort ();
          K1_1 = __VERIFIER_nondet__Bool ();
          G_1 = __VERIFIER_nondet_int ();
          if (((G_1 <= -1000000000) || (G_1 >= 1000000000)))
              abort ();
          I1_1 = __VERIFIER_nondet__Bool ();
          G1_1 = __VERIFIER_nondet_int ();
          if (((G1_1 <= -1000000000) || (G1_1 >= 1000000000)))
              abort ();
          E1_1 = __VERIFIER_nondet_int ();
          if (((E1_1 <= -1000000000) || (E1_1 >= 1000000000)))
              abort ();
          C1_1 = __VERIFIER_nondet_int ();
          if (((C1_1 <= -1000000000) || (C1_1 >= 1000000000)))
              abort ();
          A1_1 = __VERIFIER_nondet__Bool ();
          S_1 = __VERIFIER_nondet_int ();
          if (((S_1 <= -1000000000) || (S_1 >= 1000000000)))
              abort ();
          T_1 = __VERIFIER_nondet_int ();
          if (((T_1 <= -1000000000) || (T_1 >= 1000000000)))
              abort ();
          U_1 = __VERIFIER_nondet_int ();
          if (((U_1 <= -1000000000) || (U_1 >= 1000000000)))
              abort ();
          W_1 = __VERIFIER_nondet_int ();
          if (((W_1 <= -1000000000) || (W_1 >= 1000000000)))
              abort ();
          X_1 = __VERIFIER_nondet_int ();
          if (((X_1 <= -1000000000) || (X_1 >= 1000000000)))
              abort ();
          Y_1 = __VERIFIER_nondet_int ();
          if (((Y_1 <= -1000000000) || (Y_1 >= 1000000000)))
              abort ();
          Z_1 = __VERIFIER_nondet_int ();
          if (((Z_1 <= -1000000000) || (Z_1 >= 1000000000)))
              abort ();
          J1_1 = __VERIFIER_nondet_int ();
          if (((J1_1 <= -1000000000) || (J1_1 >= 1000000000)))
              abort ();
          H1_1 = __VERIFIER_nondet__Bool ();
          F1_1 = __VERIFIER_nondet__Bool ();
          D1_1 = __VERIFIER_nondet_int ();
          if (((D1_1 <= -1000000000) || (D1_1 >= 1000000000)))
              abort ();
          B1_1 = __VERIFIER_nondet_int ();
          if (((B1_1 <= -1000000000) || (B1_1 >= 1000000000)))
              abort ();
          N_1 = state_0;
          Q_1 = state_1;
          H_1 = state_2;
          R_1 = state_3;
          M_1 = state_4;
          P1_1 = state_5;
          L_1 = state_6;
          M1_1 = state_7;
          P_1 = state_8;
          K_1 = state_9;
          O1_1 = state_10;
          I_1 = state_11;
          N1_1 = state_12;
          O_1 = state_13;
          J_1 = state_14;
          A_1 = state_15;
          L1_1 = state_16;
          V_1 = state_17;
          B_1 = state_18;
          C_1 = state_19;
          D_1 = state_20;
          if (!
              ((N1_1 == S_1) && (Y_1 == U_1) && (Z_1 == X_1) && (B1_1 == Y_1)
               && (C1_1 == S_1) && (D1_1 == T_1) && (E1_1 == Z_1)
               && (G1_1 == W_1) && (N_1 == Q_1) && (M_1 == P1_1)
               && (L_1 == M1_1) && (K_1 == O1_1) && (I_1 == N1_1)
               && (J1_1 == G1_1)
               && (((!P_1) || ((Q_1 + (-1 * O1_1) + (-1 * N1_1)) <= 0)) ==
                   O_1) && (((!I1_1)
                             || ((J1_1 + (-1 * D1_1) + (-1 * C1_1)) <= 0)) ==
                            H1_1) && (F1_1 == (R_1
                                               && (A1_1 || (!(C1_1 <= B1_1)))
                                               && (E1_1 <= D1_1)
                                               && (1 <= D1_1) && (1 <= C1_1)))
               && (R_1 == P_1) && (H_1 == R_1) && (K1_1 == F1_1)
               && (K1_1 == I1_1) && ((!L1_1) || ((P1_1 + (-1 * X_1)) == 2))
               && (L1_1 || (X_1 == 0)) && ((!V_1)
                                           || ((M1_1 + (-1 * U_1)) == 2))
               && ((!V_1) || ((Q_1 + (-1 * W_1)) == 2)) && (V_1 || (U_1 == 0))
               && (V_1 || (W_1 == 0)) && (O1_1 == T_1)))
              abort ();
          state_0 = G1_1;
          state_1 = J1_1;
          state_2 = F1_1;
          state_3 = K1_1;
          state_4 = Z_1;
          state_5 = E1_1;
          state_6 = Y_1;
          state_7 = B1_1;
          state_8 = I1_1;
          state_9 = T_1;
          state_10 = D1_1;
          state_11 = S_1;
          state_12 = C1_1;
          state_13 = H1_1;
          state_14 = G_1;
          state_15 = F_1;
          state_16 = A1_1;
          state_17 = E_1;
          state_18 = U_1;
          state_19 = X_1;
          state_20 = W_1;
          goto state_0;

      default:
          abort ();
      }

    // return expression

}

