// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: vmt-chc-benchmarks/cd_e7_621_e8_714_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "cd_e7_621_e8_714_000_range.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int state_0;
    _Bool state_1;
    int state_2;
    int state_3;
    _Bool state_4;
    _Bool state_5;
    _Bool state_6;
    _Bool state_7;
    _Bool state_8;
    _Bool state_9;
    _Bool state_10;
    _Bool state_11;
    int state_12;
    _Bool state_13;
    int state_14;
    _Bool state_15;
    _Bool state_16;
    int state_17;
    _Bool state_18;
    _Bool state_19;
    _Bool state_20;
    _Bool state_21;
    int state_22;
    _Bool A_0;
    int B_0;
    _Bool C_0;
    _Bool D_0;
    _Bool E_0;
    _Bool F_0;
    _Bool G_0;
    _Bool H_0;
    _Bool I_0;
    _Bool J_0;
    int K_0;
    _Bool L_0;
    _Bool M_0;
    int N_0;
    int O_0;
    _Bool P_0;
    _Bool Q_0;
    int R_0;
    _Bool S_0;
    _Bool T_0;
    _Bool U_0;
    int V_0;
    int W_0;
    _Bool A_1;
    int B_1;
    _Bool C_1;
    _Bool D_1;
    _Bool E_1;
    _Bool F_1;
    _Bool G_1;
    _Bool H_1;
    _Bool I_1;
    _Bool J_1;
    int K_1;
    _Bool L_1;
    _Bool M_1;
    int N_1;
    int O_1;
    int P_1;
    _Bool Q_1;
    _Bool R_1;
    int S_1;
    _Bool T_1;
    _Bool U_1;
    int V_1;
    _Bool W_1;
    int X_1;
    _Bool Y_1;
    _Bool Z_1;
    _Bool A1_1;
    _Bool B1_1;
    int C1_1;
    _Bool D1_1;
    _Bool E1_1;
    _Bool F1_1;
    _Bool G1_1;
    _Bool H1_1;
    _Bool I1_1;
    _Bool J1_1;
    int K1_1;
    int L1_1;
    _Bool M1_1;
    _Bool N1_1;
    int O1_1;
    _Bool P1_1;
    _Bool Q1_1;
    _Bool R1_1;
    int S1_1;
    int T1_1;
    _Bool A_2;
    int B_2;
    _Bool C_2;
    _Bool D_2;
    _Bool E_2;
    _Bool F_2;
    _Bool G_2;
    _Bool H_2;
    _Bool I_2;
    _Bool J_2;
    int K_2;
    _Bool L_2;
    _Bool M_2;
    int N_2;
    int O_2;
    _Bool P_2;
    _Bool Q_2;
    int R_2;
    _Bool S_2;
    _Bool T_2;
    _Bool U_2;
    int V_2;
    int W_2;

    if (((state_0 <= -1000000000) || (state_0 >= 1000000000))
        || ((state_2 <= -1000000000) || (state_2 >= 1000000000))
        || ((state_3 <= -1000000000) || (state_3 >= 1000000000))
        || ((state_12 <= -1000000000) || (state_12 >= 1000000000))
        || ((state_14 <= -1000000000) || (state_14 >= 1000000000))
        || ((state_17 <= -1000000000) || (state_17 >= 1000000000))
        || ((state_22 <= -1000000000) || (state_22 >= 1000000000))
        || ((B_0 <= -1000000000) || (B_0 >= 1000000000))
        || ((K_0 <= -1000000000) || (K_0 >= 1000000000))
        || ((N_0 <= -1000000000) || (N_0 >= 1000000000))
        || ((O_0 <= -1000000000) || (O_0 >= 1000000000))
        || ((R_0 <= -1000000000) || (R_0 >= 1000000000))
        || ((V_0 <= -1000000000) || (V_0 >= 1000000000))
        || ((W_0 <= -1000000000) || (W_0 >= 1000000000))
        || ((B_1 <= -1000000000) || (B_1 >= 1000000000))
        || ((K_1 <= -1000000000) || (K_1 >= 1000000000))
        || ((N_1 <= -1000000000) || (N_1 >= 1000000000))
        || ((O_1 <= -1000000000) || (O_1 >= 1000000000))
        || ((P_1 <= -1000000000) || (P_1 >= 1000000000))
        || ((S_1 <= -1000000000) || (S_1 >= 1000000000))
        || ((V_1 <= -1000000000) || (V_1 >= 1000000000))
        || ((X_1 <= -1000000000) || (X_1 >= 1000000000))
        || ((C1_1 <= -1000000000) || (C1_1 >= 1000000000))
        || ((K1_1 <= -1000000000) || (K1_1 >= 1000000000))
        || ((L1_1 <= -1000000000) || (L1_1 >= 1000000000))
        || ((O1_1 <= -1000000000) || (O1_1 >= 1000000000))
        || ((S1_1 <= -1000000000) || (S1_1 >= 1000000000))
        || ((T1_1 <= -1000000000) || (T1_1 >= 1000000000))
        || ((B_2 <= -1000000000) || (B_2 >= 1000000000))
        || ((K_2 <= -1000000000) || (K_2 >= 1000000000))
        || ((N_2 <= -1000000000) || (N_2 >= 1000000000))
        || ((O_2 <= -1000000000) || (O_2 >= 1000000000))
        || ((R_2 <= -1000000000) || (R_2 >= 1000000000))
        || ((V_2 <= -1000000000) || (V_2 >= 1000000000))
        || ((W_2 <= -1000000000) || (W_2 >= 1000000000)))
        abort ();

    // main logic
    goto main_init;

  main_init:
    if (!
        ((W_0 == 0) && (W_0 == N_0) && (R_0 == 0) && (O_0 == B_0)
         && ((11 <= B_0) == G_0) && ((B_0 <= 9) == D_0)
         && (((!H_0) || A_0) == F_0)
         && ((M_0 && L_0 && (K_0 <= 4) && (-4 <= K_0)) == I_0)
         && (((O_0 <= 12) && (8 <= O_0)) == U_0)
         && (Q_0 == (P_0 && (0 <= O_0) && (!(16 <= O_0)))) && (Q_0 == H_0)
         && (D_0 == C_0) && (G_0 == E_0) && (I_0 == P_0) && (J_0 == A_0)
         && ((!U_0) || (V_0 == 0)) && ((!T_0) || (L_0 == (K_0 <= -1)))
         && ((!S_0) || (M_0 == (1 <= K_0))) && (L_0 || T_0) && (M_0 || S_0)
         && (!T_0) && S_0 && J_0 && (B_0 == R_0)))
        abort ();
    state_0 = O_0;
    state_1 = U_0;
    state_2 = W_0;
    state_3 = N_0;
    state_4 = J_0;
    state_5 = A_0;
    state_6 = Q_0;
    state_7 = H_0;
    state_8 = L_0;
    state_9 = T_0;
    state_10 = M_0;
    state_11 = S_0;
    state_12 = K_0;
    state_13 = I_0;
    state_14 = B_0;
    state_15 = G_0;
    state_16 = D_0;
    state_17 = R_0;
    state_18 = P_0;
    state_19 = E_0;
    state_20 = C_0;
    state_21 = F_0;
    state_22 = V_0;
    K1_1 = __VERIFIER_nondet_int ();
    if (((K1_1 <= -1000000000) || (K1_1 >= 1000000000)))
        abort ();
    I1_1 = __VERIFIER_nondet__Bool ();
    G1_1 = __VERIFIER_nondet__Bool ();
    E1_1 = __VERIFIER_nondet__Bool ();
    C1_1 = __VERIFIER_nondet_int ();
    if (((C1_1 <= -1000000000) || (C1_1 >= 1000000000)))
        abort ();
    O_1 = __VERIFIER_nondet_int ();
    if (((O_1 <= -1000000000) || (O_1 >= 1000000000)))
        abort ();
    P_1 = __VERIFIER_nondet_int ();
    if (((P_1 <= -1000000000) || (P_1 >= 1000000000)))
        abort ();
    A1_1 = __VERIFIER_nondet__Bool ();
    Q_1 = __VERIFIER_nondet__Bool ();
    R_1 = __VERIFIER_nondet__Bool ();
    S_1 = __VERIFIER_nondet_int ();
    if (((S_1 <= -1000000000) || (S_1 >= 1000000000)))
        abort ();
    T_1 = __VERIFIER_nondet__Bool ();
    U_1 = __VERIFIER_nondet__Bool ();
    V_1 = __VERIFIER_nondet_int ();
    if (((V_1 <= -1000000000) || (V_1 >= 1000000000)))
        abort ();
    W_1 = __VERIFIER_nondet__Bool ();
    X_1 = __VERIFIER_nondet_int ();
    if (((X_1 <= -1000000000) || (X_1 >= 1000000000)))
        abort ();
    Y_1 = __VERIFIER_nondet__Bool ();
    Z_1 = __VERIFIER_nondet__Bool ();
    J1_1 = __VERIFIER_nondet__Bool ();
    H1_1 = __VERIFIER_nondet__Bool ();
    F1_1 = __VERIFIER_nondet__Bool ();
    D1_1 = __VERIFIER_nondet__Bool ();
    B1_1 = __VERIFIER_nondet__Bool ();
    L1_1 = state_0;
    R1_1 = state_1;
    T1_1 = state_2;
    N_1 = state_3;
    J_1 = state_4;
    A_1 = state_5;
    N1_1 = state_6;
    H_1 = state_7;
    L_1 = state_8;
    Q1_1 = state_9;
    M_1 = state_10;
    P1_1 = state_11;
    K_1 = state_12;
    I_1 = state_13;
    B_1 = state_14;
    G_1 = state_15;
    D_1 = state_16;
    O1_1 = state_17;
    M1_1 = state_18;
    E_1 = state_19;
    C_1 = state_20;
    F_1 = state_21;
    S1_1 = state_22;
    if (!
        ((T1_1 == N_1) && (L1_1 == B_1) && (S_1 == C1_1) && (X_1 == V_1)
         && (C1_1 == O_1) && (K1_1 == X_1) && (B_1 == O1_1)
         && ((11 <= C1_1) == G1_1) && ((11 <= B_1) == G_1)
         && ((C1_1 <= 9) == E1_1) && ((B_1 <= 9) == D_1)
         && ((B1_1 || (!A1_1)) == Z_1) && ((A_1 || (!H_1)) == F_1)
         && ((J1_1 && I1_1 && (P_1 <= 4) && (-4 <= P_1)) == H1_1)
         && ((L_1 && M_1 && (K_1 <= 4) && (-4 <= K_1)) == I_1)
         && (((L1_1 <= 12) && (8 <= L1_1)) == R1_1)
         && (((S_1 <= 12) && (8 <= S_1)) == W_1) && (N1_1 == H_1)
         && (U_1 == (H_1 && T_1 && (0 <= S_1) && (!(16 <= S_1))))
         && (Y_1 == (N_1 <= 7)) && (A1_1 == U_1) && (B1_1 == Y_1)
         && (E1_1 == D1_1) && (G1_1 == F1_1) && (H1_1 == T_1) && (J_1 == A_1)
         && (I_1 == M1_1) && (G_1 == E_1) && (E_1 == R_1) && (D_1 == C_1)
         && (C_1 == Q_1) && ((!Q1_1) || (L_1 == (K_1 <= -1))) && ((!P1_1)
                                                                  || (M_1 ==
                                                                      (1 <=
                                                                       K_1)))
         && ((!Q_1) || (J1_1 == (1 <= P_1))) && ((!R_1)
                                                 || (I1_1 == (P_1 <= -1)))
         && (W_1 || ((N_1 + (-1 * V_1)) == -1)) && ((!W_1) || (V_1 == 0))
         && (I1_1 || R_1) && (J1_1 || Q_1) && (M_1 || P1_1) && (L_1 || Q1_1)
         && ((B_1 + P_1 + (-1 * O_1)) == 0)))
        abort ();
    state_0 = S_1;
    state_1 = W_1;
    state_2 = X_1;
    state_3 = K1_1;
    state_4 = Y_1;
    state_5 = B1_1;
    state_6 = U_1;
    state_7 = A1_1;
    state_8 = I1_1;
    state_9 = R_1;
    state_10 = J1_1;
    state_11 = Q_1;
    state_12 = P_1;
    state_13 = H1_1;
    state_14 = C1_1;
    state_15 = G1_1;
    state_16 = E1_1;
    state_17 = O_1;
    state_18 = T_1;
    state_19 = F1_1;
    state_20 = D1_1;
    state_21 = Z_1;
    state_22 = V_1;
    goto state_0;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  state_0:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          O_2 = state_0;
          U_2 = state_1;
          W_2 = state_2;
          N_2 = state_3;
          J_2 = state_4;
          A_2 = state_5;
          Q_2 = state_6;
          H_2 = state_7;
          L_2 = state_8;
          T_2 = state_9;
          M_2 = state_10;
          S_2 = state_11;
          K_2 = state_12;
          I_2 = state_13;
          B_2 = state_14;
          G_2 = state_15;
          D_2 = state_16;
          R_2 = state_17;
          P_2 = state_18;
          E_2 = state_19;
          C_2 = state_20;
          F_2 = state_21;
          V_2 = state_22;
          if (!(!F_2))
              abort ();
          goto main_error;

      case 1:
          K1_1 = __VERIFIER_nondet_int ();
          if (((K1_1 <= -1000000000) || (K1_1 >= 1000000000)))
              abort ();
          I1_1 = __VERIFIER_nondet__Bool ();
          G1_1 = __VERIFIER_nondet__Bool ();
          E1_1 = __VERIFIER_nondet__Bool ();
          C1_1 = __VERIFIER_nondet_int ();
          if (((C1_1 <= -1000000000) || (C1_1 >= 1000000000)))
              abort ();
          O_1 = __VERIFIER_nondet_int ();
          if (((O_1 <= -1000000000) || (O_1 >= 1000000000)))
              abort ();
          P_1 = __VERIFIER_nondet_int ();
          if (((P_1 <= -1000000000) || (P_1 >= 1000000000)))
              abort ();
          A1_1 = __VERIFIER_nondet__Bool ();
          Q_1 = __VERIFIER_nondet__Bool ();
          R_1 = __VERIFIER_nondet__Bool ();
          S_1 = __VERIFIER_nondet_int ();
          if (((S_1 <= -1000000000) || (S_1 >= 1000000000)))
              abort ();
          T_1 = __VERIFIER_nondet__Bool ();
          U_1 = __VERIFIER_nondet__Bool ();
          V_1 = __VERIFIER_nondet_int ();
          if (((V_1 <= -1000000000) || (V_1 >= 1000000000)))
              abort ();
          W_1 = __VERIFIER_nondet__Bool ();
          X_1 = __VERIFIER_nondet_int ();
          if (((X_1 <= -1000000000) || (X_1 >= 1000000000)))
              abort ();
          Y_1 = __VERIFIER_nondet__Bool ();
          Z_1 = __VERIFIER_nondet__Bool ();
          J1_1 = __VERIFIER_nondet__Bool ();
          H1_1 = __VERIFIER_nondet__Bool ();
          F1_1 = __VERIFIER_nondet__Bool ();
          D1_1 = __VERIFIER_nondet__Bool ();
          B1_1 = __VERIFIER_nondet__Bool ();
          L1_1 = state_0;
          R1_1 = state_1;
          T1_1 = state_2;
          N_1 = state_3;
          J_1 = state_4;
          A_1 = state_5;
          N1_1 = state_6;
          H_1 = state_7;
          L_1 = state_8;
          Q1_1 = state_9;
          M_1 = state_10;
          P1_1 = state_11;
          K_1 = state_12;
          I_1 = state_13;
          B_1 = state_14;
          G_1 = state_15;
          D_1 = state_16;
          O1_1 = state_17;
          M1_1 = state_18;
          E_1 = state_19;
          C_1 = state_20;
          F_1 = state_21;
          S1_1 = state_22;
          if (!
              ((T1_1 == N_1) && (L1_1 == B_1) && (S_1 == C1_1) && (X_1 == V_1)
               && (C1_1 == O_1) && (K1_1 == X_1) && (B_1 == O1_1)
               && ((11 <= C1_1) == G1_1) && ((11 <= B_1) == G_1)
               && ((C1_1 <= 9) == E1_1) && ((B_1 <= 9) == D_1)
               && ((B1_1 || (!A1_1)) == Z_1) && ((A_1 || (!H_1)) == F_1)
               && ((J1_1 && I1_1 && (P_1 <= 4) && (-4 <= P_1)) == H1_1)
               && ((L_1 && M_1 && (K_1 <= 4) && (-4 <= K_1)) == I_1)
               && (((L1_1 <= 12) && (8 <= L1_1)) == R1_1)
               && (((S_1 <= 12) && (8 <= S_1)) == W_1) && (N1_1 == H_1)
               && (U_1 == (H_1 && T_1 && (0 <= S_1) && (!(16 <= S_1))))
               && (Y_1 == (N_1 <= 7)) && (A1_1 == U_1) && (B1_1 == Y_1)
               && (E1_1 == D1_1) && (G1_1 == F1_1) && (H1_1 == T_1)
               && (J_1 == A_1) && (I_1 == M1_1) && (G_1 == E_1)
               && (E_1 == R_1) && (D_1 == C_1) && (C_1 == Q_1) && ((!Q1_1)
                                                                   || (L_1 ==
                                                                       (K_1 <=
                                                                        -1)))
               && ((!P1_1) || (M_1 == (1 <= K_1))) && ((!Q_1)
                                                       || (J1_1 ==
                                                           (1 <= P_1)))
               && ((!R_1) || (I1_1 == (P_1 <= -1))) && (W_1
                                                        || ((N_1 + (-1 * V_1))
                                                            == -1)) && ((!W_1)
                                                                        ||
                                                                        (V_1
                                                                         ==
                                                                         0))
               && (I1_1 || R_1) && (J1_1 || Q_1) && (M_1 || P1_1) && (L_1
                                                                      || Q1_1)
               && ((B_1 + P_1 + (-1 * O_1)) == 0)))
              abort ();
          state_0 = S_1;
          state_1 = W_1;
          state_2 = X_1;
          state_3 = K1_1;
          state_4 = Y_1;
          state_5 = B1_1;
          state_6 = U_1;
          state_7 = A1_1;
          state_8 = I1_1;
          state_9 = R_1;
          state_10 = J1_1;
          state_11 = Q_1;
          state_12 = P_1;
          state_13 = H1_1;
          state_14 = C1_1;
          state_15 = G1_1;
          state_16 = E1_1;
          state_17 = O_1;
          state_18 = T_1;
          state_19 = F1_1;
          state_20 = D1_1;
          state_21 = Z_1;
          state_22 = V_1;
          goto state_0;

      default:
          abort ();
      }

    // return expression

}

