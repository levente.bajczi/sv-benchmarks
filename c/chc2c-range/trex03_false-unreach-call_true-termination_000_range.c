// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: hcai-bench/trex03_false-unreach-call_true-termination_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "trex03_false-unreach-call_true-termination_000_range.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int main__bb_0;
    int main__bb_1;
    int main__bb_2;
    int main__bb_3;
    int main__bb_4;
    _Bool main__bb_5;
    int main_entry_0;
    int main_entry_1;
    int A_0;
    int B_0;
    int A_1;
    int B_1;
    int C_1;
    int D_1;
    int E_1;
    int F_1;
    _Bool G_1;
    _Bool H_1;
    _Bool I_1;
    _Bool J_1;
    _Bool K_1;
    _Bool L_1;
    _Bool M_1;
    int N_1;
    int O_1;
    int P_1;
    int Q_1;
    _Bool R_1;
    int S_1;
    int T_1;
    int U_1;
    int V_1;
    _Bool W_1;
    _Bool X_1;
    _Bool Y_1;
    int Z_1;
    int A1_1;
    int B1_1;
    int C1_1;
    int D1_1;
    _Bool E1_1;
    int A_2;
    _Bool B_2;
    _Bool C_2;
    _Bool D_2;
    int E_2;
    _Bool F_2;
    _Bool G_2;
    int H_2;
    int I_2;
    int J_2;
    int K_2;
    int L_2;
    int M_2;
    int N_2;
    int O_2;
    int P_2;
    _Bool Q_2;
    int R_2;
    int S_2;
    int T_2;
    _Bool U_2;
    int V_2;
    int W_2;
    _Bool X_2;
    int Y_2;
    _Bool Z_2;
    _Bool A1_2;
    _Bool B1_2;
    _Bool C1_2;
    _Bool D1_2;
    int E1_2;
    int F1_2;
    int G1_2;
    int H1_2;
    _Bool I1_2;
    int J1_2;
    int K1_2;
    int L1_2;
    int M1_2;
    _Bool N1_2;
    _Bool O1_2;
    _Bool P1_2;
    int Q1_2;
    int R1_2;
    int S1_2;
    int T1_2;
    int U1_2;
    _Bool V1_2;
    int A_3;
    int B_3;
    int C_3;
    int D_3;
    int E_3;
    int F_3;
    int G_3;
    _Bool H_3;
    _Bool I_3;
    _Bool J_3;
    _Bool K_3;
    _Bool L_3;
    int M_3;
    int N_3;
    int O_3;
    int P_3;
    int Q_3;
    _Bool R_3;
    int S_3;
    int T_3;
    int U_3;
    _Bool V_3;
    _Bool W_3;
    int X_3;
    _Bool Y_3;
    _Bool Z_3;
    _Bool A1_3;
    _Bool B1_3;
    _Bool C1_3;
    _Bool D1_3;
    _Bool E1_3;
    int A_4;
    _Bool B_4;
    _Bool C_4;
    _Bool D_4;
    int E_4;
    _Bool F_4;
    _Bool G_4;
    int H_4;
    int I_4;
    int J_4;
    int K_4;
    int L_4;
    int M_4;
    int N_4;
    int O_4;
    int P_4;
    _Bool Q_4;
    int R_4;
    int S_4;
    int T_4;
    _Bool U_4;
    int V_4;
    int W_4;
    int X_4;
    _Bool Y_4;
    int Z_4;
    int A1_4;
    _Bool B1_4;
    _Bool C1_4;
    _Bool D1_4;
    _Bool E1_4;
    _Bool F1_4;
    int G1_4;
    int H1_4;
    int I1_4;
    int J1_4;
    int K1_4;
    _Bool L1_4;
    int M1_4;
    int N1_4;
    int O1_4;
    int P1_4;
    int Q1_4;
    int R1_4;
    _Bool S1_4;
    int T1_4;
    int U1_4;
    int V1_4;
    _Bool W1_4;
    _Bool X1_4;
    int Y1_4;
    _Bool Z1_4;
    _Bool A2_4;
    _Bool B2_4;
    _Bool C2_4;
    _Bool D2_4;
    _Bool E2_4;
    _Bool F2_4;
    _Bool CHC_COMP_UNUSED_5;

    if (((main__bb_0 <= -1000000000) || (main__bb_0 >= 1000000000))
        || ((main__bb_1 <= -1000000000) || (main__bb_1 >= 1000000000))
        || ((main__bb_2 <= -1000000000) || (main__bb_2 >= 1000000000))
        || ((main__bb_3 <= -1000000000) || (main__bb_3 >= 1000000000))
        || ((main__bb_4 <= -1000000000) || (main__bb_4 >= 1000000000))
        || ((main_entry_0 <= -1000000000) || (main_entry_0 >= 1000000000))
        || ((main_entry_1 <= -1000000000) || (main_entry_1 >= 1000000000))
        || ((A_0 <= -1000000000) || (A_0 >= 1000000000))
        || ((B_0 <= -1000000000) || (B_0 >= 1000000000))
        || ((A_1 <= -1000000000) || (A_1 >= 1000000000))
        || ((B_1 <= -1000000000) || (B_1 >= 1000000000))
        || ((C_1 <= -1000000000) || (C_1 >= 1000000000))
        || ((D_1 <= -1000000000) || (D_1 >= 1000000000))
        || ((E_1 <= -1000000000) || (E_1 >= 1000000000))
        || ((F_1 <= -1000000000) || (F_1 >= 1000000000))
        || ((N_1 <= -1000000000) || (N_1 >= 1000000000))
        || ((O_1 <= -1000000000) || (O_1 >= 1000000000))
        || ((P_1 <= -1000000000) || (P_1 >= 1000000000))
        || ((Q_1 <= -1000000000) || (Q_1 >= 1000000000))
        || ((S_1 <= -1000000000) || (S_1 >= 1000000000))
        || ((T_1 <= -1000000000) || (T_1 >= 1000000000))
        || ((U_1 <= -1000000000) || (U_1 >= 1000000000))
        || ((V_1 <= -1000000000) || (V_1 >= 1000000000))
        || ((Z_1 <= -1000000000) || (Z_1 >= 1000000000))
        || ((A1_1 <= -1000000000) || (A1_1 >= 1000000000))
        || ((B1_1 <= -1000000000) || (B1_1 >= 1000000000))
        || ((C1_1 <= -1000000000) || (C1_1 >= 1000000000))
        || ((D1_1 <= -1000000000) || (D1_1 >= 1000000000))
        || ((A_2 <= -1000000000) || (A_2 >= 1000000000))
        || ((E_2 <= -1000000000) || (E_2 >= 1000000000))
        || ((H_2 <= -1000000000) || (H_2 >= 1000000000))
        || ((I_2 <= -1000000000) || (I_2 >= 1000000000))
        || ((J_2 <= -1000000000) || (J_2 >= 1000000000))
        || ((K_2 <= -1000000000) || (K_2 >= 1000000000))
        || ((L_2 <= -1000000000) || (L_2 >= 1000000000))
        || ((M_2 <= -1000000000) || (M_2 >= 1000000000))
        || ((N_2 <= -1000000000) || (N_2 >= 1000000000))
        || ((O_2 <= -1000000000) || (O_2 >= 1000000000))
        || ((P_2 <= -1000000000) || (P_2 >= 1000000000))
        || ((R_2 <= -1000000000) || (R_2 >= 1000000000))
        || ((S_2 <= -1000000000) || (S_2 >= 1000000000))
        || ((T_2 <= -1000000000) || (T_2 >= 1000000000))
        || ((V_2 <= -1000000000) || (V_2 >= 1000000000))
        || ((W_2 <= -1000000000) || (W_2 >= 1000000000))
        || ((Y_2 <= -1000000000) || (Y_2 >= 1000000000))
        || ((E1_2 <= -1000000000) || (E1_2 >= 1000000000))
        || ((F1_2 <= -1000000000) || (F1_2 >= 1000000000))
        || ((G1_2 <= -1000000000) || (G1_2 >= 1000000000))
        || ((H1_2 <= -1000000000) || (H1_2 >= 1000000000))
        || ((J1_2 <= -1000000000) || (J1_2 >= 1000000000))
        || ((K1_2 <= -1000000000) || (K1_2 >= 1000000000))
        || ((L1_2 <= -1000000000) || (L1_2 >= 1000000000))
        || ((M1_2 <= -1000000000) || (M1_2 >= 1000000000))
        || ((Q1_2 <= -1000000000) || (Q1_2 >= 1000000000))
        || ((R1_2 <= -1000000000) || (R1_2 >= 1000000000))
        || ((S1_2 <= -1000000000) || (S1_2 >= 1000000000))
        || ((T1_2 <= -1000000000) || (T1_2 >= 1000000000))
        || ((U1_2 <= -1000000000) || (U1_2 >= 1000000000))
        || ((A_3 <= -1000000000) || (A_3 >= 1000000000))
        || ((B_3 <= -1000000000) || (B_3 >= 1000000000))
        || ((C_3 <= -1000000000) || (C_3 >= 1000000000))
        || ((D_3 <= -1000000000) || (D_3 >= 1000000000))
        || ((E_3 <= -1000000000) || (E_3 >= 1000000000))
        || ((F_3 <= -1000000000) || (F_3 >= 1000000000))
        || ((G_3 <= -1000000000) || (G_3 >= 1000000000))
        || ((M_3 <= -1000000000) || (M_3 >= 1000000000))
        || ((N_3 <= -1000000000) || (N_3 >= 1000000000))
        || ((O_3 <= -1000000000) || (O_3 >= 1000000000))
        || ((P_3 <= -1000000000) || (P_3 >= 1000000000))
        || ((Q_3 <= -1000000000) || (Q_3 >= 1000000000))
        || ((S_3 <= -1000000000) || (S_3 >= 1000000000))
        || ((T_3 <= -1000000000) || (T_3 >= 1000000000))
        || ((U_3 <= -1000000000) || (U_3 >= 1000000000))
        || ((X_3 <= -1000000000) || (X_3 >= 1000000000))
        || ((A_4 <= -1000000000) || (A_4 >= 1000000000))
        || ((E_4 <= -1000000000) || (E_4 >= 1000000000))
        || ((H_4 <= -1000000000) || (H_4 >= 1000000000))
        || ((I_4 <= -1000000000) || (I_4 >= 1000000000))
        || ((J_4 <= -1000000000) || (J_4 >= 1000000000))
        || ((K_4 <= -1000000000) || (K_4 >= 1000000000))
        || ((L_4 <= -1000000000) || (L_4 >= 1000000000))
        || ((M_4 <= -1000000000) || (M_4 >= 1000000000))
        || ((N_4 <= -1000000000) || (N_4 >= 1000000000))
        || ((O_4 <= -1000000000) || (O_4 >= 1000000000))
        || ((P_4 <= -1000000000) || (P_4 >= 1000000000))
        || ((R_4 <= -1000000000) || (R_4 >= 1000000000))
        || ((S_4 <= -1000000000) || (S_4 >= 1000000000))
        || ((T_4 <= -1000000000) || (T_4 >= 1000000000))
        || ((V_4 <= -1000000000) || (V_4 >= 1000000000))
        || ((W_4 <= -1000000000) || (W_4 >= 1000000000))
        || ((X_4 <= -1000000000) || (X_4 >= 1000000000))
        || ((Z_4 <= -1000000000) || (Z_4 >= 1000000000))
        || ((A1_4 <= -1000000000) || (A1_4 >= 1000000000))
        || ((G1_4 <= -1000000000) || (G1_4 >= 1000000000))
        || ((H1_4 <= -1000000000) || (H1_4 >= 1000000000))
        || ((I1_4 <= -1000000000) || (I1_4 >= 1000000000))
        || ((J1_4 <= -1000000000) || (J1_4 >= 1000000000))
        || ((K1_4 <= -1000000000) || (K1_4 >= 1000000000))
        || ((M1_4 <= -1000000000) || (M1_4 >= 1000000000))
        || ((N1_4 <= -1000000000) || (N1_4 >= 1000000000))
        || ((O1_4 <= -1000000000) || (O1_4 >= 1000000000))
        || ((P1_4 <= -1000000000) || (P1_4 >= 1000000000))
        || ((Q1_4 <= -1000000000) || (Q1_4 >= 1000000000))
        || ((R1_4 <= -1000000000) || (R1_4 >= 1000000000))
        || ((T1_4 <= -1000000000) || (T1_4 >= 1000000000))
        || ((U1_4 <= -1000000000) || (U1_4 >= 1000000000))
        || ((V1_4 <= -1000000000) || (V1_4 >= 1000000000))
        || ((Y1_4 <= -1000000000) || (Y1_4 >= 1000000000)))
        abort ();

    // main logic
    goto main_init;

  main_init:
    if (!1)
        abort ();
    main_entry_0 = A_0;
    main_entry_1 = B_0;
    goto main_entry;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  main_entry:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_1 = __VERIFIER_nondet_int ();
          if (((A_1 <= -1000000000) || (A_1 >= 1000000000)))
              abort ();
          B_1 = __VERIFIER_nondet_int ();
          if (((B_1 <= -1000000000) || (B_1 >= 1000000000)))
              abort ();
          C_1 = __VERIFIER_nondet_int ();
          if (((C_1 <= -1000000000) || (C_1 >= 1000000000)))
              abort ();
          E_1 = __VERIFIER_nondet_int ();
          if (((E_1 <= -1000000000) || (E_1 >= 1000000000)))
              abort ();
          F_1 = __VERIFIER_nondet_int ();
          if (((F_1 <= -1000000000) || (F_1 >= 1000000000)))
              abort ();
          G_1 = __VERIFIER_nondet__Bool ();
          H_1 = __VERIFIER_nondet__Bool ();
          I_1 = __VERIFIER_nondet__Bool ();
          J_1 = __VERIFIER_nondet__Bool ();
          K_1 = __VERIFIER_nondet__Bool ();
          L_1 = __VERIFIER_nondet__Bool ();
          E1_1 = __VERIFIER_nondet__Bool ();
          M_1 = __VERIFIER_nondet__Bool ();
          N_1 = __VERIFIER_nondet_int ();
          if (((N_1 <= -1000000000) || (N_1 >= 1000000000)))
              abort ();
          C1_1 = __VERIFIER_nondet_int ();
          if (((C1_1 <= -1000000000) || (C1_1 >= 1000000000)))
              abort ();
          O_1 = __VERIFIER_nondet_int ();
          if (((O_1 <= -1000000000) || (O_1 >= 1000000000)))
              abort ();
          P_1 = __VERIFIER_nondet_int ();
          if (((P_1 <= -1000000000) || (P_1 >= 1000000000)))
              abort ();
          A1_1 = __VERIFIER_nondet_int ();
          if (((A1_1 <= -1000000000) || (A1_1 >= 1000000000)))
              abort ();
          Q_1 = __VERIFIER_nondet_int ();
          if (((Q_1 <= -1000000000) || (Q_1 >= 1000000000)))
              abort ();
          R_1 = __VERIFIER_nondet__Bool ();
          S_1 = __VERIFIER_nondet_int ();
          if (((S_1 <= -1000000000) || (S_1 >= 1000000000)))
              abort ();
          T_1 = __VERIFIER_nondet_int ();
          if (((T_1 <= -1000000000) || (T_1 >= 1000000000)))
              abort ();
          U_1 = __VERIFIER_nondet_int ();
          if (((U_1 <= -1000000000) || (U_1 >= 1000000000)))
              abort ();
          V_1 = __VERIFIER_nondet_int ();
          if (((V_1 <= -1000000000) || (V_1 >= 1000000000)))
              abort ();
          W_1 = __VERIFIER_nondet__Bool ();
          X_1 = __VERIFIER_nondet__Bool ();
          Y_1 = __VERIFIER_nondet__Bool ();
          D1_1 = __VERIFIER_nondet_int ();
          if (((D1_1 <= -1000000000) || (D1_1 >= 1000000000)))
              abort ();
          B1_1 = __VERIFIER_nondet_int ();
          if (((B1_1 <= -1000000000) || (B1_1 >= 1000000000)))
              abort ();
          Z_1 = main_entry_0;
          D_1 = main_entry_1;
          if (!
              ((!((O_1 == 0) == G_1)) && (!((P_1 == 0) == I_1))
               && (J_1 == (H_1 && G_1)) && (L_1 == (J_1 && I_1))
               && (A_1 == D_1) && (B_1 == D_1) && (C_1 == D_1) && (E_1 == Z_1)
               && (F_1 == Z_1) && ((!K_1) || (!X_1) || L_1) && ((!X_1)
                                                                || (!W_1)
                                                                || (Y_1 ==
                                                                    R_1))
               && ((!X_1) || (!W_1) || (E1_1 == Y_1)) && ((!X_1) || (!W_1)
                                                          || (S_1 == N_1))
               && ((!X_1) || (!W_1) || (T_1 == O_1)) && ((!X_1) || (!W_1)
                                                         || (U_1 == P_1))
               && ((!X_1) || (!W_1) || (V_1 == Q_1)) && ((!X_1) || (!W_1)
                                                         || (A1_1 == S_1))
               && ((!X_1) || (!W_1) || (B1_1 == T_1)) && ((!X_1) || (!W_1)
                                                          || (C1_1 == U_1))
               && ((!X_1) || (!W_1) || (D1_1 == V_1)) && ((!W_1)
                                                          || (X_1 && W_1))
               && ((!X_1) || (Q_1 == (M_1 ? 1 : 0))) && ((!X_1)
                                                         || (X_1 && K_1))
               && W_1 && (!((N_1 == 0) == H_1))))
              abort ();
          main__bb_0 = Z_1;
          main__bb_1 = A1_1;
          main__bb_2 = B1_1;
          main__bb_3 = C1_1;
          main__bb_4 = D1_1;
          main__bb_5 = E1_1;
          B_2 = __VERIFIER_nondet__Bool ();
          O1_2 = __VERIFIER_nondet__Bool ();
          C_2 = __VERIFIER_nondet__Bool ();
          M1_2 = __VERIFIER_nondet_int ();
          if (((M1_2 <= -1000000000) || (M1_2 >= 1000000000)))
              abort ();
          E_2 = __VERIFIER_nondet_int ();
          if (((E_2 <= -1000000000) || (E_2 >= 1000000000)))
              abort ();
          F_2 = __VERIFIER_nondet__Bool ();
          K1_2 = __VERIFIER_nondet_int ();
          if (((K1_2 <= -1000000000) || (K1_2 >= 1000000000)))
              abort ();
          G_2 = __VERIFIER_nondet__Bool ();
          H_2 = __VERIFIER_nondet_int ();
          if (((H_2 <= -1000000000) || (H_2 >= 1000000000)))
              abort ();
          I1_2 = __VERIFIER_nondet__Bool ();
          I_2 = __VERIFIER_nondet_int ();
          if (((I_2 <= -1000000000) || (I_2 >= 1000000000)))
              abort ();
          J_2 = __VERIFIER_nondet_int ();
          if (((J_2 <= -1000000000) || (J_2 >= 1000000000)))
              abort ();
          G1_2 = __VERIFIER_nondet_int ();
          if (((G1_2 <= -1000000000) || (G1_2 >= 1000000000)))
              abort ();
          E1_2 = __VERIFIER_nondet_int ();
          if (((E1_2 <= -1000000000) || (E1_2 >= 1000000000)))
              abort ();
          N_2 = __VERIFIER_nondet_int ();
          if (((N_2 <= -1000000000) || (N_2 >= 1000000000)))
              abort ();
          C1_2 = __VERIFIER_nondet__Bool ();
          O_2 = __VERIFIER_nondet_int ();
          if (((O_2 <= -1000000000) || (O_2 >= 1000000000)))
              abort ();
          P_2 = __VERIFIER_nondet_int ();
          if (((P_2 <= -1000000000) || (P_2 >= 1000000000)))
              abort ();
          A1_2 = __VERIFIER_nondet__Bool ();
          Q_2 = __VERIFIER_nondet__Bool ();
          R_2 = __VERIFIER_nondet_int ();
          if (((R_2 <= -1000000000) || (R_2 >= 1000000000)))
              abort ();
          S_2 = __VERIFIER_nondet_int ();
          if (((S_2 <= -1000000000) || (S_2 >= 1000000000)))
              abort ();
          T_2 = __VERIFIER_nondet_int ();
          if (((T_2 <= -1000000000) || (T_2 >= 1000000000)))
              abort ();
          U_2 = __VERIFIER_nondet__Bool ();
          V_2 = __VERIFIER_nondet_int ();
          if (((V_2 <= -1000000000) || (V_2 >= 1000000000)))
              abort ();
          W_2 = __VERIFIER_nondet_int ();
          if (((W_2 <= -1000000000) || (W_2 >= 1000000000)))
              abort ();
          X_2 = __VERIFIER_nondet__Bool ();
          Y_2 = __VERIFIER_nondet_int ();
          if (((Y_2 <= -1000000000) || (Y_2 >= 1000000000)))
              abort ();
          Z_2 = __VERIFIER_nondet__Bool ();
          V1_2 = __VERIFIER_nondet__Bool ();
          T1_2 = __VERIFIER_nondet_int ();
          if (((T1_2 <= -1000000000) || (T1_2 >= 1000000000)))
              abort ();
          R1_2 = __VERIFIER_nondet_int ();
          if (((R1_2 <= -1000000000) || (R1_2 >= 1000000000)))
              abort ();
          P1_2 = __VERIFIER_nondet__Bool ();
          N1_2 = __VERIFIER_nondet__Bool ();
          L1_2 = __VERIFIER_nondet_int ();
          if (((L1_2 <= -1000000000) || (L1_2 >= 1000000000)))
              abort ();
          J1_2 = __VERIFIER_nondet_int ();
          if (((J1_2 <= -1000000000) || (J1_2 >= 1000000000)))
              abort ();
          H1_2 = __VERIFIER_nondet_int ();
          if (((H1_2 <= -1000000000) || (H1_2 >= 1000000000)))
              abort ();
          F1_2 = __VERIFIER_nondet_int ();
          if (((F1_2 <= -1000000000) || (F1_2 >= 1000000000)))
              abort ();
          D1_2 = __VERIFIER_nondet__Bool ();
          B1_2 = __VERIFIER_nondet__Bool ();
          U1_2 = __VERIFIER_nondet_int ();
          if (((U1_2 <= -1000000000) || (U1_2 >= 1000000000)))
              abort ();
          S1_2 = __VERIFIER_nondet_int ();
          if (((S1_2 <= -1000000000) || (S1_2 >= 1000000000)))
              abort ();
          Q1_2 = main__bb_0;
          K_2 = main__bb_1;
          M_2 = main__bb_2;
          L_2 = main__bb_3;
          A_2 = main__bb_4;
          D_2 = main__bb_5;
          if (!
              (((!Q_2) || (!B_2) || C_2) && ((!U_2) || (!C_2) || (!B_2))
               && ((!O1_2) || (O1_2 && U_2) || (O1_2 && Q_2)) && ((!O1_2)
                                                                  || (!Q_2)
                                                                  || (O_2 ==
                                                                      I_2))
               && ((!O1_2) || (!Q_2) || (P_2 == J_2)) && ((!O1_2) || (!Q_2)
                                                          || (R_2 == K_2))
               && ((!O1_2) || (!Q_2) || (E1_2 == R_2)) && ((!O1_2) || (!Q_2)
                                                           || (F1_2 == P_2))
               && ((!O1_2) || (!Q_2) || (G1_2 == O_2)) && ((!O1_2) || (!U_2)
                                                           || (S_2 == L_2))
               && ((!O1_2) || (!U_2) || (T_2 == M_2)) && ((!O1_2) || (!U_2)
                                                          || (V_2 == N_2))
               && ((!O1_2) || (!U_2) || (E1_2 == V_2)) && ((!O1_2) || (!U_2)
                                                           || (F1_2 == T_2))
               && ((!O1_2) || (!U_2) || (G1_2 == S_2)) && ((!O1_2) || (!N1_2)
                                                           || (P1_2 == I1_2))
               && ((!O1_2) || (!N1_2) || (V1_2 == P1_2)) && ((!O1_2)
                                                             || (!N1_2)
                                                             || (J1_2 ==
                                                                 E1_2))
               && ((!O1_2) || (!N1_2) || (K1_2 == F1_2)) && ((!O1_2)
                                                             || (!N1_2)
                                                             || (L1_2 ==
                                                                 G1_2))
               && ((!O1_2) || (!N1_2) || (M1_2 == H1_2)) && ((!O1_2)
                                                             || (!N1_2)
                                                             || (R1_2 ==
                                                                 J1_2))
               && ((!O1_2) || (!N1_2) || (S1_2 == K1_2)) && ((!O1_2)
                                                             || (!N1_2)
                                                             || (T1_2 ==
                                                                 L1_2))
               && ((!O1_2) || (!N1_2) || (U1_2 == M1_2)) && ((!O1_2)
                                                             || (!N1_2)
                                                             || D1_2)
               && ((!Q_2) || (!(D_2 == F_2))) && ((!Q_2) || (!(F_2 == G_2)))
               && ((!Q_2) || (E_2 == (F_2 ? -1 : 0))) && ((!Q_2)
                                                          || (H_2 ==
                                                              (G_2 ? -1 : 0)))
               && ((!Q_2) || (I_2 == (L_2 + E_2))) && ((!Q_2)
                                                       || (J_2 ==
                                                           (M_2 + H_2)))
               && ((!Q_2) || (Q_2 && B_2)) && ((!U_2) || (N_2 == (K_2 + -1)))
               && ((!U_2) || (U_2 && B_2)) && ((!N1_2) || (O1_2 && N1_2))
               && ((!O1_2) || (!((E1_2 == 0) == A1_2))) && ((!O1_2)
                                                            ||
                                                            (!((F1_2 == 0) ==
                                                               Z_2)))
               && ((!O1_2) || (!((G1_2 == 0) == B1_2))) && ((!O1_2)
                                                            || (C1_2 ==
                                                                (A1_2
                                                                 && Z_2)))
               && ((!O1_2) || (D1_2 == (C1_2 && B1_2))) && ((!O1_2)
                                                            || (Y_2 == Q1_2))
               && ((!O1_2) || (W_2 == Q1_2)) && ((!O1_2)
                                                 || (H1_2 == (X_2 ? 1 : 0)))
               && N1_2 && (C_2 == (A_2 == 0))))
              abort ();
          main__bb_0 = Q1_2;
          main__bb_1 = R1_2;
          main__bb_2 = S1_2;
          main__bb_3 = T1_2;
          main__bb_4 = U1_2;
          main__bb_5 = V1_2;
          goto main__bb_0;

      case 1:
          A_3 = __VERIFIER_nondet_int ();
          if (((A_3 <= -1000000000) || (A_3 >= 1000000000)))
              abort ();
          B_3 = __VERIFIER_nondet_int ();
          if (((B_3 <= -1000000000) || (B_3 >= 1000000000)))
              abort ();
          C_3 = __VERIFIER_nondet_int ();
          if (((C_3 <= -1000000000) || (C_3 >= 1000000000)))
              abort ();
          E_3 = __VERIFIER_nondet_int ();
          if (((E_3 <= -1000000000) || (E_3 >= 1000000000)))
              abort ();
          F_3 = __VERIFIER_nondet_int ();
          if (((F_3 <= -1000000000) || (F_3 >= 1000000000)))
              abort ();
          H_3 = __VERIFIER_nondet__Bool ();
          I_3 = __VERIFIER_nondet__Bool ();
          J_3 = __VERIFIER_nondet__Bool ();
          K_3 = __VERIFIER_nondet__Bool ();
          L_3 = __VERIFIER_nondet__Bool ();
          E1_3 = __VERIFIER_nondet__Bool ();
          M_3 = __VERIFIER_nondet_int ();
          if (((M_3 <= -1000000000) || (M_3 >= 1000000000)))
              abort ();
          N_3 = __VERIFIER_nondet_int ();
          if (((N_3 <= -1000000000) || (N_3 >= 1000000000)))
              abort ();
          C1_3 = __VERIFIER_nondet__Bool ();
          O_3 = __VERIFIER_nondet_int ();
          if (((O_3 <= -1000000000) || (O_3 >= 1000000000)))
              abort ();
          P_3 = __VERIFIER_nondet_int ();
          if (((P_3 <= -1000000000) || (P_3 >= 1000000000)))
              abort ();
          A1_3 = __VERIFIER_nondet__Bool ();
          Q_3 = __VERIFIER_nondet_int ();
          if (((Q_3 <= -1000000000) || (Q_3 >= 1000000000)))
              abort ();
          R_3 = __VERIFIER_nondet__Bool ();
          S_3 = __VERIFIER_nondet_int ();
          if (((S_3 <= -1000000000) || (S_3 >= 1000000000)))
              abort ();
          T_3 = __VERIFIER_nondet_int ();
          if (((T_3 <= -1000000000) || (T_3 >= 1000000000)))
              abort ();
          U_3 = __VERIFIER_nondet_int ();
          if (((U_3 <= -1000000000) || (U_3 >= 1000000000)))
              abort ();
          V_3 = __VERIFIER_nondet__Bool ();
          W_3 = __VERIFIER_nondet__Bool ();
          X_3 = __VERIFIER_nondet_int ();
          if (((X_3 <= -1000000000) || (X_3 >= 1000000000)))
              abort ();
          Y_3 = __VERIFIER_nondet__Bool ();
          Z_3 = __VERIFIER_nondet__Bool ();
          D1_3 = __VERIFIER_nondet__Bool ();
          B1_3 = __VERIFIER_nondet__Bool ();
          G_3 = main_entry_0;
          D_3 = main_entry_1;
          if (!
              ((!((N_3 == 0) == H_3)) && (!((O_3 == 0) == J_3))
               && (K_3 == (I_3 && H_3)) && (L_3 == (K_3 && J_3))
               && (A_3 == D_3) && (B_3 == D_3) && (C_3 == D_3) && (E_3 == G_3)
               && (F_3 == G_3) && ((!D1_3) || (X_3 == S_3) || (!R_3))
               && ((!D1_3) || (P_3 == M_3) || (!R_3)) && ((!D1_3)
                                                          || (Q_3 == N_3)
                                                          || (!R_3))
               && ((!D1_3) || (S_3 == O_3) || (!R_3)) && ((!D1_3)
                                                          || (T_3 == P_3)
                                                          || (!R_3))
               && ((!D1_3) || (U_3 == Q_3) || (!R_3)) && ((!D1_3) || (!L_3)
                                                          || (!R_3))
               && ((!D1_3) || (V_3 == (U_3 == 0))) && ((!D1_3)
                                                       || (Z_3 ==
                                                           (W_3 && V_3)))
               && ((!D1_3) || (B1_3 == (Y_3 && Z_3))) && ((!D1_3)
                                                          ||
                                                          (!(B1_3 == C1_3)))
               && ((!D1_3) || (W_3 == (T_3 == 0))) && ((!D1_3)
                                                       || (Y_3 == (X_3 == 0)))
               && ((!D1_3) || (R_3 && D1_3)) && ((!D1_3) || (!A1_3))
               && ((!D1_3) || C1_3) && ((!E1_3) || (E1_3 && D1_3)) && E1_3
               && (!((M_3 == 0) == I_3))))
              abort ();
          CHC_COMP_UNUSED_5 = __VERIFIER_nondet__Bool ();
          if (!1)
              abort ();
          goto main_error;

      default:
          abort ();
      }
  main__bb_0:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          B_2 = __VERIFIER_nondet__Bool ();
          O1_2 = __VERIFIER_nondet__Bool ();
          C_2 = __VERIFIER_nondet__Bool ();
          M1_2 = __VERIFIER_nondet_int ();
          if (((M1_2 <= -1000000000) || (M1_2 >= 1000000000)))
              abort ();
          E_2 = __VERIFIER_nondet_int ();
          if (((E_2 <= -1000000000) || (E_2 >= 1000000000)))
              abort ();
          F_2 = __VERIFIER_nondet__Bool ();
          K1_2 = __VERIFIER_nondet_int ();
          if (((K1_2 <= -1000000000) || (K1_2 >= 1000000000)))
              abort ();
          G_2 = __VERIFIER_nondet__Bool ();
          H_2 = __VERIFIER_nondet_int ();
          if (((H_2 <= -1000000000) || (H_2 >= 1000000000)))
              abort ();
          I1_2 = __VERIFIER_nondet__Bool ();
          I_2 = __VERIFIER_nondet_int ();
          if (((I_2 <= -1000000000) || (I_2 >= 1000000000)))
              abort ();
          J_2 = __VERIFIER_nondet_int ();
          if (((J_2 <= -1000000000) || (J_2 >= 1000000000)))
              abort ();
          G1_2 = __VERIFIER_nondet_int ();
          if (((G1_2 <= -1000000000) || (G1_2 >= 1000000000)))
              abort ();
          E1_2 = __VERIFIER_nondet_int ();
          if (((E1_2 <= -1000000000) || (E1_2 >= 1000000000)))
              abort ();
          N_2 = __VERIFIER_nondet_int ();
          if (((N_2 <= -1000000000) || (N_2 >= 1000000000)))
              abort ();
          C1_2 = __VERIFIER_nondet__Bool ();
          O_2 = __VERIFIER_nondet_int ();
          if (((O_2 <= -1000000000) || (O_2 >= 1000000000)))
              abort ();
          P_2 = __VERIFIER_nondet_int ();
          if (((P_2 <= -1000000000) || (P_2 >= 1000000000)))
              abort ();
          A1_2 = __VERIFIER_nondet__Bool ();
          Q_2 = __VERIFIER_nondet__Bool ();
          R_2 = __VERIFIER_nondet_int ();
          if (((R_2 <= -1000000000) || (R_2 >= 1000000000)))
              abort ();
          S_2 = __VERIFIER_nondet_int ();
          if (((S_2 <= -1000000000) || (S_2 >= 1000000000)))
              abort ();
          T_2 = __VERIFIER_nondet_int ();
          if (((T_2 <= -1000000000) || (T_2 >= 1000000000)))
              abort ();
          U_2 = __VERIFIER_nondet__Bool ();
          V_2 = __VERIFIER_nondet_int ();
          if (((V_2 <= -1000000000) || (V_2 >= 1000000000)))
              abort ();
          W_2 = __VERIFIER_nondet_int ();
          if (((W_2 <= -1000000000) || (W_2 >= 1000000000)))
              abort ();
          X_2 = __VERIFIER_nondet__Bool ();
          Y_2 = __VERIFIER_nondet_int ();
          if (((Y_2 <= -1000000000) || (Y_2 >= 1000000000)))
              abort ();
          Z_2 = __VERIFIER_nondet__Bool ();
          V1_2 = __VERIFIER_nondet__Bool ();
          T1_2 = __VERIFIER_nondet_int ();
          if (((T1_2 <= -1000000000) || (T1_2 >= 1000000000)))
              abort ();
          R1_2 = __VERIFIER_nondet_int ();
          if (((R1_2 <= -1000000000) || (R1_2 >= 1000000000)))
              abort ();
          P1_2 = __VERIFIER_nondet__Bool ();
          N1_2 = __VERIFIER_nondet__Bool ();
          L1_2 = __VERIFIER_nondet_int ();
          if (((L1_2 <= -1000000000) || (L1_2 >= 1000000000)))
              abort ();
          J1_2 = __VERIFIER_nondet_int ();
          if (((J1_2 <= -1000000000) || (J1_2 >= 1000000000)))
              abort ();
          H1_2 = __VERIFIER_nondet_int ();
          if (((H1_2 <= -1000000000) || (H1_2 >= 1000000000)))
              abort ();
          F1_2 = __VERIFIER_nondet_int ();
          if (((F1_2 <= -1000000000) || (F1_2 >= 1000000000)))
              abort ();
          D1_2 = __VERIFIER_nondet__Bool ();
          B1_2 = __VERIFIER_nondet__Bool ();
          U1_2 = __VERIFIER_nondet_int ();
          if (((U1_2 <= -1000000000) || (U1_2 >= 1000000000)))
              abort ();
          S1_2 = __VERIFIER_nondet_int ();
          if (((S1_2 <= -1000000000) || (S1_2 >= 1000000000)))
              abort ();
          Q1_2 = main__bb_0;
          K_2 = main__bb_1;
          M_2 = main__bb_2;
          L_2 = main__bb_3;
          A_2 = main__bb_4;
          D_2 = main__bb_5;
          if (!
              (((!Q_2) || (!B_2) || C_2) && ((!U_2) || (!C_2) || (!B_2))
               && ((!O1_2) || (O1_2 && U_2) || (O1_2 && Q_2)) && ((!O1_2)
                                                                  || (!Q_2)
                                                                  || (O_2 ==
                                                                      I_2))
               && ((!O1_2) || (!Q_2) || (P_2 == J_2)) && ((!O1_2) || (!Q_2)
                                                          || (R_2 == K_2))
               && ((!O1_2) || (!Q_2) || (E1_2 == R_2)) && ((!O1_2) || (!Q_2)
                                                           || (F1_2 == P_2))
               && ((!O1_2) || (!Q_2) || (G1_2 == O_2)) && ((!O1_2) || (!U_2)
                                                           || (S_2 == L_2))
               && ((!O1_2) || (!U_2) || (T_2 == M_2)) && ((!O1_2) || (!U_2)
                                                          || (V_2 == N_2))
               && ((!O1_2) || (!U_2) || (E1_2 == V_2)) && ((!O1_2) || (!U_2)
                                                           || (F1_2 == T_2))
               && ((!O1_2) || (!U_2) || (G1_2 == S_2)) && ((!O1_2) || (!N1_2)
                                                           || (P1_2 == I1_2))
               && ((!O1_2) || (!N1_2) || (V1_2 == P1_2)) && ((!O1_2)
                                                             || (!N1_2)
                                                             || (J1_2 ==
                                                                 E1_2))
               && ((!O1_2) || (!N1_2) || (K1_2 == F1_2)) && ((!O1_2)
                                                             || (!N1_2)
                                                             || (L1_2 ==
                                                                 G1_2))
               && ((!O1_2) || (!N1_2) || (M1_2 == H1_2)) && ((!O1_2)
                                                             || (!N1_2)
                                                             || (R1_2 ==
                                                                 J1_2))
               && ((!O1_2) || (!N1_2) || (S1_2 == K1_2)) && ((!O1_2)
                                                             || (!N1_2)
                                                             || (T1_2 ==
                                                                 L1_2))
               && ((!O1_2) || (!N1_2) || (U1_2 == M1_2)) && ((!O1_2)
                                                             || (!N1_2)
                                                             || D1_2)
               && ((!Q_2) || (!(D_2 == F_2))) && ((!Q_2) || (!(F_2 == G_2)))
               && ((!Q_2) || (E_2 == (F_2 ? -1 : 0))) && ((!Q_2)
                                                          || (H_2 ==
                                                              (G_2 ? -1 : 0)))
               && ((!Q_2) || (I_2 == (L_2 + E_2))) && ((!Q_2)
                                                       || (J_2 ==
                                                           (M_2 + H_2)))
               && ((!Q_2) || (Q_2 && B_2)) && ((!U_2) || (N_2 == (K_2 + -1)))
               && ((!U_2) || (U_2 && B_2)) && ((!N1_2) || (O1_2 && N1_2))
               && ((!O1_2) || (!((E1_2 == 0) == A1_2))) && ((!O1_2)
                                                            ||
                                                            (!((F1_2 == 0) ==
                                                               Z_2)))
               && ((!O1_2) || (!((G1_2 == 0) == B1_2))) && ((!O1_2)
                                                            || (C1_2 ==
                                                                (A1_2
                                                                 && Z_2)))
               && ((!O1_2) || (D1_2 == (C1_2 && B1_2))) && ((!O1_2)
                                                            || (Y_2 == Q1_2))
               && ((!O1_2) || (W_2 == Q1_2)) && ((!O1_2)
                                                 || (H1_2 == (X_2 ? 1 : 0)))
               && N1_2 && (C_2 == (A_2 == 0))))
              abort ();
          main__bb_0 = Q1_2;
          main__bb_1 = R1_2;
          main__bb_2 = S1_2;
          main__bb_3 = T1_2;
          main__bb_4 = U1_2;
          main__bb_5 = V1_2;
          goto main__bb_0;

      case 1:
          Q1_4 = __VERIFIER_nondet_int ();
          if (((Q1_4 <= -1000000000) || (Q1_4 >= 1000000000)))
              abort ();
          M1_4 = __VERIFIER_nondet_int ();
          if (((M1_4 <= -1000000000) || (M1_4 >= 1000000000)))
              abort ();
          I1_4 = __VERIFIER_nondet_int ();
          if (((I1_4 <= -1000000000) || (I1_4 >= 1000000000)))
              abort ();
          E1_4 = __VERIFIER_nondet__Bool ();
          E2_4 = __VERIFIER_nondet__Bool ();
          A2_4 = __VERIFIER_nondet__Bool ();
          Z1_4 = __VERIFIER_nondet__Bool ();
          V1_4 = __VERIFIER_nondet_int ();
          if (((V1_4 <= -1000000000) || (V1_4 >= 1000000000)))
              abort ();
          R1_4 = __VERIFIER_nondet_int ();
          if (((R1_4 <= -1000000000) || (R1_4 >= 1000000000)))
              abort ();
          N1_4 = __VERIFIER_nondet_int ();
          if (((N1_4 <= -1000000000) || (N1_4 >= 1000000000)))
              abort ();
          J1_4 = __VERIFIER_nondet_int ();
          if (((J1_4 <= -1000000000) || (J1_4 >= 1000000000)))
              abort ();
          F1_4 = __VERIFIER_nondet__Bool ();
          F2_4 = __VERIFIER_nondet__Bool ();
          B1_4 = __VERIFIER_nondet__Bool ();
          B2_4 = __VERIFIER_nondet__Bool ();
          W1_4 = __VERIFIER_nondet__Bool ();
          S1_4 = __VERIFIER_nondet__Bool ();
          B_4 = __VERIFIER_nondet__Bool ();
          O1_4 = __VERIFIER_nondet_int ();
          if (((O1_4 <= -1000000000) || (O1_4 >= 1000000000)))
              abort ();
          C_4 = __VERIFIER_nondet__Bool ();
          E_4 = __VERIFIER_nondet_int ();
          if (((E_4 <= -1000000000) || (E_4 >= 1000000000)))
              abort ();
          F_4 = __VERIFIER_nondet__Bool ();
          K1_4 = __VERIFIER_nondet_int ();
          if (((K1_4 <= -1000000000) || (K1_4 >= 1000000000)))
              abort ();
          G_4 = __VERIFIER_nondet__Bool ();
          H_4 = __VERIFIER_nondet_int ();
          if (((H_4 <= -1000000000) || (H_4 >= 1000000000)))
              abort ();
          I_4 = __VERIFIER_nondet_int ();
          if (((I_4 <= -1000000000) || (I_4 >= 1000000000)))
              abort ();
          J_4 = __VERIFIER_nondet_int ();
          if (((J_4 <= -1000000000) || (J_4 >= 1000000000)))
              abort ();
          G1_4 = __VERIFIER_nondet_int ();
          if (((G1_4 <= -1000000000) || (G1_4 >= 1000000000)))
              abort ();
          N_4 = __VERIFIER_nondet_int ();
          if (((N_4 <= -1000000000) || (N_4 >= 1000000000)))
              abort ();
          C1_4 = __VERIFIER_nondet__Bool ();
          O_4 = __VERIFIER_nondet_int ();
          if (((O_4 <= -1000000000) || (O_4 >= 1000000000)))
              abort ();
          C2_4 = __VERIFIER_nondet__Bool ();
          P_4 = __VERIFIER_nondet_int ();
          if (((P_4 <= -1000000000) || (P_4 >= 1000000000)))
              abort ();
          Q_4 = __VERIFIER_nondet__Bool ();
          R_4 = __VERIFIER_nondet_int ();
          if (((R_4 <= -1000000000) || (R_4 >= 1000000000)))
              abort ();
          S_4 = __VERIFIER_nondet_int ();
          if (((S_4 <= -1000000000) || (S_4 >= 1000000000)))
              abort ();
          T_4 = __VERIFIER_nondet_int ();
          if (((T_4 <= -1000000000) || (T_4 >= 1000000000)))
              abort ();
          U_4 = __VERIFIER_nondet__Bool ();
          V_4 = __VERIFIER_nondet_int ();
          if (((V_4 <= -1000000000) || (V_4 >= 1000000000)))
              abort ();
          W_4 = __VERIFIER_nondet_int ();
          if (((W_4 <= -1000000000) || (W_4 >= 1000000000)))
              abort ();
          X_4 = __VERIFIER_nondet_int ();
          if (((X_4 <= -1000000000) || (X_4 >= 1000000000)))
              abort ();
          Y_4 = __VERIFIER_nondet__Bool ();
          X1_4 = __VERIFIER_nondet__Bool ();
          Z_4 = __VERIFIER_nondet_int ();
          if (((Z_4 <= -1000000000) || (Z_4 >= 1000000000)))
              abort ();
          T1_4 = __VERIFIER_nondet_int ();
          if (((T1_4 <= -1000000000) || (T1_4 >= 1000000000)))
              abort ();
          P1_4 = __VERIFIER_nondet_int ();
          if (((P1_4 <= -1000000000) || (P1_4 >= 1000000000)))
              abort ();
          L1_4 = __VERIFIER_nondet__Bool ();
          H1_4 = __VERIFIER_nondet_int ();
          if (((H1_4 <= -1000000000) || (H1_4 >= 1000000000)))
              abort ();
          D1_4 = __VERIFIER_nondet__Bool ();
          D2_4 = __VERIFIER_nondet__Bool ();
          Y1_4 = __VERIFIER_nondet_int ();
          if (((Y1_4 <= -1000000000) || (Y1_4 >= 1000000000)))
              abort ();
          U1_4 = __VERIFIER_nondet_int ();
          if (((U1_4 <= -1000000000) || (U1_4 >= 1000000000)))
              abort ();
          A1_4 = main__bb_0;
          K_4 = main__bb_1;
          M_4 = main__bb_2;
          L_4 = main__bb_3;
          A_4 = main__bb_4;
          D_4 = main__bb_5;
          if (!
              (((!U_4) || (!C_4) || (!B_4)) && (C_4 || (!Q_4) || (!B_4))
               && ((!L1_4) || (L1_4 && U_4) || (L1_4 && Q_4)) && ((!L1_4)
                                                                  || (!U_4)
                                                                  || (S_4 ==
                                                                      L_4))
               && ((!L1_4) || (!U_4) || (T_4 == M_4)) && ((!L1_4) || (!U_4)
                                                          || (V_4 == N_4))
               && ((!L1_4) || (!U_4) || (I1_4 == S_4)) && ((!L1_4) || (!U_4)
                                                           || (H1_4 == T_4))
               && ((!L1_4) || (!U_4) || (G1_4 == V_4)) && ((!L1_4) || (!Q_4)
                                                           || (P_4 == J_4))
               && ((!L1_4) || (!Q_4) || (O_4 == I_4)) && ((!L1_4) || (!Q_4)
                                                          || (R_4 == K_4))
               && ((!L1_4) || (!Q_4) || (I1_4 == O_4)) && ((!L1_4) || (!Q_4)
                                                           || (H1_4 == P_4))
               && ((!L1_4) || (!Q_4) || (G1_4 == R_4)) && ((!E2_4)
                                                           || (Y1_4 == T1_4)
                                                           || (!S1_4))
               && ((!E2_4) || (Q1_4 == N1_4) || (!S1_4)) && ((!E2_4)
                                                             || (R1_4 == O1_4)
                                                             || (!S1_4))
               && ((!E2_4) || (T1_4 == P1_4) || (!S1_4)) && ((!E2_4)
                                                             || (U1_4 == Q1_4)
                                                             || (!S1_4))
               && ((!E2_4) || (V1_4 == R1_4) || (!S1_4)) && ((!S1_4)
                                                             || (!L1_4)
                                                             || (J1_4 ==
                                                                 G1_4))
               && ((!S1_4) || (!L1_4) || (K1_4 == H1_4)) && ((!S1_4)
                                                             || (!L1_4)
                                                             || (M1_4 ==
                                                                 I1_4))
               && ((!S1_4) || (!L1_4) || (N1_4 == J1_4)) && ((!S1_4)
                                                             || (!L1_4)
                                                             || (O1_4 ==
                                                                 K1_4))
               && ((!S1_4) || (!L1_4) || (P1_4 == M1_4)) && ((!S1_4)
                                                             || (!L1_4)
                                                             || (!F1_4))
               && ((!U_4) || (N_4 == (K_4 + -1))) && ((!U_4) || (U_4 && B_4))
               && ((!Q_4) || (!(D_4 == F_4))) && ((!Q_4) || (!(F_4 == G_4)))
               && ((!Q_4) || (E_4 == (F_4 ? -1 : 0))) && ((!Q_4)
                                                          || (H_4 ==
                                                              (G_4 ? -1 : 0)))
               && ((!Q_4) || (I_4 == (L_4 + E_4))) && ((!Q_4)
                                                       || (J_4 ==
                                                           (M_4 + H_4)))
               && ((!Q_4) || (Q_4 && B_4)) && ((!E2_4)
                                               || (W1_4 == (V1_4 == 0)))
               && ((!E2_4) || (A2_4 == (X1_4 && W1_4))) && ((!E2_4)
                                                            || (C2_4 ==
                                                                (Z1_4
                                                                 && A2_4)))
               && ((!E2_4) || (!(C2_4 == D2_4))) && ((!E2_4)
                                                     || (X1_4 == (U1_4 == 0)))
               && ((!E2_4) || (Z1_4 == (Y1_4 == 0))) && ((!E2_4)
                                                         || (S1_4 && E2_4))
               && ((!E2_4) || (!B2_4)) && ((!E2_4) || D2_4) && ((!L1_4)
                                                                ||
                                                                (!((I1_4 ==
                                                                    0) ==
                                                                   D1_4)))
               && ((!L1_4) || (!((H1_4 == 0) == B1_4))) && ((!L1_4)
                                                            ||
                                                            (!((G1_4 == 0) ==
                                                               C1_4)))
               && ((!L1_4) || (F1_4 == (E1_4 && D1_4))) && ((!L1_4)
                                                            || (E1_4 ==
                                                                (C1_4
                                                                 && B1_4)))
               && ((!L1_4) || (W_4 == A1_4)) && ((!L1_4)
                                                 || (X_4 == (Y_4 ? 1 : 0)))
               && ((!L1_4) || (Z_4 == A1_4)) && ((!S1_4) || (S1_4 && L1_4))
               && ((!F2_4) || (F2_4 && E2_4)) && F2_4 && (C_4 == (A_4 == 0))))
              abort ();
          CHC_COMP_UNUSED_5 = __VERIFIER_nondet__Bool ();
          if (!1)
              abort ();
          goto main_error;

      default:
          abort ();
      }

    // return expression

}

