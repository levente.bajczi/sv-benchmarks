// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: vmt-chc-benchmarks/vmt-speed_e8_649_e7_709_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "vmt-speed_e8_649_e7_709_000_range.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int state_0;
    _Bool state_1;
    _Bool state_2;
    int state_3;
    int state_4;
    int state_5;
    int state_6;
    int state_7;
    int state_8;
    int state_9;
    _Bool state_10;
    _Bool state_11;
    int state_12;
    _Bool state_13;
    _Bool state_14;
    _Bool state_15;
    _Bool state_16;
    _Bool state_17;
    _Bool state_18;
    _Bool state_19;
    int A_0;
    int B_0;
    _Bool C_0;
    int D_0;
    _Bool E_0;
    _Bool F_0;
    _Bool G_0;
    _Bool H_0;
    _Bool I_0;
    int J_0;
    int K_0;
    int L_0;
    int M_0;
    _Bool N_0;
    _Bool O_0;
    _Bool P_0;
    _Bool Q_0;
    _Bool R_0;
    int S_0;
    int T_0;
    int A_1;
    int B_1;
    _Bool C_1;
    int D_1;
    _Bool E_1;
    _Bool F_1;
    _Bool G_1;
    _Bool H_1;
    _Bool I_1;
    int J_1;
    int K_1;
    int L_1;
    int M_1;
    int N_1;
    _Bool O_1;
    _Bool P_1;
    _Bool Q_1;
    _Bool R_1;
    int S_1;
    _Bool T_1;
    _Bool U_1;
    _Bool V_1;
    _Bool W_1;
    _Bool X_1;
    int Y_1;
    int Z_1;
    _Bool A1_1;
    _Bool B1_1;
    int C1_1;
    int D1_1;
    int E1_1;
    int F1_1;
    int G1_1;
    _Bool H1_1;
    _Bool I1_1;
    _Bool J1_1;
    _Bool K1_1;
    _Bool L1_1;
    int M1_1;
    int N1_1;
    int A_2;
    int B_2;
    _Bool C_2;
    int D_2;
    _Bool E_2;
    _Bool F_2;
    _Bool G_2;
    _Bool H_2;
    _Bool I_2;
    int J_2;
    int K_2;
    int L_2;
    int M_2;
    _Bool N_2;
    _Bool O_2;
    _Bool P_2;
    _Bool Q_2;
    _Bool R_2;
    int S_2;
    int T_2;

    if (((state_0 <= -1000000000) || (state_0 >= 1000000000))
        || ((state_3 <= -1000000000) || (state_3 >= 1000000000))
        || ((state_4 <= -1000000000) || (state_4 >= 1000000000))
        || ((state_5 <= -1000000000) || (state_5 >= 1000000000))
        || ((state_6 <= -1000000000) || (state_6 >= 1000000000))
        || ((state_7 <= -1000000000) || (state_7 >= 1000000000))
        || ((state_8 <= -1000000000) || (state_8 >= 1000000000))
        || ((state_9 <= -1000000000) || (state_9 >= 1000000000))
        || ((state_12 <= -1000000000) || (state_12 >= 1000000000))
        || ((A_0 <= -1000000000) || (A_0 >= 1000000000))
        || ((B_0 <= -1000000000) || (B_0 >= 1000000000))
        || ((D_0 <= -1000000000) || (D_0 >= 1000000000))
        || ((J_0 <= -1000000000) || (J_0 >= 1000000000))
        || ((K_0 <= -1000000000) || (K_0 >= 1000000000))
        || ((L_0 <= -1000000000) || (L_0 >= 1000000000))
        || ((M_0 <= -1000000000) || (M_0 >= 1000000000))
        || ((S_0 <= -1000000000) || (S_0 >= 1000000000))
        || ((T_0 <= -1000000000) || (T_0 >= 1000000000))
        || ((A_1 <= -1000000000) || (A_1 >= 1000000000))
        || ((B_1 <= -1000000000) || (B_1 >= 1000000000))
        || ((D_1 <= -1000000000) || (D_1 >= 1000000000))
        || ((J_1 <= -1000000000) || (J_1 >= 1000000000))
        || ((K_1 <= -1000000000) || (K_1 >= 1000000000))
        || ((L_1 <= -1000000000) || (L_1 >= 1000000000))
        || ((M_1 <= -1000000000) || (M_1 >= 1000000000))
        || ((N_1 <= -1000000000) || (N_1 >= 1000000000))
        || ((S_1 <= -1000000000) || (S_1 >= 1000000000))
        || ((Y_1 <= -1000000000) || (Y_1 >= 1000000000))
        || ((Z_1 <= -1000000000) || (Z_1 >= 1000000000))
        || ((C1_1 <= -1000000000) || (C1_1 >= 1000000000))
        || ((D1_1 <= -1000000000) || (D1_1 >= 1000000000))
        || ((E1_1 <= -1000000000) || (E1_1 >= 1000000000))
        || ((F1_1 <= -1000000000) || (F1_1 >= 1000000000))
        || ((G1_1 <= -1000000000) || (G1_1 >= 1000000000))
        || ((M1_1 <= -1000000000) || (M1_1 >= 1000000000))
        || ((N1_1 <= -1000000000) || (N1_1 >= 1000000000))
        || ((A_2 <= -1000000000) || (A_2 >= 1000000000))
        || ((B_2 <= -1000000000) || (B_2 >= 1000000000))
        || ((D_2 <= -1000000000) || (D_2 >= 1000000000))
        || ((J_2 <= -1000000000) || (J_2 >= 1000000000))
        || ((K_2 <= -1000000000) || (K_2 >= 1000000000))
        || ((L_2 <= -1000000000) || (L_2 >= 1000000000))
        || ((M_2 <= -1000000000) || (M_2 >= 1000000000))
        || ((S_2 <= -1000000000) || (S_2 >= 1000000000))
        || ((T_2 <= -1000000000) || (T_2 >= 1000000000)))
        abort ();

    // main logic
    goto main_init;

  main_init:
    if (!
        ((K_0 == J_0) && (L_0 == T_0) && (L_0 == K_0) && (M_0 == 0)
         && (M_0 == S_0) && (!((O_0 && P_0) == N_0)) && (I_0 == R_0)
         && (R_0 == P_0) && (Q_0 == O_0) && (H_0 == Q_0) && ((!G_0) || (!F_0)
                                                             ||
                                                             ((T_0 +
                                                               (-1 * S_0) +
                                                               (-1 * D_0)) ==
                                                              0)) && (G_0
                                                                      || (B_0
                                                                          ==
                                                                          A_0)
                                                                      ||
                                                                      (!F_0))
         && (G_0 || (A_0 == 2) || (!F_0)) && ((T_0 == S_0) || (G_0 && F_0))
         && (((!G_0) && F_0) || (B_0 == 1)) && ((A_0 == 0) || ((!G_0) && F_0))
         && (!I_0) && (!H_0) && (B_0 == D_0)))
        abort ();
    state_0 = M_0;
    state_1 = H_0;
    state_2 = I_0;
    state_3 = L_0;
    state_4 = T_0;
    state_5 = S_0;
    state_6 = K_0;
    state_7 = B_0;
    state_8 = D_0;
    state_9 = J_0;
    state_10 = R_0;
    state_11 = Q_0;
    state_12 = A_0;
    state_13 = F_0;
    state_14 = G_0;
    state_15 = P_0;
    state_16 = O_0;
    state_17 = N_0;
    state_18 = C_0;
    state_19 = E_0;
    G1_1 = __VERIFIER_nondet_int ();
    if (((G1_1 <= -1000000000) || (G1_1 >= 1000000000)))
        abort ();
    E1_1 = __VERIFIER_nondet_int ();
    if (((E1_1 <= -1000000000) || (E1_1 >= 1000000000)))
        abort ();
    N_1 = __VERIFIER_nondet_int ();
    if (((N_1 <= -1000000000) || (N_1 >= 1000000000)))
        abort ();
    C1_1 = __VERIFIER_nondet_int ();
    if (((C1_1 <= -1000000000) || (C1_1 >= 1000000000)))
        abort ();
    O_1 = __VERIFIER_nondet__Bool ();
    P_1 = __VERIFIER_nondet__Bool ();
    A1_1 = __VERIFIER_nondet__Bool ();
    Q_1 = __VERIFIER_nondet__Bool ();
    R_1 = __VERIFIER_nondet__Bool ();
    S_1 = __VERIFIER_nondet_int ();
    if (((S_1 <= -1000000000) || (S_1 >= 1000000000)))
        abort ();
    T_1 = __VERIFIER_nondet__Bool ();
    U_1 = __VERIFIER_nondet__Bool ();
    V_1 = __VERIFIER_nondet__Bool ();
    W_1 = __VERIFIER_nondet__Bool ();
    X_1 = __VERIFIER_nondet__Bool ();
    Y_1 = __VERIFIER_nondet_int ();
    if (((Y_1 <= -1000000000) || (Y_1 >= 1000000000)))
        abort ();
    Z_1 = __VERIFIER_nondet_int ();
    if (((Z_1 <= -1000000000) || (Z_1 >= 1000000000)))
        abort ();
    F1_1 = __VERIFIER_nondet_int ();
    if (((F1_1 <= -1000000000) || (F1_1 >= 1000000000)))
        abort ();
    D1_1 = __VERIFIER_nondet_int ();
    if (((D1_1 <= -1000000000) || (D1_1 >= 1000000000)))
        abort ();
    B1_1 = __VERIFIER_nondet__Bool ();
    M_1 = state_0;
    H_1 = state_1;
    I_1 = state_2;
    L_1 = state_3;
    N1_1 = state_4;
    M1_1 = state_5;
    K_1 = state_6;
    B_1 = state_7;
    D_1 = state_8;
    J_1 = state_9;
    L1_1 = state_10;
    K1_1 = state_11;
    A_1 = state_12;
    F_1 = state_13;
    G_1 = state_14;
    J1_1 = state_15;
    I1_1 = state_16;
    H1_1 = state_17;
    C_1 = state_18;
    E_1 = state_19;
    if (!
        ((Z_1 == G1_1) && (C1_1 == E1_1) && (F1_1 == N_1) && (G1_1 == F1_1)
         && (M_1 == M1_1) && (L_1 == N1_1) && (L_1 == K_1) && (K_1 == S_1)
         && (K_1 == J_1) && (B_1 == D_1) && (!((I1_1 && J1_1) == H1_1))
         && (!((V_1 && U_1) == T_1)) && (L1_1 == J1_1) && (K1_1 == I1_1)
         && (Q_1 == P_1) && (R_1 == O_1) && (W_1 == R_1) && (W_1 == U_1)
         && (X_1 == Q_1) && (X_1 == V_1) && (I_1 == L1_1) && (H_1 == K1_1)
         && ((!B1_1) || (!A1_1) || ((Z_1 + (-1 * Y_1) + (-1 * C1_1)) == 0))
         && (A1_1 || (D1_1 == 2) || (!B1_1)) && (A1_1 || (!B1_1)
                                                 || (E1_1 == D1_1)) && ((!G_1)
                                                                        ||
                                                                        (!F_1)
                                                                        ||
                                                                        ((N1_1
                                                                          +
                                                                          (-1
                                                                           *
                                                                           M1_1)
                                                                          +
                                                                          (-1
                                                                           *
                                                                           D_1))
                                                                         ==
                                                                         0))
         && (G_1 || (!F_1) || (B_1 == A_1)) && (G_1 || (A_1 == 2) || (!F_1))
         && ((Z_1 == Y_1) || (B1_1 && A1_1)) && ((B1_1 && (!A1_1))
                                                 || (D1_1 == 0)) && ((B1_1
                                                                      &&
                                                                      (!A1_1))
                                                                     || (E1_1
                                                                         ==
                                                                         1))
         && ((N1_1 == M1_1) || (F_1 && G_1)) && ((F_1 && (!G_1))
                                                 || (B_1 == 1)) && ((F_1
                                                                     &&
                                                                     (!G_1))
                                                                    || (A_1 ==
                                                                        0))
         && ((!L1_1) || (!((N_1 <= 0) == P_1))) && (L1_1
                                                    || (P_1 == (10 <= N_1)))
         && ((!K1_1) || (!((0 <= N_1) == O_1))) && (K1_1
                                                    || (O_1 == (N_1 <= -10)))
         && (Y_1 == S_1)))
        abort ();
    state_0 = S_1;
    state_1 = R_1;
    state_2 = Q_1;
    state_3 = G1_1;
    state_4 = Z_1;
    state_5 = Y_1;
    state_6 = F1_1;
    state_7 = E1_1;
    state_8 = C1_1;
    state_9 = N_1;
    state_10 = X_1;
    state_11 = W_1;
    state_12 = D1_1;
    state_13 = B1_1;
    state_14 = A1_1;
    state_15 = V_1;
    state_16 = U_1;
    state_17 = T_1;
    state_18 = P_1;
    state_19 = O_1;
    goto state_0;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  state_0:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          M_2 = state_0;
          H_2 = state_1;
          I_2 = state_2;
          L_2 = state_3;
          T_2 = state_4;
          S_2 = state_5;
          K_2 = state_6;
          B_2 = state_7;
          D_2 = state_8;
          J_2 = state_9;
          R_2 = state_10;
          Q_2 = state_11;
          A_2 = state_12;
          F_2 = state_13;
          G_2 = state_14;
          P_2 = state_15;
          O_2 = state_16;
          N_2 = state_17;
          C_2 = state_18;
          E_2 = state_19;
          if (!(!N_2))
              abort ();
          goto main_error;

      case 1:
          G1_1 = __VERIFIER_nondet_int ();
          if (((G1_1 <= -1000000000) || (G1_1 >= 1000000000)))
              abort ();
          E1_1 = __VERIFIER_nondet_int ();
          if (((E1_1 <= -1000000000) || (E1_1 >= 1000000000)))
              abort ();
          N_1 = __VERIFIER_nondet_int ();
          if (((N_1 <= -1000000000) || (N_1 >= 1000000000)))
              abort ();
          C1_1 = __VERIFIER_nondet_int ();
          if (((C1_1 <= -1000000000) || (C1_1 >= 1000000000)))
              abort ();
          O_1 = __VERIFIER_nondet__Bool ();
          P_1 = __VERIFIER_nondet__Bool ();
          A1_1 = __VERIFIER_nondet__Bool ();
          Q_1 = __VERIFIER_nondet__Bool ();
          R_1 = __VERIFIER_nondet__Bool ();
          S_1 = __VERIFIER_nondet_int ();
          if (((S_1 <= -1000000000) || (S_1 >= 1000000000)))
              abort ();
          T_1 = __VERIFIER_nondet__Bool ();
          U_1 = __VERIFIER_nondet__Bool ();
          V_1 = __VERIFIER_nondet__Bool ();
          W_1 = __VERIFIER_nondet__Bool ();
          X_1 = __VERIFIER_nondet__Bool ();
          Y_1 = __VERIFIER_nondet_int ();
          if (((Y_1 <= -1000000000) || (Y_1 >= 1000000000)))
              abort ();
          Z_1 = __VERIFIER_nondet_int ();
          if (((Z_1 <= -1000000000) || (Z_1 >= 1000000000)))
              abort ();
          F1_1 = __VERIFIER_nondet_int ();
          if (((F1_1 <= -1000000000) || (F1_1 >= 1000000000)))
              abort ();
          D1_1 = __VERIFIER_nondet_int ();
          if (((D1_1 <= -1000000000) || (D1_1 >= 1000000000)))
              abort ();
          B1_1 = __VERIFIER_nondet__Bool ();
          M_1 = state_0;
          H_1 = state_1;
          I_1 = state_2;
          L_1 = state_3;
          N1_1 = state_4;
          M1_1 = state_5;
          K_1 = state_6;
          B_1 = state_7;
          D_1 = state_8;
          J_1 = state_9;
          L1_1 = state_10;
          K1_1 = state_11;
          A_1 = state_12;
          F_1 = state_13;
          G_1 = state_14;
          J1_1 = state_15;
          I1_1 = state_16;
          H1_1 = state_17;
          C_1 = state_18;
          E_1 = state_19;
          if (!
              ((Z_1 == G1_1) && (C1_1 == E1_1) && (F1_1 == N_1)
               && (G1_1 == F1_1) && (M_1 == M1_1) && (L_1 == N1_1)
               && (L_1 == K_1) && (K_1 == S_1) && (K_1 == J_1) && (B_1 == D_1)
               && (!((I1_1 && J1_1) == H1_1)) && (!((V_1 && U_1) == T_1))
               && (L1_1 == J1_1) && (K1_1 == I1_1) && (Q_1 == P_1)
               && (R_1 == O_1) && (W_1 == R_1) && (W_1 == U_1) && (X_1 == Q_1)
               && (X_1 == V_1) && (I_1 == L1_1) && (H_1 == K1_1) && ((!B1_1)
                                                                     ||
                                                                     (!A1_1)
                                                                     ||
                                                                     ((Z_1 +
                                                                       (-1 *
                                                                        Y_1) +
                                                                       (-1 *
                                                                        C1_1))
                                                                      == 0))
               && (A1_1 || (D1_1 == 2) || (!B1_1)) && (A1_1 || (!B1_1)
                                                       || (E1_1 == D1_1))
               && ((!G_1) || (!F_1)
                   || ((N1_1 + (-1 * M1_1) + (-1 * D_1)) == 0)) && (G_1
                                                                    || (!F_1)
                                                                    || (B_1 ==
                                                                        A_1))
               && (G_1 || (A_1 == 2) || (!F_1)) && ((Z_1 == Y_1)
                                                    || (B1_1 && A1_1))
               && ((B1_1 && (!A1_1)) || (D1_1 == 0)) && ((B1_1 && (!A1_1))
                                                         || (E1_1 == 1))
               && ((N1_1 == M1_1) || (F_1 && G_1)) && ((F_1 && (!G_1))
                                                       || (B_1 == 1)) && ((F_1
                                                                           &&
                                                                           (!G_1))
                                                                          ||
                                                                          (A_1
                                                                           ==
                                                                           0))
               && ((!L1_1) || (!((N_1 <= 0) == P_1))) && (L1_1
                                                          || (P_1 ==
                                                              (10 <= N_1)))
               && ((!K1_1) || (!((0 <= N_1) == O_1))) && (K1_1
                                                          || (O_1 ==
                                                              (N_1 <= -10)))
               && (Y_1 == S_1)))
              abort ();
          state_0 = S_1;
          state_1 = R_1;
          state_2 = Q_1;
          state_3 = G1_1;
          state_4 = Z_1;
          state_5 = Y_1;
          state_6 = F1_1;
          state_7 = E1_1;
          state_8 = C1_1;
          state_9 = N_1;
          state_10 = X_1;
          state_11 = W_1;
          state_12 = D1_1;
          state_13 = B1_1;
          state_14 = A1_1;
          state_15 = V_1;
          state_16 = U_1;
          state_17 = T_1;
          state_18 = P_1;
          state_19 = O_1;
          goto state_0;

      default:
          abort ();
      }

    // return expression

}

