// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: eldarica-misc/027-horn_001.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "027-horn_001_range.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int INV1_0;
    int INV1_1;
    int INV1_2;
    int INV1_3;
    int INV1_4;
    int INV1_5;
    int A_3;
    int B_3;
    int C_3;
    int D_3;
    int E_3;
    int F_3;
    int A_4;
    int B_4;
    int C_4;
    int D_4;
    int E_4;
    int F_4;
    int G_4;
    int H_4;
    int I_4;
    int J_4;
    int K_4;
    int L_4;
    int A_5;
    int B_5;
    int C_5;
    int D_5;
    int E_5;
    int F_5;
    int G_5;
    int H_5;
    int I_5;
    int J_5;
    int K_5;
    int L_5;
    int M_5;
    int A_6;
    int B_6;
    int C_6;
    int D_6;
    int E_6;
    int F_6;
    int G_6;
    int H_6;
    int I_6;
    int J_6;
    int K_6;
    int L_6;
    int M_6;
    int N_6;
    int A_7;
    int B_7;
    int C_7;
    int D_7;
    int E_7;
    int F_7;
    int G_7;
    int H_7;
    int I_7;
    int J_7;
    int K_7;
    int A_8;
    int B_8;
    int C_8;
    int D_8;
    int E_8;
    int F_8;
    int G_8;
    int H_8;
    int I_8;
    int J_8;
    int K_8;
    int L_8;
    int A_9;
    int B_9;
    int C_9;
    int D_9;
    int E_9;
    int F_9;
    int G_9;
    int H_9;
    int I_9;
    int J_9;

    if (((INV1_0 <= -1000000000) || (INV1_0 >= 1000000000))
        || ((INV1_1 <= -1000000000) || (INV1_1 >= 1000000000))
        || ((INV1_2 <= -1000000000) || (INV1_2 >= 1000000000))
        || ((INV1_3 <= -1000000000) || (INV1_3 >= 1000000000))
        || ((INV1_4 <= -1000000000) || (INV1_4 >= 1000000000))
        || ((INV1_5 <= -1000000000) || (INV1_5 >= 1000000000))
        || ((A_3 <= -1000000000) || (A_3 >= 1000000000))
        || ((B_3 <= -1000000000) || (B_3 >= 1000000000))
        || ((C_3 <= -1000000000) || (C_3 >= 1000000000))
        || ((D_3 <= -1000000000) || (D_3 >= 1000000000))
        || ((E_3 <= -1000000000) || (E_3 >= 1000000000))
        || ((F_3 <= -1000000000) || (F_3 >= 1000000000))
        || ((A_4 <= -1000000000) || (A_4 >= 1000000000))
        || ((B_4 <= -1000000000) || (B_4 >= 1000000000))
        || ((C_4 <= -1000000000) || (C_4 >= 1000000000))
        || ((D_4 <= -1000000000) || (D_4 >= 1000000000))
        || ((E_4 <= -1000000000) || (E_4 >= 1000000000))
        || ((F_4 <= -1000000000) || (F_4 >= 1000000000))
        || ((G_4 <= -1000000000) || (G_4 >= 1000000000))
        || ((H_4 <= -1000000000) || (H_4 >= 1000000000))
        || ((I_4 <= -1000000000) || (I_4 >= 1000000000))
        || ((J_4 <= -1000000000) || (J_4 >= 1000000000))
        || ((K_4 <= -1000000000) || (K_4 >= 1000000000))
        || ((L_4 <= -1000000000) || (L_4 >= 1000000000))
        || ((A_5 <= -1000000000) || (A_5 >= 1000000000))
        || ((B_5 <= -1000000000) || (B_5 >= 1000000000))
        || ((C_5 <= -1000000000) || (C_5 >= 1000000000))
        || ((D_5 <= -1000000000) || (D_5 >= 1000000000))
        || ((E_5 <= -1000000000) || (E_5 >= 1000000000))
        || ((F_5 <= -1000000000) || (F_5 >= 1000000000))
        || ((G_5 <= -1000000000) || (G_5 >= 1000000000))
        || ((H_5 <= -1000000000) || (H_5 >= 1000000000))
        || ((I_5 <= -1000000000) || (I_5 >= 1000000000))
        || ((J_5 <= -1000000000) || (J_5 >= 1000000000))
        || ((K_5 <= -1000000000) || (K_5 >= 1000000000))
        || ((L_5 <= -1000000000) || (L_5 >= 1000000000))
        || ((M_5 <= -1000000000) || (M_5 >= 1000000000))
        || ((A_6 <= -1000000000) || (A_6 >= 1000000000))
        || ((B_6 <= -1000000000) || (B_6 >= 1000000000))
        || ((C_6 <= -1000000000) || (C_6 >= 1000000000))
        || ((D_6 <= -1000000000) || (D_6 >= 1000000000))
        || ((E_6 <= -1000000000) || (E_6 >= 1000000000))
        || ((F_6 <= -1000000000) || (F_6 >= 1000000000))
        || ((G_6 <= -1000000000) || (G_6 >= 1000000000))
        || ((H_6 <= -1000000000) || (H_6 >= 1000000000))
        || ((I_6 <= -1000000000) || (I_6 >= 1000000000))
        || ((J_6 <= -1000000000) || (J_6 >= 1000000000))
        || ((K_6 <= -1000000000) || (K_6 >= 1000000000))
        || ((L_6 <= -1000000000) || (L_6 >= 1000000000))
        || ((M_6 <= -1000000000) || (M_6 >= 1000000000))
        || ((N_6 <= -1000000000) || (N_6 >= 1000000000))
        || ((A_7 <= -1000000000) || (A_7 >= 1000000000))
        || ((B_7 <= -1000000000) || (B_7 >= 1000000000))
        || ((C_7 <= -1000000000) || (C_7 >= 1000000000))
        || ((D_7 <= -1000000000) || (D_7 >= 1000000000))
        || ((E_7 <= -1000000000) || (E_7 >= 1000000000))
        || ((F_7 <= -1000000000) || (F_7 >= 1000000000))
        || ((G_7 <= -1000000000) || (G_7 >= 1000000000))
        || ((H_7 <= -1000000000) || (H_7 >= 1000000000))
        || ((I_7 <= -1000000000) || (I_7 >= 1000000000))
        || ((J_7 <= -1000000000) || (J_7 >= 1000000000))
        || ((K_7 <= -1000000000) || (K_7 >= 1000000000))
        || ((A_8 <= -1000000000) || (A_8 >= 1000000000))
        || ((B_8 <= -1000000000) || (B_8 >= 1000000000))
        || ((C_8 <= -1000000000) || (C_8 >= 1000000000))
        || ((D_8 <= -1000000000) || (D_8 >= 1000000000))
        || ((E_8 <= -1000000000) || (E_8 >= 1000000000))
        || ((F_8 <= -1000000000) || (F_8 >= 1000000000))
        || ((G_8 <= -1000000000) || (G_8 >= 1000000000))
        || ((H_8 <= -1000000000) || (H_8 >= 1000000000))
        || ((I_8 <= -1000000000) || (I_8 >= 1000000000))
        || ((J_8 <= -1000000000) || (J_8 >= 1000000000))
        || ((K_8 <= -1000000000) || (K_8 >= 1000000000))
        || ((L_8 <= -1000000000) || (L_8 >= 1000000000))
        || ((A_9 <= -1000000000) || (A_9 >= 1000000000))
        || ((B_9 <= -1000000000) || (B_9 >= 1000000000))
        || ((C_9 <= -1000000000) || (C_9 >= 1000000000))
        || ((D_9 <= -1000000000) || (D_9 >= 1000000000))
        || ((E_9 <= -1000000000) || (E_9 >= 1000000000))
        || ((F_9 <= -1000000000) || (F_9 >= 1000000000))
        || ((G_9 <= -1000000000) || (G_9 >= 1000000000))
        || ((H_9 <= -1000000000) || (H_9 >= 1000000000))
        || ((I_9 <= -1000000000) || (I_9 >= 1000000000))
        || ((J_9 <= -1000000000) || (J_9 >= 1000000000)))
        abort ();

    // main logic
    goto main_init;

  main_init:
    if (!
        ((F_3 == 0) && (C_3 == 0) && (B_3 == E_3) && (A_3 >= 1)
         && (A_3 == D_3)))
        abort ();
    INV1_0 = A_3;
    INV1_1 = B_3;
    INV1_2 = C_3;
    INV1_3 = D_3;
    INV1_4 = E_3;
    INV1_5 = F_3;
    B_6 = __VERIFIER_nondet_int ();
    if (((B_6 <= -1000000000) || (B_6 >= 1000000000)))
        abort ();
    C_6 = __VERIFIER_nondet_int ();
    if (((C_6 <= -1000000000) || (C_6 >= 1000000000)))
        abort ();
    E_6 = __VERIFIER_nondet_int ();
    if (((E_6 <= -1000000000) || (E_6 >= 1000000000)))
        abort ();
    F_6 = __VERIFIER_nondet_int ();
    if (((F_6 <= -1000000000) || (F_6 >= 1000000000)))
        abort ();
    K_6 = __VERIFIER_nondet_int ();
    if (((K_6 <= -1000000000) || (K_6 >= 1000000000)))
        abort ();
    L_6 = __VERIFIER_nondet_int ();
    if (((L_6 <= -1000000000) || (L_6 >= 1000000000)))
        abort ();
    M_6 = __VERIFIER_nondet_int ();
    if (((M_6 <= -1000000000) || (M_6 >= 1000000000)))
        abort ();
    N_6 = __VERIFIER_nondet_int ();
    if (((N_6 <= -1000000000) || (N_6 >= 1000000000)))
        abort ();
    A_6 = INV1_0;
    G_6 = INV1_1;
    H_6 = INV1_2;
    D_6 = INV1_3;
    I_6 = INV1_4;
    J_6 = INV1_5;
    if (!
        ((I_6 == (E_6 + 1)) && (H_6 == (C_6 + -1)) && (M_6 == N_6)
         && (K_6 == L_6) && (J_6 == (F_6 + -1)) && (D_6 >= 1) && (G_6 >= 1)
         && (I_6 >= 1) && (K_6 >= 1) && (G_6 == (B_6 + 1))))
        abort ();
    INV1_0 = A_6;
    INV1_1 = B_6;
    INV1_2 = C_6;
    INV1_3 = D_6;
    INV1_4 = E_6;
    INV1_5 = F_6;
    goto INV1_4;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  INV2:
    goto INV2;
  INV1_0:
    switch (__VERIFIER_nondet_int ())
      {
      case 0:
          G_9 = __VERIFIER_nondet_int ();
          if (((G_9 <= -1000000000) || (G_9 >= 1000000000)))
              abort ();
          H_9 = __VERIFIER_nondet_int ();
          if (((H_9 <= -1000000000) || (H_9 >= 1000000000)))
              abort ();
          I_9 = __VERIFIER_nondet_int ();
          if (((I_9 <= -1000000000) || (I_9 >= 1000000000)))
              abort ();
          J_9 = __VERIFIER_nondet_int ();
          if (((J_9 <= -1000000000) || (J_9 >= 1000000000)))
              abort ();
          E_9 = INV1_0;
          C_9 = INV1_1;
          A_9 = INV1_2;
          F_9 = INV1_3;
          D_9 = INV1_4;
          B_9 = INV1_5;
          if (!
              ((I_9 == J_9) && (G_9 == H_9) && (!(C_9 >= 1)) && (!(D_9 >= 1))
               && (G_9 >= 1) && (!(A_9 == B_9))))
              abort ();
          goto main_error;

      case 1:
          B_5 = __VERIFIER_nondet_int ();
          if (((B_5 <= -1000000000) || (B_5 >= 1000000000)))
              abort ();
          C_5 = __VERIFIER_nondet_int ();
          if (((C_5 <= -1000000000) || (C_5 >= 1000000000)))
              abort ();
          E_5 = __VERIFIER_nondet_int ();
          if (((E_5 <= -1000000000) || (E_5 >= 1000000000)))
              abort ();
          J_5 = __VERIFIER_nondet_int ();
          if (((J_5 <= -1000000000) || (J_5 >= 1000000000)))
              abort ();
          K_5 = __VERIFIER_nondet_int ();
          if (((K_5 <= -1000000000) || (K_5 >= 1000000000)))
              abort ();
          L_5 = __VERIFIER_nondet_int ();
          if (((L_5 <= -1000000000) || (L_5 >= 1000000000)))
              abort ();
          M_5 = __VERIFIER_nondet_int ();
          if (((M_5 <= -1000000000) || (M_5 >= 1000000000)))
              abort ();
          A_5 = INV1_0;
          G_5 = INV1_1;
          H_5 = INV1_2;
          D_5 = INV1_3;
          I_5 = INV1_4;
          F_5 = INV1_5;
          if (!
              ((G_5 == (B_5 + 1)) && (L_5 == M_5) && (J_5 == K_5)
               && (I_5 == (E_5 + 1)) && (!(D_5 >= 1)) && (G_5 >= 1)
               && (J_5 >= 1) && (I_5 >= 1) && (H_5 == (C_5 + -1))))
              abort ();
          INV1_0 = A_5;
          INV1_1 = B_5;
          INV1_2 = C_5;
          INV1_3 = D_5;
          INV1_4 = E_5;
          INV1_5 = F_5;
          B_6 = __VERIFIER_nondet_int ();
          if (((B_6 <= -1000000000) || (B_6 >= 1000000000)))
              abort ();
          C_6 = __VERIFIER_nondet_int ();
          if (((C_6 <= -1000000000) || (C_6 >= 1000000000)))
              abort ();
          E_6 = __VERIFIER_nondet_int ();
          if (((E_6 <= -1000000000) || (E_6 >= 1000000000)))
              abort ();
          F_6 = __VERIFIER_nondet_int ();
          if (((F_6 <= -1000000000) || (F_6 >= 1000000000)))
              abort ();
          K_6 = __VERIFIER_nondet_int ();
          if (((K_6 <= -1000000000) || (K_6 >= 1000000000)))
              abort ();
          L_6 = __VERIFIER_nondet_int ();
          if (((L_6 <= -1000000000) || (L_6 >= 1000000000)))
              abort ();
          M_6 = __VERIFIER_nondet_int ();
          if (((M_6 <= -1000000000) || (M_6 >= 1000000000)))
              abort ();
          N_6 = __VERIFIER_nondet_int ();
          if (((N_6 <= -1000000000) || (N_6 >= 1000000000)))
              abort ();
          A_6 = INV1_0;
          G_6 = INV1_1;
          H_6 = INV1_2;
          D_6 = INV1_3;
          I_6 = INV1_4;
          J_6 = INV1_5;
          if (!
              ((I_6 == (E_6 + 1)) && (H_6 == (C_6 + -1)) && (M_6 == N_6)
               && (K_6 == L_6) && (J_6 == (F_6 + -1)) && (D_6 >= 1)
               && (G_6 >= 1) && (I_6 >= 1) && (K_6 >= 1)
               && (G_6 == (B_6 + 1))))
              abort ();
          INV1_0 = A_6;
          INV1_1 = B_6;
          INV1_2 = C_6;
          INV1_3 = D_6;
          INV1_4 = E_6;
          INV1_5 = F_6;
          goto INV1_4;

      case 2:
          B_6 = __VERIFIER_nondet_int ();
          if (((B_6 <= -1000000000) || (B_6 >= 1000000000)))
              abort ();
          C_6 = __VERIFIER_nondet_int ();
          if (((C_6 <= -1000000000) || (C_6 >= 1000000000)))
              abort ();
          E_6 = __VERIFIER_nondet_int ();
          if (((E_6 <= -1000000000) || (E_6 >= 1000000000)))
              abort ();
          F_6 = __VERIFIER_nondet_int ();
          if (((F_6 <= -1000000000) || (F_6 >= 1000000000)))
              abort ();
          K_6 = __VERIFIER_nondet_int ();
          if (((K_6 <= -1000000000) || (K_6 >= 1000000000)))
              abort ();
          L_6 = __VERIFIER_nondet_int ();
          if (((L_6 <= -1000000000) || (L_6 >= 1000000000)))
              abort ();
          M_6 = __VERIFIER_nondet_int ();
          if (((M_6 <= -1000000000) || (M_6 >= 1000000000)))
              abort ();
          N_6 = __VERIFIER_nondet_int ();
          if (((N_6 <= -1000000000) || (N_6 >= 1000000000)))
              abort ();
          A_6 = INV1_0;
          G_6 = INV1_1;
          H_6 = INV1_2;
          D_6 = INV1_3;
          I_6 = INV1_4;
          J_6 = INV1_5;
          if (!
              ((I_6 == (E_6 + 1)) && (H_6 == (C_6 + -1)) && (M_6 == N_6)
               && (K_6 == L_6) && (J_6 == (F_6 + -1)) && (D_6 >= 1)
               && (G_6 >= 1) && (I_6 >= 1) && (K_6 >= 1)
               && (G_6 == (B_6 + 1))))
              abort ();
          INV1_0 = A_6;
          INV1_1 = B_6;
          INV1_2 = C_6;
          INV1_3 = D_6;
          INV1_4 = E_6;
          INV1_5 = F_6;
          B_6 = __VERIFIER_nondet_int ();
          if (((B_6 <= -1000000000) || (B_6 >= 1000000000)))
              abort ();
          C_6 = __VERIFIER_nondet_int ();
          if (((C_6 <= -1000000000) || (C_6 >= 1000000000)))
              abort ();
          E_6 = __VERIFIER_nondet_int ();
          if (((E_6 <= -1000000000) || (E_6 >= 1000000000)))
              abort ();
          F_6 = __VERIFIER_nondet_int ();
          if (((F_6 <= -1000000000) || (F_6 >= 1000000000)))
              abort ();
          K_6 = __VERIFIER_nondet_int ();
          if (((K_6 <= -1000000000) || (K_6 >= 1000000000)))
              abort ();
          L_6 = __VERIFIER_nondet_int ();
          if (((L_6 <= -1000000000) || (L_6 >= 1000000000)))
              abort ();
          M_6 = __VERIFIER_nondet_int ();
          if (((M_6 <= -1000000000) || (M_6 >= 1000000000)))
              abort ();
          N_6 = __VERIFIER_nondet_int ();
          if (((N_6 <= -1000000000) || (N_6 >= 1000000000)))
              abort ();
          A_6 = INV1_0;
          G_6 = INV1_1;
          H_6 = INV1_2;
          D_6 = INV1_3;
          I_6 = INV1_4;
          J_6 = INV1_5;
          if (!
              ((I_6 == (E_6 + 1)) && (H_6 == (C_6 + -1)) && (M_6 == N_6)
               && (K_6 == L_6) && (J_6 == (F_6 + -1)) && (D_6 >= 1)
               && (G_6 >= 1) && (I_6 >= 1) && (K_6 >= 1)
               && (G_6 == (B_6 + 1))))
              abort ();
          INV1_0 = A_6;
          INV1_1 = B_6;
          INV1_2 = C_6;
          INV1_3 = D_6;
          INV1_4 = E_6;
          INV1_5 = F_6;
          goto INV1_4;

      case 3:
          E_7 = __VERIFIER_nondet_int ();
          if (((E_7 <= -1000000000) || (E_7 >= 1000000000)))
              abort ();
          H_7 = __VERIFIER_nondet_int ();
          if (((H_7 <= -1000000000) || (H_7 >= 1000000000)))
              abort ();
          I_7 = __VERIFIER_nondet_int ();
          if (((I_7 <= -1000000000) || (I_7 >= 1000000000)))
              abort ();
          J_7 = __VERIFIER_nondet_int ();
          if (((J_7 <= -1000000000) || (J_7 >= 1000000000)))
              abort ();
          K_7 = __VERIFIER_nondet_int ();
          if (((K_7 <= -1000000000) || (K_7 >= 1000000000)))
              abort ();
          A_7 = INV1_0;
          B_7 = INV1_1;
          C_7 = INV1_2;
          D_7 = INV1_3;
          G_7 = INV1_4;
          F_7 = INV1_5;
          if (!
              ((H_7 == I_7) && (G_7 == (E_7 + 1)) && (!(B_7 >= 1))
               && (!(D_7 >= 1)) && (H_7 >= 1) && (G_7 >= 1) && (J_7 == K_7)))
              abort ();
          INV1_0 = A_7;
          INV1_1 = B_7;
          INV1_2 = C_7;
          INV1_3 = D_7;
          INV1_4 = E_7;
          INV1_5 = F_7;
          B_6 = __VERIFIER_nondet_int ();
          if (((B_6 <= -1000000000) || (B_6 >= 1000000000)))
              abort ();
          C_6 = __VERIFIER_nondet_int ();
          if (((C_6 <= -1000000000) || (C_6 >= 1000000000)))
              abort ();
          E_6 = __VERIFIER_nondet_int ();
          if (((E_6 <= -1000000000) || (E_6 >= 1000000000)))
              abort ();
          F_6 = __VERIFIER_nondet_int ();
          if (((F_6 <= -1000000000) || (F_6 >= 1000000000)))
              abort ();
          K_6 = __VERIFIER_nondet_int ();
          if (((K_6 <= -1000000000) || (K_6 >= 1000000000)))
              abort ();
          L_6 = __VERIFIER_nondet_int ();
          if (((L_6 <= -1000000000) || (L_6 >= 1000000000)))
              abort ();
          M_6 = __VERIFIER_nondet_int ();
          if (((M_6 <= -1000000000) || (M_6 >= 1000000000)))
              abort ();
          N_6 = __VERIFIER_nondet_int ();
          if (((N_6 <= -1000000000) || (N_6 >= 1000000000)))
              abort ();
          A_6 = INV1_0;
          G_6 = INV1_1;
          H_6 = INV1_2;
          D_6 = INV1_3;
          I_6 = INV1_4;
          J_6 = INV1_5;
          if (!
              ((I_6 == (E_6 + 1)) && (H_6 == (C_6 + -1)) && (M_6 == N_6)
               && (K_6 == L_6) && (J_6 == (F_6 + -1)) && (D_6 >= 1)
               && (G_6 >= 1) && (I_6 >= 1) && (K_6 >= 1)
               && (G_6 == (B_6 + 1))))
              abort ();
          INV1_0 = A_6;
          INV1_1 = B_6;
          INV1_2 = C_6;
          INV1_3 = D_6;
          INV1_4 = E_6;
          INV1_5 = F_6;
          goto INV1_4;

      case 4:
          E_8 = __VERIFIER_nondet_int ();
          if (((E_8 <= -1000000000) || (E_8 >= 1000000000)))
              abort ();
          F_8 = __VERIFIER_nondet_int ();
          if (((F_8 <= -1000000000) || (F_8 >= 1000000000)))
              abort ();
          I_8 = __VERIFIER_nondet_int ();
          if (((I_8 <= -1000000000) || (I_8 >= 1000000000)))
              abort ();
          J_8 = __VERIFIER_nondet_int ();
          if (((J_8 <= -1000000000) || (J_8 >= 1000000000)))
              abort ();
          K_8 = __VERIFIER_nondet_int ();
          if (((K_8 <= -1000000000) || (K_8 >= 1000000000)))
              abort ();
          L_8 = __VERIFIER_nondet_int ();
          if (((L_8 <= -1000000000) || (L_8 >= 1000000000)))
              abort ();
          A_8 = INV1_0;
          B_8 = INV1_1;
          C_8 = INV1_2;
          D_8 = INV1_3;
          G_8 = INV1_4;
          H_8 = INV1_5;
          if (!
              ((K_8 == L_8) && (I_8 == J_8) && (H_8 == (F_8 + -1))
               && (!(B_8 >= 1)) && (D_8 >= 1) && (G_8 >= 1) && (I_8 >= 1)
               && (G_8 == (E_8 + 1))))
              abort ();
          INV1_0 = A_8;
          INV1_1 = B_8;
          INV1_2 = C_8;
          INV1_3 = D_8;
          INV1_4 = E_8;
          INV1_5 = F_8;
          B_6 = __VERIFIER_nondet_int ();
          if (((B_6 <= -1000000000) || (B_6 >= 1000000000)))
              abort ();
          C_6 = __VERIFIER_nondet_int ();
          if (((C_6 <= -1000000000) || (C_6 >= 1000000000)))
              abort ();
          E_6 = __VERIFIER_nondet_int ();
          if (((E_6 <= -1000000000) || (E_6 >= 1000000000)))
              abort ();
          F_6 = __VERIFIER_nondet_int ();
          if (((F_6 <= -1000000000) || (F_6 >= 1000000000)))
              abort ();
          K_6 = __VERIFIER_nondet_int ();
          if (((K_6 <= -1000000000) || (K_6 >= 1000000000)))
              abort ();
          L_6 = __VERIFIER_nondet_int ();
          if (((L_6 <= -1000000000) || (L_6 >= 1000000000)))
              abort ();
          M_6 = __VERIFIER_nondet_int ();
          if (((M_6 <= -1000000000) || (M_6 >= 1000000000)))
              abort ();
          N_6 = __VERIFIER_nondet_int ();
          if (((N_6 <= -1000000000) || (N_6 >= 1000000000)))
              abort ();
          A_6 = INV1_0;
          G_6 = INV1_1;
          H_6 = INV1_2;
          D_6 = INV1_3;
          I_6 = INV1_4;
          J_6 = INV1_5;
          if (!
              ((I_6 == (E_6 + 1)) && (H_6 == (C_6 + -1)) && (M_6 == N_6)
               && (K_6 == L_6) && (J_6 == (F_6 + -1)) && (D_6 >= 1)
               && (G_6 >= 1) && (I_6 >= 1) && (K_6 >= 1)
               && (G_6 == (B_6 + 1))))
              abort ();
          INV1_0 = A_6;
          INV1_1 = B_6;
          INV1_2 = C_6;
          INV1_3 = D_6;
          INV1_4 = E_6;
          INV1_5 = F_6;
          goto INV1_4;

      case 5:
          B_6 = __VERIFIER_nondet_int ();
          if (((B_6 <= -1000000000) || (B_6 >= 1000000000)))
              abort ();
          C_6 = __VERIFIER_nondet_int ();
          if (((C_6 <= -1000000000) || (C_6 >= 1000000000)))
              abort ();
          E_6 = __VERIFIER_nondet_int ();
          if (((E_6 <= -1000000000) || (E_6 >= 1000000000)))
              abort ();
          F_6 = __VERIFIER_nondet_int ();
          if (((F_6 <= -1000000000) || (F_6 >= 1000000000)))
              abort ();
          K_6 = __VERIFIER_nondet_int ();
          if (((K_6 <= -1000000000) || (K_6 >= 1000000000)))
              abort ();
          L_6 = __VERIFIER_nondet_int ();
          if (((L_6 <= -1000000000) || (L_6 >= 1000000000)))
              abort ();
          M_6 = __VERIFIER_nondet_int ();
          if (((M_6 <= -1000000000) || (M_6 >= 1000000000)))
              abort ();
          N_6 = __VERIFIER_nondet_int ();
          if (((N_6 <= -1000000000) || (N_6 >= 1000000000)))
              abort ();
          A_6 = INV1_0;
          G_6 = INV1_1;
          H_6 = INV1_2;
          D_6 = INV1_3;
          I_6 = INV1_4;
          J_6 = INV1_5;
          if (!
              ((I_6 == (E_6 + 1)) && (H_6 == (C_6 + -1)) && (M_6 == N_6)
               && (K_6 == L_6) && (J_6 == (F_6 + -1)) && (D_6 >= 1)
               && (G_6 >= 1) && (I_6 >= 1) && (K_6 >= 1)
               && (G_6 == (B_6 + 1))))
              abort ();
          INV1_0 = A_6;
          INV1_1 = B_6;
          INV1_2 = C_6;
          INV1_3 = D_6;
          INV1_4 = E_6;
          INV1_5 = F_6;
          goto INV1_4;

      default:
          abort ();
      }
  INV1_1:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          B_4 = __VERIFIER_nondet_int ();
          if (((B_4 <= -1000000000) || (B_4 >= 1000000000)))
              abort ();
          C_4 = __VERIFIER_nondet_int ();
          if (((C_4 <= -1000000000) || (C_4 >= 1000000000)))
              abort ();
          I_4 = __VERIFIER_nondet_int ();
          if (((I_4 <= -1000000000) || (I_4 >= 1000000000)))
              abort ();
          J_4 = __VERIFIER_nondet_int ();
          if (((J_4 <= -1000000000) || (J_4 >= 1000000000)))
              abort ();
          K_4 = __VERIFIER_nondet_int ();
          if (((K_4 <= -1000000000) || (K_4 >= 1000000000)))
              abort ();
          L_4 = __VERIFIER_nondet_int ();
          if (((L_4 <= -1000000000) || (L_4 >= 1000000000)))
              abort ();
          A_4 = INV1_0;
          G_4 = INV1_1;
          H_4 = INV1_2;
          D_4 = INV1_3;
          E_4 = INV1_4;
          F_4 = INV1_5;
          if (!
              ((K_4 == L_4) && (I_4 == J_4) && (H_4 == (C_4 + -1))
               && (!(E_4 >= 1)) && (G_4 >= 1) && (I_4 >= 1)
               && (G_4 == (B_4 + 1))))
              abort ();
          INV1_0 = A_4;
          INV1_1 = B_4;
          INV1_2 = C_4;
          INV1_3 = D_4;
          INV1_4 = E_4;
          INV1_5 = F_4;
          goto INV1_0;

      case 1:
          B_6 = __VERIFIER_nondet_int ();
          if (((B_6 <= -1000000000) || (B_6 >= 1000000000)))
              abort ();
          C_6 = __VERIFIER_nondet_int ();
          if (((C_6 <= -1000000000) || (C_6 >= 1000000000)))
              abort ();
          E_6 = __VERIFIER_nondet_int ();
          if (((E_6 <= -1000000000) || (E_6 >= 1000000000)))
              abort ();
          F_6 = __VERIFIER_nondet_int ();
          if (((F_6 <= -1000000000) || (F_6 >= 1000000000)))
              abort ();
          K_6 = __VERIFIER_nondet_int ();
          if (((K_6 <= -1000000000) || (K_6 >= 1000000000)))
              abort ();
          L_6 = __VERIFIER_nondet_int ();
          if (((L_6 <= -1000000000) || (L_6 >= 1000000000)))
              abort ();
          M_6 = __VERIFIER_nondet_int ();
          if (((M_6 <= -1000000000) || (M_6 >= 1000000000)))
              abort ();
          N_6 = __VERIFIER_nondet_int ();
          if (((N_6 <= -1000000000) || (N_6 >= 1000000000)))
              abort ();
          A_6 = INV1_0;
          G_6 = INV1_1;
          H_6 = INV1_2;
          D_6 = INV1_3;
          I_6 = INV1_4;
          J_6 = INV1_5;
          if (!
              ((I_6 == (E_6 + 1)) && (H_6 == (C_6 + -1)) && (M_6 == N_6)
               && (K_6 == L_6) && (J_6 == (F_6 + -1)) && (D_6 >= 1)
               && (G_6 >= 1) && (I_6 >= 1) && (K_6 >= 1)
               && (G_6 == (B_6 + 1))))
              abort ();
          INV1_0 = A_6;
          INV1_1 = B_6;
          INV1_2 = C_6;
          INV1_3 = D_6;
          INV1_4 = E_6;
          INV1_5 = F_6;
          goto INV1_4;

      default:
          abort ();
      }
  INV1_2:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          E_7 = __VERIFIER_nondet_int ();
          if (((E_7 <= -1000000000) || (E_7 >= 1000000000)))
              abort ();
          H_7 = __VERIFIER_nondet_int ();
          if (((H_7 <= -1000000000) || (H_7 >= 1000000000)))
              abort ();
          I_7 = __VERIFIER_nondet_int ();
          if (((I_7 <= -1000000000) || (I_7 >= 1000000000)))
              abort ();
          J_7 = __VERIFIER_nondet_int ();
          if (((J_7 <= -1000000000) || (J_7 >= 1000000000)))
              abort ();
          K_7 = __VERIFIER_nondet_int ();
          if (((K_7 <= -1000000000) || (K_7 >= 1000000000)))
              abort ();
          A_7 = INV1_0;
          B_7 = INV1_1;
          C_7 = INV1_2;
          D_7 = INV1_3;
          G_7 = INV1_4;
          F_7 = INV1_5;
          if (!
              ((H_7 == I_7) && (G_7 == (E_7 + 1)) && (!(B_7 >= 1))
               && (!(D_7 >= 1)) && (H_7 >= 1) && (G_7 >= 1) && (J_7 == K_7)))
              abort ();
          INV1_0 = A_7;
          INV1_1 = B_7;
          INV1_2 = C_7;
          INV1_3 = D_7;
          INV1_4 = E_7;
          INV1_5 = F_7;
          goto INV1_1;

      case 1:
          B_6 = __VERIFIER_nondet_int ();
          if (((B_6 <= -1000000000) || (B_6 >= 1000000000)))
              abort ();
          C_6 = __VERIFIER_nondet_int ();
          if (((C_6 <= -1000000000) || (C_6 >= 1000000000)))
              abort ();
          E_6 = __VERIFIER_nondet_int ();
          if (((E_6 <= -1000000000) || (E_6 >= 1000000000)))
              abort ();
          F_6 = __VERIFIER_nondet_int ();
          if (((F_6 <= -1000000000) || (F_6 >= 1000000000)))
              abort ();
          K_6 = __VERIFIER_nondet_int ();
          if (((K_6 <= -1000000000) || (K_6 >= 1000000000)))
              abort ();
          L_6 = __VERIFIER_nondet_int ();
          if (((L_6 <= -1000000000) || (L_6 >= 1000000000)))
              abort ();
          M_6 = __VERIFIER_nondet_int ();
          if (((M_6 <= -1000000000) || (M_6 >= 1000000000)))
              abort ();
          N_6 = __VERIFIER_nondet_int ();
          if (((N_6 <= -1000000000) || (N_6 >= 1000000000)))
              abort ();
          A_6 = INV1_0;
          G_6 = INV1_1;
          H_6 = INV1_2;
          D_6 = INV1_3;
          I_6 = INV1_4;
          J_6 = INV1_5;
          if (!
              ((I_6 == (E_6 + 1)) && (H_6 == (C_6 + -1)) && (M_6 == N_6)
               && (K_6 == L_6) && (J_6 == (F_6 + -1)) && (D_6 >= 1)
               && (G_6 >= 1) && (I_6 >= 1) && (K_6 >= 1)
               && (G_6 == (B_6 + 1))))
              abort ();
          INV1_0 = A_6;
          INV1_1 = B_6;
          INV1_2 = C_6;
          INV1_3 = D_6;
          INV1_4 = E_6;
          INV1_5 = F_6;
          goto INV1_4;

      default:
          abort ();
      }
  INV1_3:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          B_5 = __VERIFIER_nondet_int ();
          if (((B_5 <= -1000000000) || (B_5 >= 1000000000)))
              abort ();
          C_5 = __VERIFIER_nondet_int ();
          if (((C_5 <= -1000000000) || (C_5 >= 1000000000)))
              abort ();
          E_5 = __VERIFIER_nondet_int ();
          if (((E_5 <= -1000000000) || (E_5 >= 1000000000)))
              abort ();
          J_5 = __VERIFIER_nondet_int ();
          if (((J_5 <= -1000000000) || (J_5 >= 1000000000)))
              abort ();
          K_5 = __VERIFIER_nondet_int ();
          if (((K_5 <= -1000000000) || (K_5 >= 1000000000)))
              abort ();
          L_5 = __VERIFIER_nondet_int ();
          if (((L_5 <= -1000000000) || (L_5 >= 1000000000)))
              abort ();
          M_5 = __VERIFIER_nondet_int ();
          if (((M_5 <= -1000000000) || (M_5 >= 1000000000)))
              abort ();
          A_5 = INV1_0;
          G_5 = INV1_1;
          H_5 = INV1_2;
          D_5 = INV1_3;
          I_5 = INV1_4;
          F_5 = INV1_5;
          if (!
              ((G_5 == (B_5 + 1)) && (L_5 == M_5) && (J_5 == K_5)
               && (I_5 == (E_5 + 1)) && (!(D_5 >= 1)) && (G_5 >= 1)
               && (J_5 >= 1) && (I_5 >= 1) && (H_5 == (C_5 + -1))))
              abort ();
          INV1_0 = A_5;
          INV1_1 = B_5;
          INV1_2 = C_5;
          INV1_3 = D_5;
          INV1_4 = E_5;
          INV1_5 = F_5;
          goto INV1_2;

      case 1:
          B_6 = __VERIFIER_nondet_int ();
          if (((B_6 <= -1000000000) || (B_6 >= 1000000000)))
              abort ();
          C_6 = __VERIFIER_nondet_int ();
          if (((C_6 <= -1000000000) || (C_6 >= 1000000000)))
              abort ();
          E_6 = __VERIFIER_nondet_int ();
          if (((E_6 <= -1000000000) || (E_6 >= 1000000000)))
              abort ();
          F_6 = __VERIFIER_nondet_int ();
          if (((F_6 <= -1000000000) || (F_6 >= 1000000000)))
              abort ();
          K_6 = __VERIFIER_nondet_int ();
          if (((K_6 <= -1000000000) || (K_6 >= 1000000000)))
              abort ();
          L_6 = __VERIFIER_nondet_int ();
          if (((L_6 <= -1000000000) || (L_6 >= 1000000000)))
              abort ();
          M_6 = __VERIFIER_nondet_int ();
          if (((M_6 <= -1000000000) || (M_6 >= 1000000000)))
              abort ();
          N_6 = __VERIFIER_nondet_int ();
          if (((N_6 <= -1000000000) || (N_6 >= 1000000000)))
              abort ();
          A_6 = INV1_0;
          G_6 = INV1_1;
          H_6 = INV1_2;
          D_6 = INV1_3;
          I_6 = INV1_4;
          J_6 = INV1_5;
          if (!
              ((I_6 == (E_6 + 1)) && (H_6 == (C_6 + -1)) && (M_6 == N_6)
               && (K_6 == L_6) && (J_6 == (F_6 + -1)) && (D_6 >= 1)
               && (G_6 >= 1) && (I_6 >= 1) && (K_6 >= 1)
               && (G_6 == (B_6 + 1))))
              abort ();
          INV1_0 = A_6;
          INV1_1 = B_6;
          INV1_2 = C_6;
          INV1_3 = D_6;
          INV1_4 = E_6;
          INV1_5 = F_6;
          goto INV1_4;

      default:
          abort ();
      }
  INV1_4:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          E_8 = __VERIFIER_nondet_int ();
          if (((E_8 <= -1000000000) || (E_8 >= 1000000000)))
              abort ();
          F_8 = __VERIFIER_nondet_int ();
          if (((F_8 <= -1000000000) || (F_8 >= 1000000000)))
              abort ();
          I_8 = __VERIFIER_nondet_int ();
          if (((I_8 <= -1000000000) || (I_8 >= 1000000000)))
              abort ();
          J_8 = __VERIFIER_nondet_int ();
          if (((J_8 <= -1000000000) || (J_8 >= 1000000000)))
              abort ();
          K_8 = __VERIFIER_nondet_int ();
          if (((K_8 <= -1000000000) || (K_8 >= 1000000000)))
              abort ();
          L_8 = __VERIFIER_nondet_int ();
          if (((L_8 <= -1000000000) || (L_8 >= 1000000000)))
              abort ();
          A_8 = INV1_0;
          B_8 = INV1_1;
          C_8 = INV1_2;
          D_8 = INV1_3;
          G_8 = INV1_4;
          H_8 = INV1_5;
          if (!
              ((K_8 == L_8) && (I_8 == J_8) && (H_8 == (F_8 + -1))
               && (!(B_8 >= 1)) && (D_8 >= 1) && (G_8 >= 1) && (I_8 >= 1)
               && (G_8 == (E_8 + 1))))
              abort ();
          INV1_0 = A_8;
          INV1_1 = B_8;
          INV1_2 = C_8;
          INV1_3 = D_8;
          INV1_4 = E_8;
          INV1_5 = F_8;
          goto INV1_3;

      case 1:
          B_6 = __VERIFIER_nondet_int ();
          if (((B_6 <= -1000000000) || (B_6 >= 1000000000)))
              abort ();
          C_6 = __VERIFIER_nondet_int ();
          if (((C_6 <= -1000000000) || (C_6 >= 1000000000)))
              abort ();
          E_6 = __VERIFIER_nondet_int ();
          if (((E_6 <= -1000000000) || (E_6 >= 1000000000)))
              abort ();
          F_6 = __VERIFIER_nondet_int ();
          if (((F_6 <= -1000000000) || (F_6 >= 1000000000)))
              abort ();
          K_6 = __VERIFIER_nondet_int ();
          if (((K_6 <= -1000000000) || (K_6 >= 1000000000)))
              abort ();
          L_6 = __VERIFIER_nondet_int ();
          if (((L_6 <= -1000000000) || (L_6 >= 1000000000)))
              abort ();
          M_6 = __VERIFIER_nondet_int ();
          if (((M_6 <= -1000000000) || (M_6 >= 1000000000)))
              abort ();
          N_6 = __VERIFIER_nondet_int ();
          if (((N_6 <= -1000000000) || (N_6 >= 1000000000)))
              abort ();
          A_6 = INV1_0;
          G_6 = INV1_1;
          H_6 = INV1_2;
          D_6 = INV1_3;
          I_6 = INV1_4;
          J_6 = INV1_5;
          if (!
              ((I_6 == (E_6 + 1)) && (H_6 == (C_6 + -1)) && (M_6 == N_6)
               && (K_6 == L_6) && (J_6 == (F_6 + -1)) && (D_6 >= 1)
               && (G_6 >= 1) && (I_6 >= 1) && (K_6 >= 1)
               && (G_6 == (B_6 + 1))))
              abort ();
          INV1_0 = A_6;
          INV1_1 = B_6;
          INV1_2 = C_6;
          INV1_3 = D_6;
          INV1_4 = E_6;
          INV1_5 = F_6;
          goto INV1_4;

      default:
          abort ();
      }

    // return expression

}

