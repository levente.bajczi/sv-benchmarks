// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: hopv/CE-1CFA05_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "CE-1CFA05_000_range.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int f_1034_unknown_58_0;
    int f_1034_unknown_58_1;
    int f_1034_unknown_58_2;
    int f_1034_unknown_58_3;
    int f_1034_unknown_58_4;
    int f_1034_unknown_58_5;
    int f_1034_unknown_58_6;
    int f_1034_unknown_58_7;
    int f_1034_unknown_58_8;
    int f_1034_unknown_58_9;
    int f_1034_unknown_58_10;
    int f_1034_unknown_58_11;
    int f_1034_unknown_58_12;
    int f_1034_unknown_58_13;
    int f_1034_unknown_58_14;
    int f_1034_unknown_58_15;
    int f_1034_unknown_58_16;
    int f_1034_unknown_58_17;
    int f_1034_unknown_58_18;
    int f_1034_unknown_58_19;
    int f_1034_unknown_58_20;
    int f_1034_unknown_58_21;
    int f_1034_unknown_58_22;
    int fail_unknown_96_0;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    int E_0;
    int F_0;
    int G_0;
    int H_0;
    int I_0;
    int J_0;
    int K_0;
    int L_0;
    int M_0;
    int N_0;
    int O_0;
    int P_0;
    int Q_0;
    int R_0;
    int S_0;
    int T_0;
    int U_0;
    int V_0;
    int W_0;
    int X_0;
    int A_1;
    int B_1;
    int C_1;
    int D_1;
    int E_1;
    int F_1;
    int G_1;
    int H_1;
    int I_1;
    int J_1;
    int K_1;
    int L_1;
    int M_1;
    int N_1;
    int O_1;
    int P_1;
    int Q_1;
    int R_1;
    int S_1;
    int T_1;
    int U_1;
    int V_1;
    int W_1;
    int X_1;
    int Y_1;
    int Z_1;
    int A1_1;
    int B1_1;
    int C1_1;
    int D1_1;
    int E1_1;
    int F1_1;
    int G1_1;
    int H1_1;
    int I1_1;
    int J1_1;
    int K1_1;
    int L1_1;
    int M1_1;
    int N1_1;
    int O1_1;
    int P1_1;
    int Q1_1;
    int R1_1;
    int S1_1;
    int T1_1;
    int U1_1;
    int V1_1;
    int W1_1;
    int X1_1;
    int Y1_1;
    int Z1_1;
    int A2_1;
    int B2_1;
    int C2_1;
    int D2_1;
    int E2_1;
    int F2_1;
    int G2_1;
    int H2_1;
    int I2_1;
    int J2_1;
    int K2_1;
    int L2_1;
    int M2_1;
    int N2_1;
    int O2_1;
    int P2_1;
    int Q2_1;
    int R2_1;
    int S2_1;
    int T2_1;
    int U2_1;
    int V2_1;
    int W2_1;
    int X2_1;
    int Y2_1;
    int A_2;

    if (((f_1034_unknown_58_0 <= -1000000000)
         || (f_1034_unknown_58_0 >= 1000000000))
        || ((f_1034_unknown_58_1 <= -1000000000)
            || (f_1034_unknown_58_1 >= 1000000000))
        || ((f_1034_unknown_58_2 <= -1000000000)
            || (f_1034_unknown_58_2 >= 1000000000))
        || ((f_1034_unknown_58_3 <= -1000000000)
            || (f_1034_unknown_58_3 >= 1000000000))
        || ((f_1034_unknown_58_4 <= -1000000000)
            || (f_1034_unknown_58_4 >= 1000000000))
        || ((f_1034_unknown_58_5 <= -1000000000)
            || (f_1034_unknown_58_5 >= 1000000000))
        || ((f_1034_unknown_58_6 <= -1000000000)
            || (f_1034_unknown_58_6 >= 1000000000))
        || ((f_1034_unknown_58_7 <= -1000000000)
            || (f_1034_unknown_58_7 >= 1000000000))
        || ((f_1034_unknown_58_8 <= -1000000000)
            || (f_1034_unknown_58_8 >= 1000000000))
        || ((f_1034_unknown_58_9 <= -1000000000)
            || (f_1034_unknown_58_9 >= 1000000000))
        || ((f_1034_unknown_58_10 <= -1000000000)
            || (f_1034_unknown_58_10 >= 1000000000))
        || ((f_1034_unknown_58_11 <= -1000000000)
            || (f_1034_unknown_58_11 >= 1000000000))
        || ((f_1034_unknown_58_12 <= -1000000000)
            || (f_1034_unknown_58_12 >= 1000000000))
        || ((f_1034_unknown_58_13 <= -1000000000)
            || (f_1034_unknown_58_13 >= 1000000000))
        || ((f_1034_unknown_58_14 <= -1000000000)
            || (f_1034_unknown_58_14 >= 1000000000))
        || ((f_1034_unknown_58_15 <= -1000000000)
            || (f_1034_unknown_58_15 >= 1000000000))
        || ((f_1034_unknown_58_16 <= -1000000000)
            || (f_1034_unknown_58_16 >= 1000000000))
        || ((f_1034_unknown_58_17 <= -1000000000)
            || (f_1034_unknown_58_17 >= 1000000000))
        || ((f_1034_unknown_58_18 <= -1000000000)
            || (f_1034_unknown_58_18 >= 1000000000))
        || ((f_1034_unknown_58_19 <= -1000000000)
            || (f_1034_unknown_58_19 >= 1000000000))
        || ((f_1034_unknown_58_20 <= -1000000000)
            || (f_1034_unknown_58_20 >= 1000000000))
        || ((f_1034_unknown_58_21 <= -1000000000)
            || (f_1034_unknown_58_21 >= 1000000000))
        || ((f_1034_unknown_58_22 <= -1000000000)
            || (f_1034_unknown_58_22 >= 1000000000))
        || ((fail_unknown_96_0 <= -1000000000)
            || (fail_unknown_96_0 >= 1000000000)) || ((A_0 <= -1000000000)
                                                      || (A_0 >= 1000000000))
        || ((B_0 <= -1000000000) || (B_0 >= 1000000000))
        || ((C_0 <= -1000000000) || (C_0 >= 1000000000))
        || ((D_0 <= -1000000000) || (D_0 >= 1000000000))
        || ((E_0 <= -1000000000) || (E_0 >= 1000000000))
        || ((F_0 <= -1000000000) || (F_0 >= 1000000000))
        || ((G_0 <= -1000000000) || (G_0 >= 1000000000))
        || ((H_0 <= -1000000000) || (H_0 >= 1000000000))
        || ((I_0 <= -1000000000) || (I_0 >= 1000000000))
        || ((J_0 <= -1000000000) || (J_0 >= 1000000000))
        || ((K_0 <= -1000000000) || (K_0 >= 1000000000))
        || ((L_0 <= -1000000000) || (L_0 >= 1000000000))
        || ((M_0 <= -1000000000) || (M_0 >= 1000000000))
        || ((N_0 <= -1000000000) || (N_0 >= 1000000000))
        || ((O_0 <= -1000000000) || (O_0 >= 1000000000))
        || ((P_0 <= -1000000000) || (P_0 >= 1000000000))
        || ((Q_0 <= -1000000000) || (Q_0 >= 1000000000))
        || ((R_0 <= -1000000000) || (R_0 >= 1000000000))
        || ((S_0 <= -1000000000) || (S_0 >= 1000000000))
        || ((T_0 <= -1000000000) || (T_0 >= 1000000000))
        || ((U_0 <= -1000000000) || (U_0 >= 1000000000))
        || ((V_0 <= -1000000000) || (V_0 >= 1000000000))
        || ((W_0 <= -1000000000) || (W_0 >= 1000000000))
        || ((X_0 <= -1000000000) || (X_0 >= 1000000000))
        || ((A_1 <= -1000000000) || (A_1 >= 1000000000))
        || ((B_1 <= -1000000000) || (B_1 >= 1000000000))
        || ((C_1 <= -1000000000) || (C_1 >= 1000000000))
        || ((D_1 <= -1000000000) || (D_1 >= 1000000000))
        || ((E_1 <= -1000000000) || (E_1 >= 1000000000))
        || ((F_1 <= -1000000000) || (F_1 >= 1000000000))
        || ((G_1 <= -1000000000) || (G_1 >= 1000000000))
        || ((H_1 <= -1000000000) || (H_1 >= 1000000000))
        || ((I_1 <= -1000000000) || (I_1 >= 1000000000))
        || ((J_1 <= -1000000000) || (J_1 >= 1000000000))
        || ((K_1 <= -1000000000) || (K_1 >= 1000000000))
        || ((L_1 <= -1000000000) || (L_1 >= 1000000000))
        || ((M_1 <= -1000000000) || (M_1 >= 1000000000))
        || ((N_1 <= -1000000000) || (N_1 >= 1000000000))
        || ((O_1 <= -1000000000) || (O_1 >= 1000000000))
        || ((P_1 <= -1000000000) || (P_1 >= 1000000000))
        || ((Q_1 <= -1000000000) || (Q_1 >= 1000000000))
        || ((R_1 <= -1000000000) || (R_1 >= 1000000000))
        || ((S_1 <= -1000000000) || (S_1 >= 1000000000))
        || ((T_1 <= -1000000000) || (T_1 >= 1000000000))
        || ((U_1 <= -1000000000) || (U_1 >= 1000000000))
        || ((V_1 <= -1000000000) || (V_1 >= 1000000000))
        || ((W_1 <= -1000000000) || (W_1 >= 1000000000))
        || ((X_1 <= -1000000000) || (X_1 >= 1000000000))
        || ((Y_1 <= -1000000000) || (Y_1 >= 1000000000))
        || ((Z_1 <= -1000000000) || (Z_1 >= 1000000000))
        || ((A1_1 <= -1000000000) || (A1_1 >= 1000000000))
        || ((B1_1 <= -1000000000) || (B1_1 >= 1000000000))
        || ((C1_1 <= -1000000000) || (C1_1 >= 1000000000))
        || ((D1_1 <= -1000000000) || (D1_1 >= 1000000000))
        || ((E1_1 <= -1000000000) || (E1_1 >= 1000000000))
        || ((F1_1 <= -1000000000) || (F1_1 >= 1000000000))
        || ((G1_1 <= -1000000000) || (G1_1 >= 1000000000))
        || ((H1_1 <= -1000000000) || (H1_1 >= 1000000000))
        || ((I1_1 <= -1000000000) || (I1_1 >= 1000000000))
        || ((J1_1 <= -1000000000) || (J1_1 >= 1000000000))
        || ((K1_1 <= -1000000000) || (K1_1 >= 1000000000))
        || ((L1_1 <= -1000000000) || (L1_1 >= 1000000000))
        || ((M1_1 <= -1000000000) || (M1_1 >= 1000000000))
        || ((N1_1 <= -1000000000) || (N1_1 >= 1000000000))
        || ((O1_1 <= -1000000000) || (O1_1 >= 1000000000))
        || ((P1_1 <= -1000000000) || (P1_1 >= 1000000000))
        || ((Q1_1 <= -1000000000) || (Q1_1 >= 1000000000))
        || ((R1_1 <= -1000000000) || (R1_1 >= 1000000000))
        || ((S1_1 <= -1000000000) || (S1_1 >= 1000000000))
        || ((T1_1 <= -1000000000) || (T1_1 >= 1000000000))
        || ((U1_1 <= -1000000000) || (U1_1 >= 1000000000))
        || ((V1_1 <= -1000000000) || (V1_1 >= 1000000000))
        || ((W1_1 <= -1000000000) || (W1_1 >= 1000000000))
        || ((X1_1 <= -1000000000) || (X1_1 >= 1000000000))
        || ((Y1_1 <= -1000000000) || (Y1_1 >= 1000000000))
        || ((Z1_1 <= -1000000000) || (Z1_1 >= 1000000000))
        || ((A2_1 <= -1000000000) || (A2_1 >= 1000000000))
        || ((B2_1 <= -1000000000) || (B2_1 >= 1000000000))
        || ((C2_1 <= -1000000000) || (C2_1 >= 1000000000))
        || ((D2_1 <= -1000000000) || (D2_1 >= 1000000000))
        || ((E2_1 <= -1000000000) || (E2_1 >= 1000000000))
        || ((F2_1 <= -1000000000) || (F2_1 >= 1000000000))
        || ((G2_1 <= -1000000000) || (G2_1 >= 1000000000))
        || ((H2_1 <= -1000000000) || (H2_1 >= 1000000000))
        || ((I2_1 <= -1000000000) || (I2_1 >= 1000000000))
        || ((J2_1 <= -1000000000) || (J2_1 >= 1000000000))
        || ((K2_1 <= -1000000000) || (K2_1 >= 1000000000))
        || ((L2_1 <= -1000000000) || (L2_1 >= 1000000000))
        || ((M2_1 <= -1000000000) || (M2_1 >= 1000000000))
        || ((N2_1 <= -1000000000) || (N2_1 >= 1000000000))
        || ((O2_1 <= -1000000000) || (O2_1 >= 1000000000))
        || ((P2_1 <= -1000000000) || (P2_1 >= 1000000000))
        || ((Q2_1 <= -1000000000) || (Q2_1 >= 1000000000))
        || ((R2_1 <= -1000000000) || (R2_1 >= 1000000000))
        || ((S2_1 <= -1000000000) || (S2_1 >= 1000000000))
        || ((T2_1 <= -1000000000) || (T2_1 >= 1000000000))
        || ((U2_1 <= -1000000000) || (U2_1 >= 1000000000))
        || ((V2_1 <= -1000000000) || (V2_1 >= 1000000000))
        || ((W2_1 <= -1000000000) || (W2_1 >= 1000000000))
        || ((X2_1 <= -1000000000) || (X2_1 >= 1000000000))
        || ((Y2_1 <= -1000000000) || (Y2_1 >= 1000000000))
        || ((A_2 <= -1000000000) || (A_2 >= 1000000000)))
        abort ();

    // main logic
    goto main_init;

  main_init:
    if (!
        ((A_1 == 0) && (H_1 == 0) && (G_1 == 0) && (F_1 == 0) && (E_1 == 0)
         && (D_1 == 0) && (C_1 == 0) && (X_1 == 0) && (W_1 == 0) && (V_1 == 0)
         && (U_1 == 0) && (T_1 == 0) && (S_1 == 0) && (R_1 == 0) && (Q_1 == 0)
         && (P_1 == 0) && (O_1 == 0) && (N_1 == 0) && (M_1 == 0) && (L_1 == 0)
         && (K_1 == 0) && (J_1 == 0) && (I_1 == 0) && (I2_1 == 0)
         && (H2_1 == 0) && (G2_1 == 0) && (F2_1 == 0) && (E2_1 == 0)
         && (D2_1 == 0) && (C2_1 == 0) && (B2_1 == 0) && (A2_1 == 0)
         && (Z1_1 == 0) && (Y1_1 == 0) && (X1_1 == 0) && (W1_1 == 0)
         && (V1_1 == 0) && (U1_1 == 0) && (T1_1 == 0) && (S1_1 == 0)
         && (R1_1 == 0) && (Q1_1 == 0) && (P1_1 == 0) && (O1_1 == 0)
         && (N1_1 == 0) && (M1_1 == 0) && (L1_1 == 0) && (K1_1 == 0)
         && (J1_1 == 0) && (I1_1 == 0) && (H1_1 == 0) && (G1_1 == 0)
         && (F1_1 == 0) && (E1_1 == 0) && (D1_1 == 0) && (C1_1 == 0)
         && (B1_1 == 0) && (A1_1 == 0) && (Z_1 == 0) && (Y_1 == 0)
         && (Y2_1 == 0) && (X2_1 == 0) && (W2_1 == 0) && (V2_1 == 0)
         && (U2_1 == 0) && (T2_1 == 0) && (S2_1 == 1) && (R2_1 == 0)
         && (Q2_1 == 0) && (P2_1 == 0) && (O2_1 == 0) && (N2_1 == 0)
         && (M2_1 == 0) && (L2_1 == 0) && (K2_1 == 0) && (J2_1 == 0)
         && (B_1 == 0)))
        abort ();
    f_1034_unknown_58_0 = S2_1;
    f_1034_unknown_58_1 = R2_1;
    f_1034_unknown_58_2 = Q2_1;
    f_1034_unknown_58_3 = P2_1;
    f_1034_unknown_58_4 = O2_1;
    f_1034_unknown_58_5 = N2_1;
    f_1034_unknown_58_6 = M2_1;
    f_1034_unknown_58_7 = L2_1;
    f_1034_unknown_58_8 = K2_1;
    f_1034_unknown_58_9 = A_1;
    f_1034_unknown_58_10 = J2_1;
    f_1034_unknown_58_11 = I2_1;
    f_1034_unknown_58_12 = H2_1;
    f_1034_unknown_58_13 = G2_1;
    f_1034_unknown_58_14 = F2_1;
    f_1034_unknown_58_15 = E2_1;
    f_1034_unknown_58_16 = D2_1;
    f_1034_unknown_58_17 = C2_1;
    f_1034_unknown_58_18 = C_1;
    f_1034_unknown_58_19 = B2_1;
    f_1034_unknown_58_20 = A2_1;
    f_1034_unknown_58_21 = Z1_1;
    f_1034_unknown_58_22 = Y1_1;
    X_0 = __VERIFIER_nondet_int ();
    if (((X_0 <= -1000000000) || (X_0 >= 1000000000)))
        abort ();
    W_0 = f_1034_unknown_58_0;
    V_0 = f_1034_unknown_58_1;
    U_0 = f_1034_unknown_58_2;
    T_0 = f_1034_unknown_58_3;
    S_0 = f_1034_unknown_58_4;
    R_0 = f_1034_unknown_58_5;
    Q_0 = f_1034_unknown_58_6;
    P_0 = f_1034_unknown_58_7;
    O_0 = f_1034_unknown_58_8;
    N_0 = f_1034_unknown_58_9;
    M_0 = f_1034_unknown_58_10;
    L_0 = f_1034_unknown_58_11;
    K_0 = f_1034_unknown_58_12;
    J_0 = f_1034_unknown_58_13;
    I_0 = f_1034_unknown_58_14;
    H_0 = f_1034_unknown_58_15;
    G_0 = f_1034_unknown_58_16;
    F_0 = f_1034_unknown_58_17;
    E_0 = f_1034_unknown_58_18;
    D_0 = f_1034_unknown_58_19;
    C_0 = f_1034_unknown_58_20;
    B_0 = f_1034_unknown_58_21;
    A_0 = f_1034_unknown_58_22;
    if (!((X_0 == 1) && (!(0 == S_0))))
        abort ();
    fail_unknown_96_0 = X_0;
    A_2 = fail_unknown_96_0;
    if (!1)
        abort ();
    goto main_error;
  main_error:
    reach_error ();
  main_final:
    goto main_final;

    // return expression

}

