// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: vmt-chc-benchmarks/vmt-car_all_e3_1068_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "vmt-car_all_e3_1068_000_range.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    _Bool state_0;
    _Bool state_1;
    _Bool state_2;
    _Bool state_3;
    _Bool state_4;
    _Bool state_5;
    _Bool state_6;
    _Bool state_7;
    _Bool state_8;
    _Bool state_9;
    _Bool state_10;
    int state_11;
    int state_12;
    int state_13;
    int state_14;
    int state_15;
    int state_16;
    int state_17;
    int state_18;
    _Bool state_19;
    _Bool state_20;
    _Bool state_21;
    _Bool state_22;
    _Bool state_23;
    _Bool state_24;
    _Bool state_25;
    int state_26;
    int state_27;
    int state_28;
    _Bool state_29;
    _Bool state_30;
    _Bool state_31;
    _Bool state_32;
    int state_33;
    int state_34;
    _Bool A_0;
    int B_0;
    _Bool C_0;
    _Bool D_0;
    _Bool E_0;
    _Bool F_0;
    _Bool G_0;
    _Bool H_0;
    int I_0;
    int J_0;
    int K_0;
    int L_0;
    int M_0;
    int N_0;
    _Bool O_0;
    _Bool P_0;
    _Bool Q_0;
    _Bool R_0;
    _Bool S_0;
    _Bool T_0;
    _Bool U_0;
    _Bool V_0;
    int W_0;
    int X_0;
    _Bool Y_0;
    _Bool Z_0;
    _Bool A1_0;
    _Bool B1_0;
    _Bool C1_0;
    int D1_0;
    int E1_0;
    int F1_0;
    int G1_0;
    _Bool H1_0;
    _Bool I1_0;
    _Bool A_1;
    int B_1;
    _Bool C_1;
    _Bool D_1;
    _Bool E_1;
    _Bool F_1;
    _Bool G_1;
    _Bool H_1;
    int I_1;
    int J_1;
    int K_1;
    int L_1;
    int M_1;
    int N_1;
    _Bool O_1;
    _Bool P_1;
    _Bool Q_1;
    _Bool R_1;
    _Bool S_1;
    _Bool T_1;
    _Bool U_1;
    _Bool V_1;
    int W_1;
    int X_1;
    _Bool Y_1;
    _Bool Z_1;
    int A1_1;
    _Bool B1_1;
    _Bool C1_1;
    int D1_1;
    int E1_1;
    _Bool F1_1;
    _Bool G1_1;
    _Bool H1_1;
    _Bool I1_1;
    _Bool J1_1;
    _Bool K1_1;
    int L1_1;
    int M1_1;
    int N1_1;
    int O1_1;
    int P1_1;
    _Bool Q1_1;
    _Bool R1_1;
    _Bool S1_1;
    _Bool T1_1;
    int U1_1;
    _Bool V1_1;
    _Bool W1_1;
    _Bool X1_1;
    _Bool Y1_1;
    _Bool Z1_1;
    _Bool A2_1;
    int B2_1;
    int C2_1;
    int D2_1;
    int E2_1;
    _Bool F2_1;
    _Bool G2_1;
    _Bool H2_1;
    _Bool I2_1;
    _Bool J2_1;
    _Bool K2_1;
    _Bool L2_1;
    int M2_1;
    int N2_1;
    int O2_1;
    int P2_1;
    _Bool Q2_1;
    _Bool R2_1;
    _Bool A_2;
    int B_2;
    _Bool C_2;
    _Bool D_2;
    _Bool E_2;
    _Bool F_2;
    _Bool G_2;
    _Bool H_2;
    int I_2;
    int J_2;
    int K_2;
    int L_2;
    int M_2;
    int N_2;
    _Bool O_2;
    _Bool P_2;
    _Bool Q_2;
    _Bool R_2;
    _Bool S_2;
    _Bool T_2;
    _Bool U_2;
    _Bool V_2;
    int W_2;
    int X_2;
    _Bool Y_2;
    _Bool Z_2;
    _Bool A1_2;
    _Bool B1_2;
    _Bool C1_2;
    int D1_2;
    int E1_2;
    int F1_2;
    int G1_2;
    _Bool H1_2;
    _Bool I1_2;

    if (((state_11 <= -1000000000) || (state_11 >= 1000000000))
        || ((state_12 <= -1000000000) || (state_12 >= 1000000000))
        || ((state_13 <= -1000000000) || (state_13 >= 1000000000))
        || ((state_14 <= -1000000000) || (state_14 >= 1000000000))
        || ((state_15 <= -1000000000) || (state_15 >= 1000000000))
        || ((state_16 <= -1000000000) || (state_16 >= 1000000000))
        || ((state_17 <= -1000000000) || (state_17 >= 1000000000))
        || ((state_18 <= -1000000000) || (state_18 >= 1000000000))
        || ((state_26 <= -1000000000) || (state_26 >= 1000000000))
        || ((state_27 <= -1000000000) || (state_27 >= 1000000000))
        || ((state_28 <= -1000000000) || (state_28 >= 1000000000))
        || ((state_33 <= -1000000000) || (state_33 >= 1000000000))
        || ((state_34 <= -1000000000) || (state_34 >= 1000000000))
        || ((B_0 <= -1000000000) || (B_0 >= 1000000000))
        || ((I_0 <= -1000000000) || (I_0 >= 1000000000))
        || ((J_0 <= -1000000000) || (J_0 >= 1000000000))
        || ((K_0 <= -1000000000) || (K_0 >= 1000000000))
        || ((L_0 <= -1000000000) || (L_0 >= 1000000000))
        || ((M_0 <= -1000000000) || (M_0 >= 1000000000))
        || ((N_0 <= -1000000000) || (N_0 >= 1000000000))
        || ((W_0 <= -1000000000) || (W_0 >= 1000000000))
        || ((X_0 <= -1000000000) || (X_0 >= 1000000000))
        || ((D1_0 <= -1000000000) || (D1_0 >= 1000000000))
        || ((E1_0 <= -1000000000) || (E1_0 >= 1000000000))
        || ((F1_0 <= -1000000000) || (F1_0 >= 1000000000))
        || ((G1_0 <= -1000000000) || (G1_0 >= 1000000000))
        || ((B_1 <= -1000000000) || (B_1 >= 1000000000))
        || ((I_1 <= -1000000000) || (I_1 >= 1000000000))
        || ((J_1 <= -1000000000) || (J_1 >= 1000000000))
        || ((K_1 <= -1000000000) || (K_1 >= 1000000000))
        || ((L_1 <= -1000000000) || (L_1 >= 1000000000))
        || ((M_1 <= -1000000000) || (M_1 >= 1000000000))
        || ((N_1 <= -1000000000) || (N_1 >= 1000000000))
        || ((W_1 <= -1000000000) || (W_1 >= 1000000000))
        || ((X_1 <= -1000000000) || (X_1 >= 1000000000))
        || ((A1_1 <= -1000000000) || (A1_1 >= 1000000000))
        || ((D1_1 <= -1000000000) || (D1_1 >= 1000000000))
        || ((E1_1 <= -1000000000) || (E1_1 >= 1000000000))
        || ((L1_1 <= -1000000000) || (L1_1 >= 1000000000))
        || ((M1_1 <= -1000000000) || (M1_1 >= 1000000000))
        || ((N1_1 <= -1000000000) || (N1_1 >= 1000000000))
        || ((O1_1 <= -1000000000) || (O1_1 >= 1000000000))
        || ((P1_1 <= -1000000000) || (P1_1 >= 1000000000))
        || ((U1_1 <= -1000000000) || (U1_1 >= 1000000000))
        || ((B2_1 <= -1000000000) || (B2_1 >= 1000000000))
        || ((C2_1 <= -1000000000) || (C2_1 >= 1000000000))
        || ((D2_1 <= -1000000000) || (D2_1 >= 1000000000))
        || ((E2_1 <= -1000000000) || (E2_1 >= 1000000000))
        || ((M2_1 <= -1000000000) || (M2_1 >= 1000000000))
        || ((N2_1 <= -1000000000) || (N2_1 >= 1000000000))
        || ((O2_1 <= -1000000000) || (O2_1 >= 1000000000))
        || ((P2_1 <= -1000000000) || (P2_1 >= 1000000000))
        || ((B_2 <= -1000000000) || (B_2 >= 1000000000))
        || ((I_2 <= -1000000000) || (I_2 >= 1000000000))
        || ((J_2 <= -1000000000) || (J_2 >= 1000000000))
        || ((K_2 <= -1000000000) || (K_2 >= 1000000000))
        || ((L_2 <= -1000000000) || (L_2 >= 1000000000))
        || ((M_2 <= -1000000000) || (M_2 >= 1000000000))
        || ((N_2 <= -1000000000) || (N_2 >= 1000000000))
        || ((W_2 <= -1000000000) || (W_2 >= 1000000000))
        || ((X_2 <= -1000000000) || (X_2 >= 1000000000))
        || ((D1_2 <= -1000000000) || (D1_2 >= 1000000000))
        || ((E1_2 <= -1000000000) || (E1_2 >= 1000000000))
        || ((F1_2 <= -1000000000) || (F1_2 >= 1000000000))
        || ((G1_2 <= -1000000000) || (G1_2 >= 1000000000)))
        abort ();

    // main logic
    goto main_init;

  main_init:
    if (!
        (((3 <= M_0) == D_0) && ((4 <= K_0) == F_0)
         &&
         (((!A_0)
           || ((0 <= G1_0) && (0 <= B_0) && (!(11 <= G1_0))
               && (!(4 <= B_0)))) == O_0) && (!((Z_0 && Y_0) == H1_0))
         && (V_0 == A_0) && (U_0 == T_0) && (S_0 == R_0) && (F_0 == E_0)
         && (D_0 == C_0) && (I1_0 == (H1_0 && (!(32767 <= G1_0))))
         && (I1_0 == V_0) && (C1_0 == Q_0) && (B1_0 == S_0) && (A1_0 == U_0)
         && (H_0 == G_0) && (Q_0 == P_0) && (I_0 == G1_0) && (M_0 == B_0)
         && (K_0 == J_0) && (F1_0 == 0) && (F1_0 == K_0) && (E1_0 == 0)
         && (E1_0 == M_0) && (D1_0 == 0) && (D1_0 == I_0) && ((X_0 == W_0)
                                                              || (!Q_0)
                                                              || S_0)
         && ((X_0 == 0) || (Q_0 && (!S_0))) && C1_0 && (!B1_0) && (!A1_0)
         && ((I_0 == 10) == H_0)))
        abort ();
    state_0 = I1_0;
    state_1 = V_0;
    state_2 = Z_0;
    state_3 = Y_0;
    state_4 = H1_0;
    state_5 = A1_0;
    state_6 = U_0;
    state_7 = B1_0;
    state_8 = S_0;
    state_9 = C1_0;
    state_10 = Q_0;
    state_11 = X_0;
    state_12 = W_0;
    state_13 = F1_0;
    state_14 = K_0;
    state_15 = E1_0;
    state_16 = M_0;
    state_17 = D1_0;
    state_18 = I_0;
    state_19 = H_0;
    state_20 = F_0;
    state_21 = D_0;
    state_22 = A_0;
    state_23 = T_0;
    state_24 = R_0;
    state_25 = P_0;
    state_26 = J_0;
    state_27 = B_0;
    state_28 = G1_0;
    state_29 = G_0;
    state_30 = E_0;
    state_31 = C_0;
    state_32 = O_0;
    state_33 = L_0;
    state_34 = N_0;
    Q1_1 = __VERIFIER_nondet__Bool ();
    M1_1 = __VERIFIER_nondet_int ();
    if (((M1_1 <= -1000000000) || (M1_1 >= 1000000000)))
        abort ();
    I1_1 = __VERIFIER_nondet__Bool ();
    I2_1 = __VERIFIER_nondet__Bool ();
    E1_1 = __VERIFIER_nondet_int ();
    if (((E1_1 <= -1000000000) || (E1_1 >= 1000000000)))
        abort ();
    E2_1 = __VERIFIER_nondet_int ();
    if (((E2_1 <= -1000000000) || (E2_1 >= 1000000000)))
        abort ();
    A1_1 = __VERIFIER_nondet_int ();
    if (((A1_1 <= -1000000000) || (A1_1 >= 1000000000)))
        abort ();
    A2_1 = __VERIFIER_nondet__Bool ();
    Z1_1 = __VERIFIER_nondet__Bool ();
    V1_1 = __VERIFIER_nondet__Bool ();
    R1_1 = __VERIFIER_nondet__Bool ();
    N1_1 = __VERIFIER_nondet_int ();
    if (((N1_1 <= -1000000000) || (N1_1 >= 1000000000)))
        abort ();
    J1_1 = __VERIFIER_nondet__Bool ();
    F1_1 = __VERIFIER_nondet__Bool ();
    F2_1 = __VERIFIER_nondet__Bool ();
    B1_1 = __VERIFIER_nondet__Bool ();
    B2_1 = __VERIFIER_nondet_int ();
    if (((B2_1 <= -1000000000) || (B2_1 >= 1000000000)))
        abort ();
    W1_1 = __VERIFIER_nondet__Bool ();
    S1_1 = __VERIFIER_nondet__Bool ();
    O1_1 = __VERIFIER_nondet_int ();
    if (((O1_1 <= -1000000000) || (O1_1 >= 1000000000)))
        abort ();
    K1_1 = __VERIFIER_nondet__Bool ();
    G1_1 = __VERIFIER_nondet__Bool ();
    G2_1 = __VERIFIER_nondet__Bool ();
    C1_1 = __VERIFIER_nondet__Bool ();
    C2_1 = __VERIFIER_nondet_int ();
    if (((C2_1 <= -1000000000) || (C2_1 >= 1000000000)))
        abort ();
    X1_1 = __VERIFIER_nondet__Bool ();
    T1_1 = __VERIFIER_nondet__Bool ();
    P1_1 = __VERIFIER_nondet_int ();
    if (((P1_1 <= -1000000000) || (P1_1 >= 1000000000)))
        abort ();
    L1_1 = __VERIFIER_nondet_int ();
    if (((L1_1 <= -1000000000) || (L1_1 >= 1000000000)))
        abort ();
    H1_1 = __VERIFIER_nondet__Bool ();
    H2_1 = __VERIFIER_nondet__Bool ();
    D1_1 = __VERIFIER_nondet_int ();
    if (((D1_1 <= -1000000000) || (D1_1 >= 1000000000)))
        abort ();
    D2_1 = __VERIFIER_nondet_int ();
    if (((D2_1 <= -1000000000) || (D2_1 >= 1000000000)))
        abort ();
    Y1_1 = __VERIFIER_nondet__Bool ();
    U1_1 = __VERIFIER_nondet_int ();
    if (((U1_1 <= -1000000000) || (U1_1 >= 1000000000)))
        abort ();
    R2_1 = state_0;
    V_1 = state_1;
    Z_1 = state_2;
    Y_1 = state_3;
    Q2_1 = state_4;
    J2_1 = state_5;
    U_1 = state_6;
    K2_1 = state_7;
    S_1 = state_8;
    L2_1 = state_9;
    Q_1 = state_10;
    X_1 = state_11;
    W_1 = state_12;
    O2_1 = state_13;
    K_1 = state_14;
    N2_1 = state_15;
    M_1 = state_16;
    M2_1 = state_17;
    I_1 = state_18;
    H_1 = state_19;
    F_1 = state_20;
    D_1 = state_21;
    A_1 = state_22;
    T_1 = state_23;
    R_1 = state_24;
    P_1 = state_25;
    J_1 = state_26;
    B_1 = state_27;
    P2_1 = state_28;
    G_1 = state_29;
    E_1 = state_30;
    C_1 = state_31;
    O_1 = state_32;
    L_1 = state_33;
    N_1 = state_34;
    if (!
        (((I_1 == 10) == H_1) && ((3 <= C2_1) == W1_1) && ((3 <= M_1) == D_1)
         && ((4 <= E2_1) == Y1_1) && ((4 <= K_1) == F_1)
         &&
         (((!T1_1)
           || ((0 <= U1_1) && (0 <= P1_1) && (!(11 <= P1_1))
               && (!(4 <= U1_1)))) == S1_1) && (((!A_1) || ((0 <= B_1)
                                                            && (0 <= P2_1)
                                                            && (!(11 <= P2_1))
                                                            && (!(4 <= B_1))))
                                                == O_1)
         && (!((H1_1 && G1_1) == Q1_1)) && (!((Y_1 && Z_1) == Q2_1))
         && (R2_1 == V_1) && (L2_1 == Q_1) && (K2_1 == S_1) && (J2_1 == U_1)
         && (B1_1 == K1_1) && (B1_1 == F2_1) && (C1_1 == I1_1)
         && (C1_1 == H2_1) && (F1_1 == J1_1) && (F1_1 == G2_1)
         && (G1_1 == J1_1) && (I1_1 == (H1_1 && (!G1_1)))
         && (K1_1 == ((!D_1) && (!F_1) && (!H_1) && Q_1))
         && (R1_1 == (V_1 && Q1_1 && (!(32767 <= P1_1)))) && (W1_1 == V1_1)
         && (Y1_1 == X1_1) && (A2_1 == Z1_1) && (I2_1 == R1_1)
         && (I2_1 == T1_1) && (V_1 == A_1) && (U_1 == T_1) && (S_1 == R_1)
         && (Q_1 == P_1) && (H_1 == G_1) && (F_1 == E_1) && (D_1 == C_1)
         && (O2_1 == K_1) && (N2_1 == M_1) && (M2_1 == I_1) && (L1_1 == D1_1)
         && (N1_1 == M1_1) && (O1_1 == E1_1) && (B2_1 == L1_1)
         && (B2_1 == P1_1) && (C2_1 == M1_1) && (C2_1 == U1_1)
         && (E2_1 == O1_1) && (E2_1 == D2_1) && (M_1 == B_1) && (K_1 == J_1)
         && (I_1 == P2_1) && ((!C1_1) || (!B1_1)
                              || ((M_1 + (-1 * A1_1)) == 1)) && ((!C1_1)
                                                                 || (!B1_1)
                                                                 ||
                                                                 ((I_1 +
                                                                   (-1 *
                                                                    D1_1)) ==
                                                                  -1))
         && (F1_1 || (N1_1 == A1_1) || (!B1_1)) && (S_1 || (X_1 == W_1)
                                                    || (!Q_1))
         && ((M_1 == A1_1) || (C1_1 && B1_1)) && ((C1_1 && B1_1)
                                                  || (I_1 == D1_1))
         && ((N1_1 == 0) || ((!F1_1) && B1_1)) && ((X_1 == 0)
                                                   || (Q_1 && (!S_1)))
         && ((!F1_1) || ((K_1 + (-1 * E1_1)) == -1)) && (F1_1
                                                         || (K_1 == E1_1))
         && ((B2_1 == 10) == A2_1)))
        abort ();
    state_0 = R1_1;
    state_1 = I2_1;
    state_2 = G1_1;
    state_3 = H1_1;
    state_4 = Q1_1;
    state_5 = I1_1;
    state_6 = C1_1;
    state_7 = J1_1;
    state_8 = F1_1;
    state_9 = K1_1;
    state_10 = B1_1;
    state_11 = N1_1;
    state_12 = A1_1;
    state_13 = O1_1;
    state_14 = E2_1;
    state_15 = M1_1;
    state_16 = C2_1;
    state_17 = L1_1;
    state_18 = B2_1;
    state_19 = A2_1;
    state_20 = Y1_1;
    state_21 = W1_1;
    state_22 = T1_1;
    state_23 = H2_1;
    state_24 = G2_1;
    state_25 = F2_1;
    state_26 = D2_1;
    state_27 = U1_1;
    state_28 = P1_1;
    state_29 = Z1_1;
    state_30 = X1_1;
    state_31 = V1_1;
    state_32 = S1_1;
    state_33 = D1_1;
    state_34 = E1_1;
    goto state_0;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  state_0:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          I1_2 = state_0;
          V_2 = state_1;
          Z_2 = state_2;
          Y_2 = state_3;
          H1_2 = state_4;
          A1_2 = state_5;
          U_2 = state_6;
          B1_2 = state_7;
          S_2 = state_8;
          C1_2 = state_9;
          Q_2 = state_10;
          X_2 = state_11;
          W_2 = state_12;
          F1_2 = state_13;
          K_2 = state_14;
          E1_2 = state_15;
          M_2 = state_16;
          D1_2 = state_17;
          I_2 = state_18;
          H_2 = state_19;
          F_2 = state_20;
          D_2 = state_21;
          A_2 = state_22;
          T_2 = state_23;
          R_2 = state_24;
          P_2 = state_25;
          J_2 = state_26;
          B_2 = state_27;
          G1_2 = state_28;
          G_2 = state_29;
          E_2 = state_30;
          C_2 = state_31;
          O_2 = state_32;
          L_2 = state_33;
          N_2 = state_34;
          if (!(!O_2))
              abort ();
          goto main_error;

      case 1:
          Q1_1 = __VERIFIER_nondet__Bool ();
          M1_1 = __VERIFIER_nondet_int ();
          if (((M1_1 <= -1000000000) || (M1_1 >= 1000000000)))
              abort ();
          I1_1 = __VERIFIER_nondet__Bool ();
          I2_1 = __VERIFIER_nondet__Bool ();
          E1_1 = __VERIFIER_nondet_int ();
          if (((E1_1 <= -1000000000) || (E1_1 >= 1000000000)))
              abort ();
          E2_1 = __VERIFIER_nondet_int ();
          if (((E2_1 <= -1000000000) || (E2_1 >= 1000000000)))
              abort ();
          A1_1 = __VERIFIER_nondet_int ();
          if (((A1_1 <= -1000000000) || (A1_1 >= 1000000000)))
              abort ();
          A2_1 = __VERIFIER_nondet__Bool ();
          Z1_1 = __VERIFIER_nondet__Bool ();
          V1_1 = __VERIFIER_nondet__Bool ();
          R1_1 = __VERIFIER_nondet__Bool ();
          N1_1 = __VERIFIER_nondet_int ();
          if (((N1_1 <= -1000000000) || (N1_1 >= 1000000000)))
              abort ();
          J1_1 = __VERIFIER_nondet__Bool ();
          F1_1 = __VERIFIER_nondet__Bool ();
          F2_1 = __VERIFIER_nondet__Bool ();
          B1_1 = __VERIFIER_nondet__Bool ();
          B2_1 = __VERIFIER_nondet_int ();
          if (((B2_1 <= -1000000000) || (B2_1 >= 1000000000)))
              abort ();
          W1_1 = __VERIFIER_nondet__Bool ();
          S1_1 = __VERIFIER_nondet__Bool ();
          O1_1 = __VERIFIER_nondet_int ();
          if (((O1_1 <= -1000000000) || (O1_1 >= 1000000000)))
              abort ();
          K1_1 = __VERIFIER_nondet__Bool ();
          G1_1 = __VERIFIER_nondet__Bool ();
          G2_1 = __VERIFIER_nondet__Bool ();
          C1_1 = __VERIFIER_nondet__Bool ();
          C2_1 = __VERIFIER_nondet_int ();
          if (((C2_1 <= -1000000000) || (C2_1 >= 1000000000)))
              abort ();
          X1_1 = __VERIFIER_nondet__Bool ();
          T1_1 = __VERIFIER_nondet__Bool ();
          P1_1 = __VERIFIER_nondet_int ();
          if (((P1_1 <= -1000000000) || (P1_1 >= 1000000000)))
              abort ();
          L1_1 = __VERIFIER_nondet_int ();
          if (((L1_1 <= -1000000000) || (L1_1 >= 1000000000)))
              abort ();
          H1_1 = __VERIFIER_nondet__Bool ();
          H2_1 = __VERIFIER_nondet__Bool ();
          D1_1 = __VERIFIER_nondet_int ();
          if (((D1_1 <= -1000000000) || (D1_1 >= 1000000000)))
              abort ();
          D2_1 = __VERIFIER_nondet_int ();
          if (((D2_1 <= -1000000000) || (D2_1 >= 1000000000)))
              abort ();
          Y1_1 = __VERIFIER_nondet__Bool ();
          U1_1 = __VERIFIER_nondet_int ();
          if (((U1_1 <= -1000000000) || (U1_1 >= 1000000000)))
              abort ();
          R2_1 = state_0;
          V_1 = state_1;
          Z_1 = state_2;
          Y_1 = state_3;
          Q2_1 = state_4;
          J2_1 = state_5;
          U_1 = state_6;
          K2_1 = state_7;
          S_1 = state_8;
          L2_1 = state_9;
          Q_1 = state_10;
          X_1 = state_11;
          W_1 = state_12;
          O2_1 = state_13;
          K_1 = state_14;
          N2_1 = state_15;
          M_1 = state_16;
          M2_1 = state_17;
          I_1 = state_18;
          H_1 = state_19;
          F_1 = state_20;
          D_1 = state_21;
          A_1 = state_22;
          T_1 = state_23;
          R_1 = state_24;
          P_1 = state_25;
          J_1 = state_26;
          B_1 = state_27;
          P2_1 = state_28;
          G_1 = state_29;
          E_1 = state_30;
          C_1 = state_31;
          O_1 = state_32;
          L_1 = state_33;
          N_1 = state_34;
          if (!
              (((I_1 == 10) == H_1) && ((3 <= C2_1) == W1_1)
               && ((3 <= M_1) == D_1) && ((4 <= E2_1) == Y1_1)
               && ((4 <= K_1) == F_1)
               &&
               (((!T1_1)
                 || ((0 <= U1_1) && (0 <= P1_1) && (!(11 <= P1_1))
                     && (!(4 <= U1_1)))) == S1_1) && (((!A_1) || ((0 <= B_1)
                                                                  && (0 <=
                                                                      P2_1)
                                                                  &&
                                                                  (!(11 <=
                                                                     P2_1))
                                                                  &&
                                                                  (!(4 <=
                                                                     B_1))))
                                                      == O_1)
               && (!((H1_1 && G1_1) == Q1_1)) && (!((Y_1 && Z_1) == Q2_1))
               && (R2_1 == V_1) && (L2_1 == Q_1) && (K2_1 == S_1)
               && (J2_1 == U_1) && (B1_1 == K1_1) && (B1_1 == F2_1)
               && (C1_1 == I1_1) && (C1_1 == H2_1) && (F1_1 == J1_1)
               && (F1_1 == G2_1) && (G1_1 == J1_1)
               && (I1_1 == (H1_1 && (!G1_1)))
               && (K1_1 == ((!D_1) && (!F_1) && (!H_1) && Q_1))
               && (R1_1 == (V_1 && Q1_1 && (!(32767 <= P1_1))))
               && (W1_1 == V1_1) && (Y1_1 == X1_1) && (A2_1 == Z1_1)
               && (I2_1 == R1_1) && (I2_1 == T1_1) && (V_1 == A_1)
               && (U_1 == T_1) && (S_1 == R_1) && (Q_1 == P_1) && (H_1 == G_1)
               && (F_1 == E_1) && (D_1 == C_1) && (O2_1 == K_1)
               && (N2_1 == M_1) && (M2_1 == I_1) && (L1_1 == D1_1)
               && (N1_1 == M1_1) && (O1_1 == E1_1) && (B2_1 == L1_1)
               && (B2_1 == P1_1) && (C2_1 == M1_1) && (C2_1 == U1_1)
               && (E2_1 == O1_1) && (E2_1 == D2_1) && (M_1 == B_1)
               && (K_1 == J_1) && (I_1 == P2_1) && ((!C1_1) || (!B1_1)
                                                    || ((M_1 + (-1 * A1_1)) ==
                                                        1)) && ((!C1_1)
                                                                || (!B1_1)
                                                                ||
                                                                ((I_1 +
                                                                  (-1 *
                                                                   D1_1)) ==
                                                                 -1)) && (F1_1
                                                                          ||
                                                                          (N1_1
                                                                           ==
                                                                           A1_1)
                                                                          ||
                                                                          (!B1_1))
               && (S_1 || (X_1 == W_1) || (!Q_1)) && ((M_1 == A1_1)
                                                      || (C1_1 && B1_1))
               && ((C1_1 && B1_1) || (I_1 == D1_1)) && ((N1_1 == 0)
                                                        || ((!F1_1) && B1_1))
               && ((X_1 == 0) || (Q_1 && (!S_1))) && ((!F1_1)
                                                      || ((K_1 + (-1 * E1_1))
                                                          == -1)) && (F1_1
                                                                      || (K_1
                                                                          ==
                                                                          E1_1))
               && ((B2_1 == 10) == A2_1)))
              abort ();
          state_0 = R1_1;
          state_1 = I2_1;
          state_2 = G1_1;
          state_3 = H1_1;
          state_4 = Q1_1;
          state_5 = I1_1;
          state_6 = C1_1;
          state_7 = J1_1;
          state_8 = F1_1;
          state_9 = K1_1;
          state_10 = B1_1;
          state_11 = N1_1;
          state_12 = A1_1;
          state_13 = O1_1;
          state_14 = E2_1;
          state_15 = M1_1;
          state_16 = C2_1;
          state_17 = L1_1;
          state_18 = B2_1;
          state_19 = A2_1;
          state_20 = Y1_1;
          state_21 = W1_1;
          state_22 = T1_1;
          state_23 = H2_1;
          state_24 = G2_1;
          state_25 = F2_1;
          state_26 = D2_1;
          state_27 = U1_1;
          state_28 = P1_1;
          state_29 = Z1_1;
          state_30 = X1_1;
          state_31 = V1_1;
          state_32 = S1_1;
          state_33 = D1_1;
          state_34 = E1_1;
          goto state_0;

      default:
          abort ();
      }

    // return expression

}

