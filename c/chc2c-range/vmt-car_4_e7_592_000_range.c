// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: vmt-chc-benchmarks/vmt-car_4_e7_592_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "vmt-car_4_e7_592_000_range.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    _Bool state_0;
    _Bool state_1;
    int state_2;
    int state_3;
    int state_4;
    _Bool state_5;
    _Bool state_6;
    _Bool state_7;
    _Bool state_8;
    _Bool state_9;
    _Bool state_10;
    _Bool state_11;
    _Bool state_12;
    _Bool state_13;
    int state_14;
    int state_15;
    int state_16;
    int state_17;
    int state_18;
    _Bool state_19;
    _Bool state_20;
    _Bool state_21;
    _Bool state_22;
    _Bool state_23;
    _Bool state_24;
    _Bool state_25;
    int state_26;
    int state_27;
    int state_28;
    _Bool state_29;
    _Bool state_30;
    _Bool state_31;
    _Bool state_32;
    int state_33;
    int state_34;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    _Bool E_0;
    _Bool F_0;
    _Bool G_0;
    _Bool H_0;
    _Bool I_0;
    _Bool J_0;
    int K_0;
    int L_0;
    _Bool M_0;
    int N_0;
    int O_0;
    int P_0;
    int Q_0;
    int R_0;
    int S_0;
    _Bool T_0;
    _Bool U_0;
    _Bool V_0;
    _Bool W_0;
    _Bool X_0;
    _Bool Y_0;
    _Bool Z_0;
    _Bool A1_0;
    _Bool B1_0;
    int C1_0;
    _Bool D1_0;
    _Bool E1_0;
    _Bool F1_0;
    _Bool G1_0;
    _Bool H1_0;
    _Bool I1_0;
    int A_1;
    int B_1;
    int C_1;
    int D_1;
    _Bool E_1;
    _Bool F_1;
    _Bool G_1;
    _Bool H_1;
    _Bool I_1;
    _Bool J_1;
    int K_1;
    int L_1;
    _Bool M_1;
    int N_1;
    int O_1;
    int P_1;
    int Q_1;
    int R_1;
    int S_1;
    _Bool T_1;
    _Bool U_1;
    _Bool V_1;
    _Bool W_1;
    _Bool X_1;
    _Bool Y_1;
    _Bool Z_1;
    int A1_1;
    _Bool B1_1;
    _Bool C1_1;
    int D1_1;
    int E1_1;
    _Bool F1_1;
    _Bool G1_1;
    _Bool H1_1;
    _Bool I1_1;
    _Bool J1_1;
    _Bool K1_1;
    int L1_1;
    int M1_1;
    int N1_1;
    int O1_1;
    _Bool P1_1;
    _Bool Q1_1;
    _Bool R1_1;
    _Bool S1_1;
    int T1_1;
    _Bool U1_1;
    _Bool V1_1;
    _Bool W1_1;
    _Bool X1_1;
    _Bool Y1_1;
    _Bool Z1_1;
    int A2_1;
    int B2_1;
    int C2_1;
    int D2_1;
    int E2_1;
    _Bool F2_1;
    _Bool G2_1;
    _Bool H2_1;
    _Bool I2_1;
    _Bool J2_1;
    _Bool K2_1;
    int L2_1;
    _Bool M2_1;
    _Bool N2_1;
    _Bool O2_1;
    _Bool P2_1;
    _Bool Q2_1;
    _Bool R2_1;
    int A_2;
    int B_2;
    int C_2;
    int D_2;
    _Bool E_2;
    _Bool F_2;
    _Bool G_2;
    _Bool H_2;
    _Bool I_2;
    _Bool J_2;
    int K_2;
    int L_2;
    _Bool M_2;
    int N_2;
    int O_2;
    int P_2;
    int Q_2;
    int R_2;
    int S_2;
    _Bool T_2;
    _Bool U_2;
    _Bool V_2;
    _Bool W_2;
    _Bool X_2;
    _Bool Y_2;
    _Bool Z_2;
    _Bool A1_2;
    _Bool B1_2;
    int C1_2;
    _Bool D1_2;
    _Bool E1_2;
    _Bool F1_2;
    _Bool G1_2;
    _Bool H1_2;
    _Bool I1_2;

    if (((state_2 <= -1000000000) || (state_2 >= 1000000000))
        || ((state_3 <= -1000000000) || (state_3 >= 1000000000))
        || ((state_4 <= -1000000000) || (state_4 >= 1000000000))
        || ((state_14 <= -1000000000) || (state_14 >= 1000000000))
        || ((state_15 <= -1000000000) || (state_15 >= 1000000000))
        || ((state_16 <= -1000000000) || (state_16 >= 1000000000))
        || ((state_17 <= -1000000000) || (state_17 >= 1000000000))
        || ((state_18 <= -1000000000) || (state_18 >= 1000000000))
        || ((state_26 <= -1000000000) || (state_26 >= 1000000000))
        || ((state_27 <= -1000000000) || (state_27 >= 1000000000))
        || ((state_28 <= -1000000000) || (state_28 >= 1000000000))
        || ((state_33 <= -1000000000) || (state_33 >= 1000000000))
        || ((state_34 <= -1000000000) || (state_34 >= 1000000000))
        || ((A_0 <= -1000000000) || (A_0 >= 1000000000))
        || ((B_0 <= -1000000000) || (B_0 >= 1000000000))
        || ((C_0 <= -1000000000) || (C_0 >= 1000000000))
        || ((D_0 <= -1000000000) || (D_0 >= 1000000000))
        || ((K_0 <= -1000000000) || (K_0 >= 1000000000))
        || ((L_0 <= -1000000000) || (L_0 >= 1000000000))
        || ((N_0 <= -1000000000) || (N_0 >= 1000000000))
        || ((O_0 <= -1000000000) || (O_0 >= 1000000000))
        || ((P_0 <= -1000000000) || (P_0 >= 1000000000))
        || ((Q_0 <= -1000000000) || (Q_0 >= 1000000000))
        || ((R_0 <= -1000000000) || (R_0 >= 1000000000))
        || ((S_0 <= -1000000000) || (S_0 >= 1000000000))
        || ((C1_0 <= -1000000000) || (C1_0 >= 1000000000))
        || ((A_1 <= -1000000000) || (A_1 >= 1000000000))
        || ((B_1 <= -1000000000) || (B_1 >= 1000000000))
        || ((C_1 <= -1000000000) || (C_1 >= 1000000000))
        || ((D_1 <= -1000000000) || (D_1 >= 1000000000))
        || ((K_1 <= -1000000000) || (K_1 >= 1000000000))
        || ((L_1 <= -1000000000) || (L_1 >= 1000000000))
        || ((N_1 <= -1000000000) || (N_1 >= 1000000000))
        || ((O_1 <= -1000000000) || (O_1 >= 1000000000))
        || ((P_1 <= -1000000000) || (P_1 >= 1000000000))
        || ((Q_1 <= -1000000000) || (Q_1 >= 1000000000))
        || ((R_1 <= -1000000000) || (R_1 >= 1000000000))
        || ((S_1 <= -1000000000) || (S_1 >= 1000000000))
        || ((A1_1 <= -1000000000) || (A1_1 >= 1000000000))
        || ((D1_1 <= -1000000000) || (D1_1 >= 1000000000))
        || ((E1_1 <= -1000000000) || (E1_1 >= 1000000000))
        || ((L1_1 <= -1000000000) || (L1_1 >= 1000000000))
        || ((M1_1 <= -1000000000) || (M1_1 >= 1000000000))
        || ((N1_1 <= -1000000000) || (N1_1 >= 1000000000))
        || ((O1_1 <= -1000000000) || (O1_1 >= 1000000000))
        || ((T1_1 <= -1000000000) || (T1_1 >= 1000000000))
        || ((A2_1 <= -1000000000) || (A2_1 >= 1000000000))
        || ((B2_1 <= -1000000000) || (B2_1 >= 1000000000))
        || ((C2_1 <= -1000000000) || (C2_1 >= 1000000000))
        || ((D2_1 <= -1000000000) || (D2_1 >= 1000000000))
        || ((E2_1 <= -1000000000) || (E2_1 >= 1000000000))
        || ((L2_1 <= -1000000000) || (L2_1 >= 1000000000))
        || ((A_2 <= -1000000000) || (A_2 >= 1000000000))
        || ((B_2 <= -1000000000) || (B_2 >= 1000000000))
        || ((C_2 <= -1000000000) || (C_2 >= 1000000000))
        || ((D_2 <= -1000000000) || (D_2 >= 1000000000))
        || ((K_2 <= -1000000000) || (K_2 >= 1000000000))
        || ((L_2 <= -1000000000) || (L_2 >= 1000000000))
        || ((N_2 <= -1000000000) || (N_2 >= 1000000000))
        || ((O_2 <= -1000000000) || (O_2 >= 1000000000))
        || ((P_2 <= -1000000000) || (P_2 >= 1000000000))
        || ((Q_2 <= -1000000000) || (Q_2 >= 1000000000))
        || ((R_2 <= -1000000000) || (R_2 >= 1000000000))
        || ((S_2 <= -1000000000) || (S_2 >= 1000000000))
        || ((C1_2 <= -1000000000) || (C1_2 >= 1000000000)))
        abort ();

    // main logic
    goto main_init;

  main_init:
    if (!
        ((Q_0 == 0) && (Q_0 == D_0) && (P_0 == 0) && (P_0 == B_0)
         && (A_0 == O_0) && (D_0 == C_0) && (K_0 == 0) && (K_0 == A_0)
         && ((A_0 == 10) == I1_0) && ((4 <= D_0) == G1_0)
         && ((3 <= B_0) == E1_0) && (((!B1_0) || (0 <= C1_0)) == A1_0)
         && (!((X_0 && W_0) == Y_0)) && (V_0 == M_0) && (U_0 == H_0)
         && (T_0 == F_0) && (F_0 == E_0) && (I1_0 == H1_0) && (G1_0 == F1_0)
         && (E1_0 == D1_0) && (H_0 == G_0) && (J_0 == B1_0) && (M_0 == I_0)
         && (Z_0 == J_0) && (Z_0 == Y_0) && (H_0 || (S_0 == R_0) || (!F_0))
         && ((S_0 == 0) || ((!H_0) && F_0)) && (!V_0) && (!U_0) && T_0
         && (B_0 == C1_0)))
        abort ();
    state_0 = Z_0;
    state_1 = Y_0;
    state_2 = Q_0;
    state_3 = P_0;
    state_4 = K_0;
    state_5 = U_0;
    state_6 = V_0;
    state_7 = T_0;
    state_8 = J_0;
    state_9 = X_0;
    state_10 = W_0;
    state_11 = M_0;
    state_12 = H_0;
    state_13 = F_0;
    state_14 = S_0;
    state_15 = R_0;
    state_16 = D_0;
    state_17 = B_0;
    state_18 = A_0;
    state_19 = I1_0;
    state_20 = G1_0;
    state_21 = E1_0;
    state_22 = B1_0;
    state_23 = I_0;
    state_24 = G_0;
    state_25 = E_0;
    state_26 = C_0;
    state_27 = C1_0;
    state_28 = O_0;
    state_29 = H1_0;
    state_30 = F1_0;
    state_31 = D1_0;
    state_32 = A1_0;
    state_33 = L_0;
    state_34 = N_0;
    Q1_1 = __VERIFIER_nondet__Bool ();
    M1_1 = __VERIFIER_nondet_int ();
    if (((M1_1 <= -1000000000) || (M1_1 >= 1000000000)))
        abort ();
    I1_1 = __VERIFIER_nondet__Bool ();
    I2_1 = __VERIFIER_nondet__Bool ();
    E1_1 = __VERIFIER_nondet_int ();
    if (((E1_1 <= -1000000000) || (E1_1 >= 1000000000)))
        abort ();
    E2_1 = __VERIFIER_nondet_int ();
    if (((E2_1 <= -1000000000) || (E2_1 >= 1000000000)))
        abort ();
    A1_1 = __VERIFIER_nondet_int ();
    if (((A1_1 <= -1000000000) || (A1_1 >= 1000000000)))
        abort ();
    A2_1 = __VERIFIER_nondet_int ();
    if (((A2_1 <= -1000000000) || (A2_1 >= 1000000000)))
        abort ();
    Z1_1 = __VERIFIER_nondet__Bool ();
    V1_1 = __VERIFIER_nondet__Bool ();
    R1_1 = __VERIFIER_nondet__Bool ();
    N1_1 = __VERIFIER_nondet_int ();
    if (((N1_1 <= -1000000000) || (N1_1 >= 1000000000)))
        abort ();
    J1_1 = __VERIFIER_nondet__Bool ();
    F1_1 = __VERIFIER_nondet__Bool ();
    F2_1 = __VERIFIER_nondet__Bool ();
    B1_1 = __VERIFIER_nondet__Bool ();
    B2_1 = __VERIFIER_nondet_int ();
    if (((B2_1 <= -1000000000) || (B2_1 >= 1000000000)))
        abort ();
    W1_1 = __VERIFIER_nondet__Bool ();
    S1_1 = __VERIFIER_nondet__Bool ();
    O1_1 = __VERIFIER_nondet_int ();
    if (((O1_1 <= -1000000000) || (O1_1 >= 1000000000)))
        abort ();
    K1_1 = __VERIFIER_nondet__Bool ();
    G1_1 = __VERIFIER_nondet__Bool ();
    G2_1 = __VERIFIER_nondet__Bool ();
    C1_1 = __VERIFIER_nondet__Bool ();
    C2_1 = __VERIFIER_nondet_int ();
    if (((C2_1 <= -1000000000) || (C2_1 >= 1000000000)))
        abort ();
    X1_1 = __VERIFIER_nondet__Bool ();
    T1_1 = __VERIFIER_nondet_int ();
    if (((T1_1 <= -1000000000) || (T1_1 >= 1000000000)))
        abort ();
    P1_1 = __VERIFIER_nondet__Bool ();
    L1_1 = __VERIFIER_nondet_int ();
    if (((L1_1 <= -1000000000) || (L1_1 >= 1000000000)))
        abort ();
    H1_1 = __VERIFIER_nondet__Bool ();
    H2_1 = __VERIFIER_nondet__Bool ();
    D1_1 = __VERIFIER_nondet_int ();
    if (((D1_1 <= -1000000000) || (D1_1 >= 1000000000)))
        abort ();
    D2_1 = __VERIFIER_nondet_int ();
    if (((D2_1 <= -1000000000) || (D2_1 >= 1000000000)))
        abort ();
    Y1_1 = __VERIFIER_nondet__Bool ();
    U1_1 = __VERIFIER_nondet__Bool ();
    Z_1 = state_0;
    Y_1 = state_1;
    Q_1 = state_2;
    P_1 = state_3;
    K_1 = state_4;
    U_1 = state_5;
    V_1 = state_6;
    T_1 = state_7;
    J_1 = state_8;
    X_1 = state_9;
    W_1 = state_10;
    M_1 = state_11;
    H_1 = state_12;
    F_1 = state_13;
    S_1 = state_14;
    R_1 = state_15;
    D_1 = state_16;
    B_1 = state_17;
    A_1 = state_18;
    R2_1 = state_19;
    P2_1 = state_20;
    N2_1 = state_21;
    K2_1 = state_22;
    I_1 = state_23;
    G_1 = state_24;
    E_1 = state_25;
    C_1 = state_26;
    L2_1 = state_27;
    O_1 = state_28;
    Q2_1 = state_29;
    O2_1 = state_30;
    M2_1 = state_31;
    J2_1 = state_32;
    L_1 = state_33;
    N_1 = state_34;
    if (!
        ((N1_1 == M1_1) && (O1_1 == E1_1) && (B2_1 == L1_1) && (B2_1 == A2_1)
         && (C2_1 == M1_1) && (C2_1 == T1_1) && (E2_1 == O1_1)
         && (E2_1 == D2_1) && (Q_1 == D_1) && (P_1 == B_1) && (K_1 == A_1)
         && (D_1 == C_1) && (B_1 == L2_1) && (A_1 == O_1)
         && ((B2_1 == 10) == Z1_1) && ((A_1 == 10) == R2_1)
         && ((4 <= E2_1) == X1_1) && ((4 <= D_1) == P2_1)
         && ((3 <= C2_1) == V1_1) && ((3 <= B_1) == N2_1)
         && (((!K2_1) || (0 <= L2_1)) == J2_1)
         && (((!S1_1) || (0 <= T1_1)) == R1_1) && (!((H1_1 && G1_1) == P1_1))
         && (!((W_1 && X_1) == Y_1)) && (R2_1 == Q2_1) && (P2_1 == O2_1)
         && (N2_1 == M2_1) && (B1_1 == K1_1) && (B1_1 == F2_1)
         && (C1_1 == I1_1) && (C1_1 == H2_1) && (F1_1 == J1_1)
         && (F1_1 == G2_1) && (G1_1 == J1_1) && (I1_1 == (H1_1 && (!G1_1)))
         && (K1_1 == (F_1 && (!N2_1) && (!P2_1) && (!R2_1)))
         && (Q1_1 == (J_1 || P1_1)) && (V1_1 == U1_1) && (X1_1 == W1_1)
         && (Z1_1 == Y1_1) && (I2_1 == Q1_1) && (I2_1 == S1_1) && (Z_1 == J_1)
         && (V_1 == M_1) && (U_1 == H_1) && (T_1 == F_1) && (M_1 == I_1)
         && (J_1 == K2_1) && (H_1 == G_1) && (F_1 == E_1) && ((!C1_1)
                                                              || (!B1_1)
                                                              ||
                                                              ((B_1 +
                                                                (-1 *
                                                                 A1_1)) ==
                                                               -1))
         && ((!C1_1) || (!B1_1) || ((A_1 + (-1 * D1_1)) == -1)) && (F1_1
                                                                    || (N1_1
                                                                        ==
                                                                        A1_1)
                                                                    ||
                                                                    (!B1_1))
         && (H_1 || (S_1 == R_1) || (!F_1)) && ((B_1 == A1_1)
                                                || (C1_1 && B1_1)) && ((C1_1
                                                                        &&
                                                                        B1_1)
                                                                       || (A_1
                                                                           ==
                                                                           D1_1))
         && ((N1_1 == 0) || ((!F1_1) && B1_1)) && ((S_1 == 0)
                                                   || (F_1 && (!H_1)))
         && ((!F1_1) || ((D_1 + (-1 * E1_1)) == -1)) && (F1_1
                                                         || (D_1 == E1_1))
         && (L1_1 == D1_1)))
        abort ();
    state_0 = Q1_1;
    state_1 = P1_1;
    state_2 = O1_1;
    state_3 = M1_1;
    state_4 = L1_1;
    state_5 = J1_1;
    state_6 = I1_1;
    state_7 = K1_1;
    state_8 = I2_1;
    state_9 = G1_1;
    state_10 = H1_1;
    state_11 = C1_1;
    state_12 = F1_1;
    state_13 = B1_1;
    state_14 = N1_1;
    state_15 = A1_1;
    state_16 = E2_1;
    state_17 = C2_1;
    state_18 = B2_1;
    state_19 = Z1_1;
    state_20 = X1_1;
    state_21 = V1_1;
    state_22 = S1_1;
    state_23 = H2_1;
    state_24 = G2_1;
    state_25 = F2_1;
    state_26 = D2_1;
    state_27 = T1_1;
    state_28 = A2_1;
    state_29 = Y1_1;
    state_30 = W1_1;
    state_31 = U1_1;
    state_32 = R1_1;
    state_33 = D1_1;
    state_34 = E1_1;
    goto state_0;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  state_0:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          Z_2 = state_0;
          Y_2 = state_1;
          Q_2 = state_2;
          P_2 = state_3;
          K_2 = state_4;
          U_2 = state_5;
          V_2 = state_6;
          T_2 = state_7;
          J_2 = state_8;
          X_2 = state_9;
          W_2 = state_10;
          M_2 = state_11;
          H_2 = state_12;
          F_2 = state_13;
          S_2 = state_14;
          R_2 = state_15;
          D_2 = state_16;
          B_2 = state_17;
          A_2 = state_18;
          I1_2 = state_19;
          G1_2 = state_20;
          E1_2 = state_21;
          B1_2 = state_22;
          I_2 = state_23;
          G_2 = state_24;
          E_2 = state_25;
          C_2 = state_26;
          C1_2 = state_27;
          O_2 = state_28;
          H1_2 = state_29;
          F1_2 = state_30;
          D1_2 = state_31;
          A1_2 = state_32;
          L_2 = state_33;
          N_2 = state_34;
          if (!(!A1_2))
              abort ();
          goto main_error;

      case 1:
          Q1_1 = __VERIFIER_nondet__Bool ();
          M1_1 = __VERIFIER_nondet_int ();
          if (((M1_1 <= -1000000000) || (M1_1 >= 1000000000)))
              abort ();
          I1_1 = __VERIFIER_nondet__Bool ();
          I2_1 = __VERIFIER_nondet__Bool ();
          E1_1 = __VERIFIER_nondet_int ();
          if (((E1_1 <= -1000000000) || (E1_1 >= 1000000000)))
              abort ();
          E2_1 = __VERIFIER_nondet_int ();
          if (((E2_1 <= -1000000000) || (E2_1 >= 1000000000)))
              abort ();
          A1_1 = __VERIFIER_nondet_int ();
          if (((A1_1 <= -1000000000) || (A1_1 >= 1000000000)))
              abort ();
          A2_1 = __VERIFIER_nondet_int ();
          if (((A2_1 <= -1000000000) || (A2_1 >= 1000000000)))
              abort ();
          Z1_1 = __VERIFIER_nondet__Bool ();
          V1_1 = __VERIFIER_nondet__Bool ();
          R1_1 = __VERIFIER_nondet__Bool ();
          N1_1 = __VERIFIER_nondet_int ();
          if (((N1_1 <= -1000000000) || (N1_1 >= 1000000000)))
              abort ();
          J1_1 = __VERIFIER_nondet__Bool ();
          F1_1 = __VERIFIER_nondet__Bool ();
          F2_1 = __VERIFIER_nondet__Bool ();
          B1_1 = __VERIFIER_nondet__Bool ();
          B2_1 = __VERIFIER_nondet_int ();
          if (((B2_1 <= -1000000000) || (B2_1 >= 1000000000)))
              abort ();
          W1_1 = __VERIFIER_nondet__Bool ();
          S1_1 = __VERIFIER_nondet__Bool ();
          O1_1 = __VERIFIER_nondet_int ();
          if (((O1_1 <= -1000000000) || (O1_1 >= 1000000000)))
              abort ();
          K1_1 = __VERIFIER_nondet__Bool ();
          G1_1 = __VERIFIER_nondet__Bool ();
          G2_1 = __VERIFIER_nondet__Bool ();
          C1_1 = __VERIFIER_nondet__Bool ();
          C2_1 = __VERIFIER_nondet_int ();
          if (((C2_1 <= -1000000000) || (C2_1 >= 1000000000)))
              abort ();
          X1_1 = __VERIFIER_nondet__Bool ();
          T1_1 = __VERIFIER_nondet_int ();
          if (((T1_1 <= -1000000000) || (T1_1 >= 1000000000)))
              abort ();
          P1_1 = __VERIFIER_nondet__Bool ();
          L1_1 = __VERIFIER_nondet_int ();
          if (((L1_1 <= -1000000000) || (L1_1 >= 1000000000)))
              abort ();
          H1_1 = __VERIFIER_nondet__Bool ();
          H2_1 = __VERIFIER_nondet__Bool ();
          D1_1 = __VERIFIER_nondet_int ();
          if (((D1_1 <= -1000000000) || (D1_1 >= 1000000000)))
              abort ();
          D2_1 = __VERIFIER_nondet_int ();
          if (((D2_1 <= -1000000000) || (D2_1 >= 1000000000)))
              abort ();
          Y1_1 = __VERIFIER_nondet__Bool ();
          U1_1 = __VERIFIER_nondet__Bool ();
          Z_1 = state_0;
          Y_1 = state_1;
          Q_1 = state_2;
          P_1 = state_3;
          K_1 = state_4;
          U_1 = state_5;
          V_1 = state_6;
          T_1 = state_7;
          J_1 = state_8;
          X_1 = state_9;
          W_1 = state_10;
          M_1 = state_11;
          H_1 = state_12;
          F_1 = state_13;
          S_1 = state_14;
          R_1 = state_15;
          D_1 = state_16;
          B_1 = state_17;
          A_1 = state_18;
          R2_1 = state_19;
          P2_1 = state_20;
          N2_1 = state_21;
          K2_1 = state_22;
          I_1 = state_23;
          G_1 = state_24;
          E_1 = state_25;
          C_1 = state_26;
          L2_1 = state_27;
          O_1 = state_28;
          Q2_1 = state_29;
          O2_1 = state_30;
          M2_1 = state_31;
          J2_1 = state_32;
          L_1 = state_33;
          N_1 = state_34;
          if (!
              ((N1_1 == M1_1) && (O1_1 == E1_1) && (B2_1 == L1_1)
               && (B2_1 == A2_1) && (C2_1 == M1_1) && (C2_1 == T1_1)
               && (E2_1 == O1_1) && (E2_1 == D2_1) && (Q_1 == D_1)
               && (P_1 == B_1) && (K_1 == A_1) && (D_1 == C_1)
               && (B_1 == L2_1) && (A_1 == O_1) && ((B2_1 == 10) == Z1_1)
               && ((A_1 == 10) == R2_1) && ((4 <= E2_1) == X1_1)
               && ((4 <= D_1) == P2_1) && ((3 <= C2_1) == V1_1)
               && ((3 <= B_1) == N2_1) && (((!K2_1) || (0 <= L2_1)) == J2_1)
               && (((!S1_1) || (0 <= T1_1)) == R1_1)
               && (!((H1_1 && G1_1) == P1_1)) && (!((W_1 && X_1) == Y_1))
               && (R2_1 == Q2_1) && (P2_1 == O2_1) && (N2_1 == M2_1)
               && (B1_1 == K1_1) && (B1_1 == F2_1) && (C1_1 == I1_1)
               && (C1_1 == H2_1) && (F1_1 == J1_1) && (F1_1 == G2_1)
               && (G1_1 == J1_1) && (I1_1 == (H1_1 && (!G1_1)))
               && (K1_1 == (F_1 && (!N2_1) && (!P2_1) && (!R2_1)))
               && (Q1_1 == (J_1 || P1_1)) && (V1_1 == U1_1) && (X1_1 == W1_1)
               && (Z1_1 == Y1_1) && (I2_1 == Q1_1) && (I2_1 == S1_1)
               && (Z_1 == J_1) && (V_1 == M_1) && (U_1 == H_1) && (T_1 == F_1)
               && (M_1 == I_1) && (J_1 == K2_1) && (H_1 == G_1)
               && (F_1 == E_1) && ((!C1_1) || (!B1_1)
                                   || ((B_1 + (-1 * A1_1)) == -1)) && ((!C1_1)
                                                                       ||
                                                                       (!B1_1)
                                                                       ||
                                                                       ((A_1 +
                                                                         (-1 *
                                                                          D1_1))
                                                                        ==
                                                                        -1))
               && (F1_1 || (N1_1 == A1_1) || (!B1_1)) && (H_1 || (S_1 == R_1)
                                                          || (!F_1))
               && ((B_1 == A1_1) || (C1_1 && B1_1)) && ((C1_1 && B1_1)
                                                        || (A_1 == D1_1))
               && ((N1_1 == 0) || ((!F1_1) && B1_1)) && ((S_1 == 0)
                                                         || (F_1 && (!H_1)))
               && ((!F1_1) || ((D_1 + (-1 * E1_1)) == -1)) && (F1_1
                                                               || (D_1 ==
                                                                   E1_1))
               && (L1_1 == D1_1)))
              abort ();
          state_0 = Q1_1;
          state_1 = P1_1;
          state_2 = O1_1;
          state_3 = M1_1;
          state_4 = L1_1;
          state_5 = J1_1;
          state_6 = I1_1;
          state_7 = K1_1;
          state_8 = I2_1;
          state_9 = G1_1;
          state_10 = H1_1;
          state_11 = C1_1;
          state_12 = F1_1;
          state_13 = B1_1;
          state_14 = N1_1;
          state_15 = A1_1;
          state_16 = E2_1;
          state_17 = C2_1;
          state_18 = B2_1;
          state_19 = Z1_1;
          state_20 = X1_1;
          state_21 = V1_1;
          state_22 = S1_1;
          state_23 = H2_1;
          state_24 = G2_1;
          state_25 = F2_1;
          state_26 = D2_1;
          state_27 = T1_1;
          state_28 = A2_1;
          state_29 = Y1_1;
          state_30 = W1_1;
          state_31 = U1_1;
          state_32 = R1_1;
          state_33 = D1_1;
          state_34 = E1_1;
          goto state_0;

      default:
          abort ();
      }

    // return expression

}

