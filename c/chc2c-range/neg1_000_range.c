// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: hopv/neg1_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "neg1_000_range.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int g_unknown_3_0;
    int g_unknown_3_1;
    int g_unknown_3_2;
    int twice_unknown_11_0;
    int twice_unknown_11_1;
    int twice_unknown_13_0;
    int twice_unknown_13_1;
    int neg_unknown_7_0;
    int neg_unknown_7_1;
    int twice_unknown_15_0;
    int twice_unknown_15_1;
    int twice_unknown_9_0;
    int twice_unknown_9_1;
    int neg_unknown_5_0;
    int neg_unknown_5_1;
    int A_0;
    int B_0;
    int C_0;
    int A_1;
    int B_1;
    int C_1;
    int D_1;
    int E_1;
    int A_2;
    int B_2;
    int C_2;
    int D_2;
    int A_3;
    int B_3;
    int A_4;
    int B_4;
    int C_4;
    int A_5;
    int B_5;
    int A_6;
    int B_6;
    int C_6;
    int D_6;
    int E_6;
    int A_7;
    int B_7;
    int C_7;
    int D_7;
    int E_7;
    int A_8;
    int B_8;
    int C_8;
    int D_8;
    int E_8;

    if (((g_unknown_3_0 <= -1000000000) || (g_unknown_3_0 >= 1000000000))
        || ((g_unknown_3_1 <= -1000000000) || (g_unknown_3_1 >= 1000000000))
        || ((g_unknown_3_2 <= -1000000000) || (g_unknown_3_2 >= 1000000000))
        || ((twice_unknown_11_0 <= -1000000000)
            || (twice_unknown_11_0 >= 1000000000))
        || ((twice_unknown_11_1 <= -1000000000)
            || (twice_unknown_11_1 >= 1000000000))
        || ((twice_unknown_13_0 <= -1000000000)
            || (twice_unknown_13_0 >= 1000000000))
        || ((twice_unknown_13_1 <= -1000000000)
            || (twice_unknown_13_1 >= 1000000000))
        || ((neg_unknown_7_0 <= -1000000000)
            || (neg_unknown_7_0 >= 1000000000))
        || ((neg_unknown_7_1 <= -1000000000)
            || (neg_unknown_7_1 >= 1000000000))
        || ((twice_unknown_15_0 <= -1000000000)
            || (twice_unknown_15_0 >= 1000000000))
        || ((twice_unknown_15_1 <= -1000000000)
            || (twice_unknown_15_1 >= 1000000000))
        || ((twice_unknown_9_0 <= -1000000000)
            || (twice_unknown_9_0 >= 1000000000))
        || ((twice_unknown_9_1 <= -1000000000)
            || (twice_unknown_9_1 >= 1000000000))
        || ((neg_unknown_5_0 <= -1000000000)
            || (neg_unknown_5_0 >= 1000000000))
        || ((neg_unknown_5_1 <= -1000000000)
            || (neg_unknown_5_1 >= 1000000000)) || ((A_0 <= -1000000000)
                                                    || (A_0 >= 1000000000))
        || ((B_0 <= -1000000000) || (B_0 >= 1000000000))
        || ((C_0 <= -1000000000) || (C_0 >= 1000000000))
        || ((A_1 <= -1000000000) || (A_1 >= 1000000000))
        || ((B_1 <= -1000000000) || (B_1 >= 1000000000))
        || ((C_1 <= -1000000000) || (C_1 >= 1000000000))
        || ((D_1 <= -1000000000) || (D_1 >= 1000000000))
        || ((E_1 <= -1000000000) || (E_1 >= 1000000000))
        || ((A_2 <= -1000000000) || (A_2 >= 1000000000))
        || ((B_2 <= -1000000000) || (B_2 >= 1000000000))
        || ((C_2 <= -1000000000) || (C_2 >= 1000000000))
        || ((D_2 <= -1000000000) || (D_2 >= 1000000000))
        || ((A_3 <= -1000000000) || (A_3 >= 1000000000))
        || ((B_3 <= -1000000000) || (B_3 >= 1000000000))
        || ((A_4 <= -1000000000) || (A_4 >= 1000000000))
        || ((B_4 <= -1000000000) || (B_4 >= 1000000000))
        || ((C_4 <= -1000000000) || (C_4 >= 1000000000))
        || ((A_5 <= -1000000000) || (A_5 >= 1000000000))
        || ((B_5 <= -1000000000) || (B_5 >= 1000000000))
        || ((A_6 <= -1000000000) || (A_6 >= 1000000000))
        || ((B_6 <= -1000000000) || (B_6 >= 1000000000))
        || ((C_6 <= -1000000000) || (C_6 >= 1000000000))
        || ((D_6 <= -1000000000) || (D_6 >= 1000000000))
        || ((E_6 <= -1000000000) || (E_6 >= 1000000000))
        || ((A_7 <= -1000000000) || (A_7 >= 1000000000))
        || ((B_7 <= -1000000000) || (B_7 >= 1000000000))
        || ((C_7 <= -1000000000) || (C_7 >= 1000000000))
        || ((D_7 <= -1000000000) || (D_7 >= 1000000000))
        || ((E_7 <= -1000000000) || (E_7 >= 1000000000))
        || ((A_8 <= -1000000000) || (A_8 >= 1000000000))
        || ((B_8 <= -1000000000) || (B_8 >= 1000000000))
        || ((C_8 <= -1000000000) || (C_8 >= 1000000000))
        || ((D_8 <= -1000000000) || (D_8 >= 1000000000))
        || ((E_8 <= -1000000000) || (E_8 >= 1000000000)))
        abort ();

    // main logic
    goto main_init;

  main_init:
    if (!(A_0 == B_0))
        abort ();
    g_unknown_3_0 = A_0;
    g_unknown_3_1 = C_0;
    g_unknown_3_2 = B_0;
    D_1 = __VERIFIER_nondet_int ();
    if (((D_1 <= -1000000000) || (D_1 >= 1000000000)))
        abort ();
    E_1 = __VERIFIER_nondet_int ();
    if (((E_1 <= -1000000000) || (E_1 >= 1000000000)))
        abort ();
    A_1 = g_unknown_3_0;
    C_1 = g_unknown_3_1;
    B_1 = g_unknown_3_2;
    if (!((!(0 == E_1)) && (D_1 == 1) && (!((0 == E_1) == (B_1 >= 0)))))
        abort ();
    twice_unknown_13_0 = A_1;
    twice_unknown_13_1 = C_1;
    A_5 = twice_unknown_13_0;
    B_5 = twice_unknown_13_1;
    if (!1)
        abort ();
    twice_unknown_9_0 = A_5;
    twice_unknown_9_1 = B_5;
    C_6 = __VERIFIER_nondet_int ();
    if (((C_6 <= -1000000000) || (C_6 >= 1000000000)))
        abort ();
    D_6 = __VERIFIER_nondet_int ();
    if (((D_6 <= -1000000000) || (D_6 >= 1000000000)))
        abort ();
    E_6 = __VERIFIER_nondet_int ();
    if (((E_6 <= -1000000000) || (E_6 >= 1000000000)))
        abort ();
    B_6 = twice_unknown_9_0;
    A_6 = twice_unknown_9_1;
    if (!((!(0 == E_6)) && (D_6 == 1) && (!((0 == E_6) == (C_6 >= 0)))))
        abort ();
    neg_unknown_5_0 = B_6;
    neg_unknown_5_1 = A_6;
    A_2 = __VERIFIER_nondet_int ();
    if (((A_2 <= -1000000000) || (A_2 >= 1000000000)))
        abort ();
    B_2 = __VERIFIER_nondet_int ();
    if (((B_2 <= -1000000000) || (B_2 >= 1000000000)))
        abort ();
    D_2 = neg_unknown_5_0;
    C_2 = neg_unknown_5_1;
    if (!((C_2 == 1) && (A_2 == (-1 * D_2))))
        abort ();
    neg_unknown_7_0 = A_2;
    neg_unknown_7_1 = B_2;
    C_7 = __VERIFIER_nondet_int ();
    if (((C_7 <= -1000000000) || (C_7 >= 1000000000)))
        abort ();
    D_7 = __VERIFIER_nondet_int ();
    if (((D_7 <= -1000000000) || (D_7 >= 1000000000)))
        abort ();
    E_7 = __VERIFIER_nondet_int ();
    if (((E_7 <= -1000000000) || (E_7 >= 1000000000)))
        abort ();
    B_7 = neg_unknown_7_0;
    A_7 = neg_unknown_7_1;
    if (!((!(0 == E_7)) && (D_7 == 1) && (!((0 == E_7) == (C_7 >= 0)))))
        abort ();
    twice_unknown_11_0 = B_7;
    twice_unknown_11_1 = A_7;
    goto twice_unknown_11;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  twice_unknown_11:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_4 = __VERIFIER_nondet_int ();
          if (((A_4 <= -1000000000) || (A_4 >= 1000000000)))
              abort ();
          C_4 = twice_unknown_11_0;
          B_4 = twice_unknown_11_1;
          if (!(A_4 == C_4))
              abort ();
          twice_unknown_15_0 = A_4;
          twice_unknown_15_1 = B_4;
          A_8 = __VERIFIER_nondet_int ();
          if (((A_8 <= -1000000000) || (A_8 >= 1000000000)))
              abort ();
          B_8 = __VERIFIER_nondet_int ();
          if (((B_8 <= -1000000000) || (B_8 >= 1000000000)))
              abort ();
          E_8 = __VERIFIER_nondet_int ();
          if (((E_8 <= -1000000000) || (E_8 >= 1000000000)))
              abort ();
          D_8 = twice_unknown_15_0;
          C_8 = twice_unknown_15_1;
          if (!
              ((!((0 == E_8) == (A_8 >= 0))) && (0 == B_8) && (!(0 == E_8))
               && (C_8 == 1) && (!((0 == B_8) == (D_8 >= 0)))))
              abort ();
          goto main_error;

      case 1:
          A_3 = twice_unknown_11_0;
          B_3 = twice_unknown_11_1;
          if (!1)
              abort ();
          twice_unknown_9_0 = A_3;
          twice_unknown_9_1 = B_3;
          C_6 = __VERIFIER_nondet_int ();
          if (((C_6 <= -1000000000) || (C_6 >= 1000000000)))
              abort ();
          D_6 = __VERIFIER_nondet_int ();
          if (((D_6 <= -1000000000) || (D_6 >= 1000000000)))
              abort ();
          E_6 = __VERIFIER_nondet_int ();
          if (((E_6 <= -1000000000) || (E_6 >= 1000000000)))
              abort ();
          B_6 = twice_unknown_9_0;
          A_6 = twice_unknown_9_1;
          if (!((!(0 == E_6)) && (D_6 == 1) && (!((0 == E_6) == (C_6 >= 0)))))
              abort ();
          neg_unknown_5_0 = B_6;
          neg_unknown_5_1 = A_6;
          A_2 = __VERIFIER_nondet_int ();
          if (((A_2 <= -1000000000) || (A_2 >= 1000000000)))
              abort ();
          B_2 = __VERIFIER_nondet_int ();
          if (((B_2 <= -1000000000) || (B_2 >= 1000000000)))
              abort ();
          D_2 = neg_unknown_5_0;
          C_2 = neg_unknown_5_1;
          if (!((C_2 == 1) && (A_2 == (-1 * D_2))))
              abort ();
          neg_unknown_7_0 = A_2;
          neg_unknown_7_1 = B_2;
          C_7 = __VERIFIER_nondet_int ();
          if (((C_7 <= -1000000000) || (C_7 >= 1000000000)))
              abort ();
          D_7 = __VERIFIER_nondet_int ();
          if (((D_7 <= -1000000000) || (D_7 >= 1000000000)))
              abort ();
          E_7 = __VERIFIER_nondet_int ();
          if (((E_7 <= -1000000000) || (E_7 >= 1000000000)))
              abort ();
          B_7 = neg_unknown_7_0;
          A_7 = neg_unknown_7_1;
          if (!((!(0 == E_7)) && (D_7 == 1) && (!((0 == E_7) == (C_7 >= 0)))))
              abort ();
          twice_unknown_11_0 = B_7;
          twice_unknown_11_1 = A_7;
          goto twice_unknown_11;

      default:
          abort ();
      }

    // return expression

}

