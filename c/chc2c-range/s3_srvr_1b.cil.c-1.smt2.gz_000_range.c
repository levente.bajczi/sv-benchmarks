// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: tricera-benchmarks/s3_srvr_1b.cil.c-1.smt2.gz_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "s3_srvr_1b.cil.c-1.smt2.gz_000_range.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int inv_main9_0;
    int inv_main9_1;
    int inv_main9_2;
    int inv_main9_3;
    int inv_main3_0;
    int inv_main45_0;
    int inv_main45_1;
    int inv_main45_2;
    int inv_main45_3;
    int inv_main56_0;
    int inv_main56_1;
    int inv_main56_2;
    int inv_main56_3;
    int A_0;
    int A_1;
    int B_1;
    int C_1;
    int D_1;
    int A_2;
    int B_2;
    int C_2;
    int D_2;
    int A_3;
    int B_3;
    int C_3;
    int D_3;
    int E_3;
    int F_3;
    int A_4;
    int B_4;
    int C_4;
    int D_4;
    int E_4;
    int F_4;
    int A_5;
    int B_5;
    int C_5;
    int D_5;
    int E_5;
    int A_6;
    int B_6;
    int C_6;
    int D_6;
    int E_6;
    int A_7;
    int B_7;
    int C_7;
    int D_7;
    int E_7;
    int F_7;
    int A_8;
    int B_8;
    int C_8;
    int D_8;
    int E_8;
    int F_8;
    int A_9;
    int B_9;
    int C_9;
    int D_9;
    int E_9;
    int F_9;
    int A_10;
    int B_10;
    int C_10;
    int D_10;
    int E_10;
    int A_11;
    int B_11;
    int C_11;
    int D_11;
    int E_11;
    int F_11;
    int A_12;
    int B_12;
    int C_12;
    int D_12;
    int E_12;
    int A_13;
    int B_13;
    int C_13;
    int D_13;
    int E_13;
    int A_14;
    int B_14;
    int C_14;
    int D_14;
    int E_14;
    int A_15;
    int B_15;
    int C_15;
    int D_15;
    int A_16;
    int B_16;
    int C_16;
    int D_16;

    if (((inv_main9_0 <= -1000000000) || (inv_main9_0 >= 1000000000))
        || ((inv_main9_1 <= -1000000000) || (inv_main9_1 >= 1000000000))
        || ((inv_main9_2 <= -1000000000) || (inv_main9_2 >= 1000000000))
        || ((inv_main9_3 <= -1000000000) || (inv_main9_3 >= 1000000000))
        || ((inv_main3_0 <= -1000000000) || (inv_main3_0 >= 1000000000))
        || ((inv_main45_0 <= -1000000000) || (inv_main45_0 >= 1000000000))
        || ((inv_main45_1 <= -1000000000) || (inv_main45_1 >= 1000000000))
        || ((inv_main45_2 <= -1000000000) || (inv_main45_2 >= 1000000000))
        || ((inv_main45_3 <= -1000000000) || (inv_main45_3 >= 1000000000))
        || ((inv_main56_0 <= -1000000000) || (inv_main56_0 >= 1000000000))
        || ((inv_main56_1 <= -1000000000) || (inv_main56_1 >= 1000000000))
        || ((inv_main56_2 <= -1000000000) || (inv_main56_2 >= 1000000000))
        || ((inv_main56_3 <= -1000000000) || (inv_main56_3 >= 1000000000))
        || ((A_0 <= -1000000000) || (A_0 >= 1000000000))
        || ((A_1 <= -1000000000) || (A_1 >= 1000000000))
        || ((B_1 <= -1000000000) || (B_1 >= 1000000000))
        || ((C_1 <= -1000000000) || (C_1 >= 1000000000))
        || ((D_1 <= -1000000000) || (D_1 >= 1000000000))
        || ((A_2 <= -1000000000) || (A_2 >= 1000000000))
        || ((B_2 <= -1000000000) || (B_2 >= 1000000000))
        || ((C_2 <= -1000000000) || (C_2 >= 1000000000))
        || ((D_2 <= -1000000000) || (D_2 >= 1000000000))
        || ((A_3 <= -1000000000) || (A_3 >= 1000000000))
        || ((B_3 <= -1000000000) || (B_3 >= 1000000000))
        || ((C_3 <= -1000000000) || (C_3 >= 1000000000))
        || ((D_3 <= -1000000000) || (D_3 >= 1000000000))
        || ((E_3 <= -1000000000) || (E_3 >= 1000000000))
        || ((F_3 <= -1000000000) || (F_3 >= 1000000000))
        || ((A_4 <= -1000000000) || (A_4 >= 1000000000))
        || ((B_4 <= -1000000000) || (B_4 >= 1000000000))
        || ((C_4 <= -1000000000) || (C_4 >= 1000000000))
        || ((D_4 <= -1000000000) || (D_4 >= 1000000000))
        || ((E_4 <= -1000000000) || (E_4 >= 1000000000))
        || ((F_4 <= -1000000000) || (F_4 >= 1000000000))
        || ((A_5 <= -1000000000) || (A_5 >= 1000000000))
        || ((B_5 <= -1000000000) || (B_5 >= 1000000000))
        || ((C_5 <= -1000000000) || (C_5 >= 1000000000))
        || ((D_5 <= -1000000000) || (D_5 >= 1000000000))
        || ((E_5 <= -1000000000) || (E_5 >= 1000000000))
        || ((A_6 <= -1000000000) || (A_6 >= 1000000000))
        || ((B_6 <= -1000000000) || (B_6 >= 1000000000))
        || ((C_6 <= -1000000000) || (C_6 >= 1000000000))
        || ((D_6 <= -1000000000) || (D_6 >= 1000000000))
        || ((E_6 <= -1000000000) || (E_6 >= 1000000000))
        || ((A_7 <= -1000000000) || (A_7 >= 1000000000))
        || ((B_7 <= -1000000000) || (B_7 >= 1000000000))
        || ((C_7 <= -1000000000) || (C_7 >= 1000000000))
        || ((D_7 <= -1000000000) || (D_7 >= 1000000000))
        || ((E_7 <= -1000000000) || (E_7 >= 1000000000))
        || ((F_7 <= -1000000000) || (F_7 >= 1000000000))
        || ((A_8 <= -1000000000) || (A_8 >= 1000000000))
        || ((B_8 <= -1000000000) || (B_8 >= 1000000000))
        || ((C_8 <= -1000000000) || (C_8 >= 1000000000))
        || ((D_8 <= -1000000000) || (D_8 >= 1000000000))
        || ((E_8 <= -1000000000) || (E_8 >= 1000000000))
        || ((F_8 <= -1000000000) || (F_8 >= 1000000000))
        || ((A_9 <= -1000000000) || (A_9 >= 1000000000))
        || ((B_9 <= -1000000000) || (B_9 >= 1000000000))
        || ((C_9 <= -1000000000) || (C_9 >= 1000000000))
        || ((D_9 <= -1000000000) || (D_9 >= 1000000000))
        || ((E_9 <= -1000000000) || (E_9 >= 1000000000))
        || ((F_9 <= -1000000000) || (F_9 >= 1000000000))
        || ((A_10 <= -1000000000) || (A_10 >= 1000000000))
        || ((B_10 <= -1000000000) || (B_10 >= 1000000000))
        || ((C_10 <= -1000000000) || (C_10 >= 1000000000))
        || ((D_10 <= -1000000000) || (D_10 >= 1000000000))
        || ((E_10 <= -1000000000) || (E_10 >= 1000000000))
        || ((A_11 <= -1000000000) || (A_11 >= 1000000000))
        || ((B_11 <= -1000000000) || (B_11 >= 1000000000))
        || ((C_11 <= -1000000000) || (C_11 >= 1000000000))
        || ((D_11 <= -1000000000) || (D_11 >= 1000000000))
        || ((E_11 <= -1000000000) || (E_11 >= 1000000000))
        || ((F_11 <= -1000000000) || (F_11 >= 1000000000))
        || ((A_12 <= -1000000000) || (A_12 >= 1000000000))
        || ((B_12 <= -1000000000) || (B_12 >= 1000000000))
        || ((C_12 <= -1000000000) || (C_12 >= 1000000000))
        || ((D_12 <= -1000000000) || (D_12 >= 1000000000))
        || ((E_12 <= -1000000000) || (E_12 >= 1000000000))
        || ((A_13 <= -1000000000) || (A_13 >= 1000000000))
        || ((B_13 <= -1000000000) || (B_13 >= 1000000000))
        || ((C_13 <= -1000000000) || (C_13 >= 1000000000))
        || ((D_13 <= -1000000000) || (D_13 >= 1000000000))
        || ((E_13 <= -1000000000) || (E_13 >= 1000000000))
        || ((A_14 <= -1000000000) || (A_14 >= 1000000000))
        || ((B_14 <= -1000000000) || (B_14 >= 1000000000))
        || ((C_14 <= -1000000000) || (C_14 >= 1000000000))
        || ((D_14 <= -1000000000) || (D_14 >= 1000000000))
        || ((E_14 <= -1000000000) || (E_14 >= 1000000000))
        || ((A_15 <= -1000000000) || (A_15 >= 1000000000))
        || ((B_15 <= -1000000000) || (B_15 >= 1000000000))
        || ((C_15 <= -1000000000) || (C_15 >= 1000000000))
        || ((D_15 <= -1000000000) || (D_15 >= 1000000000))
        || ((A_16 <= -1000000000) || (A_16 >= 1000000000))
        || ((B_16 <= -1000000000) || (B_16 >= 1000000000))
        || ((C_16 <= -1000000000) || (C_16 >= 1000000000))
        || ((D_16 <= -1000000000) || (D_16 >= 1000000000)))
        abort ();

    // main logic
    goto main_init;

  main_init:
    if (!1)
        abort ();
    inv_main3_0 = A_0;
    A_13 = __VERIFIER_nondet_int ();
    if (((A_13 <= -1000000000) || (A_13 >= 1000000000)))
        abort ();
    B_13 = __VERIFIER_nondet_int ();
    if (((B_13 <= -1000000000) || (B_13 >= 1000000000)))
        abort ();
    C_13 = __VERIFIER_nondet_int ();
    if (((C_13 <= -1000000000) || (C_13 >= 1000000000)))
        abort ();
    D_13 = __VERIFIER_nondet_int ();
    if (((D_13 <= -1000000000) || (D_13 >= 1000000000)))
        abort ();
    E_13 = inv_main3_0;
    if (!((D_13 == 0) && (A_13 == 8466)))
        abort ();
    inv_main9_0 = A_13;
    inv_main9_1 = C_13;
    inv_main9_2 = D_13;
    inv_main9_3 = B_13;
    D_9 = __VERIFIER_nondet_int ();
    if (((D_9 <= -1000000000) || (D_9 >= 1000000000)))
        abort ();
    F_9 = __VERIFIER_nondet_int ();
    if (((F_9 <= -1000000000) || (F_9 >= 1000000000)))
        abort ();
    B_9 = inv_main9_0;
    C_9 = inv_main9_1;
    A_9 = inv_main9_2;
    E_9 = inv_main9_3;
    if (!
        ((!(B_9 == 8466)) && (!(B_9 == 8512)) && (A_9 == 3) && (D_9 == 4)
         && (C_9 == 0) && (F_9 == 8656) && ((!(3 <= A_9)) || (!(B_9 <= 8512)))
         && (B_9 == 8640)))
        abort ();
    inv_main9_0 = F_9;
    inv_main9_1 = C_9;
    inv_main9_2 = D_9;
    inv_main9_3 = E_9;
    goto inv_main9_7;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  inv_main45:
    switch (__VERIFIER_nondet_int ())
      {
      case 0:
          A_11 = __VERIFIER_nondet_int ();
          if (((A_11 <= -1000000000) || (A_11 >= 1000000000)))
              abort ();
          B_11 = __VERIFIER_nondet_int ();
          if (((B_11 <= -1000000000) || (B_11 >= 1000000000)))
              abort ();
          E_11 = inv_main45_0;
          D_11 = inv_main45_1;
          C_11 = inv_main45_2;
          F_11 = inv_main45_3;
          if (!
              ((A_11 == 5) && (!(D_11 == 0)) && (C_11 == 4)
               && (B_11 == 8640)))
              abort ();
          inv_main9_0 = B_11;
          inv_main9_1 = D_11;
          inv_main9_2 = A_11;
          inv_main9_3 = F_11;
          D_9 = __VERIFIER_nondet_int ();
          if (((D_9 <= -1000000000) || (D_9 >= 1000000000)))
              abort ();
          F_9 = __VERIFIER_nondet_int ();
          if (((F_9 <= -1000000000) || (F_9 >= 1000000000)))
              abort ();
          B_9 = inv_main9_0;
          C_9 = inv_main9_1;
          A_9 = inv_main9_2;
          E_9 = inv_main9_3;
          if (!
              ((!(B_9 == 8466)) && (!(B_9 == 8512)) && (A_9 == 3)
               && (D_9 == 4) && (C_9 == 0) && (F_9 == 8656) && ((!(3 <= A_9))
                                                                ||
                                                                (!(B_9 <=
                                                                   8512)))
               && (B_9 == 8640)))
              abort ();
          inv_main9_0 = F_9;
          inv_main9_1 = C_9;
          inv_main9_2 = D_9;
          inv_main9_3 = E_9;
          goto inv_main9_7;

      case 1:
          A_12 = __VERIFIER_nondet_int ();
          if (((A_12 <= -1000000000) || (A_12 >= 1000000000)))
              abort ();
          B_12 = inv_main45_0;
          E_12 = inv_main45_1;
          D_12 = inv_main45_2;
          C_12 = inv_main45_3;
          if (!
              ((!(D_12 == 4)) && (!(D_12 == 5)) && (!(E_12 == 0))
               && (A_12 == 8640)))
              abort ();
          inv_main9_0 = A_12;
          inv_main9_1 = E_12;
          inv_main9_2 = D_12;
          inv_main9_3 = C_12;
          D_9 = __VERIFIER_nondet_int ();
          if (((D_9 <= -1000000000) || (D_9 >= 1000000000)))
              abort ();
          F_9 = __VERIFIER_nondet_int ();
          if (((F_9 <= -1000000000) || (F_9 >= 1000000000)))
              abort ();
          B_9 = inv_main9_0;
          C_9 = inv_main9_1;
          A_9 = inv_main9_2;
          E_9 = inv_main9_3;
          if (!
              ((!(B_9 == 8466)) && (!(B_9 == 8512)) && (A_9 == 3)
               && (D_9 == 4) && (C_9 == 0) && (F_9 == 8656) && ((!(3 <= A_9))
                                                                ||
                                                                (!(B_9 <=
                                                                   8512)))
               && (B_9 == 8640)))
              abort ();
          inv_main9_0 = F_9;
          inv_main9_1 = C_9;
          inv_main9_2 = D_9;
          inv_main9_3 = E_9;
          goto inv_main9_7;

      case 2:
          B_2 = inv_main45_0;
          A_2 = inv_main45_1;
          C_2 = inv_main45_2;
          D_2 = inv_main45_3;
          if (!((C_2 == 5) && (!(C_2 == 4))))
              abort ();
          inv_main56_0 = B_2;
          inv_main56_1 = A_2;
          inv_main56_2 = C_2;
          inv_main56_3 = D_2;
          C_16 = inv_main56_0;
          A_16 = inv_main56_1;
          B_16 = inv_main56_2;
          D_16 = inv_main56_3;
          if (!1)
              abort ();
          goto main_error;

      default:
          abort ();
      }
  inv_main9_0:
    switch (__VERIFIER_nondet_int ())
      {
      case 0:
          A_14 = __VERIFIER_nondet_int ();
          if (((A_14 <= -1000000000) || (A_14 >= 1000000000)))
              abort ();
          B_14 = inv_main9_0;
          D_14 = inv_main9_1;
          C_14 = inv_main9_2;
          E_14 = inv_main9_3;
          if (!
              ((C_14 == 2) && (!(B_14 == 8640)) && (B_14 == 8656)
               && (!(B_14 == 8466)) && (!(B_14 == 8512)) && ((!(B_14 <= 8512))
                                                             ||
                                                             (!(3 <= C_14)))
               && (A_14 == 3)))
              abort ();
          inv_main45_0 = B_14;
          inv_main45_1 = D_14;
          inv_main45_2 = A_14;
          inv_main45_3 = E_14;
          goto inv_main45;

      case 1:
          C_15 = inv_main9_0;
          A_15 = inv_main9_1;
          B_15 = inv_main9_2;
          D_15 = inv_main9_3;
          if (!
              ((C_15 == 8656) && (!(C_15 == 8466)) && (!(C_15 == 8512))
               && (!(B_15 == 2)) && ((!(C_15 <= 8512)) || (!(3 <= B_15)))
               && (!(C_15 == 8640))))
              abort ();
          inv_main45_0 = C_15;
          inv_main45_1 = A_15;
          inv_main45_2 = B_15;
          inv_main45_3 = D_15;
          goto inv_main45;

      case 2:
          A_3 = __VERIFIER_nondet_int ();
          if (((A_3 <= -1000000000) || (A_3 >= 1000000000)))
              abort ();
          D_3 = __VERIFIER_nondet_int ();
          if (((D_3 <= -1000000000) || (D_3 >= 1000000000)))
              abort ();
          E_3 = inv_main9_0;
          F_3 = inv_main9_1;
          B_3 = inv_main9_2;
          C_3 = inv_main9_3;
          if (!
              ((A_3 == 8656) && (E_3 == 8466) && (D_3 == 2) && (!(F_3 == 0))
               && ((!(3 <= B_3)) || (!(E_3 <= 8512))) && (B_3 == 0)))
              abort ();
          inv_main9_0 = A_3;
          inv_main9_1 = F_3;
          inv_main9_2 = D_3;
          inv_main9_3 = C_3;
          D_9 = __VERIFIER_nondet_int ();
          if (((D_9 <= -1000000000) || (D_9 >= 1000000000)))
              abort ();
          F_9 = __VERIFIER_nondet_int ();
          if (((F_9 <= -1000000000) || (F_9 >= 1000000000)))
              abort ();
          B_9 = inv_main9_0;
          C_9 = inv_main9_1;
          A_9 = inv_main9_2;
          E_9 = inv_main9_3;
          if (!
              ((!(B_9 == 8466)) && (!(B_9 == 8512)) && (A_9 == 3)
               && (D_9 == 4) && (C_9 == 0) && (F_9 == 8656) && ((!(3 <= A_9))
                                                                ||
                                                                (!(B_9 <=
                                                                   8512)))
               && (B_9 == 8640)))
              abort ();
          inv_main9_0 = F_9;
          inv_main9_1 = C_9;
          inv_main9_2 = D_9;
          inv_main9_3 = E_9;
          goto inv_main9_7;

      case 3:
          E_4 = __VERIFIER_nondet_int ();
          if (((E_4 <= -1000000000) || (E_4 >= 1000000000)))
              abort ();
          F_4 = __VERIFIER_nondet_int ();
          if (((F_4 <= -1000000000) || (F_4 >= 1000000000)))
              abort ();
          D_4 = inv_main9_0;
          C_4 = inv_main9_1;
          B_4 = inv_main9_2;
          A_4 = inv_main9_3;
          if (!
              ((E_4 == 2) && (D_4 == 8466) && (C_4 == 0) && (F_4 == 8512)
               && ((!(3 <= B_4)) || (!(D_4 <= 8512))) && (B_4 == 0)))
              abort ();
          inv_main9_0 = F_4;
          inv_main9_1 = C_4;
          inv_main9_2 = E_4;
          inv_main9_3 = A_4;
          D_9 = __VERIFIER_nondet_int ();
          if (((D_9 <= -1000000000) || (D_9 >= 1000000000)))
              abort ();
          F_9 = __VERIFIER_nondet_int ();
          if (((F_9 <= -1000000000) || (F_9 >= 1000000000)))
              abort ();
          B_9 = inv_main9_0;
          C_9 = inv_main9_1;
          A_9 = inv_main9_2;
          E_9 = inv_main9_3;
          if (!
              ((!(B_9 == 8466)) && (!(B_9 == 8512)) && (A_9 == 3)
               && (D_9 == 4) && (C_9 == 0) && (F_9 == 8656) && ((!(3 <= A_9))
                                                                ||
                                                                (!(B_9 <=
                                                                   8512)))
               && (B_9 == 8640)))
              abort ();
          inv_main9_0 = F_9;
          inv_main9_1 = C_9;
          inv_main9_2 = D_9;
          inv_main9_3 = E_9;
          goto inv_main9_7;

      case 4:
          A_6 = __VERIFIER_nondet_int ();
          if (((A_6 <= -1000000000) || (A_6 >= 1000000000)))
              abort ();
          D_6 = inv_main9_0;
          C_6 = inv_main9_1;
          E_6 = inv_main9_2;
          B_6 = inv_main9_3;
          if (!
              ((D_6 == 8466) && (C_6 == 0) && (!(E_6 == 0))
               && ((!(D_6 <= 8512)) || (!(3 <= E_6))) && (A_6 == 8512)))
              abort ();
          inv_main9_0 = A_6;
          inv_main9_1 = C_6;
          inv_main9_2 = E_6;
          inv_main9_3 = B_6;
          D_9 = __VERIFIER_nondet_int ();
          if (((D_9 <= -1000000000) || (D_9 >= 1000000000)))
              abort ();
          F_9 = __VERIFIER_nondet_int ();
          if (((F_9 <= -1000000000) || (F_9 >= 1000000000)))
              abort ();
          B_9 = inv_main9_0;
          C_9 = inv_main9_1;
          A_9 = inv_main9_2;
          E_9 = inv_main9_3;
          if (!
              ((!(B_9 == 8466)) && (!(B_9 == 8512)) && (A_9 == 3)
               && (D_9 == 4) && (C_9 == 0) && (F_9 == 8656) && ((!(3 <= A_9))
                                                                ||
                                                                (!(B_9 <=
                                                                   8512)))
               && (B_9 == 8640)))
              abort ();
          inv_main9_0 = F_9;
          inv_main9_1 = C_9;
          inv_main9_2 = D_9;
          inv_main9_3 = E_9;
          goto inv_main9_7;

      case 5:
          D_7 = __VERIFIER_nondet_int ();
          if (((D_7 <= -1000000000) || (D_7 >= 1000000000)))
              abort ();
          E_7 = __VERIFIER_nondet_int ();
          if (((E_7 <= -1000000000) || (E_7 >= 1000000000)))
              abort ();
          F_7 = inv_main9_0;
          A_7 = inv_main9_1;
          C_7 = inv_main9_2;
          B_7 = inv_main9_3;
          if (!
              ((!(D_7 == 0)) && (!(F_7 == 8466)) && (F_7 == 8512)
               && ((!(3 <= C_7)) || (!(F_7 <= 8512))) && (E_7 == 8466)))
              abort ();
          inv_main9_0 = E_7;
          inv_main9_1 = A_7;
          inv_main9_2 = C_7;
          inv_main9_3 = D_7;
          D_9 = __VERIFIER_nondet_int ();
          if (((D_9 <= -1000000000) || (D_9 >= 1000000000)))
              abort ();
          F_9 = __VERIFIER_nondet_int ();
          if (((F_9 <= -1000000000) || (F_9 >= 1000000000)))
              abort ();
          B_9 = inv_main9_0;
          C_9 = inv_main9_1;
          A_9 = inv_main9_2;
          E_9 = inv_main9_3;
          if (!
              ((!(B_9 == 8466)) && (!(B_9 == 8512)) && (A_9 == 3)
               && (D_9 == 4) && (C_9 == 0) && (F_9 == 8656) && ((!(3 <= A_9))
                                                                ||
                                                                (!(B_9 <=
                                                                   8512)))
               && (B_9 == 8640)))
              abort ();
          inv_main9_0 = F_9;
          inv_main9_1 = C_9;
          inv_main9_2 = D_9;
          inv_main9_3 = E_9;
          goto inv_main9_7;

      case 6:
          C_8 = __VERIFIER_nondet_int ();
          if (((C_8 <= -1000000000) || (C_8 >= 1000000000)))
              abort ();
          E_8 = __VERIFIER_nondet_int ();
          if (((E_8 <= -1000000000) || (E_8 >= 1000000000)))
              abort ();
          A_8 = inv_main9_0;
          F_8 = inv_main9_1;
          B_8 = inv_main9_2;
          D_8 = inv_main9_3;
          if (!
              ((A_8 == 8512) && (E_8 == 0) && (C_8 == 8640)
               && ((!(3 <= B_8)) || (!(A_8 <= 8512))) && (!(A_8 == 8466))))
              abort ();
          inv_main9_0 = C_8;
          inv_main9_1 = F_8;
          inv_main9_2 = B_8;
          inv_main9_3 = E_8;
          D_9 = __VERIFIER_nondet_int ();
          if (((D_9 <= -1000000000) || (D_9 >= 1000000000)))
              abort ();
          F_9 = __VERIFIER_nondet_int ();
          if (((F_9 <= -1000000000) || (F_9 >= 1000000000)))
              abort ();
          B_9 = inv_main9_0;
          C_9 = inv_main9_1;
          A_9 = inv_main9_2;
          E_9 = inv_main9_3;
          if (!
              ((!(B_9 == 8466)) && (!(B_9 == 8512)) && (A_9 == 3)
               && (D_9 == 4) && (C_9 == 0) && (F_9 == 8656) && ((!(3 <= A_9))
                                                                ||
                                                                (!(B_9 <=
                                                                   8512)))
               && (B_9 == 8640)))
              abort ();
          inv_main9_0 = F_9;
          inv_main9_1 = C_9;
          inv_main9_2 = D_9;
          inv_main9_3 = E_9;
          goto inv_main9_7;

      case 7:
          D_9 = __VERIFIER_nondet_int ();
          if (((D_9 <= -1000000000) || (D_9 >= 1000000000)))
              abort ();
          F_9 = __VERIFIER_nondet_int ();
          if (((F_9 <= -1000000000) || (F_9 >= 1000000000)))
              abort ();
          B_9 = inv_main9_0;
          C_9 = inv_main9_1;
          A_9 = inv_main9_2;
          E_9 = inv_main9_3;
          if (!
              ((!(B_9 == 8466)) && (!(B_9 == 8512)) && (A_9 == 3)
               && (D_9 == 4) && (C_9 == 0) && (F_9 == 8656) && ((!(3 <= A_9))
                                                                ||
                                                                (!(B_9 <=
                                                                   8512)))
               && (B_9 == 8640)))
              abort ();
          inv_main9_0 = F_9;
          inv_main9_1 = C_9;
          inv_main9_2 = D_9;
          inv_main9_3 = E_9;
          D_9 = __VERIFIER_nondet_int ();
          if (((D_9 <= -1000000000) || (D_9 >= 1000000000)))
              abort ();
          F_9 = __VERIFIER_nondet_int ();
          if (((F_9 <= -1000000000) || (F_9 >= 1000000000)))
              abort ();
          B_9 = inv_main9_0;
          C_9 = inv_main9_1;
          A_9 = inv_main9_2;
          E_9 = inv_main9_3;
          if (!
              ((!(B_9 == 8466)) && (!(B_9 == 8512)) && (A_9 == 3)
               && (D_9 == 4) && (C_9 == 0) && (F_9 == 8656) && ((!(3 <= A_9))
                                                                ||
                                                                (!(B_9 <=
                                                                   8512)))
               && (B_9 == 8640)))
              abort ();
          inv_main9_0 = F_9;
          inv_main9_1 = C_9;
          inv_main9_2 = D_9;
          inv_main9_3 = E_9;
          goto inv_main9_7;

      case 8:
          B_10 = __VERIFIER_nondet_int ();
          if (((B_10 <= -1000000000) || (B_10 >= 1000000000)))
              abort ();
          C_10 = inv_main9_0;
          E_10 = inv_main9_1;
          D_10 = inv_main9_2;
          A_10 = inv_main9_3;
          if (!
              ((C_10 == 8640) && (!(C_10 == 8466)) && (!(C_10 == 8512))
               && (B_10 == 8656) && (E_10 == 0) && ((!(C_10 <= 8512))
                                                    || (!(3 <= D_10)))
               && (!(D_10 == 3))))
              abort ();
          inv_main9_0 = B_10;
          inv_main9_1 = E_10;
          inv_main9_2 = D_10;
          inv_main9_3 = A_10;
          D_9 = __VERIFIER_nondet_int ();
          if (((D_9 <= -1000000000) || (D_9 >= 1000000000)))
              abort ();
          F_9 = __VERIFIER_nondet_int ();
          if (((F_9 <= -1000000000) || (F_9 >= 1000000000)))
              abort ();
          B_9 = inv_main9_0;
          C_9 = inv_main9_1;
          A_9 = inv_main9_2;
          E_9 = inv_main9_3;
          if (!
              ((!(B_9 == 8466)) && (!(B_9 == 8512)) && (A_9 == 3)
               && (D_9 == 4) && (C_9 == 0) && (F_9 == 8656) && ((!(3 <= A_9))
                                                                ||
                                                                (!(B_9 <=
                                                                   8512)))
               && (B_9 == 8640)))
              abort ();
          inv_main9_0 = F_9;
          inv_main9_1 = C_9;
          inv_main9_2 = D_9;
          inv_main9_3 = E_9;
          goto inv_main9_7;

      case 9:
          D_9 = __VERIFIER_nondet_int ();
          if (((D_9 <= -1000000000) || (D_9 >= 1000000000)))
              abort ();
          F_9 = __VERIFIER_nondet_int ();
          if (((F_9 <= -1000000000) || (F_9 >= 1000000000)))
              abort ();
          B_9 = inv_main9_0;
          C_9 = inv_main9_1;
          A_9 = inv_main9_2;
          E_9 = inv_main9_3;
          if (!
              ((!(B_9 == 8466)) && (!(B_9 == 8512)) && (A_9 == 3)
               && (D_9 == 4) && (C_9 == 0) && (F_9 == 8656) && ((!(3 <= A_9))
                                                                ||
                                                                (!(B_9 <=
                                                                   8512)))
               && (B_9 == 8640)))
              abort ();
          inv_main9_0 = F_9;
          inv_main9_1 = C_9;
          inv_main9_2 = D_9;
          inv_main9_3 = E_9;
          goto inv_main9_7;

      case 10:
          A_1 = inv_main9_0;
          D_1 = inv_main9_1;
          C_1 = inv_main9_2;
          B_1 = inv_main9_3;
          if (!((A_1 <= 8512) && (3 <= C_1)))
              abort ();
          inv_main56_0 = A_1;
          inv_main56_1 = D_1;
          inv_main56_2 = C_1;
          inv_main56_3 = B_1;
          C_16 = inv_main56_0;
          A_16 = inv_main56_1;
          B_16 = inv_main56_2;
          D_16 = inv_main56_3;
          if (!1)
              abort ();
          goto main_error;

      default:
          abort ();
      }
  inv_main9_1:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          C_5 = __VERIFIER_nondet_int ();
          if (((C_5 <= -1000000000) || (C_5 >= 1000000000)))
              abort ();
          B_5 = inv_main9_0;
          A_5 = inv_main9_1;
          E_5 = inv_main9_2;
          D_5 = inv_main9_3;
          if (!
              ((C_5 == 8656) && (B_5 == 8466) && (!(E_5 == 0))
               && ((!(3 <= E_5)) || (!(B_5 <= 8512))) && (!(A_5 == 0))))
              abort ();
          inv_main9_0 = C_5;
          inv_main9_1 = A_5;
          inv_main9_2 = E_5;
          inv_main9_3 = D_5;
          goto inv_main9_0;

      case 1:
          D_9 = __VERIFIER_nondet_int ();
          if (((D_9 <= -1000000000) || (D_9 >= 1000000000)))
              abort ();
          F_9 = __VERIFIER_nondet_int ();
          if (((F_9 <= -1000000000) || (F_9 >= 1000000000)))
              abort ();
          B_9 = inv_main9_0;
          C_9 = inv_main9_1;
          A_9 = inv_main9_2;
          E_9 = inv_main9_3;
          if (!
              ((!(B_9 == 8466)) && (!(B_9 == 8512)) && (A_9 == 3)
               && (D_9 == 4) && (C_9 == 0) && (F_9 == 8656) && ((!(3 <= A_9))
                                                                ||
                                                                (!(B_9 <=
                                                                   8512)))
               && (B_9 == 8640)))
              abort ();
          inv_main9_0 = F_9;
          inv_main9_1 = C_9;
          inv_main9_2 = D_9;
          inv_main9_3 = E_9;
          goto inv_main9_7;

      default:
          abort ();
      }
  inv_main9_2:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          D_7 = __VERIFIER_nondet_int ();
          if (((D_7 <= -1000000000) || (D_7 >= 1000000000)))
              abort ();
          E_7 = __VERIFIER_nondet_int ();
          if (((E_7 <= -1000000000) || (E_7 >= 1000000000)))
              abort ();
          F_7 = inv_main9_0;
          A_7 = inv_main9_1;
          C_7 = inv_main9_2;
          B_7 = inv_main9_3;
          if (!
              ((!(D_7 == 0)) && (!(F_7 == 8466)) && (F_7 == 8512)
               && ((!(3 <= C_7)) || (!(F_7 <= 8512))) && (E_7 == 8466)))
              abort ();
          inv_main9_0 = E_7;
          inv_main9_1 = A_7;
          inv_main9_2 = C_7;
          inv_main9_3 = D_7;
          goto inv_main9_1;

      case 1:
          D_9 = __VERIFIER_nondet_int ();
          if (((D_9 <= -1000000000) || (D_9 >= 1000000000)))
              abort ();
          F_9 = __VERIFIER_nondet_int ();
          if (((F_9 <= -1000000000) || (F_9 >= 1000000000)))
              abort ();
          B_9 = inv_main9_0;
          C_9 = inv_main9_1;
          A_9 = inv_main9_2;
          E_9 = inv_main9_3;
          if (!
              ((!(B_9 == 8466)) && (!(B_9 == 8512)) && (A_9 == 3)
               && (D_9 == 4) && (C_9 == 0) && (F_9 == 8656) && ((!(3 <= A_9))
                                                                ||
                                                                (!(B_9 <=
                                                                   8512)))
               && (B_9 == 8640)))
              abort ();
          inv_main9_0 = F_9;
          inv_main9_1 = C_9;
          inv_main9_2 = D_9;
          inv_main9_3 = E_9;
          goto inv_main9_7;

      default:
          abort ();
      }
  inv_main9_3:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_3 = __VERIFIER_nondet_int ();
          if (((A_3 <= -1000000000) || (A_3 >= 1000000000)))
              abort ();
          D_3 = __VERIFIER_nondet_int ();
          if (((D_3 <= -1000000000) || (D_3 >= 1000000000)))
              abort ();
          E_3 = inv_main9_0;
          F_3 = inv_main9_1;
          B_3 = inv_main9_2;
          C_3 = inv_main9_3;
          if (!
              ((A_3 == 8656) && (E_3 == 8466) && (D_3 == 2) && (!(F_3 == 0))
               && ((!(3 <= B_3)) || (!(E_3 <= 8512))) && (B_3 == 0)))
              abort ();
          inv_main9_0 = A_3;
          inv_main9_1 = F_3;
          inv_main9_2 = D_3;
          inv_main9_3 = C_3;
          goto inv_main9_2;

      case 1:
          D_9 = __VERIFIER_nondet_int ();
          if (((D_9 <= -1000000000) || (D_9 >= 1000000000)))
              abort ();
          F_9 = __VERIFIER_nondet_int ();
          if (((F_9 <= -1000000000) || (F_9 >= 1000000000)))
              abort ();
          B_9 = inv_main9_0;
          C_9 = inv_main9_1;
          A_9 = inv_main9_2;
          E_9 = inv_main9_3;
          if (!
              ((!(B_9 == 8466)) && (!(B_9 == 8512)) && (A_9 == 3)
               && (D_9 == 4) && (C_9 == 0) && (F_9 == 8656) && ((!(3 <= A_9))
                                                                ||
                                                                (!(B_9 <=
                                                                   8512)))
               && (B_9 == 8640)))
              abort ();
          inv_main9_0 = F_9;
          inv_main9_1 = C_9;
          inv_main9_2 = D_9;
          inv_main9_3 = E_9;
          goto inv_main9_7;

      default:
          abort ();
      }
  inv_main9_4:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          C_8 = __VERIFIER_nondet_int ();
          if (((C_8 <= -1000000000) || (C_8 >= 1000000000)))
              abort ();
          E_8 = __VERIFIER_nondet_int ();
          if (((E_8 <= -1000000000) || (E_8 >= 1000000000)))
              abort ();
          A_8 = inv_main9_0;
          F_8 = inv_main9_1;
          B_8 = inv_main9_2;
          D_8 = inv_main9_3;
          if (!
              ((A_8 == 8512) && (E_8 == 0) && (C_8 == 8640)
               && ((!(3 <= B_8)) || (!(A_8 <= 8512))) && (!(A_8 == 8466))))
              abort ();
          inv_main9_0 = C_8;
          inv_main9_1 = F_8;
          inv_main9_2 = B_8;
          inv_main9_3 = E_8;
          goto inv_main9_3;

      case 1:
          D_9 = __VERIFIER_nondet_int ();
          if (((D_9 <= -1000000000) || (D_9 >= 1000000000)))
              abort ();
          F_9 = __VERIFIER_nondet_int ();
          if (((F_9 <= -1000000000) || (F_9 >= 1000000000)))
              abort ();
          B_9 = inv_main9_0;
          C_9 = inv_main9_1;
          A_9 = inv_main9_2;
          E_9 = inv_main9_3;
          if (!
              ((!(B_9 == 8466)) && (!(B_9 == 8512)) && (A_9 == 3)
               && (D_9 == 4) && (C_9 == 0) && (F_9 == 8656) && ((!(3 <= A_9))
                                                                ||
                                                                (!(B_9 <=
                                                                   8512)))
               && (B_9 == 8640)))
              abort ();
          inv_main9_0 = F_9;
          inv_main9_1 = C_9;
          inv_main9_2 = D_9;
          inv_main9_3 = E_9;
          goto inv_main9_7;

      default:
          abort ();
      }
  inv_main9_5:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          E_4 = __VERIFIER_nondet_int ();
          if (((E_4 <= -1000000000) || (E_4 >= 1000000000)))
              abort ();
          F_4 = __VERIFIER_nondet_int ();
          if (((F_4 <= -1000000000) || (F_4 >= 1000000000)))
              abort ();
          D_4 = inv_main9_0;
          C_4 = inv_main9_1;
          B_4 = inv_main9_2;
          A_4 = inv_main9_3;
          if (!
              ((E_4 == 2) && (D_4 == 8466) && (C_4 == 0) && (F_4 == 8512)
               && ((!(3 <= B_4)) || (!(D_4 <= 8512))) && (B_4 == 0)))
              abort ();
          inv_main9_0 = F_4;
          inv_main9_1 = C_4;
          inv_main9_2 = E_4;
          inv_main9_3 = A_4;
          goto inv_main9_4;

      case 1:
          D_9 = __VERIFIER_nondet_int ();
          if (((D_9 <= -1000000000) || (D_9 >= 1000000000)))
              abort ();
          F_9 = __VERIFIER_nondet_int ();
          if (((F_9 <= -1000000000) || (F_9 >= 1000000000)))
              abort ();
          B_9 = inv_main9_0;
          C_9 = inv_main9_1;
          A_9 = inv_main9_2;
          E_9 = inv_main9_3;
          if (!
              ((!(B_9 == 8466)) && (!(B_9 == 8512)) && (A_9 == 3)
               && (D_9 == 4) && (C_9 == 0) && (F_9 == 8656) && ((!(3 <= A_9))
                                                                ||
                                                                (!(B_9 <=
                                                                   8512)))
               && (B_9 == 8640)))
              abort ();
          inv_main9_0 = F_9;
          inv_main9_1 = C_9;
          inv_main9_2 = D_9;
          inv_main9_3 = E_9;
          goto inv_main9_7;

      default:
          abort ();
      }
  inv_main9_6:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_6 = __VERIFIER_nondet_int ();
          if (((A_6 <= -1000000000) || (A_6 >= 1000000000)))
              abort ();
          D_6 = inv_main9_0;
          C_6 = inv_main9_1;
          E_6 = inv_main9_2;
          B_6 = inv_main9_3;
          if (!
              ((D_6 == 8466) && (C_6 == 0) && (!(E_6 == 0))
               && ((!(D_6 <= 8512)) || (!(3 <= E_6))) && (A_6 == 8512)))
              abort ();
          inv_main9_0 = A_6;
          inv_main9_1 = C_6;
          inv_main9_2 = E_6;
          inv_main9_3 = B_6;
          goto inv_main9_5;

      case 1:
          D_9 = __VERIFIER_nondet_int ();
          if (((D_9 <= -1000000000) || (D_9 >= 1000000000)))
              abort ();
          F_9 = __VERIFIER_nondet_int ();
          if (((F_9 <= -1000000000) || (F_9 >= 1000000000)))
              abort ();
          B_9 = inv_main9_0;
          C_9 = inv_main9_1;
          A_9 = inv_main9_2;
          E_9 = inv_main9_3;
          if (!
              ((!(B_9 == 8466)) && (!(B_9 == 8512)) && (A_9 == 3)
               && (D_9 == 4) && (C_9 == 0) && (F_9 == 8656) && ((!(3 <= A_9))
                                                                ||
                                                                (!(B_9 <=
                                                                   8512)))
               && (B_9 == 8640)))
              abort ();
          inv_main9_0 = F_9;
          inv_main9_1 = C_9;
          inv_main9_2 = D_9;
          inv_main9_3 = E_9;
          goto inv_main9_7;

      default:
          abort ();
      }
  inv_main9_7:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          B_10 = __VERIFIER_nondet_int ();
          if (((B_10 <= -1000000000) || (B_10 >= 1000000000)))
              abort ();
          C_10 = inv_main9_0;
          E_10 = inv_main9_1;
          D_10 = inv_main9_2;
          A_10 = inv_main9_3;
          if (!
              ((C_10 == 8640) && (!(C_10 == 8466)) && (!(C_10 == 8512))
               && (B_10 == 8656) && (E_10 == 0) && ((!(C_10 <= 8512))
                                                    || (!(3 <= D_10)))
               && (!(D_10 == 3))))
              abort ();
          inv_main9_0 = B_10;
          inv_main9_1 = E_10;
          inv_main9_2 = D_10;
          inv_main9_3 = A_10;
          goto inv_main9_6;

      case 1:
          D_9 = __VERIFIER_nondet_int ();
          if (((D_9 <= -1000000000) || (D_9 >= 1000000000)))
              abort ();
          F_9 = __VERIFIER_nondet_int ();
          if (((F_9 <= -1000000000) || (F_9 >= 1000000000)))
              abort ();
          B_9 = inv_main9_0;
          C_9 = inv_main9_1;
          A_9 = inv_main9_2;
          E_9 = inv_main9_3;
          if (!
              ((!(B_9 == 8466)) && (!(B_9 == 8512)) && (A_9 == 3)
               && (D_9 == 4) && (C_9 == 0) && (F_9 == 8656) && ((!(3 <= A_9))
                                                                ||
                                                                (!(B_9 <=
                                                                   8512)))
               && (B_9 == 8640)))
              abort ();
          inv_main9_0 = F_9;
          inv_main9_1 = C_9;
          inv_main9_2 = D_9;
          inv_main9_3 = E_9;
          goto inv_main9_7;

      default:
          abort ();
      }

    // return expression

}

