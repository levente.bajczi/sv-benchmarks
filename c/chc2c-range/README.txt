# This file is part of the SV-Benchmarks collection of verification tasks:
# https://github.com/sosy-lab/sv-benchmarks
#
# SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
#
# SPDX-License-Identifier: Apache-2.0
This directory contains C files that have been transformed from CHC queries. CHC files sourced from https://github.com/chc-comp/.

This variant contains the _range_ transformation option's output, meaning the variables are bound to a smaller range than permitted by the C standard.

