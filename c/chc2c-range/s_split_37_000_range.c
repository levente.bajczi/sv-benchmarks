// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: aeval-benchmarks/s_split_37_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "s_split_37_000_range.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int inv_0;
    int inv_1;
    int inv_2;
    int v_0_0;
    int v_1_0;
    int v_2_0;
    int A_1;
    int B_1;
    int C_1;
    int D_1;
    int E_1;
    int F_1;
    int A_2;
    int B_2;
    int C_2;
    _Bool CHC_COMP_UNUSED_3;

    if (((inv_0 <= -1000000000) || (inv_0 >= 1000000000))
        || ((inv_1 <= -1000000000) || (inv_1 >= 1000000000))
        || ((inv_2 <= -1000000000) || (inv_2 >= 1000000000))
        || ((v_0_0 <= -1000000000) || (v_0_0 >= 1000000000))
        || ((v_1_0 <= -1000000000) || (v_1_0 >= 1000000000))
        || ((v_2_0 <= -1000000000) || (v_2_0 >= 1000000000))
        || ((A_1 <= -1000000000) || (A_1 >= 1000000000))
        || ((B_1 <= -1000000000) || (B_1 >= 1000000000))
        || ((C_1 <= -1000000000) || (C_1 >= 1000000000))
        || ((D_1 <= -1000000000) || (D_1 >= 1000000000))
        || ((E_1 <= -1000000000) || (E_1 >= 1000000000))
        || ((F_1 <= -1000000000) || (F_1 >= 1000000000))
        || ((A_2 <= -1000000000) || (A_2 >= 1000000000))
        || ((B_2 <= -1000000000) || (B_2 >= 1000000000))
        || ((C_2 <= -1000000000) || (C_2 >= 1000000000)))
        abort ();

    // main logic
    goto main_init;

  main_init:
    if (!((0 == v_0_0) && (0 == v_1_0) && (0 == v_2_0)))
        abort ();
    inv_0 = v_0_0;
    inv_1 = v_1_0;
    inv_2 = v_2_0;
    D_1 = __VERIFIER_nondet_int ();
    if (((D_1 <= -1000000000) || (D_1 >= 1000000000)))
        abort ();
    E_1 = __VERIFIER_nondet_int ();
    if (((E_1 <= -1000000000) || (E_1 >= 1000000000)))
        abort ();
    F_1 = __VERIFIER_nondet_int ();
    if (((F_1 <= -1000000000) || (F_1 >= 1000000000)))
        abort ();
    B_1 = inv_0;
    A_1 = inv_1;
    C_1 = inv_2;
    if (!
        ((E_1 == ((B_1 == 0) ? 523 : (A_1 + C_1)))
         && (F_1 == ((B_1 == 0) ? C_1 : 250)) && (D_1 == (B_1 + 1))))
        abort ();
    inv_0 = D_1;
    inv_1 = E_1;
    inv_2 = F_1;
    goto inv_0;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  inv_0:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          B_2 = inv_0;
          C_2 = inv_1;
          A_2 = inv_2;
          if (!((C_2 <= 2500) && (B_2 >= 10)))
              abort ();
          CHC_COMP_UNUSED_3 = __VERIFIER_nondet__Bool ();
          if (!1)
              abort ();
          goto main_error;

      case 1:
          D_1 = __VERIFIER_nondet_int ();
          if (((D_1 <= -1000000000) || (D_1 >= 1000000000)))
              abort ();
          E_1 = __VERIFIER_nondet_int ();
          if (((E_1 <= -1000000000) || (E_1 >= 1000000000)))
              abort ();
          F_1 = __VERIFIER_nondet_int ();
          if (((F_1 <= -1000000000) || (F_1 >= 1000000000)))
              abort ();
          B_1 = inv_0;
          A_1 = inv_1;
          C_1 = inv_2;
          if (!
              ((E_1 == ((B_1 == 0) ? 523 : (A_1 + C_1)))
               && (F_1 == ((B_1 == 0) ? C_1 : 250)) && (D_1 == (B_1 + 1))))
              abort ();
          inv_0 = D_1;
          inv_1 = E_1;
          inv_2 = F_1;
          goto inv_0;

      default:
          abort ();
      }

    // return expression

}

