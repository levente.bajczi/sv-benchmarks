// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: tricera-benchmarks/apache-escape-absolute.i.v+cfa-reducer.c-1.smt2.gz_006.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "apache-escape-absolute.i.v+cfa-reducer.c-1.smt2.gz_006_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int inv_main76_0;
    int inv_main76_1;
    int inv_main76_2;
    int inv_main76_3;
    int inv_main76_4;
    int inv_main76_5;
    int inv_main76_6;
    int inv_main76_7;
    int inv_main76_8;
    int inv_main76_9;
    int inv_main76_10;
    int inv_main76_11;
    int inv_main76_12;
    int inv_main76_13;
    int inv_main76_14;
    int inv_main76_15;
    int inv_main76_16;
    int inv_main68_0;
    int inv_main68_1;
    int inv_main68_2;
    int inv_main68_3;
    int inv_main68_4;
    int inv_main68_5;
    int inv_main68_6;
    int inv_main68_7;
    int inv_main68_8;
    int inv_main68_9;
    int inv_main68_10;
    int inv_main68_11;
    int inv_main68_12;
    int inv_main68_13;
    int inv_main68_14;
    int inv_main68_15;
    int inv_main7_0;
    int inv_main7_1;
    int inv_main7_2;
    int inv_main7_3;
    int inv_main7_4;
    int inv_main7_5;
    int inv_main7_6;
    int inv_main101_0;
    int inv_main101_1;
    int inv_main101_2;
    int inv_main101_3;
    int inv_main101_4;
    int inv_main101_5;
    int inv_main101_6;
    int inv_main101_7;
    int inv_main101_8;
    int inv_main101_9;
    int inv_main101_10;
    int inv_main101_11;
    int inv_main101_12;
    int inv_main101_13;
    int inv_main101_14;
    int inv_main101_15;
    int inv_main101_16;
    int inv_main101_17;
    int inv_main101_18;
    int inv_main101_19;
    int inv_main101_20;
    int inv_main101_21;
    int inv_main101_22;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    int E_0;
    int F_0;
    int G_0;
    int A_2;
    int B_2;
    int C_2;
    int D_2;
    int E_2;
    int F_2;
    int G_2;
    int H_2;
    int I_2;
    int J_2;
    int K_2;
    int L_2;
    int M_2;
    int N_2;
    int O_2;
    int P_2;
    int Q_2;
    int R_2;
    int S_2;
    int T_2;
    int U_2;
    int V_2;
    int W_2;
    int X_2;
    int Y_2;
    int Z_2;
    int A1_2;
    int B1_2;
    int C1_2;
    int D1_2;
    int E1_2;
    int F1_2;
    int G1_2;
    int H1_2;
    int I1_2;
    int J1_2;
    int K1_2;
    int L1_2;
    int M1_2;
    int N1_2;
    int O1_2;
    int P1_2;
    int Q1_2;
    int R1_2;
    int S1_2;
    int T1_2;
    int U1_2;
    int V1_2;
    int W1_2;
    int X1_2;
    int Y1_2;
    int Z1_2;
    int A2_2;
    int B2_2;
    int C2_2;
    int D2_2;
    int E2_2;
    int F2_2;
    int G2_2;
    int H2_2;
    int I2_2;
    int J2_2;
    int K2_2;
    int L2_2;
    int M2_2;
    int N2_2;
    int O2_2;
    int P2_2;
    int Q2_2;
    int R2_2;
    int S2_2;
    int T2_2;
    int U2_2;
    int V2_2;
    int W2_2;
    int X2_2;
    int Y2_2;
    int v_77_2;
    int A_7;
    int B_7;
    int C_7;
    int D_7;
    int E_7;
    int F_7;
    int G_7;
    int H_7;
    int I_7;
    int J_7;
    int K_7;
    int L_7;
    int M_7;
    int N_7;
    int O_7;
    int P_7;
    int Q_7;
    int A_8;
    int B_8;
    int C_8;
    int D_8;
    int E_8;
    int F_8;
    int G_8;
    int H_8;
    int I_8;
    int J_8;
    int K_8;
    int L_8;
    int M_8;
    int N_8;
    int O_8;
    int P_8;
    int Q_8;
    int A_15;
    int B_15;
    int C_15;
    int D_15;
    int E_15;
    int F_15;
    int G_15;
    int H_15;
    int I_15;
    int J_15;
    int K_15;
    int L_15;
    int M_15;
    int N_15;
    int O_15;
    int P_15;
    int Q_15;
    int R_15;
    int S_15;
    int T_15;
    int U_15;
    int V_15;
    int W_15;
    int X_15;
    int Y_15;
    int Z_15;
    int A1_15;
    int B1_15;
    int C1_15;
    int D1_15;
    int E1_15;
    int F1_15;
    int G1_15;
    int H1_15;
    int I1_15;
    int J1_15;
    int K1_15;
    int L1_15;
    int M1_15;
    int N1_15;
    int O1_15;
    int P1_15;
    int Q1_15;
    int R1_15;
    int S1_15;
    int T1_15;
    int U1_15;
    int V1_15;
    int W1_15;
    int X1_15;
    int Y1_15;
    int Z1_15;
    int A2_15;
    int B2_15;
    int C2_15;
    int D2_15;
    int A_16;
    int B_16;
    int C_16;
    int D_16;
    int E_16;
    int F_16;
    int G_16;
    int H_16;
    int I_16;
    int J_16;
    int K_16;
    int L_16;
    int M_16;
    int N_16;
    int O_16;
    int P_16;
    int Q_16;
    int R_16;
    int S_16;
    int T_16;
    int U_16;
    int V_16;
    int W_16;
    int X_16;
    int Y_16;
    int Z_16;
    int A1_16;
    int B1_16;
    int C1_16;
    int D1_16;
    int E1_16;
    int F1_16;
    int G1_16;
    int H1_16;
    int I1_16;
    int J1_16;
    int K1_16;
    int L1_16;
    int M1_16;
    int N1_16;
    int O1_16;
    int P1_16;
    int Q1_16;
    int R1_16;
    int S1_16;
    int T1_16;
    int U1_16;
    int V1_16;
    int W1_16;
    int X1_16;
    int Y1_16;
    int Z1_16;
    int A2_16;
    int B2_16;
    int C2_16;
    int D2_16;
    int E2_16;
    int v_57_16;
    int A_26;
    int B_26;
    int C_26;
    int D_26;
    int E_26;
    int F_26;
    int G_26;
    int H_26;
    int I_26;
    int J_26;
    int K_26;
    int L_26;
    int M_26;
    int N_26;
    int O_26;
    int P_26;
    int Q_26;
    int R_26;
    int S_26;
    int T_26;
    int U_26;
    int V_26;
    int W_26;



    // main logic
    goto main_init;

  main_init:
    if (!((A_0 == 0) && (E_0 == 0)))
        abort ();
    inv_main7_0 = E_0;
    inv_main7_1 = A_0;
    inv_main7_2 = G_0;
    inv_main7_3 = F_0;
    inv_main7_4 = B_0;
    inv_main7_5 = D_0;
    inv_main7_6 = C_0;
    Q1_16 = __VERIFIER_nondet_int ();
    M1_16 = __VERIFIER_nondet_int ();
    I1_16 = __VERIFIER_nondet_int ();
    E1_16 = __VERIFIER_nondet_int ();
    E2_16 = __VERIFIER_nondet_int ();
    A1_16 = __VERIFIER_nondet_int ();
    A2_16 = __VERIFIER_nondet_int ();
    Z1_16 = __VERIFIER_nondet_int ();
    R1_16 = __VERIFIER_nondet_int ();
    N1_16 = __VERIFIER_nondet_int ();
    W1_16 = __VERIFIER_nondet_int ();
    v_57_16 = __VERIFIER_nondet_int ();
    S1_16 = __VERIFIER_nondet_int ();
    A_16 = __VERIFIER_nondet_int ();
    B_16 = __VERIFIER_nondet_int ();
    O1_16 = __VERIFIER_nondet_int ();
    C_16 = __VERIFIER_nondet_int ();
    D_16 = __VERIFIER_nondet_int ();
    F_16 = __VERIFIER_nondet_int ();
    K1_16 = __VERIFIER_nondet_int ();
    G_16 = __VERIFIER_nondet_int ();
    H_16 = __VERIFIER_nondet_int ();
    I_16 = __VERIFIER_nondet_int ();
    J_16 = __VERIFIER_nondet_int ();
    G1_16 = __VERIFIER_nondet_int ();
    K_16 = __VERIFIER_nondet_int ();
    L_16 = __VERIFIER_nondet_int ();
    M_16 = __VERIFIER_nondet_int ();
    N_16 = __VERIFIER_nondet_int ();
    C1_16 = __VERIFIER_nondet_int ();
    O_16 = __VERIFIER_nondet_int ();
    C2_16 = __VERIFIER_nondet_int ();
    P_16 = __VERIFIER_nondet_int ();
    Q_16 = __VERIFIER_nondet_int ();
    R_16 = __VERIFIER_nondet_int ();
    S_16 = __VERIFIER_nondet_int ();
    T_16 = __VERIFIER_nondet_int ();
    U_16 = __VERIFIER_nondet_int ();
    V_16 = __VERIFIER_nondet_int ();
    W_16 = __VERIFIER_nondet_int ();
    X_16 = __VERIFIER_nondet_int ();
    Y_16 = __VERIFIER_nondet_int ();
    Z_16 = __VERIFIER_nondet_int ();
    T1_16 = __VERIFIER_nondet_int ();
    P1_16 = __VERIFIER_nondet_int ();
    L1_16 = __VERIFIER_nondet_int ();
    H1_16 = __VERIFIER_nondet_int ();
    D1_16 = __VERIFIER_nondet_int ();
    D2_16 = __VERIFIER_nondet_int ();
    Y1_16 = __VERIFIER_nondet_int ();
    U1_16 = __VERIFIER_nondet_int ();
    E_16 = inv_main7_0;
    V1_16 = inv_main7_1;
    B1_16 = inv_main7_2;
    B2_16 = inv_main7_3;
    F1_16 = inv_main7_4;
    X1_16 = inv_main7_5;
    J1_16 = inv_main7_6;
    if (!
        ((U1_16 == H1_16) && (T1_16 == T_16) && (S1_16 == A_16)
         && (R1_16 == A2_16) && (Q1_16 == M1_16) && (P1_16 == E2_16)
         && (!(N1_16 == 0)) && (!(M1_16 == 0)) && (L1_16 == Z_16)
         && (K1_16 == J1_16) && (I1_16 == W_16) && (H1_16 == A2_16)
         && (G1_16 == L1_16) && (E1_16 == D_16) && (D1_16 == E_16)
         && (C1_16 == A1_16) && (A1_16 == P1_16) && (!(Z_16 == 0))
         && (Y_16 == F_16) && (W_16 == V1_16) && (V_16 == R1_16)
         && (U_16 == V_16) && (T_16 == S_16) && (S_16 == D1_16)
         && (R_16 == L_16) && (Q_16 == O1_16) && (P_16 == Q1_16)
         && (O_16 == U_16) && (N_16 == K_16) && (M_16 == R_16)
         && (L_16 == Q_16) && (K_16 == M1_16) && (J_16 == Z1_16)
         && (I_16 == N1_16) && (H_16 == N1_16) && (!(G_16 == 0))
         && (F_16 == I1_16) && (D_16 == C2_16) && (C_16 == D2_16)
         && (!(B_16 == 0)) && (A_16 == K1_16) && (E2_16 == X_16)
         && (D2_16 == Y1_16) && (C2_16 == B_16) && (!(A2_16 == 0))
         && (Z1_16 == U1_16) && (Y1_16 == B_16) && (-1000000 <= O1_16)
         && (-1000000 <= X_16) && (-1000000 <= A2_16) && (1 <= O1_16)
         && (1 <= X_16) && (!(0 <= (A2_16 + (-1 * O1_16)))) && (0 <= A2_16)
         && (O1_16 <= 1000000) && (X_16 <= 1000000) && (A2_16 <= 1000000)
         && (((!(1 <= (L_16 + (-1 * U1_16)))) && (N1_16 == 0))
             || ((1 <= (L_16 + (-1 * U1_16))) && (N1_16 == 1)))
         && (((!(1 <= H1_16)) && (M1_16 == 0))
             || ((1 <= H1_16) && (M1_16 == 1)))
         && (((!(0 <= (O1_16 + (-1 * A2_16)))) && (B_16 == 0))
             || ((0 <= (O1_16 + (-1 * A2_16))) && (B_16 == 1)))
         && (((0 <= Z1_16) && (G_16 == 1))
             || ((!(0 <= Z1_16)) && (G_16 == 0))) && (W1_16 == S1_16)
         && (v_57_16 == G_16)))
        abort ();
    inv_main68_0 = T1_16;
    inv_main68_1 = Y_16;
    inv_main68_2 = O_16;
    inv_main68_3 = M_16;
    inv_main68_4 = C1_16;
    inv_main68_5 = J_16;
    inv_main68_6 = W1_16;
    inv_main68_7 = E1_16;
    inv_main68_8 = C_16;
    inv_main68_9 = N_16;
    inv_main68_10 = P_16;
    inv_main68_11 = G1_16;
    inv_main68_12 = I_16;
    inv_main68_13 = H_16;
    inv_main68_14 = G_16;
    inv_main68_15 = v_57_16;
    Q1_15 = __VERIFIER_nondet_int ();
    M1_15 = __VERIFIER_nondet_int ();
    I1_15 = __VERIFIER_nondet_int ();
    E1_15 = __VERIFIER_nondet_int ();
    A1_15 = __VERIFIER_nondet_int ();
    Z1_15 = __VERIFIER_nondet_int ();
    V1_15 = __VERIFIER_nondet_int ();
    R1_15 = __VERIFIER_nondet_int ();
    N1_15 = __VERIFIER_nondet_int ();
    J1_15 = __VERIFIER_nondet_int ();
    F1_15 = __VERIFIER_nondet_int ();
    W1_15 = __VERIFIER_nondet_int ();
    S1_15 = __VERIFIER_nondet_int ();
    A_15 = __VERIFIER_nondet_int ();
    B_15 = __VERIFIER_nondet_int ();
    C_15 = __VERIFIER_nondet_int ();
    D_15 = __VERIFIER_nondet_int ();
    E_15 = __VERIFIER_nondet_int ();
    F_15 = __VERIFIER_nondet_int ();
    G_15 = __VERIFIER_nondet_int ();
    H_15 = __VERIFIER_nondet_int ();
    I_15 = __VERIFIER_nondet_int ();
    K_15 = __VERIFIER_nondet_int ();
    L_15 = __VERIFIER_nondet_int ();
    O_15 = __VERIFIER_nondet_int ();
    C2_15 = __VERIFIER_nondet_int ();
    Q_15 = __VERIFIER_nondet_int ();
    R_15 = __VERIFIER_nondet_int ();
    S_15 = __VERIFIER_nondet_int ();
    T_15 = __VERIFIER_nondet_int ();
    U_15 = __VERIFIER_nondet_int ();
    V_15 = __VERIFIER_nondet_int ();
    W_15 = __VERIFIER_nondet_int ();
    X_15 = __VERIFIER_nondet_int ();
    X1_15 = __VERIFIER_nondet_int ();
    Z_15 = __VERIFIER_nondet_int ();
    T1_15 = __VERIFIER_nondet_int ();
    P1_15 = __VERIFIER_nondet_int ();
    H1_15 = __VERIFIER_nondet_int ();
    D2_15 = __VERIFIER_nondet_int ();
    O1_15 = inv_main68_0;
    U1_15 = inv_main68_1;
    N_15 = inv_main68_2;
    A2_15 = inv_main68_3;
    B2_15 = inv_main68_4;
    D1_15 = inv_main68_5;
    C1_15 = inv_main68_6;
    M_15 = inv_main68_7;
    L1_15 = inv_main68_8;
    J_15 = inv_main68_9;
    G1_15 = inv_main68_10;
    Y_15 = inv_main68_11;
    B1_15 = inv_main68_12;
    K1_15 = inv_main68_13;
    P_15 = inv_main68_14;
    Y1_15 = inv_main68_15;
    if (!
        ((V1_15 == D1_15) && (T1_15 == X1_15) && (S1_15 == I1_15)
         && (R1_15 == E1_15) && (Q1_15 == A2_15) && (P1_15 == V1_15)
         && (N1_15 == H1_15) && (!(M1_15 == 0)) && (J1_15 == B_15)
         && (I1_15 == Y1_15) && (H1_15 == U1_15) && (F1_15 == I_15)
         && (E1_15 == 0) && (!(D1_15 == (A2_15 + -1))) && (A1_15 == M1_15)
         && (Z_15 == Y_15) && (X_15 == J_15) && (W_15 == W1_15)
         && (V_15 == (P1_15 + 1)) && (U_15 == B1_15) && (T_15 == Z_15)
         && (S_15 == U_15) && (R_15 == K_15) && (Q_15 == X_15)
         && (O_15 == R1_15) && (L_15 == Q1_15) && (K_15 == M_15)
         && (I_15 == O1_15) && (!(H_15 == 0)) && (G_15 == F_15)
         && (F_15 == L1_15) && (E_15 == K1_15) && (D_15 == B2_15)
         && (C_15 == D_15) && (B_15 == P_15) && (A_15 == M1_15)
         && (D2_15 == Z1_15) && (C2_15 == E_15) && (Z1_15 == C1_15)
         && (X1_15 == G1_15)
         && (((!(1 <= (A2_15 + (-1 * D1_15)))) && (M1_15 == 0))
             || ((1 <= (A2_15 + (-1 * D1_15))) && (M1_15 == 1)))
         && (((0 <= V1_15) && (H_15 == 1))
             || ((!(0 <= V1_15)) && (H_15 == 0))) && (W1_15 == N_15)))
        abort ();
    inv_main68_0 = F1_15;
    inv_main68_1 = N1_15;
    inv_main68_2 = W_15;
    inv_main68_3 = L_15;
    inv_main68_4 = C_15;
    inv_main68_5 = V_15;
    inv_main68_6 = D2_15;
    inv_main68_7 = R_15;
    inv_main68_8 = G_15;
    inv_main68_9 = Q_15;
    inv_main68_10 = T1_15;
    inv_main68_11 = T_15;
    inv_main68_12 = S_15;
    inv_main68_13 = C2_15;
    inv_main68_14 = J1_15;
    inv_main68_15 = S1_15;
    goto inv_main68_0;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  inv_main41:
    goto inv_main41;
  inv_main185:
    goto inv_main185;
  inv_main67:
    goto inv_main67;
  inv_main203:
    goto inv_main203;
  inv_main196:
    goto inv_main196;
  inv_main178:
    goto inv_main178;
  inv_main144:
    goto inv_main144;
  inv_main90:
    goto inv_main90;
  inv_main125:
    goto inv_main125;
  inv_main48:
    goto inv_main48;
  inv_main108:
    goto inv_main108;
  inv_main83:
    goto inv_main83;
  inv_main133:
    goto inv_main133;
  inv_main151:
    goto inv_main151;
  inv_main164:
    goto inv_main164;
  inv_main171:
    goto inv_main171;
  inv_main60:
    goto inv_main60;
  inv_main132:
    goto inv_main132;
  inv_main68_0:
    switch (__VERIFIER_nondet_int ())
      {
      case 0:
          E_7 = __VERIFIER_nondet_int ();
          I_7 = inv_main68_0;
          L_7 = inv_main68_1;
          D_7 = inv_main68_2;
          Q_7 = inv_main68_3;
          J_7 = inv_main68_4;
          O_7 = inv_main68_5;
          G_7 = inv_main68_6;
          B_7 = inv_main68_7;
          A_7 = inv_main68_8;
          P_7 = inv_main68_9;
          M_7 = inv_main68_10;
          C_7 = inv_main68_11;
          H_7 = inv_main68_12;
          F_7 = inv_main68_13;
          K_7 = inv_main68_14;
          N_7 = inv_main68_15;
          if (!((!(O_7 == (Q_7 + -1))) && (!(E_7 == 0))))
              abort ();
          inv_main76_0 = I_7;
          inv_main76_1 = L_7;
          inv_main76_2 = D_7;
          inv_main76_3 = Q_7;
          inv_main76_4 = J_7;
          inv_main76_5 = O_7;
          inv_main76_6 = G_7;
          inv_main76_7 = B_7;
          inv_main76_8 = A_7;
          inv_main76_9 = P_7;
          inv_main76_10 = M_7;
          inv_main76_11 = C_7;
          inv_main76_12 = H_7;
          inv_main76_13 = F_7;
          inv_main76_14 = K_7;
          inv_main76_15 = N_7;
          inv_main76_16 = E_7;
          Q1_2 = __VERIFIER_nondet_int ();
          M1_2 = __VERIFIER_nondet_int ();
          I2_2 = __VERIFIER_nondet_int ();
          E2_2 = __VERIFIER_nondet_int ();
          A2_2 = __VERIFIER_nondet_int ();
          V1_2 = __VERIFIER_nondet_int ();
          V2_2 = __VERIFIER_nondet_int ();
          R1_2 = __VERIFIER_nondet_int ();
          N1_2 = __VERIFIER_nondet_int ();
          N2_2 = __VERIFIER_nondet_int ();
          J1_2 = __VERIFIER_nondet_int ();
          J2_2 = __VERIFIER_nondet_int ();
          F1_2 = __VERIFIER_nondet_int ();
          F2_2 = __VERIFIER_nondet_int ();
          B1_2 = __VERIFIER_nondet_int ();
          B2_2 = __VERIFIER_nondet_int ();
          W2_2 = __VERIFIER_nondet_int ();
          S2_2 = __VERIFIER_nondet_int ();
          A_2 = __VERIFIER_nondet_int ();
          B_2 = __VERIFIER_nondet_int ();
          O1_2 = __VERIFIER_nondet_int ();
          C_2 = __VERIFIER_nondet_int ();
          O2_2 = __VERIFIER_nondet_int ();
          D_2 = __VERIFIER_nondet_int ();
          E_2 = __VERIFIER_nondet_int ();
          F_2 = __VERIFIER_nondet_int ();
          G_2 = __VERIFIER_nondet_int ();
          H_2 = __VERIFIER_nondet_int ();
          J_2 = __VERIFIER_nondet_int ();
          G1_2 = __VERIFIER_nondet_int ();
          K_2 = __VERIFIER_nondet_int ();
          G2_2 = __VERIFIER_nondet_int ();
          L_2 = __VERIFIER_nondet_int ();
          M_2 = __VERIFIER_nondet_int ();
          N_2 = __VERIFIER_nondet_int ();
          C1_2 = __VERIFIER_nondet_int ();
          O_2 = __VERIFIER_nondet_int ();
          C2_2 = __VERIFIER_nondet_int ();
          P_2 = __VERIFIER_nondet_int ();
          Q_2 = __VERIFIER_nondet_int ();
          U_2 = __VERIFIER_nondet_int ();
          V_2 = __VERIFIER_nondet_int ();
          W_2 = __VERIFIER_nondet_int ();
          X_2 = __VERIFIER_nondet_int ();
          Y_2 = __VERIFIER_nondet_int ();
          X1_2 = __VERIFIER_nondet_int ();
          Z_2 = __VERIFIER_nondet_int ();
          X2_2 = __VERIFIER_nondet_int ();
          T1_2 = __VERIFIER_nondet_int ();
          T2_2 = __VERIFIER_nondet_int ();
          P1_2 = __VERIFIER_nondet_int ();
          P2_2 = __VERIFIER_nondet_int ();
          L2_2 = __VERIFIER_nondet_int ();
          H1_2 = __VERIFIER_nondet_int ();
          H2_2 = __VERIFIER_nondet_int ();
          D1_2 = __VERIFIER_nondet_int ();
          D2_2 = __VERIFIER_nondet_int ();
          Y1_2 = __VERIFIER_nondet_int ();
          U1_2 = __VERIFIER_nondet_int ();
          U2_2 = __VERIFIER_nondet_int ();
          v_77_2 = __VERIFIER_nondet_int ();
          Z1_2 = inv_main76_0;
          Q2_2 = inv_main76_1;
          A1_2 = inv_main76_2;
          T_2 = inv_main76_3;
          S1_2 = inv_main76_4;
          S_2 = inv_main76_5;
          Y2_2 = inv_main76_6;
          K1_2 = inv_main76_7;
          R_2 = inv_main76_8;
          M2_2 = inv_main76_9;
          I_2 = inv_main76_10;
          E1_2 = inv_main76_11;
          W1_2 = inv_main76_12;
          L1_2 = inv_main76_13;
          R2_2 = inv_main76_14;
          K2_2 = inv_main76_15;
          I1_2 = inv_main76_16;
          if (!
              ((O2_2 == Y2_2) && (N2_2 == M_2) && (!(L2_2 == (E_2 + -1)))
               && (L2_2 == Y1_2) && (J2_2 == X1_2) && (I2_2 == K2_2)
               && (H2_2 == S1_2) && (G2_2 == T1_2) && (F2_2 == V1_2)
               && (E2_2 == G2_2) && (D2_2 == J_2) && (C2_2 == D_2)
               && (B2_2 == A1_2) && (A2_2 == I1_2) && (Y1_2 == S_2)
               && (X1_2 == I_2) && (V1_2 == G_2) && (U1_2 == R2_2)
               && (T1_2 == M2_2) && (R1_2 == Q_2) && (Q1_2 == N_2)
               && (P1_2 == W2_2) && (O1_2 == W1_2) && (N1_2 == J2_2)
               && (M1_2 == Q2_2) && (J1_2 == N2_2) && (H1_2 == O1_2)
               && (G1_2 == H2_2) && (F1_2 == K_2) && (D1_2 == T_2)
               && (C1_2 == O2_2) && (!(B1_2 == 0)) && (Z_2 == L1_2)
               && (Y_2 == M1_2) && (X_2 == H1_2) && (W_2 == F_2)
               && (V_2 == R_2) && (U_2 == S2_2) && (Q_2 == B2_2)
               && (P_2 == E1_2) && (O_2 == T2_2) && (N_2 == A2_2)
               && (M_2 == Z1_2) && (L_2 == E_2) && (K_2 == P_2)
               && (J_2 == V_2) && (H_2 == G1_2) && (!(G_2 == 0))
               && (F_2 == K1_2) && (E_2 == D1_2) && (D_2 == G_2)
               && (C_2 == L2_2) && (B_2 == 0) && (A_2 == B1_2)
               && (X2_2 == C1_2) && (W2_2 == U1_2) && (V2_2 == Y_2)
               && (U2_2 == W_2) && (T2_2 == Z_2) && (S2_2 == I2_2)
               && (((2 <= (E_2 + (-1 * L2_2))) && (B_2 == 1))
                   || ((!(2 <= (E_2 + (-1 * L2_2)))) && (B_2 == 0)))
               && (((1 <= (T_2 + (-1 * S_2))) && (G_2 == 1))
                   || ((!(1 <= (T_2 + (-1 * S_2)))) && (G_2 == 0)))
               && (((0 <= Y1_2) && (B1_2 == 1))
                   || ((!(0 <= Y1_2)) && (B1_2 == 0))) && (P2_2 == B1_2)
               && (v_77_2 == B_2)))
              abort ();
          inv_main101_0 = J1_2;
          inv_main101_1 = V2_2;
          inv_main101_2 = R1_2;
          inv_main101_3 = L_2;
          inv_main101_4 = H_2;
          inv_main101_5 = C_2;
          inv_main101_6 = X2_2;
          inv_main101_7 = U2_2;
          inv_main101_8 = D2_2;
          inv_main101_9 = E2_2;
          inv_main101_10 = N1_2;
          inv_main101_11 = F1_2;
          inv_main101_12 = X_2;
          inv_main101_13 = O_2;
          inv_main101_14 = P1_2;
          inv_main101_15 = U_2;
          inv_main101_16 = Q1_2;
          inv_main101_17 = C2_2;
          inv_main101_18 = F2_2;
          inv_main101_19 = A_2;
          inv_main101_20 = P2_2;
          inv_main101_21 = B_2;
          inv_main101_22 = v_77_2;
          C_26 = inv_main101_0;
          W_26 = inv_main101_1;
          I_26 = inv_main101_2;
          T_26 = inv_main101_3;
          J_26 = inv_main101_4;
          S_26 = inv_main101_5;
          R_26 = inv_main101_6;
          K_26 = inv_main101_7;
          P_26 = inv_main101_8;
          G_26 = inv_main101_9;
          E_26 = inv_main101_10;
          U_26 = inv_main101_11;
          V_26 = inv_main101_12;
          Q_26 = inv_main101_13;
          B_26 = inv_main101_14;
          O_26 = inv_main101_15;
          H_26 = inv_main101_16;
          M_26 = inv_main101_17;
          D_26 = inv_main101_18;
          N_26 = inv_main101_19;
          F_26 = inv_main101_20;
          A_26 = inv_main101_21;
          L_26 = inv_main101_22;
          if (!1)
              abort ();
          goto main_error;

      case 1:
          A_8 = __VERIFIER_nondet_int ();
          Q_8 = inv_main68_0;
          O_8 = inv_main68_1;
          E_8 = inv_main68_2;
          P_8 = inv_main68_3;
          C_8 = inv_main68_4;
          D_8 = inv_main68_5;
          J_8 = inv_main68_6;
          L_8 = inv_main68_7;
          M_8 = inv_main68_8;
          N_8 = inv_main68_9;
          G_8 = inv_main68_10;
          B_8 = inv_main68_11;
          H_8 = inv_main68_12;
          F_8 = inv_main68_13;
          K_8 = inv_main68_14;
          I_8 = inv_main68_15;
          if (!(D_8 == (P_8 + -1)))
              abort ();
          inv_main76_0 = Q_8;
          inv_main76_1 = O_8;
          inv_main76_2 = E_8;
          inv_main76_3 = P_8;
          inv_main76_4 = C_8;
          inv_main76_5 = D_8;
          inv_main76_6 = J_8;
          inv_main76_7 = L_8;
          inv_main76_8 = M_8;
          inv_main76_9 = N_8;
          inv_main76_10 = G_8;
          inv_main76_11 = B_8;
          inv_main76_12 = H_8;
          inv_main76_13 = F_8;
          inv_main76_14 = K_8;
          inv_main76_15 = I_8;
          inv_main76_16 = A_8;
          Q1_2 = __VERIFIER_nondet_int ();
          M1_2 = __VERIFIER_nondet_int ();
          I2_2 = __VERIFIER_nondet_int ();
          E2_2 = __VERIFIER_nondet_int ();
          A2_2 = __VERIFIER_nondet_int ();
          V1_2 = __VERIFIER_nondet_int ();
          V2_2 = __VERIFIER_nondet_int ();
          R1_2 = __VERIFIER_nondet_int ();
          N1_2 = __VERIFIER_nondet_int ();
          N2_2 = __VERIFIER_nondet_int ();
          J1_2 = __VERIFIER_nondet_int ();
          J2_2 = __VERIFIER_nondet_int ();
          F1_2 = __VERIFIER_nondet_int ();
          F2_2 = __VERIFIER_nondet_int ();
          B1_2 = __VERIFIER_nondet_int ();
          B2_2 = __VERIFIER_nondet_int ();
          W2_2 = __VERIFIER_nondet_int ();
          S2_2 = __VERIFIER_nondet_int ();
          A_2 = __VERIFIER_nondet_int ();
          B_2 = __VERIFIER_nondet_int ();
          O1_2 = __VERIFIER_nondet_int ();
          C_2 = __VERIFIER_nondet_int ();
          O2_2 = __VERIFIER_nondet_int ();
          D_2 = __VERIFIER_nondet_int ();
          E_2 = __VERIFIER_nondet_int ();
          F_2 = __VERIFIER_nondet_int ();
          G_2 = __VERIFIER_nondet_int ();
          H_2 = __VERIFIER_nondet_int ();
          J_2 = __VERIFIER_nondet_int ();
          G1_2 = __VERIFIER_nondet_int ();
          K_2 = __VERIFIER_nondet_int ();
          G2_2 = __VERIFIER_nondet_int ();
          L_2 = __VERIFIER_nondet_int ();
          M_2 = __VERIFIER_nondet_int ();
          N_2 = __VERIFIER_nondet_int ();
          C1_2 = __VERIFIER_nondet_int ();
          O_2 = __VERIFIER_nondet_int ();
          C2_2 = __VERIFIER_nondet_int ();
          P_2 = __VERIFIER_nondet_int ();
          Q_2 = __VERIFIER_nondet_int ();
          U_2 = __VERIFIER_nondet_int ();
          V_2 = __VERIFIER_nondet_int ();
          W_2 = __VERIFIER_nondet_int ();
          X_2 = __VERIFIER_nondet_int ();
          Y_2 = __VERIFIER_nondet_int ();
          X1_2 = __VERIFIER_nondet_int ();
          Z_2 = __VERIFIER_nondet_int ();
          X2_2 = __VERIFIER_nondet_int ();
          T1_2 = __VERIFIER_nondet_int ();
          T2_2 = __VERIFIER_nondet_int ();
          P1_2 = __VERIFIER_nondet_int ();
          P2_2 = __VERIFIER_nondet_int ();
          L2_2 = __VERIFIER_nondet_int ();
          H1_2 = __VERIFIER_nondet_int ();
          H2_2 = __VERIFIER_nondet_int ();
          D1_2 = __VERIFIER_nondet_int ();
          D2_2 = __VERIFIER_nondet_int ();
          Y1_2 = __VERIFIER_nondet_int ();
          U1_2 = __VERIFIER_nondet_int ();
          U2_2 = __VERIFIER_nondet_int ();
          v_77_2 = __VERIFIER_nondet_int ();
          Z1_2 = inv_main76_0;
          Q2_2 = inv_main76_1;
          A1_2 = inv_main76_2;
          T_2 = inv_main76_3;
          S1_2 = inv_main76_4;
          S_2 = inv_main76_5;
          Y2_2 = inv_main76_6;
          K1_2 = inv_main76_7;
          R_2 = inv_main76_8;
          M2_2 = inv_main76_9;
          I_2 = inv_main76_10;
          E1_2 = inv_main76_11;
          W1_2 = inv_main76_12;
          L1_2 = inv_main76_13;
          R2_2 = inv_main76_14;
          K2_2 = inv_main76_15;
          I1_2 = inv_main76_16;
          if (!
              ((O2_2 == Y2_2) && (N2_2 == M_2) && (!(L2_2 == (E_2 + -1)))
               && (L2_2 == Y1_2) && (J2_2 == X1_2) && (I2_2 == K2_2)
               && (H2_2 == S1_2) && (G2_2 == T1_2) && (F2_2 == V1_2)
               && (E2_2 == G2_2) && (D2_2 == J_2) && (C2_2 == D_2)
               && (B2_2 == A1_2) && (A2_2 == I1_2) && (Y1_2 == S_2)
               && (X1_2 == I_2) && (V1_2 == G_2) && (U1_2 == R2_2)
               && (T1_2 == M2_2) && (R1_2 == Q_2) && (Q1_2 == N_2)
               && (P1_2 == W2_2) && (O1_2 == W1_2) && (N1_2 == J2_2)
               && (M1_2 == Q2_2) && (J1_2 == N2_2) && (H1_2 == O1_2)
               && (G1_2 == H2_2) && (F1_2 == K_2) && (D1_2 == T_2)
               && (C1_2 == O2_2) && (!(B1_2 == 0)) && (Z_2 == L1_2)
               && (Y_2 == M1_2) && (X_2 == H1_2) && (W_2 == F_2)
               && (V_2 == R_2) && (U_2 == S2_2) && (Q_2 == B2_2)
               && (P_2 == E1_2) && (O_2 == T2_2) && (N_2 == A2_2)
               && (M_2 == Z1_2) && (L_2 == E_2) && (K_2 == P_2)
               && (J_2 == V_2) && (H_2 == G1_2) && (!(G_2 == 0))
               && (F_2 == K1_2) && (E_2 == D1_2) && (D_2 == G_2)
               && (C_2 == L2_2) && (B_2 == 0) && (A_2 == B1_2)
               && (X2_2 == C1_2) && (W2_2 == U1_2) && (V2_2 == Y_2)
               && (U2_2 == W_2) && (T2_2 == Z_2) && (S2_2 == I2_2)
               && (((2 <= (E_2 + (-1 * L2_2))) && (B_2 == 1))
                   || ((!(2 <= (E_2 + (-1 * L2_2)))) && (B_2 == 0)))
               && (((1 <= (T_2 + (-1 * S_2))) && (G_2 == 1))
                   || ((!(1 <= (T_2 + (-1 * S_2)))) && (G_2 == 0)))
               && (((0 <= Y1_2) && (B1_2 == 1))
                   || ((!(0 <= Y1_2)) && (B1_2 == 0))) && (P2_2 == B1_2)
               && (v_77_2 == B_2)))
              abort ();
          inv_main101_0 = J1_2;
          inv_main101_1 = V2_2;
          inv_main101_2 = R1_2;
          inv_main101_3 = L_2;
          inv_main101_4 = H_2;
          inv_main101_5 = C_2;
          inv_main101_6 = X2_2;
          inv_main101_7 = U2_2;
          inv_main101_8 = D2_2;
          inv_main101_9 = E2_2;
          inv_main101_10 = N1_2;
          inv_main101_11 = F1_2;
          inv_main101_12 = X_2;
          inv_main101_13 = O_2;
          inv_main101_14 = P1_2;
          inv_main101_15 = U_2;
          inv_main101_16 = Q1_2;
          inv_main101_17 = C2_2;
          inv_main101_18 = F2_2;
          inv_main101_19 = A_2;
          inv_main101_20 = P2_2;
          inv_main101_21 = B_2;
          inv_main101_22 = v_77_2;
          C_26 = inv_main101_0;
          W_26 = inv_main101_1;
          I_26 = inv_main101_2;
          T_26 = inv_main101_3;
          J_26 = inv_main101_4;
          S_26 = inv_main101_5;
          R_26 = inv_main101_6;
          K_26 = inv_main101_7;
          P_26 = inv_main101_8;
          G_26 = inv_main101_9;
          E_26 = inv_main101_10;
          U_26 = inv_main101_11;
          V_26 = inv_main101_12;
          Q_26 = inv_main101_13;
          B_26 = inv_main101_14;
          O_26 = inv_main101_15;
          H_26 = inv_main101_16;
          M_26 = inv_main101_17;
          D_26 = inv_main101_18;
          N_26 = inv_main101_19;
          F_26 = inv_main101_20;
          A_26 = inv_main101_21;
          L_26 = inv_main101_22;
          if (!1)
              abort ();
          goto main_error;

      case 2:
          Q1_15 = __VERIFIER_nondet_int ();
          M1_15 = __VERIFIER_nondet_int ();
          I1_15 = __VERIFIER_nondet_int ();
          E1_15 = __VERIFIER_nondet_int ();
          A1_15 = __VERIFIER_nondet_int ();
          Z1_15 = __VERIFIER_nondet_int ();
          V1_15 = __VERIFIER_nondet_int ();
          R1_15 = __VERIFIER_nondet_int ();
          N1_15 = __VERIFIER_nondet_int ();
          J1_15 = __VERIFIER_nondet_int ();
          F1_15 = __VERIFIER_nondet_int ();
          W1_15 = __VERIFIER_nondet_int ();
          S1_15 = __VERIFIER_nondet_int ();
          A_15 = __VERIFIER_nondet_int ();
          B_15 = __VERIFIER_nondet_int ();
          C_15 = __VERIFIER_nondet_int ();
          D_15 = __VERIFIER_nondet_int ();
          E_15 = __VERIFIER_nondet_int ();
          F_15 = __VERIFIER_nondet_int ();
          G_15 = __VERIFIER_nondet_int ();
          H_15 = __VERIFIER_nondet_int ();
          I_15 = __VERIFIER_nondet_int ();
          K_15 = __VERIFIER_nondet_int ();
          L_15 = __VERIFIER_nondet_int ();
          O_15 = __VERIFIER_nondet_int ();
          C2_15 = __VERIFIER_nondet_int ();
          Q_15 = __VERIFIER_nondet_int ();
          R_15 = __VERIFIER_nondet_int ();
          S_15 = __VERIFIER_nondet_int ();
          T_15 = __VERIFIER_nondet_int ();
          U_15 = __VERIFIER_nondet_int ();
          V_15 = __VERIFIER_nondet_int ();
          W_15 = __VERIFIER_nondet_int ();
          X_15 = __VERIFIER_nondet_int ();
          X1_15 = __VERIFIER_nondet_int ();
          Z_15 = __VERIFIER_nondet_int ();
          T1_15 = __VERIFIER_nondet_int ();
          P1_15 = __VERIFIER_nondet_int ();
          H1_15 = __VERIFIER_nondet_int ();
          D2_15 = __VERIFIER_nondet_int ();
          O1_15 = inv_main68_0;
          U1_15 = inv_main68_1;
          N_15 = inv_main68_2;
          A2_15 = inv_main68_3;
          B2_15 = inv_main68_4;
          D1_15 = inv_main68_5;
          C1_15 = inv_main68_6;
          M_15 = inv_main68_7;
          L1_15 = inv_main68_8;
          J_15 = inv_main68_9;
          G1_15 = inv_main68_10;
          Y_15 = inv_main68_11;
          B1_15 = inv_main68_12;
          K1_15 = inv_main68_13;
          P_15 = inv_main68_14;
          Y1_15 = inv_main68_15;
          if (!
              ((V1_15 == D1_15) && (T1_15 == X1_15) && (S1_15 == I1_15)
               && (R1_15 == E1_15) && (Q1_15 == A2_15) && (P1_15 == V1_15)
               && (N1_15 == H1_15) && (!(M1_15 == 0)) && (J1_15 == B_15)
               && (I1_15 == Y1_15) && (H1_15 == U1_15) && (F1_15 == I_15)
               && (E1_15 == 0) && (!(D1_15 == (A2_15 + -1)))
               && (A1_15 == M1_15) && (Z_15 == Y_15) && (X_15 == J_15)
               && (W_15 == W1_15) && (V_15 == (P1_15 + 1)) && (U_15 == B1_15)
               && (T_15 == Z_15) && (S_15 == U_15) && (R_15 == K_15)
               && (Q_15 == X_15) && (O_15 == R1_15) && (L_15 == Q1_15)
               && (K_15 == M_15) && (I_15 == O1_15) && (!(H_15 == 0))
               && (G_15 == F_15) && (F_15 == L1_15) && (E_15 == K1_15)
               && (D_15 == B2_15) && (C_15 == D_15) && (B_15 == P_15)
               && (A_15 == M1_15) && (D2_15 == Z1_15) && (C2_15 == E_15)
               && (Z1_15 == C1_15) && (X1_15 == G1_15)
               && (((!(1 <= (A2_15 + (-1 * D1_15)))) && (M1_15 == 0))
                   || ((1 <= (A2_15 + (-1 * D1_15))) && (M1_15 == 1)))
               && (((0 <= V1_15) && (H_15 == 1))
                   || ((!(0 <= V1_15)) && (H_15 == 0))) && (W1_15 == N_15)))
              abort ();
          inv_main68_0 = F1_15;
          inv_main68_1 = N1_15;
          inv_main68_2 = W_15;
          inv_main68_3 = L_15;
          inv_main68_4 = C_15;
          inv_main68_5 = V_15;
          inv_main68_6 = D2_15;
          inv_main68_7 = R_15;
          inv_main68_8 = G_15;
          inv_main68_9 = Q_15;
          inv_main68_10 = T1_15;
          inv_main68_11 = T_15;
          inv_main68_12 = S_15;
          inv_main68_13 = C2_15;
          inv_main68_14 = J1_15;
          inv_main68_15 = S1_15;
          goto inv_main68_0;

      default:
          abort ();
      }

    // return expression

}

