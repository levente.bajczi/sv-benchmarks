// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: tricera-benchmarks/apache-escape-absolute.i.v+cfa-reducer.c-1.smt2.gz_003.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "apache-escape-absolute.i.v+cfa-reducer.c-1.smt2.gz_003_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int inv_main67_0;
    int inv_main67_1;
    int inv_main67_2;
    int inv_main67_3;
    int inv_main67_4;
    int inv_main67_5;
    int inv_main67_6;
    int inv_main67_7;
    int inv_main67_8;
    int inv_main67_9;
    int inv_main67_10;
    int inv_main67_11;
    int inv_main67_12;
    int inv_main67_13;
    int inv_main67_14;
    int inv_main67_15;
    int inv_main7_0;
    int inv_main7_1;
    int inv_main7_2;
    int inv_main7_3;
    int inv_main7_4;
    int inv_main7_5;
    int inv_main7_6;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    int E_0;
    int F_0;
    int G_0;
    int A_23;
    int B_23;
    int C_23;
    int D_23;
    int E_23;
    int F_23;
    int G_23;
    int H_23;
    int I_23;
    int J_23;
    int K_23;
    int L_23;
    int M_23;
    int N_23;
    int O_23;
    int P_23;
    int Q_23;
    int R_23;
    int S_23;
    int T_23;
    int U_23;
    int V_23;
    int W_23;
    int X_23;
    int Y_23;
    int Z_23;
    int A1_23;
    int B1_23;
    int C1_23;
    int D1_23;
    int E1_23;
    int F1_23;
    int G1_23;
    int H1_23;
    int I1_23;
    int J1_23;
    int K1_23;
    int L1_23;
    int M1_23;
    int N1_23;
    int O1_23;
    int P1_23;
    int Q1_23;
    int R1_23;
    int S1_23;
    int T1_23;
    int U1_23;
    int V1_23;
    int W1_23;
    int X1_23;
    int Y1_23;
    int Z1_23;
    int A2_23;
    int B2_23;
    int C2_23;
    int D2_23;
    int E2_23;
    int v_57_23;
    int A_26;
    int B_26;
    int C_26;
    int D_26;
    int E_26;
    int F_26;
    int G_26;
    int H_26;
    int I_26;
    int J_26;
    int K_26;
    int L_26;
    int M_26;
    int N_26;
    int O_26;
    int P_26;



    // main logic
    goto main_init;

  main_init:
    if (!((A_0 == 0) && (E_0 == 0)))
        abort ();
    inv_main7_0 = E_0;
    inv_main7_1 = A_0;
    inv_main7_2 = G_0;
    inv_main7_3 = F_0;
    inv_main7_4 = B_0;
    inv_main7_5 = D_0;
    inv_main7_6 = C_0;
    Q1_23 = __VERIFIER_nondet_int ();
    M1_23 = __VERIFIER_nondet_int ();
    I1_23 = __VERIFIER_nondet_int ();
    E2_23 = __VERIFIER_nondet_int ();
    A1_23 = __VERIFIER_nondet_int ();
    A2_23 = __VERIFIER_nondet_int ();
    Z1_23 = __VERIFIER_nondet_int ();
    V1_23 = __VERIFIER_nondet_int ();
    R1_23 = __VERIFIER_nondet_int ();
    N1_23 = __VERIFIER_nondet_int ();
    J1_23 = __VERIFIER_nondet_int ();
    F1_23 = __VERIFIER_nondet_int ();
    B1_23 = __VERIFIER_nondet_int ();
    B2_23 = __VERIFIER_nondet_int ();
    W1_23 = __VERIFIER_nondet_int ();
    v_57_23 = __VERIFIER_nondet_int ();
    S1_23 = __VERIFIER_nondet_int ();
    A_23 = __VERIFIER_nondet_int ();
    B_23 = __VERIFIER_nondet_int ();
    O1_23 = __VERIFIER_nondet_int ();
    C_23 = __VERIFIER_nondet_int ();
    D_23 = __VERIFIER_nondet_int ();
    E_23 = __VERIFIER_nondet_int ();
    F_23 = __VERIFIER_nondet_int ();
    K1_23 = __VERIFIER_nondet_int ();
    H_23 = __VERIFIER_nondet_int ();
    I_23 = __VERIFIER_nondet_int ();
    J_23 = __VERIFIER_nondet_int ();
    G1_23 = __VERIFIER_nondet_int ();
    K_23 = __VERIFIER_nondet_int ();
    L_23 = __VERIFIER_nondet_int ();
    M_23 = __VERIFIER_nondet_int ();
    N_23 = __VERIFIER_nondet_int ();
    O_23 = __VERIFIER_nondet_int ();
    C2_23 = __VERIFIER_nondet_int ();
    P_23 = __VERIFIER_nondet_int ();
    Q_23 = __VERIFIER_nondet_int ();
    R_23 = __VERIFIER_nondet_int ();
    V_23 = __VERIFIER_nondet_int ();
    W_23 = __VERIFIER_nondet_int ();
    X_23 = __VERIFIER_nondet_int ();
    X1_23 = __VERIFIER_nondet_int ();
    Z_23 = __VERIFIER_nondet_int ();
    T1_23 = __VERIFIER_nondet_int ();
    P1_23 = __VERIFIER_nondet_int ();
    L1_23 = __VERIFIER_nondet_int ();
    H1_23 = __VERIFIER_nondet_int ();
    D1_23 = __VERIFIER_nondet_int ();
    D2_23 = __VERIFIER_nondet_int ();
    Y1_23 = __VERIFIER_nondet_int ();
    U1_23 = __VERIFIER_nondet_int ();
    S_23 = inv_main7_0;
    E1_23 = inv_main7_1;
    Y_23 = inv_main7_2;
    G_23 = inv_main7_3;
    U_23 = inv_main7_4;
    C1_23 = inv_main7_5;
    T_23 = inv_main7_6;
    if (!
        ((!(W1_23 == 0)) && (!(V1_23 == 0)) && (U1_23 == S_23)
         && (T1_23 == Z_23) && (!(S1_23 == 0)) && (R1_23 == I1_23)
         && (Q1_23 == R_23) && (P1_23 == S1_23) && (O1_23 == X1_23)
         && (M1_23 == X1_23) && (L1_23 == M_23) && (K1_23 == K_23)
         && (J1_23 == B_23) && (I1_23 == S1_23) && (H1_23 == A1_23)
         && (G1_23 == P1_23) && (F1_23 == E1_23) && (D1_23 == L1_23)
         && (B1_23 == J_23) && (A1_23 == K1_23) && (Z_23 == U1_23)
         && (X_23 == I_23) && (W_23 == Q_23) && (V_23 == D1_23)
         && (R_23 == A2_23) && (Q_23 == D2_23) && (P_23 == N1_23)
         && (O_23 == 0) && (N_23 == F_23) && (M_23 == V1_23) && (L_23 == A_23)
         && (K_23 == T_23) && (J_23 == F1_23) && (I_23 == E2_23)
         && (H_23 == C2_23) && (!(F_23 == 0)) && (E_23 == V1_23)
         && (C_23 == L_23) && (B_23 == W1_23) && (A_23 == E_23)
         && (E2_23 == Y1_23) && (D2_23 == F_23) && (C2_23 == N_23)
         && (B2_23 == T1_23) && (A2_23 == P_23) && (Z1_23 == B1_23)
         && (Y1_23 == D_23) && (-1000000 <= V1_23) && (-1000000 <= N1_23)
         && (-1000000 <= D_23) && (1 <= N1_23) && (1 <= D_23)
         && (!(0 <= (V1_23 + (-1 * D_23)))) && (0 <= V1_23)
         && (V1_23 <= 1000000) && (N1_23 <= 1000000) && (D_23 <= 1000000)
         && (((1 <= (E2_23 + (-1 * L1_23))) && (X1_23 == 1))
             || ((!(1 <= (E2_23 + (-1 * L1_23)))) && (X1_23 == 0)))
         && (((1 <= M_23) && (S1_23 == 1))
             || ((!(1 <= M_23)) && (S1_23 == 0)))
         && (((0 <= (D_23 + (-1 * V1_23))) && (F_23 == 1))
             || ((!(0 <= (D_23 + (-1 * V1_23)))) && (F_23 == 0)))
         && (((0 <= D1_23) && (O_23 == 1))
             || ((!(0 <= D1_23)) && (O_23 == 0))) && (!(X1_23 == 0))
         && (v_57_23 == O_23)))
        abort ();
    inv_main67_0 = B2_23;
    inv_main67_1 = Z1_23;
    inv_main67_2 = C_23;
    inv_main67_3 = X_23;
    inv_main67_4 = Q1_23;
    inv_main67_5 = V_23;
    inv_main67_6 = H1_23;
    inv_main67_7 = W_23;
    inv_main67_8 = H_23;
    inv_main67_9 = R1_23;
    inv_main67_10 = G1_23;
    inv_main67_11 = J1_23;
    inv_main67_12 = O1_23;
    inv_main67_13 = M1_23;
    inv_main67_14 = O_23;
    inv_main67_15 = v_57_23;
    C_26 = inv_main67_0;
    J_26 = inv_main67_1;
    M_26 = inv_main67_2;
    L_26 = inv_main67_3;
    B_26 = inv_main67_4;
    D_26 = inv_main67_5;
    O_26 = inv_main67_6;
    N_26 = inv_main67_7;
    G_26 = inv_main67_8;
    P_26 = inv_main67_9;
    F_26 = inv_main67_10;
    H_26 = inv_main67_11;
    K_26 = inv_main67_12;
    E_26 = inv_main67_13;
    A_26 = inv_main67_14;
    I_26 = inv_main67_15;
    if (!1)
        abort ();
    goto main_error;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  inv_main41:
    goto inv_main41;
  inv_main185:
    goto inv_main185;
  inv_main76:
    goto inv_main76;
  inv_main203:
    goto inv_main203;
  inv_main196:
    goto inv_main196;
  inv_main178:
    goto inv_main178;
  inv_main144:
    goto inv_main144;
  inv_main90:
    goto inv_main90;
  inv_main125:
    goto inv_main125;
  inv_main68:
    goto inv_main68;
  inv_main48:
    goto inv_main48;
  inv_main108:
    goto inv_main108;
  inv_main83:
    goto inv_main83;
  inv_main133:
    goto inv_main133;
  inv_main151:
    goto inv_main151;
  inv_main101:
    goto inv_main101;
  inv_main164:
    goto inv_main164;
  inv_main171:
    goto inv_main171;
  inv_main60:
    goto inv_main60;
  inv_main132:
    goto inv_main132;

    // return expression

}

