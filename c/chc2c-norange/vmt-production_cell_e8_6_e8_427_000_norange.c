// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: vmt-chc-benchmarks/vmt-production_cell_e8_6_e8_427_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "vmt-production_cell_e8_6_e8_427_000_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    _Bool state_0;
    _Bool state_1;
    _Bool state_2;
    _Bool state_3;
    _Bool state_4;
    _Bool state_5;
    _Bool state_6;
    _Bool state_7;
    _Bool state_8;
    _Bool state_9;
    _Bool state_10;
    _Bool state_11;
    _Bool state_12;
    _Bool state_13;
    _Bool state_14;
    _Bool state_15;
    _Bool state_16;
    _Bool state_17;
    _Bool state_18;
    _Bool state_19;
    _Bool state_20;
    _Bool state_21;
    _Bool state_22;
    _Bool state_23;
    _Bool state_24;
    _Bool state_25;
    _Bool state_26;
    _Bool state_27;
    _Bool state_28;
    _Bool state_29;
    _Bool state_30;
    _Bool state_31;
    _Bool state_32;
    _Bool A_0;
    _Bool B_0;
    _Bool C_0;
    _Bool D_0;
    _Bool E_0;
    _Bool F_0;
    _Bool G_0;
    _Bool H_0;
    _Bool I_0;
    _Bool J_0;
    _Bool K_0;
    _Bool L_0;
    _Bool M_0;
    _Bool N_0;
    _Bool O_0;
    _Bool P_0;
    _Bool Q_0;
    _Bool R_0;
    _Bool S_0;
    _Bool T_0;
    _Bool U_0;
    _Bool V_0;
    _Bool W_0;
    _Bool X_0;
    _Bool Y_0;
    _Bool Z_0;
    _Bool A1_0;
    _Bool B1_0;
    _Bool C1_0;
    _Bool D1_0;
    _Bool E1_0;
    _Bool F1_0;
    _Bool G1_0;
    _Bool A_1;
    _Bool B_1;
    _Bool C_1;
    _Bool D_1;
    _Bool E_1;
    _Bool F_1;
    _Bool G_1;
    _Bool H_1;
    _Bool I_1;
    _Bool J_1;
    _Bool K_1;
    _Bool L_1;
    _Bool M_1;
    _Bool N_1;
    _Bool O_1;
    _Bool P_1;
    _Bool Q_1;
    _Bool R_1;
    _Bool S_1;
    _Bool T_1;
    _Bool U_1;
    _Bool V_1;
    _Bool W_1;
    _Bool X_1;
    _Bool Y_1;
    _Bool Z_1;
    _Bool A1_1;
    _Bool B1_1;
    _Bool C1_1;
    _Bool D1_1;
    _Bool E1_1;
    _Bool F1_1;
    _Bool G1_1;
    _Bool H1_1;
    _Bool I1_1;
    _Bool J1_1;
    _Bool K1_1;
    _Bool L1_1;
    _Bool M1_1;
    _Bool N1_1;
    _Bool O1_1;
    _Bool P1_1;
    _Bool Q1_1;
    _Bool R1_1;
    _Bool S1_1;
    _Bool T1_1;
    _Bool U1_1;
    _Bool V1_1;
    _Bool W1_1;
    _Bool X1_1;
    _Bool Y1_1;
    _Bool Z1_1;
    _Bool A2_1;
    _Bool B2_1;
    _Bool C2_1;
    _Bool D2_1;
    _Bool E2_1;
    _Bool F2_1;
    _Bool G2_1;
    _Bool H2_1;
    _Bool I2_1;
    _Bool J2_1;
    _Bool K2_1;
    _Bool L2_1;
    _Bool M2_1;
    _Bool N2_1;
    _Bool A_2;
    _Bool B_2;
    _Bool C_2;
    _Bool D_2;
    _Bool E_2;
    _Bool F_2;
    _Bool G_2;
    _Bool H_2;
    _Bool I_2;
    _Bool J_2;
    _Bool K_2;
    _Bool L_2;
    _Bool M_2;
    _Bool N_2;
    _Bool O_2;
    _Bool P_2;
    _Bool Q_2;
    _Bool R_2;
    _Bool S_2;
    _Bool T_2;
    _Bool U_2;
    _Bool V_2;
    _Bool W_2;
    _Bool X_2;
    _Bool Y_2;
    _Bool Z_2;
    _Bool A1_2;
    _Bool B1_2;
    _Bool C1_2;
    _Bool D1_2;
    _Bool E1_2;
    _Bool F1_2;
    _Bool G1_2;



    // main logic
    goto main_init;

  main_init:
    if (!
        (((O_0 && M_0) == F_0) && ((Y_0 && X_0) == E1_0)
         && (C_0 == (B_0 && A_0)) && (C_0 == A1_0) && (!(D_0 == I_0))
         && (E_0 == (D_0 && F1_0)) && (E_0 == V_0) && (!(F_0 == J_0))
         && (G_0 == P_0) && (H_0 == (F_0 && G1_0)) && (H_0 == W_0)
         && (I_0 == B1_0) && (J_0 == C1_0) && (L_0 == K_0) && (L_0 == T_0)
         && (N_0 == M_0) && (N_0 == U_0) && (V_0 == A_0) && (W_0 == B_0)
         && (A1_0 == Z_0) && (B1_0 == X_0) && (C1_0 == Y_0) && (Q_0 || (!A_0)
                                                                || (!B_0))
         && ((!U_0) || (!T_0) || S_0) && ((Q_0 == D1_0) || (B_0 && A_0))
         && ((U_0 && T_0)
             || (S_0 ==
                 (R_0 && ((!E1_0) || (!A_0) || (!B_0))
                  && (((!A_0) && (!E1_0)) || ((!B_0) && (!A_0))
                      || ((!B_0) && (!E1_0)))))) && ((!D1_0) || (!E1_0))
         && ((!Z_0) || (O_0 == R_0)) && (Z_0 || R_0) && G1_0 && F1_0 && G_0
         && ((O_0 && K_0) == D_0)))
        abort ();
    state_0 = N_0;
    state_1 = U_0;
    state_2 = L_0;
    state_3 = T_0;
    state_4 = J_0;
    state_5 = C1_0;
    state_6 = Y_0;
    state_7 = I_0;
    state_8 = B1_0;
    state_9 = X_0;
    state_10 = H_0;
    state_11 = W_0;
    state_12 = E_0;
    state_13 = V_0;
    state_14 = C_0;
    state_15 = A1_0;
    state_16 = S_0;
    state_17 = Z_0;
    state_18 = O_0;
    state_19 = R_0;
    state_20 = E1_0;
    state_21 = B_0;
    state_22 = A_0;
    state_23 = Q_0;
    state_24 = D1_0;
    state_25 = M_0;
    state_26 = F_0;
    state_27 = G_0;
    state_28 = P_0;
    state_29 = K_0;
    state_30 = D_0;
    state_31 = G1_0;
    state_32 = F1_0;
    Q1_1 = __VERIFIER_nondet__Bool ();
    M1_1 = __VERIFIER_nondet__Bool ();
    I1_1 = __VERIFIER_nondet__Bool ();
    I2_1 = __VERIFIER_nondet__Bool ();
    E1_1 = __VERIFIER_nondet__Bool ();
    E2_1 = __VERIFIER_nondet__Bool ();
    A2_1 = __VERIFIER_nondet__Bool ();
    Z1_1 = __VERIFIER_nondet__Bool ();
    V1_1 = __VERIFIER_nondet__Bool ();
    R1_1 = __VERIFIER_nondet__Bool ();
    N1_1 = __VERIFIER_nondet__Bool ();
    J1_1 = __VERIFIER_nondet__Bool ();
    J2_1 = __VERIFIER_nondet__Bool ();
    F1_1 = __VERIFIER_nondet__Bool ();
    F2_1 = __VERIFIER_nondet__Bool ();
    B2_1 = __VERIFIER_nondet__Bool ();
    W1_1 = __VERIFIER_nondet__Bool ();
    S1_1 = __VERIFIER_nondet__Bool ();
    O1_1 = __VERIFIER_nondet__Bool ();
    K1_1 = __VERIFIER_nondet__Bool ();
    G1_1 = __VERIFIER_nondet__Bool ();
    G2_1 = __VERIFIER_nondet__Bool ();
    C2_1 = __VERIFIER_nondet__Bool ();
    X1_1 = __VERIFIER_nondet__Bool ();
    T1_1 = __VERIFIER_nondet__Bool ();
    P1_1 = __VERIFIER_nondet__Bool ();
    L1_1 = __VERIFIER_nondet__Bool ();
    H1_1 = __VERIFIER_nondet__Bool ();
    H2_1 = __VERIFIER_nondet__Bool ();
    D1_1 = __VERIFIER_nondet__Bool ();
    D2_1 = __VERIFIER_nondet__Bool ();
    Y1_1 = __VERIFIER_nondet__Bool ();
    U1_1 = __VERIFIER_nondet__Bool ();
    N_1 = state_0;
    U_1 = state_1;
    L_1 = state_2;
    T_1 = state_3;
    J_1 = state_4;
    C1_1 = state_5;
    Y_1 = state_6;
    I_1 = state_7;
    B1_1 = state_8;
    X_1 = state_9;
    H_1 = state_10;
    W_1 = state_11;
    E_1 = state_12;
    V_1 = state_13;
    C_1 = state_14;
    A1_1 = state_15;
    S_1 = state_16;
    Z_1 = state_17;
    O_1 = state_18;
    R_1 = state_19;
    L2_1 = state_20;
    B_1 = state_21;
    A_1 = state_22;
    Q_1 = state_23;
    K2_1 = state_24;
    M_1 = state_25;
    F_1 = state_26;
    G_1 = state_27;
    P_1 = state_28;
    K_1 = state_29;
    D_1 = state_30;
    N2_1 = state_31;
    M2_1 = state_32;
    if (!
        (((V1_1 && T1_1) == N1_1) && ((F2_1 && E2_1) == E1_1)
         && ((X_1 && Y_1) == L2_1) && ((M_1 && O_1) == F_1)
         && ((K_1 && O_1) == D_1) && (I1_1 == H1_1) && (K1_1 == J1_1)
         && (M1_1 == (L1_1 && F1_1 && ((!D_1) || (!M2_1))))
         && (O1_1 == (N1_1 && G1_1 && ((!F_1) || (!N2_1))))
         && (P1_1 == (D_1 && (!L1_1))) && (Q1_1 == (F_1 && (!N1_1)))
         && (S1_1 == ((!K_1) && R1_1)) && (U1_1 == ((!M_1) && T1_1))
         && (W1_1 == H1_1) && (A2_1 == S1_1) && (B2_1 == U1_1)
         && (C2_1 == M1_1) && (C2_1 == X1_1) && (D2_1 == O1_1)
         && (D2_1 == Y1_1) && (H2_1 == K1_1) && (H2_1 == G2_1)
         && (I2_1 == P1_1) && (I2_1 == E2_1) && (J2_1 == Q1_1)
         && (J2_1 == F2_1) && (C1_1 == Y_1) && (B1_1 == X_1) && (A1_1 == Z_1)
         && (W_1 == B_1) && (V_1 == A_1) && (N_1 == U_1) && (!(M_1 == F1_1))
         && (L_1 == T_1) && (!(K_1 == G1_1)) && (J_1 == C1_1) && (I_1 == B1_1)
         && (H_1 == W_1) && (G_1 == P_1) && (E_1 == V_1) && (C_1 == A1_1)
         && ((!Y1_1) || (!X1_1) || J1_1) && ((!B2_1) || (!A2_1) || I1_1)
         && (S_1 || (!T_1) || (!U_1)) && (Q_1 || (!A_1) || (!B_1))
         && ((J1_1 == D1_1) || (Y1_1 && X1_1)) && ((B2_1 && A2_1)
                                                   || (I1_1 ==
                                                       (Z1_1
                                                        && ((!Y1_1) || (!X1_1)
                                                            || (!E1_1))
                                                        &&
                                                        (((!Y1_1) && (!X1_1))
                                                         || ((!Y1_1)
                                                             && (!E1_1))
                                                         || ((!X1_1)
                                                             && (!E1_1))))))
         && ((T_1 && U_1)
             || (S_1 ==
                 (R_1 && ((!L2_1) || (!A_1) || (!B_1))
                  && (((!A_1) && (!L2_1)) || ((!B_1) && (!L2_1))
                      || ((!A_1) && (!B_1)))))) && ((Q_1 == K2_1) || (A_1
                                                                      && B_1))
         && (E1_1 || (A1_1 == D1_1)) && ((!E1_1) || (!D1_1)) && ((!G2_1)
                                                                 || (Z1_1 ==
                                                                     V1_1))
         && (G2_1 || Z1_1) && ((!Z_1) || (O_1 == R_1)) && (R_1 || Z_1)
         && ((V1_1 && R1_1) == L1_1)))
        abort ();
    state_0 = U1_1;
    state_1 = B2_1;
    state_2 = S1_1;
    state_3 = A2_1;
    state_4 = Q1_1;
    state_5 = J2_1;
    state_6 = F2_1;
    state_7 = P1_1;
    state_8 = I2_1;
    state_9 = E2_1;
    state_10 = O1_1;
    state_11 = D2_1;
    state_12 = M1_1;
    state_13 = C2_1;
    state_14 = K1_1;
    state_15 = H2_1;
    state_16 = I1_1;
    state_17 = G2_1;
    state_18 = V1_1;
    state_19 = Z1_1;
    state_20 = E1_1;
    state_21 = Y1_1;
    state_22 = X1_1;
    state_23 = J1_1;
    state_24 = D1_1;
    state_25 = T1_1;
    state_26 = N1_1;
    state_27 = H1_1;
    state_28 = W1_1;
    state_29 = R1_1;
    state_30 = L1_1;
    state_31 = G1_1;
    state_32 = F1_1;
    goto state_0;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  state_0:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          N_2 = state_0;
          U_2 = state_1;
          L_2 = state_2;
          T_2 = state_3;
          J_2 = state_4;
          C1_2 = state_5;
          Y_2 = state_6;
          I_2 = state_7;
          B1_2 = state_8;
          X_2 = state_9;
          H_2 = state_10;
          W_2 = state_11;
          E_2 = state_12;
          V_2 = state_13;
          C_2 = state_14;
          A1_2 = state_15;
          S_2 = state_16;
          Z_2 = state_17;
          O_2 = state_18;
          R_2 = state_19;
          E1_2 = state_20;
          B_2 = state_21;
          A_2 = state_22;
          Q_2 = state_23;
          D1_2 = state_24;
          M_2 = state_25;
          F_2 = state_26;
          G_2 = state_27;
          P_2 = state_28;
          K_2 = state_29;
          D_2 = state_30;
          G1_2 = state_31;
          F1_2 = state_32;
          if (!(!P_2))
              abort ();
          goto main_error;

      case 1:
          Q1_1 = __VERIFIER_nondet__Bool ();
          M1_1 = __VERIFIER_nondet__Bool ();
          I1_1 = __VERIFIER_nondet__Bool ();
          I2_1 = __VERIFIER_nondet__Bool ();
          E1_1 = __VERIFIER_nondet__Bool ();
          E2_1 = __VERIFIER_nondet__Bool ();
          A2_1 = __VERIFIER_nondet__Bool ();
          Z1_1 = __VERIFIER_nondet__Bool ();
          V1_1 = __VERIFIER_nondet__Bool ();
          R1_1 = __VERIFIER_nondet__Bool ();
          N1_1 = __VERIFIER_nondet__Bool ();
          J1_1 = __VERIFIER_nondet__Bool ();
          J2_1 = __VERIFIER_nondet__Bool ();
          F1_1 = __VERIFIER_nondet__Bool ();
          F2_1 = __VERIFIER_nondet__Bool ();
          B2_1 = __VERIFIER_nondet__Bool ();
          W1_1 = __VERIFIER_nondet__Bool ();
          S1_1 = __VERIFIER_nondet__Bool ();
          O1_1 = __VERIFIER_nondet__Bool ();
          K1_1 = __VERIFIER_nondet__Bool ();
          G1_1 = __VERIFIER_nondet__Bool ();
          G2_1 = __VERIFIER_nondet__Bool ();
          C2_1 = __VERIFIER_nondet__Bool ();
          X1_1 = __VERIFIER_nondet__Bool ();
          T1_1 = __VERIFIER_nondet__Bool ();
          P1_1 = __VERIFIER_nondet__Bool ();
          L1_1 = __VERIFIER_nondet__Bool ();
          H1_1 = __VERIFIER_nondet__Bool ();
          H2_1 = __VERIFIER_nondet__Bool ();
          D1_1 = __VERIFIER_nondet__Bool ();
          D2_1 = __VERIFIER_nondet__Bool ();
          Y1_1 = __VERIFIER_nondet__Bool ();
          U1_1 = __VERIFIER_nondet__Bool ();
          N_1 = state_0;
          U_1 = state_1;
          L_1 = state_2;
          T_1 = state_3;
          J_1 = state_4;
          C1_1 = state_5;
          Y_1 = state_6;
          I_1 = state_7;
          B1_1 = state_8;
          X_1 = state_9;
          H_1 = state_10;
          W_1 = state_11;
          E_1 = state_12;
          V_1 = state_13;
          C_1 = state_14;
          A1_1 = state_15;
          S_1 = state_16;
          Z_1 = state_17;
          O_1 = state_18;
          R_1 = state_19;
          L2_1 = state_20;
          B_1 = state_21;
          A_1 = state_22;
          Q_1 = state_23;
          K2_1 = state_24;
          M_1 = state_25;
          F_1 = state_26;
          G_1 = state_27;
          P_1 = state_28;
          K_1 = state_29;
          D_1 = state_30;
          N2_1 = state_31;
          M2_1 = state_32;
          if (!
              (((V1_1 && T1_1) == N1_1) && ((F2_1 && E2_1) == E1_1)
               && ((X_1 && Y_1) == L2_1) && ((M_1 && O_1) == F_1)
               && ((K_1 && O_1) == D_1) && (I1_1 == H1_1) && (K1_1 == J1_1)
               && (M1_1 == (L1_1 && F1_1 && ((!D_1) || (!M2_1))))
               && (O1_1 == (N1_1 && G1_1 && ((!F_1) || (!N2_1))))
               && (P1_1 == (D_1 && (!L1_1))) && (Q1_1 == (F_1 && (!N1_1)))
               && (S1_1 == ((!K_1) && R1_1)) && (U1_1 == ((!M_1) && T1_1))
               && (W1_1 == H1_1) && (A2_1 == S1_1) && (B2_1 == U1_1)
               && (C2_1 == M1_1) && (C2_1 == X1_1) && (D2_1 == O1_1)
               && (D2_1 == Y1_1) && (H2_1 == K1_1) && (H2_1 == G2_1)
               && (I2_1 == P1_1) && (I2_1 == E2_1) && (J2_1 == Q1_1)
               && (J2_1 == F2_1) && (C1_1 == Y_1) && (B1_1 == X_1)
               && (A1_1 == Z_1) && (W_1 == B_1) && (V_1 == A_1)
               && (N_1 == U_1) && (!(M_1 == F1_1)) && (L_1 == T_1)
               && (!(K_1 == G1_1)) && (J_1 == C1_1) && (I_1 == B1_1)
               && (H_1 == W_1) && (G_1 == P_1) && (E_1 == V_1)
               && (C_1 == A1_1) && ((!Y1_1) || (!X1_1) || J1_1) && ((!B2_1)
                                                                    || (!A2_1)
                                                                    || I1_1)
               && (S_1 || (!T_1) || (!U_1)) && (Q_1 || (!A_1) || (!B_1))
               && ((J1_1 == D1_1) || (Y1_1 && X1_1)) && ((B2_1 && A2_1)
                                                         || (I1_1 ==
                                                             (Z1_1
                                                              && ((!Y1_1)
                                                                  || (!X1_1)
                                                                  || (!E1_1))
                                                              &&
                                                              (((!Y1_1)
                                                                && (!X1_1))
                                                               || ((!Y1_1)
                                                                   && (!E1_1))
                                                               || ((!X1_1)
                                                                   &&
                                                                   (!E1_1))))))
               && ((T_1 && U_1)
                   || (S_1 ==
                       (R_1 && ((!L2_1) || (!A_1) || (!B_1))
                        && (((!A_1) && (!L2_1)) || ((!B_1) && (!L2_1))
                            || ((!A_1) && (!B_1)))))) && ((Q_1 == K2_1)
                                                          || (A_1 && B_1))
               && (E1_1 || (A1_1 == D1_1)) && ((!E1_1) || (!D1_1)) && ((!G2_1)
                                                                       ||
                                                                       (Z1_1
                                                                        ==
                                                                        V1_1))
               && (G2_1 || Z1_1) && ((!Z_1) || (O_1 == R_1)) && (R_1 || Z_1)
               && ((V1_1 && R1_1) == L1_1)))
              abort ();
          state_0 = U1_1;
          state_1 = B2_1;
          state_2 = S1_1;
          state_3 = A2_1;
          state_4 = Q1_1;
          state_5 = J2_1;
          state_6 = F2_1;
          state_7 = P1_1;
          state_8 = I2_1;
          state_9 = E2_1;
          state_10 = O1_1;
          state_11 = D2_1;
          state_12 = M1_1;
          state_13 = C2_1;
          state_14 = K1_1;
          state_15 = H2_1;
          state_16 = I1_1;
          state_17 = G2_1;
          state_18 = V1_1;
          state_19 = Z1_1;
          state_20 = E1_1;
          state_21 = Y1_1;
          state_22 = X1_1;
          state_23 = J1_1;
          state_24 = D1_1;
          state_25 = T1_1;
          state_26 = N1_1;
          state_27 = H1_1;
          state_28 = W1_1;
          state_29 = R1_1;
          state_30 = L1_1;
          state_31 = G1_1;
          state_32 = F1_1;
          goto state_0;

      default:
          abort ();
      }

    // return expression

}

