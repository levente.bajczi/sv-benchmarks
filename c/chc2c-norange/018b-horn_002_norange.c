// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: eldarica-misc/018b-horn_002.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "018b-horn_002_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int INV1_0;
    int INV1_1;
    int INV1_2;
    int INV1_3;
    int INV1_4;
    int INV1_5;
    int INV1_6;
    int INV1_7;
    int A_8;
    int B_8;
    int C_8;
    int D_8;
    int E_8;
    int F_8;
    int G_8;
    int H_8;
    int I_8;
    int J_8;
    int A_9;
    int B_9;
    int C_9;
    int D_9;
    int E_9;
    int F_9;
    int G_9;
    int H_9;
    int I_9;
    int J_9;
    int K_9;
    int L_9;
    int A_10;
    int B_10;
    int C_10;
    int D_10;
    int E_10;
    int F_10;
    int G_10;
    int H_10;
    int I_10;
    int J_10;
    int A_11;
    int B_11;
    int C_11;
    int D_11;
    int E_11;
    int F_11;
    int G_11;
    int H_11;
    int A_16;
    int B_16;
    int C_16;
    int D_16;
    int E_16;
    int F_16;
    int G_16;
    int H_16;



    // main logic
    goto main_init;

  main_init:
    if (!
        ((F_11 == 2) && (E_11 == 1) && (D_11 == 1) && (C_11 == 0)
         && (B_11 == 1) && (A_11 == 1) && (H_11 <= 0) && (G_11 <= 0)
         && (G_11 == H_11)))
        abort ();
    INV1_0 = A_11;
    INV1_1 = B_11;
    INV1_2 = C_11;
    INV1_3 = D_11;
    INV1_4 = E_11;
    INV1_5 = F_11;
    INV1_6 = G_11;
    INV1_7 = H_11;
    B_10 = __VERIFIER_nondet_int ();
    C_10 = __VERIFIER_nondet_int ();
    A_10 = INV1_0;
    I_10 = INV1_1;
    J_10 = INV1_2;
    D_10 = INV1_3;
    E_10 = INV1_4;
    F_10 = INV1_5;
    G_10 = INV1_6;
    H_10 = INV1_7;
    if (!
        ((I_10 == (B_10 + -1)) && (G_10 == H_10)
         && (!((D_10 + (-1 * E_10)) >= 1)) && (A_10 >= I_10) && (H_10 <= 0)
         && (G_10 <= 0) && (J_10 == (C_10 + -2))))
        abort ();
    INV1_0 = A_10;
    INV1_1 = B_10;
    INV1_2 = C_10;
    INV1_3 = D_10;
    INV1_4 = E_10;
    INV1_5 = F_10;
    INV1_6 = G_10;
    INV1_7 = H_10;
    goto INV1_2;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  INV2:
    goto INV2;
  INV3:
    goto INV3;
  INV4:
    goto INV4;
  INV1_0:
    switch (__VERIFIER_nondet_int ())
      {
      case 0:
          C_16 = INV1_0;
          D_16 = INV1_1;
          A_16 = INV1_2;
          E_16 = INV1_3;
          F_16 = INV1_4;
          B_16 = INV1_5;
          G_16 = INV1_6;
          H_16 = INV1_7;
          if (!
              ((!(A_16 == B_16)) && (!((E_16 + (-1 * F_16)) >= 1))
               && (!(C_16 >= D_16)) && (H_16 <= 0) && (G_16 <= 0)
               && (G_16 == H_16)))
              abort ();
          goto main_error;

      case 1:
          E_8 = __VERIFIER_nondet_int ();
          F_8 = __VERIFIER_nondet_int ();
          A_8 = INV1_0;
          B_8 = INV1_1;
          C_8 = INV1_2;
          D_8 = INV1_3;
          I_8 = INV1_4;
          J_8 = INV1_5;
          G_8 = INV1_6;
          H_8 = INV1_7;
          if (!
              ((I_8 == (E_8 + -1)) && (G_8 == H_8)
               && ((D_8 + (-1 * I_8)) >= 1) && (!(A_8 >= B_8)) && (H_8 <= 0)
               && (G_8 <= 0) && (J_8 == (F_8 + -2))))
              abort ();
          INV1_0 = A_8;
          INV1_1 = B_8;
          INV1_2 = C_8;
          INV1_3 = D_8;
          INV1_4 = E_8;
          INV1_5 = F_8;
          INV1_6 = G_8;
          INV1_7 = H_8;
          B_10 = __VERIFIER_nondet_int ();
          C_10 = __VERIFIER_nondet_int ();
          A_10 = INV1_0;
          I_10 = INV1_1;
          J_10 = INV1_2;
          D_10 = INV1_3;
          E_10 = INV1_4;
          F_10 = INV1_5;
          G_10 = INV1_6;
          H_10 = INV1_7;
          if (!
              ((I_10 == (B_10 + -1)) && (G_10 == H_10)
               && (!((D_10 + (-1 * E_10)) >= 1)) && (A_10 >= I_10)
               && (H_10 <= 0) && (G_10 <= 0) && (J_10 == (C_10 + -2))))
              abort ();
          INV1_0 = A_10;
          INV1_1 = B_10;
          INV1_2 = C_10;
          INV1_3 = D_10;
          INV1_4 = E_10;
          INV1_5 = F_10;
          INV1_6 = G_10;
          INV1_7 = H_10;
          goto INV1_2;

      case 2:
          B_10 = __VERIFIER_nondet_int ();
          C_10 = __VERIFIER_nondet_int ();
          A_10 = INV1_0;
          I_10 = INV1_1;
          J_10 = INV1_2;
          D_10 = INV1_3;
          E_10 = INV1_4;
          F_10 = INV1_5;
          G_10 = INV1_6;
          H_10 = INV1_7;
          if (!
              ((I_10 == (B_10 + -1)) && (G_10 == H_10)
               && (!((D_10 + (-1 * E_10)) >= 1)) && (A_10 >= I_10)
               && (H_10 <= 0) && (G_10 <= 0) && (J_10 == (C_10 + -2))))
              abort ();
          INV1_0 = A_10;
          INV1_1 = B_10;
          INV1_2 = C_10;
          INV1_3 = D_10;
          INV1_4 = E_10;
          INV1_5 = F_10;
          INV1_6 = G_10;
          INV1_7 = H_10;
          B_10 = __VERIFIER_nondet_int ();
          C_10 = __VERIFIER_nondet_int ();
          A_10 = INV1_0;
          I_10 = INV1_1;
          J_10 = INV1_2;
          D_10 = INV1_3;
          E_10 = INV1_4;
          F_10 = INV1_5;
          G_10 = INV1_6;
          H_10 = INV1_7;
          if (!
              ((I_10 == (B_10 + -1)) && (G_10 == H_10)
               && (!((D_10 + (-1 * E_10)) >= 1)) && (A_10 >= I_10)
               && (H_10 <= 0) && (G_10 <= 0) && (J_10 == (C_10 + -2))))
              abort ();
          INV1_0 = A_10;
          INV1_1 = B_10;
          INV1_2 = C_10;
          INV1_3 = D_10;
          INV1_4 = E_10;
          INV1_5 = F_10;
          INV1_6 = G_10;
          INV1_7 = H_10;
          goto INV1_2;

      case 3:
          B_10 = __VERIFIER_nondet_int ();
          C_10 = __VERIFIER_nondet_int ();
          A_10 = INV1_0;
          I_10 = INV1_1;
          J_10 = INV1_2;
          D_10 = INV1_3;
          E_10 = INV1_4;
          F_10 = INV1_5;
          G_10 = INV1_6;
          H_10 = INV1_7;
          if (!
              ((I_10 == (B_10 + -1)) && (G_10 == H_10)
               && (!((D_10 + (-1 * E_10)) >= 1)) && (A_10 >= I_10)
               && (H_10 <= 0) && (G_10 <= 0) && (J_10 == (C_10 + -2))))
              abort ();
          INV1_0 = A_10;
          INV1_1 = B_10;
          INV1_2 = C_10;
          INV1_3 = D_10;
          INV1_4 = E_10;
          INV1_5 = F_10;
          INV1_6 = G_10;
          INV1_7 = H_10;
          goto INV1_2;

      default:
          abort ();
      }
  INV1_1:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          B_9 = __VERIFIER_nondet_int ();
          C_9 = __VERIFIER_nondet_int ();
          E_9 = __VERIFIER_nondet_int ();
          F_9 = __VERIFIER_nondet_int ();
          A_9 = INV1_0;
          I_9 = INV1_1;
          J_9 = INV1_2;
          D_9 = INV1_3;
          K_9 = INV1_4;
          L_9 = INV1_5;
          G_9 = INV1_6;
          H_9 = INV1_7;
          if (!
              ((K_9 == (E_9 + -1)) && (J_9 == (C_9 + -2))
               && (I_9 == (B_9 + -1)) && (G_9 == H_9)
               && ((D_9 + (-1 * K_9)) >= 1) && (A_9 >= I_9) && (H_9 <= 0)
               && (G_9 <= 0) && (L_9 == (F_9 + -2))))
              abort ();
          INV1_0 = A_9;
          INV1_1 = B_9;
          INV1_2 = C_9;
          INV1_3 = D_9;
          INV1_4 = E_9;
          INV1_5 = F_9;
          INV1_6 = G_9;
          INV1_7 = H_9;
          goto INV1_0;

      case 1:
          B_10 = __VERIFIER_nondet_int ();
          C_10 = __VERIFIER_nondet_int ();
          A_10 = INV1_0;
          I_10 = INV1_1;
          J_10 = INV1_2;
          D_10 = INV1_3;
          E_10 = INV1_4;
          F_10 = INV1_5;
          G_10 = INV1_6;
          H_10 = INV1_7;
          if (!
              ((I_10 == (B_10 + -1)) && (G_10 == H_10)
               && (!((D_10 + (-1 * E_10)) >= 1)) && (A_10 >= I_10)
               && (H_10 <= 0) && (G_10 <= 0) && (J_10 == (C_10 + -2))))
              abort ();
          INV1_0 = A_10;
          INV1_1 = B_10;
          INV1_2 = C_10;
          INV1_3 = D_10;
          INV1_4 = E_10;
          INV1_5 = F_10;
          INV1_6 = G_10;
          INV1_7 = H_10;
          goto INV1_2;

      default:
          abort ();
      }
  INV1_2:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          E_8 = __VERIFIER_nondet_int ();
          F_8 = __VERIFIER_nondet_int ();
          A_8 = INV1_0;
          B_8 = INV1_1;
          C_8 = INV1_2;
          D_8 = INV1_3;
          I_8 = INV1_4;
          J_8 = INV1_5;
          G_8 = INV1_6;
          H_8 = INV1_7;
          if (!
              ((I_8 == (E_8 + -1)) && (G_8 == H_8)
               && ((D_8 + (-1 * I_8)) >= 1) && (!(A_8 >= B_8)) && (H_8 <= 0)
               && (G_8 <= 0) && (J_8 == (F_8 + -2))))
              abort ();
          INV1_0 = A_8;
          INV1_1 = B_8;
          INV1_2 = C_8;
          INV1_3 = D_8;
          INV1_4 = E_8;
          INV1_5 = F_8;
          INV1_6 = G_8;
          INV1_7 = H_8;
          goto INV1_1;

      case 1:
          B_10 = __VERIFIER_nondet_int ();
          C_10 = __VERIFIER_nondet_int ();
          A_10 = INV1_0;
          I_10 = INV1_1;
          J_10 = INV1_2;
          D_10 = INV1_3;
          E_10 = INV1_4;
          F_10 = INV1_5;
          G_10 = INV1_6;
          H_10 = INV1_7;
          if (!
              ((I_10 == (B_10 + -1)) && (G_10 == H_10)
               && (!((D_10 + (-1 * E_10)) >= 1)) && (A_10 >= I_10)
               && (H_10 <= 0) && (G_10 <= 0) && (J_10 == (C_10 + -2))))
              abort ();
          INV1_0 = A_10;
          INV1_1 = B_10;
          INV1_2 = C_10;
          INV1_3 = D_10;
          INV1_4 = E_10;
          INV1_5 = F_10;
          INV1_6 = G_10;
          INV1_7 = H_10;
          goto INV1_2;

      default:
          abort ();
      }

    // return expression

}

