// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: eldarica-misc/digits10_inl_merged_safe.c-1_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "digits10_inl_merged_safe.c-1_000_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int inv_main9_0;
    int inv_main9_1;
    int inv_main9_2;
    int inv_main9_3;
    int inv_main9_4;
    int inv_main9_5;
    int inv_main9_6;
    int inv_main11_0;
    int inv_main11_1;
    int inv_main11_2;
    int inv_main11_3;
    int inv_main11_4;
    int inv_main11_5;
    int inv_main11_6;
    int inv_main4_0;
    int inv_main4_1;
    int A_0;
    int v_1_0;
    int A_1;
    int B_1;
    int C_1;
    int D_1;
    int E_1;
    int F_1;
    int G_1;
    int A_4;
    int B_4;
    int C_4;
    int D_4;
    int E_4;
    int F_4;
    int G_4;
    int H_4;
    int I_4;
    int J_4;
    int A_5;
    int B_5;
    int C_5;
    int D_5;
    int E_5;
    int F_5;
    int G_5;
    int H_5;
    int I_5;
    int J_5;
    int K_5;
    int L_5;
    int M_5;
    int N_5;
    int O_5;
    int P_5;
    int Q_5;
    int R_5;
    int S_5;
    int T_5;
    int U_5;
    int V_5;
    int W_5;
    int A_6;
    int B_6;
    int C_6;
    int D_6;
    int E_6;
    int F_6;
    int G_6;
    int H_6;
    int I_6;
    int J_6;
    int K_6;
    int L_6;
    int M_6;
    int N_6;
    int O_6;
    int P_6;
    int Q_6;
    int R_6;
    int S_6;
    int T_6;
    int U_6;
    int V_6;
    int W_6;
    int X_6;
    int Y_6;
    int Z_6;
    int A1_6;
    int B1_6;
    int C1_6;
    int D1_6;
    int E1_6;
    int F1_6;
    int G1_6;
    int H1_6;
    int I1_6;
    int J1_6;
    int K1_6;
    int L1_6;
    int M1_6;
    int N1_6;
    int O1_6;
    int P1_6;
    int Q1_6;
    int R1_6;
    int S1_6;
    int T1_6;
    int U1_6;
    int V1_6;
    int W1_6;
    int X1_6;
    int Y1_6;
    int Z1_6;
    int A2_6;
    int B2_6;
    int C2_6;
    int D2_6;
    int E2_6;
    int F2_6;
    int G2_6;
    int H2_6;
    int I2_6;
    int J2_6;
    int K2_6;
    int L2_6;
    int M2_6;
    int N2_6;
    int O2_6;
    int P2_6;
    int Q2_6;
    int R2_6;
    int S2_6;
    int T2_6;
    int U2_6;
    int V2_6;
    int W2_6;
    int X2_6;
    int Y2_6;
    int Z2_6;
    int A3_6;
    int B3_6;
    int C3_6;
    int D3_6;
    int E3_6;
    int F3_6;
    int G3_6;
    int H3_6;
    int I3_6;
    int J3_6;
    int K3_6;
    int L3_6;
    int M3_6;
    int N3_6;
    int O3_6;
    int P3_6;
    int Q3_6;
    int R3_6;
    int S3_6;
    int T3_6;
    int U3_6;
    int V3_6;
    int W3_6;
    int X3_6;
    int A_7;
    int B_7;
    int C_7;
    int D_7;
    int E_7;
    int F_7;
    int G_7;
    int H_7;
    int I_7;
    int J_7;
    int K_7;
    int L_7;
    int M_7;
    int N_7;
    int O_7;
    int P_7;
    int Q_7;
    int R_7;
    int S_7;
    int T_7;
    int U_7;
    int V_7;
    int W_7;
    int X_7;
    int Y_7;
    int Z_7;
    int A1_7;
    int B1_7;
    int C1_7;
    int D1_7;
    int E1_7;
    int F1_7;
    int G1_7;
    int H1_7;
    int I1_7;
    int J1_7;
    int K1_7;
    int L1_7;
    int M1_7;
    int N1_7;
    int O1_7;
    int P1_7;
    int Q1_7;
    int R1_7;
    int S1_7;
    int T1_7;
    int U1_7;
    int V1_7;
    int W1_7;
    int X1_7;
    int Y1_7;
    int Z1_7;
    int A2_7;
    int B2_7;
    int C2_7;
    int D2_7;
    int E2_7;
    int F2_7;
    int G2_7;
    int H2_7;
    int I2_7;
    int J2_7;
    int K2_7;
    int L2_7;
    int M2_7;
    int N2_7;
    int O2_7;
    int P2_7;
    int Q2_7;
    int R2_7;
    int S2_7;
    int T2_7;
    int U2_7;
    int V2_7;
    int W2_7;
    int X2_7;
    int Y2_7;
    int Z2_7;
    int A3_7;
    int B3_7;
    int C3_7;
    int D3_7;
    int E3_7;
    int F3_7;
    int G3_7;
    int H3_7;
    int I3_7;
    int J3_7;
    int K3_7;
    int L3_7;
    int M3_7;
    int N3_7;
    int O3_7;
    int P3_7;
    int Q3_7;
    int R3_7;
    int S3_7;
    int T3_7;
    int U3_7;
    int V3_7;
    int W3_7;
    int X3_7;
    int Y3_7;
    int Z3_7;
    int A4_7;
    int B4_7;
    int C4_7;
    int D4_7;
    int E4_7;
    int F4_7;
    int G4_7;
    int H4_7;
    int I4_7;
    int J4_7;
    int K4_7;
    int L4_7;
    int M4_7;
    int N4_7;
    int O4_7;
    int P4_7;
    int Q4_7;
    int R4_7;
    int S4_7;
    int T4_7;
    int U4_7;
    int V4_7;
    int W4_7;
    int X4_7;
    int Y4_7;
    int Z4_7;
    int A5_7;
    int B5_7;
    int C5_7;
    int D5_7;
    int E5_7;
    int F5_7;
    int G5_7;
    int H5_7;
    int I5_7;
    int J5_7;
    int K5_7;
    int L5_7;
    int M5_7;
    int N5_7;
    int O5_7;
    int P5_7;
    int Q5_7;
    int R5_7;
    int S5_7;
    int T5_7;
    int U5_7;
    int V5_7;
    int W5_7;
    int X5_7;
    int Y5_7;
    int Z5_7;
    int A6_7;
    int B6_7;
    int C6_7;
    int D6_7;
    int E6_7;
    int F6_7;
    int G6_7;
    int H6_7;
    int I6_7;
    int J6_7;
    int K6_7;
    int L6_7;
    int M6_7;
    int N6_7;
    int O6_7;
    int P6_7;
    int Q6_7;
    int R6_7;
    int S6_7;
    int T6_7;
    int U6_7;
    int V6_7;
    int W6_7;
    int X6_7;
    int Y6_7;
    int Z6_7;
    int A7_7;
    int B7_7;
    int C7_7;
    int D7_7;
    int E7_7;
    int F7_7;
    int G7_7;
    int H7_7;
    int I7_7;
    int J7_7;
    int K7_7;
    int L7_7;
    int M7_7;
    int N7_7;
    int O7_7;
    int P7_7;
    int Q7_7;
    int R7_7;
    int S7_7;
    int T7_7;
    int U7_7;
    int V7_7;
    int W7_7;
    int X7_7;
    int Y7_7;
    int Z7_7;
    int A8_7;
    int B8_7;
    int C8_7;
    int D8_7;
    int E8_7;
    int F8_7;
    int G8_7;
    int H8_7;
    int I8_7;
    int J8_7;
    int K8_7;
    int L8_7;
    int M8_7;
    int N8_7;
    int O8_7;
    int P8_7;
    int Q8_7;
    int R8_7;
    int S8_7;
    int T8_7;
    int U8_7;
    int V8_7;
    int W8_7;
    int X8_7;
    int Y8_7;
    int Z8_7;
    int A9_7;
    int B9_7;
    int C9_7;
    int D9_7;
    int E9_7;
    int F9_7;
    int G9_7;
    int H9_7;
    int I9_7;
    int J9_7;
    int K9_7;
    int L9_7;
    int M9_7;
    int N9_7;
    int O9_7;
    int P9_7;
    int Q9_7;
    int R9_7;
    int S9_7;
    int T9_7;
    int U9_7;
    int V9_7;
    int W9_7;
    int X9_7;
    int Y9_7;
    int Z9_7;
    int A10_7;
    int B10_7;
    int C10_7;
    int D10_7;
    int E10_7;
    int F10_7;
    int G10_7;
    int H10_7;
    int I10_7;
    int J10_7;
    int K10_7;
    int L10_7;
    int M10_7;
    int N10_7;
    int O10_7;
    int P10_7;
    int Q10_7;
    int R10_7;
    int S10_7;
    int T10_7;
    int U10_7;
    int V10_7;
    int W10_7;
    int X10_7;
    int Y10_7;
    int Z10_7;
    int A11_7;
    int B11_7;
    int C11_7;
    int D11_7;
    int E11_7;
    int F11_7;
    int G11_7;
    int H11_7;
    int I11_7;
    int J11_7;
    int K11_7;
    int L11_7;
    int M11_7;
    int N11_7;
    int O11_7;
    int P11_7;
    int Q11_7;
    int R11_7;
    int S11_7;
    int T11_7;
    int U11_7;
    int V11_7;
    int W11_7;
    int X11_7;
    int Y11_7;
    int Z11_7;
    int A12_7;
    int B12_7;
    int C12_7;
    int D12_7;
    int E12_7;
    int F12_7;
    int A_13;
    int B_13;
    int C_13;
    int D_13;
    int v_4_13;
    int v_5_13;
    int v_6_13;
    int A_14;
    int B_14;
    int C_14;
    int D_14;
    int E_14;
    int F_14;
    int G_14;



    // main logic
    goto main_init;

  main_init:
    if (!(1 == v_1_0))
        abort ();
    inv_main4_0 = A_0;
    inv_main4_1 = v_1_0;
    A_13 = __VERIFIER_nondet_int ();
    D_13 = __VERIFIER_nondet_int ();
    v_4_13 = __VERIFIER_nondet_int ();
    v_6_13 = __VERIFIER_nondet_int ();
    v_5_13 = __VERIFIER_nondet_int ();
    C_13 = inv_main4_0;
    B_13 = inv_main4_1;
    if (!
        ((-9 <= ((-1 * C_13) + (10 * D_13))) && (-9 <= (C_13 + (-10 * D_13)))
         && ((1 <= C_13) || (!(1 <= (C_13 + (-10 * D_13)))))
         && ((!(1 <= ((-1 * C_13) + (10 * D_13)))) || (C_13 <= -1))
         && (A_13 == -1) && (v_4_13 == C_13) && (1 == v_5_13)
         && (1 == v_6_13)))
        abort ();
    inv_main9_0 = C_13;
    inv_main9_1 = B_13;
    inv_main9_2 = D_13;
    inv_main9_3 = v_4_13;
    inv_main9_4 = v_5_13;
    inv_main9_5 = v_6_13;
    inv_main9_6 = A_13;
    D_1 = inv_main9_0;
    A_1 = inv_main9_1;
    B_1 = inv_main9_2;
    E_1 = inv_main9_3;
    C_1 = inv_main9_4;
    F_1 = inv_main9_5;
    G_1 = inv_main9_6;
    if (!(1 <= B_1))
        abort ();
    inv_main11_0 = D_1;
    inv_main11_1 = A_1;
    inv_main11_2 = B_1;
    inv_main11_3 = E_1;
    inv_main11_4 = C_1;
    inv_main11_5 = F_1;
    inv_main11_6 = G_1;
    goto inv_main11;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  inv_main26:
    goto inv_main26;
  inv_main11:
    switch (__VERIFIER_nondet_int ())
      {
      case 0:
          D_14 = inv_main11_0;
          A_14 = inv_main11_1;
          B_14 = inv_main11_2;
          E_14 = inv_main11_3;
          C_14 = inv_main11_4;
          F_14 = inv_main11_5;
          G_14 = inv_main11_6;
          if (!(B_14 == 0))
              abort ();
          goto main_error;

      case 1:
          A_4 = __VERIFIER_nondet_int ();
          I_4 = __VERIFIER_nondet_int ();
          J_4 = __VERIFIER_nondet_int ();
          E_4 = inv_main11_0;
          B_4 = inv_main11_1;
          C_4 = inv_main11_2;
          F_4 = inv_main11_3;
          D_4 = inv_main11_4;
          G_4 = inv_main11_5;
          H_4 = inv_main11_6;
          if (!
              ((-9 <= ((-1 * C_4) + (10 * I_4)))
               && (-9 <= ((-1 * C_4) + (10 * J_4)))
               && (-9 <= (C_4 + (-10 * I_4))) && (-9 <= (C_4 + (-10 * J_4)))
               && (!(1 <= I_4)) && ((!(1 <= (C_4 + (-10 * I_4))))
                                    || (1 <= C_4)) && ((1 <= C_4)
                                                       ||
                                                       (!(1 <=
                                                          (C_4 +
                                                           (-10 * J_4)))))
               && ((!(1 <= ((-1 * C_4) + (10 * I_4)))) || (C_4 <= -1))
               && ((C_4 <= -1) || (!(1 <= ((-1 * C_4) + (10 * J_4)))))
               && (A_4 == (B_4 + 1))))
              abort ();
          inv_main9_0 = E_4;
          inv_main9_1 = A_4;
          inv_main9_2 = J_4;
          inv_main9_3 = F_4;
          inv_main9_4 = D_4;
          inv_main9_5 = G_4;
          inv_main9_6 = H_4;
          D_1 = inv_main9_0;
          A_1 = inv_main9_1;
          B_1 = inv_main9_2;
          E_1 = inv_main9_3;
          C_1 = inv_main9_4;
          F_1 = inv_main9_5;
          G_1 = inv_main9_6;
          if (!(1 <= B_1))
              abort ();
          inv_main11_0 = D_1;
          inv_main11_1 = A_1;
          inv_main11_2 = B_1;
          inv_main11_3 = E_1;
          inv_main11_4 = C_1;
          inv_main11_5 = F_1;
          inv_main11_6 = G_1;
          goto inv_main11;

      case 2:
          A_5 = __VERIFIER_nondet_int ();
          I_5 = __VERIFIER_nondet_int ();
          J_5 = __VERIFIER_nondet_int ();
          K_5 = __VERIFIER_nondet_int ();
          L_5 = __VERIFIER_nondet_int ();
          M_5 = __VERIFIER_nondet_int ();
          N_5 = __VERIFIER_nondet_int ();
          O_5 = __VERIFIER_nondet_int ();
          P_5 = __VERIFIER_nondet_int ();
          Q_5 = __VERIFIER_nondet_int ();
          R_5 = __VERIFIER_nondet_int ();
          S_5 = __VERIFIER_nondet_int ();
          T_5 = __VERIFIER_nondet_int ();
          U_5 = __VERIFIER_nondet_int ();
          V_5 = __VERIFIER_nondet_int ();
          W_5 = __VERIFIER_nondet_int ();
          E_5 = inv_main11_0;
          B_5 = inv_main11_1;
          C_5 = inv_main11_2;
          F_5 = inv_main11_3;
          D_5 = inv_main11_4;
          G_5 = inv_main11_5;
          H_5 = inv_main11_6;
          if (!
              ((-9 <= ((-1 * J_5) + (10 * I_5)))
               && (-9 <= ((-1 * C_5) + (10 * K_5)))
               && (-9 <= ((-1 * C_5) + (10 * J_5)))
               && (-9 <= ((-1 * C_5) + (10 * P_5)))
               && (-9 <= ((-1 * C_5) + (10 * S_5)))
               && (-9 <= ((-1 * C_5) + (10 * R_5)))
               && (-9 <= ((-1 * R_5) + (10 * Q_5)))
               && (-9 <= (K_5 + (-10 * I_5))) && (-9 <= (C_5 + (-10 * K_5)))
               && (-9 <= (C_5 + (-10 * J_5))) && (-9 <= (C_5 + (-10 * P_5)))
               && (-9 <= (C_5 + (-10 * S_5))) && (-9 <= (C_5 + (-10 * R_5)))
               && (-9 <= (S_5 + (-10 * Q_5))) && (!(1 <= I_5)) && (1 <= P_5)
               && ((1 <= C_5) || (!(1 <= (C_5 + (-10 * K_5))))) && ((1 <= C_5)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_5 +
                                                                        (-10 *
                                                                         J_5)))))
               && ((1 <= C_5) || (!(1 <= (C_5 + (-10 * P_5)))))
               && ((!(1 <= (C_5 + (-10 * S_5)))) || (1 <= C_5)) && ((1 <= C_5)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_5 +
                                                                        (-10 *
                                                                         R_5)))))
               && ((!(1 <= ((-1 * C_5) + (10 * K_5)))) || (C_5 <= -1))
               && ((C_5 <= -1) || (!(1 <= ((-1 * C_5) + (10 * J_5)))))
               && ((C_5 <= -1) || (!(1 <= ((-1 * C_5) + (10 * P_5)))))
               && ((!(1 <= ((-1 * C_5) + (10 * S_5)))) || (C_5 <= -1))
               && ((!(1 <= ((-1 * C_5) + (10 * R_5)))) || (C_5 <= -1))
               &&
               ((((C_5 <= -1) || (!(1 <= ((-1 * C_5) + (10 * W_5)))))
                 && ((1 <= C_5) || (!(1 <= (C_5 + (-10 * W_5)))))
                 && (W_5 <= -1) && (-9 <= (C_5 + (-10 * W_5)))
                 && (-9 <= ((-1 * C_5) + (10 * W_5)))) || (((C_5 <= -1)
                                                            ||
                                                            (!(1 <=
                                                               ((-1 * C_5) +
                                                                (10 * V_5)))))
                                                           && ((1 <= C_5)
                                                               ||
                                                               (!(1 <=
                                                                  (C_5 +
                                                                   (-10 *
                                                                    V_5)))))
                                                           &&
                                                           (!(1 <=
                                                              ((-1 * V_5) +
                                                               (10 * Q_5))))
                                                           && (-9 <=
                                                               (C_5 +
                                                                (-10 * V_5)))
                                                           && (-9 <=
                                                               ((-1 * C_5) +
                                                                (10 * V_5)))))
               &&
               ((((C_5 <= -1) || (!(1 <= ((-1 * C_5) + (10 * U_5)))))
                 && ((1 <= C_5) || (!(1 <= (C_5 + (-10 * U_5)))))
                 && (1 <= U_5) && (-9 <= (C_5 + (-10 * U_5)))
                 && (-9 <= ((-1 * C_5) + (10 * U_5)))) || (((C_5 <= -1)
                                                            ||
                                                            (!(1 <=
                                                               ((-1 * C_5) +
                                                                (10 * T_5)))))
                                                           && ((1 <= C_5)
                                                               ||
                                                               (!(1 <=
                                                                  (C_5 +
                                                                   (-10 *
                                                                    T_5)))))
                                                           &&
                                                           (!(1 <=
                                                              (T_5 +
                                                               (-10 * Q_5))))
                                                           && (-9 <=
                                                               (C_5 +
                                                                (-10 * T_5)))
                                                           && (-9 <=
                                                               ((-1 * C_5) +
                                                                (10 * T_5)))))
               &&
               ((((C_5 <= -1) || (!(1 <= ((-1 * C_5) + (10 * O_5)))))
                 && ((1 <= C_5) || (!(1 <= (C_5 + (-10 * O_5)))))
                 && (O_5 <= -1) && (-9 <= (C_5 + (-10 * O_5)))
                 && (-9 <= ((-1 * C_5) + (10 * O_5)))) || (((C_5 <= -1)
                                                            ||
                                                            (!(1 <=
                                                               ((-1 * C_5) +
                                                                (10 * N_5)))))
                                                           && ((1 <= C_5)
                                                               ||
                                                               (!(1 <=
                                                                  (C_5 +
                                                                   (-10 *
                                                                    N_5)))))
                                                           &&
                                                           (!(1 <=
                                                              ((-1 * N_5) +
                                                               (10 * I_5))))
                                                           && (-9 <=
                                                               (C_5 +
                                                                (-10 * N_5)))
                                                           && (-9 <=
                                                               ((-1 * C_5) +
                                                                (10 * N_5)))))
               &&
               ((((!(1 <= ((-1 * C_5) + (10 * M_5)))) || (C_5 <= -1))
                 && ((!(1 <= (C_5 + (-10 * M_5)))) || (1 <= C_5))
                 && (1 <= M_5) && (-9 <= (C_5 + (-10 * M_5)))
                 && (-9 <= ((-1 * C_5) + (10 * M_5))))
                || (((!(1 <= ((-1 * C_5) + (10 * L_5)))) || (C_5 <= -1))
                    && ((!(1 <= (C_5 + (-10 * L_5)))) || (1 <= C_5))
                    && (!(1 <= (L_5 + (-10 * I_5))))
                    && (-9 <= (C_5 + (-10 * L_5)))
                    && (-9 <= ((-1 * C_5) + (10 * L_5)))))
               && (A_5 == (B_5 + 2))))
              abort ();
          inv_main9_0 = E_5;
          inv_main9_1 = A_5;
          inv_main9_2 = Q_5;
          inv_main9_3 = F_5;
          inv_main9_4 = D_5;
          inv_main9_5 = G_5;
          inv_main9_6 = H_5;
          D_1 = inv_main9_0;
          A_1 = inv_main9_1;
          B_1 = inv_main9_2;
          E_1 = inv_main9_3;
          C_1 = inv_main9_4;
          F_1 = inv_main9_5;
          G_1 = inv_main9_6;
          if (!(1 <= B_1))
              abort ();
          inv_main11_0 = D_1;
          inv_main11_1 = A_1;
          inv_main11_2 = B_1;
          inv_main11_3 = E_1;
          inv_main11_4 = C_1;
          inv_main11_5 = F_1;
          inv_main11_6 = G_1;
          goto inv_main11;

      case 3:
          Q1_6 = __VERIFIER_nondet_int ();
          Q2_6 = __VERIFIER_nondet_int ();
          Q3_6 = __VERIFIER_nondet_int ();
          I1_6 = __VERIFIER_nondet_int ();
          I2_6 = __VERIFIER_nondet_int ();
          I3_6 = __VERIFIER_nondet_int ();
          A1_6 = __VERIFIER_nondet_int ();
          A2_6 = __VERIFIER_nondet_int ();
          A3_6 = __VERIFIER_nondet_int ();
          Z1_6 = __VERIFIER_nondet_int ();
          Z2_6 = __VERIFIER_nondet_int ();
          R1_6 = __VERIFIER_nondet_int ();
          R2_6 = __VERIFIER_nondet_int ();
          R3_6 = __VERIFIER_nondet_int ();
          J1_6 = __VERIFIER_nondet_int ();
          J2_6 = __VERIFIER_nondet_int ();
          J3_6 = __VERIFIER_nondet_int ();
          B1_6 = __VERIFIER_nondet_int ();
          B2_6 = __VERIFIER_nondet_int ();
          B3_6 = __VERIFIER_nondet_int ();
          S1_6 = __VERIFIER_nondet_int ();
          S2_6 = __VERIFIER_nondet_int ();
          S3_6 = __VERIFIER_nondet_int ();
          A_6 = __VERIFIER_nondet_int ();
          K1_6 = __VERIFIER_nondet_int ();
          K2_6 = __VERIFIER_nondet_int ();
          K3_6 = __VERIFIER_nondet_int ();
          I_6 = __VERIFIER_nondet_int ();
          J_6 = __VERIFIER_nondet_int ();
          K_6 = __VERIFIER_nondet_int ();
          L_6 = __VERIFIER_nondet_int ();
          M_6 = __VERIFIER_nondet_int ();
          N_6 = __VERIFIER_nondet_int ();
          C1_6 = __VERIFIER_nondet_int ();
          O_6 = __VERIFIER_nondet_int ();
          C2_6 = __VERIFIER_nondet_int ();
          P_6 = __VERIFIER_nondet_int ();
          C3_6 = __VERIFIER_nondet_int ();
          Q_6 = __VERIFIER_nondet_int ();
          R_6 = __VERIFIER_nondet_int ();
          S_6 = __VERIFIER_nondet_int ();
          T_6 = __VERIFIER_nondet_int ();
          U_6 = __VERIFIER_nondet_int ();
          V_6 = __VERIFIER_nondet_int ();
          W_6 = __VERIFIER_nondet_int ();
          X_6 = __VERIFIER_nondet_int ();
          Y_6 = __VERIFIER_nondet_int ();
          Z_6 = __VERIFIER_nondet_int ();
          T1_6 = __VERIFIER_nondet_int ();
          T2_6 = __VERIFIER_nondet_int ();
          T3_6 = __VERIFIER_nondet_int ();
          L1_6 = __VERIFIER_nondet_int ();
          L2_6 = __VERIFIER_nondet_int ();
          L3_6 = __VERIFIER_nondet_int ();
          D1_6 = __VERIFIER_nondet_int ();
          D2_6 = __VERIFIER_nondet_int ();
          D3_6 = __VERIFIER_nondet_int ();
          U1_6 = __VERIFIER_nondet_int ();
          U2_6 = __VERIFIER_nondet_int ();
          U3_6 = __VERIFIER_nondet_int ();
          M1_6 = __VERIFIER_nondet_int ();
          M2_6 = __VERIFIER_nondet_int ();
          M3_6 = __VERIFIER_nondet_int ();
          E1_6 = __VERIFIER_nondet_int ();
          E2_6 = __VERIFIER_nondet_int ();
          E3_6 = __VERIFIER_nondet_int ();
          V1_6 = __VERIFIER_nondet_int ();
          V2_6 = __VERIFIER_nondet_int ();
          V3_6 = __VERIFIER_nondet_int ();
          N1_6 = __VERIFIER_nondet_int ();
          N2_6 = __VERIFIER_nondet_int ();
          N3_6 = __VERIFIER_nondet_int ();
          F1_6 = __VERIFIER_nondet_int ();
          F2_6 = __VERIFIER_nondet_int ();
          F3_6 = __VERIFIER_nondet_int ();
          W1_6 = __VERIFIER_nondet_int ();
          W2_6 = __VERIFIER_nondet_int ();
          W3_6 = __VERIFIER_nondet_int ();
          O1_6 = __VERIFIER_nondet_int ();
          O2_6 = __VERIFIER_nondet_int ();
          O3_6 = __VERIFIER_nondet_int ();
          G1_6 = __VERIFIER_nondet_int ();
          G2_6 = __VERIFIER_nondet_int ();
          G3_6 = __VERIFIER_nondet_int ();
          X1_6 = __VERIFIER_nondet_int ();
          X2_6 = __VERIFIER_nondet_int ();
          X3_6 = __VERIFIER_nondet_int ();
          P1_6 = __VERIFIER_nondet_int ();
          P2_6 = __VERIFIER_nondet_int ();
          P3_6 = __VERIFIER_nondet_int ();
          H1_6 = __VERIFIER_nondet_int ();
          H2_6 = __VERIFIER_nondet_int ();
          H3_6 = __VERIFIER_nondet_int ();
          Y1_6 = __VERIFIER_nondet_int ();
          Y2_6 = __VERIFIER_nondet_int ();
          E_6 = inv_main11_0;
          B_6 = inv_main11_1;
          C_6 = inv_main11_2;
          F_6 = inv_main11_3;
          D_6 = inv_main11_4;
          G_6 = inv_main11_5;
          H_6 = inv_main11_6;
          if (!
              ((-9 <= ((-1 * Q2_6) + (10 * P2_6)))
               && (-9 <= ((-1 * I2_6) + (10 * H2_6)))
               && (-9 <= ((-1 * J2_6) + (10 * I2_6)))
               && (-9 <= ((-1 * A2_6) + (10 * Z1_6)))
               && (-9 <= ((-1 * R_6) + (10 * Q_6)))
               && (-9 <= ((-1 * J_6) + (10 * I_6)))
               && (-9 <= ((-1 * K_6) + (10 * J_6)))
               && (-9 <= ((-1 * C_6) + (10 * R2_6)))
               && (-9 <= ((-1 * C_6) + (10 * Q2_6)))
               && (-9 <= ((-1 * C_6) + (10 * K2_6)))
               && (-9 <= ((-1 * C_6) + (10 * J2_6)))
               && (-9 <= ((-1 * C_6) + (10 * G2_6)))
               && (-9 <= ((-1 * C_6) + (10 * B2_6)))
               && (-9 <= ((-1 * C_6) + (10 * A2_6)))
               && (-9 <= ((-1 * C_6) + (10 * S_6)))
               && (-9 <= ((-1 * C_6) + (10 * R_6)))
               && (-9 <= ((-1 * C_6) + (10 * L_6)))
               && (-9 <= ((-1 * C_6) + (10 * K_6)))
               && (-9 <= (R2_6 + (-10 * P2_6)))
               && (-9 <= (P2_6 + (-10 * H2_6)))
               && (-9 <= (K2_6 + (-10 * I2_6)))
               && (-9 <= (B2_6 + (-10 * Z1_6))) && (-9 <= (S_6 + (-10 * Q_6)))
               && (-9 <= (Q_6 + (-10 * I_6))) && (-9 <= (L_6 + (-10 * J_6)))
               && (-9 <= (C_6 + (-10 * R2_6))) && (-9 <= (C_6 + (-10 * Q2_6)))
               && (-9 <= (C_6 + (-10 * K2_6))) && (-9 <= (C_6 + (-10 * J2_6)))
               && (-9 <= (C_6 + (-10 * G2_6))) && (-9 <= (C_6 + (-10 * B2_6)))
               && (-9 <= (C_6 + (-10 * A2_6))) && (-9 <= (C_6 + (-10 * S_6)))
               && (-9 <= (C_6 + (-10 * R_6))) && (-9 <= (C_6 + (-10 * L_6)))
               && (-9 <= (C_6 + (-10 * K_6))) && (1 <= G2_6) && (1 <= Z1_6)
               && (!(1 <= I_6)) && ((1 <= C_6)
                                    || (!(1 <= (C_6 + (-10 * R2_6)))))
               && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * Q2_6)))))
               && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * K2_6)))))
               && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * J2_6)))))
               && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * G2_6)))))
               && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * B2_6)))))
               && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * A2_6)))))
               && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * S_6))))) && ((1 <= C_6)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_6 +
                                                                        (-10 *
                                                                         R_6)))))
               && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * L_6)))))
               && ((!(1 <= (C_6 + (-10 * K_6)))) || (1 <= C_6))
               && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * R2_6)))))
               && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * Q2_6)))))
               && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * K2_6)))))
               && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * J2_6)))))
               && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * G2_6)))))
               && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * B2_6)))))
               && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * A2_6)))))
               && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * S_6)))))
               && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * R_6)))))
               && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * L_6)))))
               && ((!(1 <= ((-1 * C_6) + (10 * K_6)))) || (C_6 <= -1))
               &&
               ((((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * O3_6)))))
                   && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * O3_6)))))
                   && (1 <= O3_6) && (-9 <= (C_6 + (-10 * O3_6)))
                   && (-9 <= ((-1 * C_6) + (10 * O3_6)))) || (((C_6 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    N3_6)))))
                                                              && ((1 <= C_6)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_6 +
                                                                      (-10 *
                                                                       N3_6)))))
                                                              &&
                                                              (!(1 <=
                                                                 (N3_6 +
                                                                  (-10 *
                                                                   K3_6))))
                                                              && (-9 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    N3_6)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    N3_6)))))
                 &&
                 ((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * Q3_6)))))
                   && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * Q3_6)))))
                   && (Q3_6 <= -1) && (-9 <= (C_6 + (-10 * Q3_6)))
                   && (-9 <= ((-1 * C_6) + (10 * Q3_6)))) || (((C_6 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    P3_6)))))
                                                              && ((1 <= C_6)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_6 +
                                                                      (-10 *
                                                                       P3_6)))))
                                                              &&
                                                              (!(1 <=
                                                                 ((-1 *
                                                                   P3_6) +
                                                                  (10 *
                                                                   K3_6))))
                                                              && (-9 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    P3_6)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    P3_6)))))
                 && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * L3_6)))))
                 && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * M3_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * L3_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * M3_6)))))
                 && (!(1 <= ((-1 * K3_6) + (10 * H2_6))))
                 && (-9 <= (M3_6 + (-10 * K3_6)))
                 && (-9 <= (C_6 + (-10 * L3_6)))
                 && (-9 <= (C_6 + (-10 * M3_6)))
                 && (-9 <= ((-1 * L3_6) + (10 * K3_6)))
                 && (-9 <= ((-1 * C_6) + (10 * L3_6)))
                 && (-9 <= ((-1 * C_6) + (10 * M3_6))))
                ||
                (((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * V3_6)))))
                   && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * V3_6)))))
                   && (1 <= V3_6) && (-9 <= (C_6 + (-10 * V3_6)))
                   && (-9 <= ((-1 * C_6) + (10 * V3_6)))) || (((C_6 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    U3_6)))))
                                                              && ((1 <= C_6)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_6 +
                                                                      (-10 *
                                                                       U3_6)))))
                                                              &&
                                                              (!(1 <=
                                                                 (U3_6 +
                                                                  (-10 *
                                                                   R3_6))))
                                                              && (-9 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    U3_6)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    U3_6)))))
                 &&
                 ((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * X3_6)))))
                   && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * X3_6)))))
                   && (X3_6 <= -1) && (-9 <= (C_6 + (-10 * X3_6)))
                   && (-9 <= ((-1 * C_6) + (10 * X3_6)))) || (((C_6 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    W3_6)))))
                                                              && ((1 <= C_6)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_6 +
                                                                      (-10 *
                                                                       W3_6)))))
                                                              &&
                                                              (!(1 <=
                                                                 ((-1 *
                                                                   W3_6) +
                                                                  (10 *
                                                                   R3_6))))
                                                              && (-9 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    W3_6)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    W3_6)))))
                 && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * S3_6)))))
                 && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * T3_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * S3_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * T3_6)))))
                 && (R3_6 <= -1) && (-9 <= (T3_6 + (-10 * R3_6)))
                 && (-9 <= (C_6 + (-10 * S3_6)))
                 && (-9 <= (C_6 + (-10 * T3_6)))
                 && (-9 <= ((-1 * S3_6) + (10 * R3_6)))
                 && (-9 <= ((-1 * C_6) + (10 * S3_6)))
                 && (-9 <= ((-1 * C_6) + (10 * T3_6)))))
               &&
               ((((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * A3_6)))))
                   && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * A3_6)))))
                   && (1 <= A3_6) && (-9 <= (C_6 + (-10 * A3_6)))
                   && (-9 <= ((-1 * C_6) + (10 * A3_6)))) || (((C_6 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    Z2_6)))))
                                                              && ((1 <= C_6)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_6 +
                                                                      (-10 *
                                                                       Z2_6)))))
                                                              &&
                                                              (!(1 <=
                                                                 (Z2_6 +
                                                                  (-10 *
                                                                   W2_6))))
                                                              && (-9 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    Z2_6)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    Z2_6)))))
                 &&
                 ((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * C3_6)))))
                   && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * C3_6)))))
                   && (C3_6 <= -1) && (-9 <= (C_6 + (-10 * C3_6)))
                   && (-9 <= ((-1 * C_6) + (10 * C3_6)))) || (((C_6 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    B3_6)))))
                                                              && ((1 <= C_6)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_6 +
                                                                      (-10 *
                                                                       B3_6)))))
                                                              &&
                                                              (!(1 <=
                                                                 ((-1 *
                                                                   B3_6) +
                                                                  (10 *
                                                                   W2_6))))
                                                              && (-9 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    B3_6)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    B3_6)))))
                 && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * X2_6)))))
                 && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * Y2_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * X2_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * Y2_6)))))
                 && (!(1 <= (W2_6 + (-10 * H2_6))))
                 && (-9 <= (C_6 + (-10 * X2_6)))
                 && (-9 <= (C_6 + (-10 * Y2_6)))
                 && (-9 <= (Y2_6 + (-10 * W2_6)))
                 && (-9 <= ((-1 * C_6) + (10 * X2_6)))
                 && (-9 <= ((-1 * C_6) + (10 * Y2_6)))
                 && (-9 <= ((-1 * X2_6) + (10 * W2_6))))
                ||
                (((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * H3_6)))))
                   && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * H3_6)))))
                   && (1 <= H3_6) && (-9 <= (C_6 + (-10 * H3_6)))
                   && (-9 <= ((-1 * C_6) + (10 * H3_6)))) || (((C_6 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    G3_6)))))
                                                              && ((1 <= C_6)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_6 +
                                                                      (-10 *
                                                                       G3_6)))))
                                                              &&
                                                              (!(1 <=
                                                                 (G3_6 +
                                                                  (-10 *
                                                                   D3_6))))
                                                              && (-9 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    G3_6)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    G3_6)))))
                 &&
                 ((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * J3_6)))))
                   && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * J3_6)))))
                   && (J3_6 <= -1) && (-9 <= (C_6 + (-10 * J3_6)))
                   && (-9 <= ((-1 * C_6) + (10 * J3_6)))) || (((C_6 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    I3_6)))))
                                                              && ((1 <= C_6)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_6 +
                                                                      (-10 *
                                                                       I3_6)))))
                                                              &&
                                                              (!(1 <=
                                                                 ((-1 *
                                                                   I3_6) +
                                                                  (10 *
                                                                   D3_6))))
                                                              && (-9 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    I3_6)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    I3_6)))))
                 && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * F3_6)))))
                 && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * E3_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * F3_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * E3_6)))))
                 && (1 <= D3_6) && (-9 <= (F3_6 + (-10 * D3_6)))
                 && (-9 <= (C_6 + (-10 * F3_6)))
                 && (-9 <= (C_6 + (-10 * E3_6)))
                 && (-9 <= ((-1 * E3_6) + (10 * D3_6)))
                 && (-9 <= ((-1 * C_6) + (10 * F3_6)))
                 && (-9 <= ((-1 * C_6) + (10 * E3_6)))))
               &&
               ((((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * P1_6)))))
                   && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * P1_6)))))
                   && (1 <= P1_6) && (-9 <= (C_6 + (-10 * P1_6)))
                   && (-9 <= ((-1 * C_6) + (10 * P1_6)))) || (((C_6 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    O1_6)))))
                                                              && ((1 <= C_6)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_6 +
                                                                      (-10 *
                                                                       O1_6)))))
                                                              &&
                                                              (!(1 <=
                                                                 (O1_6 +
                                                                  (-10 *
                                                                   L1_6))))
                                                              && (-9 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    O1_6)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    O1_6)))))
                 &&
                 ((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * R1_6)))))
                   && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * R1_6)))))
                   && (R1_6 <= -1) && (-9 <= (C_6 + (-10 * R1_6)))
                   && (-9 <= ((-1 * C_6) + (10 * R1_6)))) || (((C_6 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    Q1_6)))))
                                                              && ((1 <= C_6)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_6 +
                                                                      (-10 *
                                                                       Q1_6)))))
                                                              &&
                                                              (!(1 <=
                                                                 ((-1 *
                                                                   Q1_6) +
                                                                  (10 *
                                                                   L1_6))))
                                                              && (-9 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    Q1_6)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    Q1_6)))))
                 && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * M1_6)))))
                 && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * N1_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * M1_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * N1_6)))))
                 && (!(1 <= ((-1 * L1_6) + (10 * I_6))))
                 && (-9 <= (C_6 + (-10 * M1_6)))
                 && (-9 <= (C_6 + (-10 * N1_6)))
                 && (-9 <= (N1_6 + (-10 * L1_6)))
                 && (-9 <= ((-1 * C_6) + (10 * M1_6)))
                 && (-9 <= ((-1 * C_6) + (10 * N1_6)))
                 && (-9 <= ((-1 * M1_6) + (10 * L1_6))))
                ||
                (((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * W1_6)))))
                   && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * W1_6)))))
                   && (1 <= W1_6) && (-9 <= (C_6 + (-10 * W1_6)))
                   && (-9 <= ((-1 * C_6) + (10 * W1_6)))) || (((C_6 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    V1_6)))))
                                                              && ((1 <= C_6)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_6 +
                                                                      (-10 *
                                                                       V1_6)))))
                                                              &&
                                                              (!(1 <=
                                                                 (V1_6 +
                                                                  (-10 *
                                                                   S1_6))))
                                                              && (-9 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    V1_6)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    V1_6)))))
                 &&
                 ((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * Y1_6)))))
                   && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * Y1_6)))))
                   && (Y1_6 <= -1) && (-9 <= (C_6 + (-10 * Y1_6)))
                   && (-9 <= ((-1 * C_6) + (10 * Y1_6)))) || (((C_6 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    X1_6)))))
                                                              && ((1 <= C_6)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_6 +
                                                                      (-10 *
                                                                       X1_6)))))
                                                              &&
                                                              (!(1 <=
                                                                 ((-1 *
                                                                   X1_6) +
                                                                  (10 *
                                                                   S1_6))))
                                                              && (-9 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    X1_6)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    X1_6)))))
                 && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * T1_6)))))
                 && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * U1_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * T1_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * U1_6)))))
                 && (S1_6 <= -1) && (-9 <= (C_6 + (-10 * T1_6)))
                 && (-9 <= (C_6 + (-10 * U1_6)))
                 && (-9 <= (U1_6 + (-10 * S1_6)))
                 && (-9 <= ((-1 * C_6) + (10 * T1_6)))
                 && (-9 <= ((-1 * C_6) + (10 * U1_6)))
                 && (-9 <= ((-1 * T1_6) + (10 * S1_6)))))
               &&
               ((((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * B1_6)))))
                   && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * B1_6)))))
                   && (1 <= B1_6) && (-9 <= (C_6 + (-10 * B1_6)))
                   && (-9 <= ((-1 * C_6) + (10 * B1_6)))) || (((C_6 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    A1_6)))))
                                                              && ((1 <= C_6)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_6 +
                                                                      (-10 *
                                                                       A1_6)))))
                                                              &&
                                                              (!(1 <=
                                                                 (A1_6 +
                                                                  (-10 *
                                                                   X_6))))
                                                              && (-9 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    A1_6)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    A1_6)))))
                 &&
                 ((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * D1_6)))))
                   && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * D1_6)))))
                   && (D1_6 <= -1) && (-9 <= (C_6 + (-10 * D1_6)))
                   && (-9 <= ((-1 * C_6) + (10 * D1_6)))) || (((C_6 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    C1_6)))))
                                                              && ((1 <= C_6)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_6 +
                                                                      (-10 *
                                                                       C1_6)))))
                                                              &&
                                                              (!(1 <=
                                                                 ((-1 *
                                                                   C1_6) +
                                                                  (10 *
                                                                   X_6))))
                                                              && (-9 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    C1_6)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    C1_6)))))
                 && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * Y_6)))))
                 && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * Z_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * Y_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * Z_6)))))
                 && (!(1 <= (X_6 + (-10 * I_6))))
                 && (-9 <= (C_6 + (-10 * Y_6))) && (-9 <= (C_6 + (-10 * Z_6)))
                 && (-9 <= (Z_6 + (-10 * X_6)))
                 && (-9 <= ((-1 * C_6) + (10 * Y_6)))
                 && (-9 <= ((-1 * C_6) + (10 * Z_6)))
                 && (-9 <= ((-1 * Y_6) + (10 * X_6))))
                ||
                (((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * I1_6)))))
                   && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * I1_6)))))
                   && (1 <= I1_6) && (-9 <= (C_6 + (-10 * I1_6)))
                   && (-9 <= ((-1 * C_6) + (10 * I1_6)))) || (((C_6 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    H1_6)))))
                                                              && ((1 <= C_6)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_6 +
                                                                      (-10 *
                                                                       H1_6)))))
                                                              &&
                                                              (!(1 <=
                                                                 (H1_6 +
                                                                  (-10 *
                                                                   E1_6))))
                                                              && (-9 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    H1_6)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    H1_6)))))
                 &&
                 ((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * K1_6)))))
                   && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * K1_6)))))
                   && (K1_6 <= -1) && (-9 <= (C_6 + (-10 * K1_6)))
                   && (-9 <= ((-1 * C_6) + (10 * K1_6)))) || (((C_6 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    J1_6)))))
                                                              && ((1 <= C_6)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_6 +
                                                                      (-10 *
                                                                       J1_6)))))
                                                              &&
                                                              (!(1 <=
                                                                 ((-1 *
                                                                   J1_6) +
                                                                  (10 *
                                                                   E1_6))))
                                                              && (-9 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    J1_6)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_6) +
                                                                   (10 *
                                                                    J1_6)))))
                 && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * F1_6)))))
                 && ((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * G1_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * F1_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * G1_6)))))
                 && (1 <= E1_6) && (-9 <= (C_6 + (-10 * F1_6)))
                 && (-9 <= (C_6 + (-10 * G1_6)))
                 && (-9 <= (G1_6 + (-10 * E1_6)))
                 && (-9 <= ((-1 * C_6) + (10 * F1_6)))
                 && (-9 <= ((-1 * C_6) + (10 * G1_6)))
                 && (-9 <= ((-1 * F1_6) + (10 * E1_6))))) && ((((C_6 <= -1)
                                                                ||
                                                                (!(1 <=
                                                                   ((-1 *
                                                                     C_6) +
                                                                    (10 *
                                                                     F2_6)))))
                                                               && ((1 <= C_6)
                                                                   ||
                                                                   (!(1 <=
                                                                      (C_6 +
                                                                       (-10 *
                                                                        F2_6)))))
                                                               && (F2_6 <= -1)
                                                               && (-9 <=
                                                                   (C_6 +
                                                                    (-10 *
                                                                     F2_6)))
                                                               && (-9 <=
                                                                   ((-1 *
                                                                     C_6) +
                                                                    (10 *
                                                                     F2_6))))
                                                              ||
                                                              (((C_6 <= -1)
                                                                ||
                                                                (!(1 <=
                                                                   ((-1 *
                                                                     C_6) +
                                                                    (10 *
                                                                     E2_6)))))
                                                               && ((1 <= C_6)
                                                                   ||
                                                                   (!(1 <=
                                                                      (C_6 +
                                                                       (-10 *
                                                                        E2_6)))))
                                                               &&
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    E2_6) +
                                                                   (10 *
                                                                    Z1_6))))
                                                               && (-9 <=
                                                                   (C_6 +
                                                                    (-10 *
                                                                     E2_6)))
                                                               && (-9 <=
                                                                   ((-1 *
                                                                     C_6) +
                                                                    (10 *
                                                                     E2_6)))))
               &&
               ((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * D2_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * D2_6)))))
                 && (1 <= D2_6) && (-9 <= (C_6 + (-10 * D2_6)))
                 && (-9 <= ((-1 * C_6) + (10 * D2_6)))) || (((C_6 <= -1)
                                                             ||
                                                             (!(1 <=
                                                                ((-1 * C_6) +
                                                                 (10 *
                                                                  C2_6)))))
                                                            && ((1 <= C_6)
                                                                ||
                                                                (!(1 <=
                                                                   (C_6 +
                                                                    (-10 *
                                                                     C2_6)))))
                                                            &&
                                                            (!(1 <=
                                                               (C2_6 +
                                                                (-10 *
                                                                 Z1_6))))
                                                            && (-9 <=
                                                                (C_6 +
                                                                 (-10 *
                                                                  C2_6)))
                                                            && (-9 <=
                                                                ((-1 * C_6) +
                                                                 (10 *
                                                                  C2_6)))))
               &&
               ((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * V2_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * V2_6)))))
                 && (V2_6 <= -1) && (-9 <= (C_6 + (-10 * V2_6)))
                 && (-9 <= ((-1 * C_6) + (10 * V2_6)))) || (((C_6 <= -1)
                                                             ||
                                                             (!(1 <=
                                                                ((-1 * C_6) +
                                                                 (10 *
                                                                  U2_6)))))
                                                            && ((1 <= C_6)
                                                                ||
                                                                (!(1 <=
                                                                   (C_6 +
                                                                    (-10 *
                                                                     U2_6)))))
                                                            &&
                                                            (!(1 <=
                                                               ((-1 * U2_6) +
                                                                (10 * P2_6))))
                                                            && (-9 <=
                                                                (C_6 +
                                                                 (-10 *
                                                                  U2_6)))
                                                            && (-9 <=
                                                                ((-1 * C_6) +
                                                                 (10 *
                                                                  U2_6)))))
               &&
               ((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * O2_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * O2_6)))))
                 && (O2_6 <= -1) && (-9 <= (C_6 + (-10 * O2_6)))
                 && (-9 <= ((-1 * C_6) + (10 * O2_6)))) || (((C_6 <= -1)
                                                             ||
                                                             (!(1 <=
                                                                ((-1 * C_6) +
                                                                 (10 *
                                                                  N2_6)))))
                                                            && ((1 <= C_6)
                                                                ||
                                                                (!(1 <=
                                                                   (C_6 +
                                                                    (-10 *
                                                                     N2_6)))))
                                                            &&
                                                            (!(1 <=
                                                               ((-1 * N2_6) +
                                                                (10 * I2_6))))
                                                            && (-9 <=
                                                                (C_6 +
                                                                 (-10 *
                                                                  N2_6)))
                                                            && (-9 <=
                                                                ((-1 * C_6) +
                                                                 (10 *
                                                                  N2_6)))))
               &&
               ((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * T2_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * T2_6)))))
                 && (1 <= T2_6) && (-9 <= (C_6 + (-10 * T2_6)))
                 && (-9 <= ((-1 * C_6) + (10 * T2_6)))) || (((C_6 <= -1)
                                                             ||
                                                             (!(1 <=
                                                                ((-1 * C_6) +
                                                                 (10 *
                                                                  S2_6)))))
                                                            && ((1 <= C_6)
                                                                ||
                                                                (!(1 <=
                                                                   (C_6 +
                                                                    (-10 *
                                                                     S2_6)))))
                                                            &&
                                                            (!(1 <=
                                                               (S2_6 +
                                                                (-10 *
                                                                 P2_6))))
                                                            && (-9 <=
                                                                (C_6 +
                                                                 (-10 *
                                                                  S2_6)))
                                                            && (-9 <=
                                                                ((-1 * C_6) +
                                                                 (10 *
                                                                  S2_6)))))
               &&
               ((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * W_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * W_6)))))
                 && (W_6 <= -1) && (-9 <= (C_6 + (-10 * W_6)))
                 && (-9 <= ((-1 * C_6) + (10 * W_6)))) || (((C_6 <= -1)
                                                            ||
                                                            (!(1 <=
                                                               ((-1 * C_6) +
                                                                (10 * V_6)))))
                                                           && ((1 <= C_6)
                                                               ||
                                                               (!(1 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    V_6)))))
                                                           &&
                                                           (!(1 <=
                                                              ((-1 * V_6) +
                                                               (10 * Q_6))))
                                                           && (-9 <=
                                                               (C_6 +
                                                                (-10 * V_6)))
                                                           && (-9 <=
                                                               ((-1 * C_6) +
                                                                (10 * V_6)))))
               &&
               ((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * M2_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * M2_6)))))
                 && (1 <= M2_6) && (-9 <= (C_6 + (-10 * M2_6)))
                 && (-9 <= ((-1 * C_6) + (10 * M2_6)))) || (((C_6 <= -1)
                                                             ||
                                                             (!(1 <=
                                                                ((-1 * C_6) +
                                                                 (10 *
                                                                  L2_6)))))
                                                            && ((1 <= C_6)
                                                                ||
                                                                (!(1 <=
                                                                   (C_6 +
                                                                    (-10 *
                                                                     L2_6)))))
                                                            &&
                                                            (!(1 <=
                                                               (L2_6 +
                                                                (-10 *
                                                                 I2_6))))
                                                            && (-9 <=
                                                                (C_6 +
                                                                 (-10 *
                                                                  L2_6)))
                                                            && (-9 <=
                                                                ((-1 * C_6) +
                                                                 (10 *
                                                                  L2_6)))))
               &&
               ((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * P_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * P_6)))))
                 && (P_6 <= -1) && (-9 <= (C_6 + (-10 * P_6)))
                 && (-9 <= ((-1 * C_6) + (10 * P_6)))) || (((C_6 <= -1)
                                                            ||
                                                            (!(1 <=
                                                               ((-1 * C_6) +
                                                                (10 * O_6)))))
                                                           && ((1 <= C_6)
                                                               ||
                                                               (!(1 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    O_6)))))
                                                           &&
                                                           (!(1 <=
                                                              ((-1 * O_6) +
                                                               (10 * J_6))))
                                                           && (-9 <=
                                                               (C_6 +
                                                                (-10 * O_6)))
                                                           && (-9 <=
                                                               ((-1 * C_6) +
                                                                (10 * O_6)))))
               &&
               ((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * U_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * U_6)))))
                 && (1 <= U_6) && (-9 <= (C_6 + (-10 * U_6)))
                 && (-9 <= ((-1 * C_6) + (10 * U_6)))) || (((C_6 <= -1)
                                                            ||
                                                            (!(1 <=
                                                               ((-1 * C_6) +
                                                                (10 * T_6)))))
                                                           && ((1 <= C_6)
                                                               ||
                                                               (!(1 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    T_6)))))
                                                           &&
                                                           (!(1 <=
                                                              (T_6 +
                                                               (-10 * Q_6))))
                                                           && (-9 <=
                                                               (C_6 +
                                                                (-10 * T_6)))
                                                           && (-9 <=
                                                               ((-1 * C_6) +
                                                                (10 * T_6)))))
               &&
               ((((C_6 <= -1) || (!(1 <= ((-1 * C_6) + (10 * N_6)))))
                 && ((1 <= C_6) || (!(1 <= (C_6 + (-10 * N_6)))))
                 && (1 <= N_6) && (-9 <= (C_6 + (-10 * N_6)))
                 && (-9 <= ((-1 * C_6) + (10 * N_6)))) || (((C_6 <= -1)
                                                            ||
                                                            (!(1 <=
                                                               ((-1 * C_6) +
                                                                (10 * M_6)))))
                                                           && ((1 <= C_6)
                                                               ||
                                                               (!(1 <=
                                                                  (C_6 +
                                                                   (-10 *
                                                                    M_6)))))
                                                           &&
                                                           (!(1 <=
                                                              (M_6 +
                                                               (-10 * J_6))))
                                                           && (-9 <=
                                                               (C_6 +
                                                                (-10 * M_6)))
                                                           && (-9 <=
                                                               ((-1 * C_6) +
                                                                (10 * M_6)))))
               && (A_6 == (B_6 + 3))))
              abort ();
          inv_main9_0 = E_6;
          inv_main9_1 = A_6;
          inv_main9_2 = H2_6;
          inv_main9_3 = F_6;
          inv_main9_4 = D_6;
          inv_main9_5 = G_6;
          inv_main9_6 = H_6;
          D_1 = inv_main9_0;
          A_1 = inv_main9_1;
          B_1 = inv_main9_2;
          E_1 = inv_main9_3;
          C_1 = inv_main9_4;
          F_1 = inv_main9_5;
          G_1 = inv_main9_6;
          if (!(1 <= B_1))
              abort ();
          inv_main11_0 = D_1;
          inv_main11_1 = A_1;
          inv_main11_2 = B_1;
          inv_main11_3 = E_1;
          inv_main11_4 = C_1;
          inv_main11_5 = F_1;
          inv_main11_6 = G_1;
          goto inv_main11;

      case 4:
          Q1_7 = __VERIFIER_nondet_int ();
          Q2_7 = __VERIFIER_nondet_int ();
          Q3_7 = __VERIFIER_nondet_int ();
          Q4_7 = __VERIFIER_nondet_int ();
          Q5_7 = __VERIFIER_nondet_int ();
          Q6_7 = __VERIFIER_nondet_int ();
          Q7_7 = __VERIFIER_nondet_int ();
          Q8_7 = __VERIFIER_nondet_int ();
          Q9_7 = __VERIFIER_nondet_int ();
          A1_7 = __VERIFIER_nondet_int ();
          A2_7 = __VERIFIER_nondet_int ();
          A3_7 = __VERIFIER_nondet_int ();
          A4_7 = __VERIFIER_nondet_int ();
          A5_7 = __VERIFIER_nondet_int ();
          A6_7 = __VERIFIER_nondet_int ();
          A7_7 = __VERIFIER_nondet_int ();
          A8_7 = __VERIFIER_nondet_int ();
          A9_7 = __VERIFIER_nondet_int ();
          R1_7 = __VERIFIER_nondet_int ();
          R2_7 = __VERIFIER_nondet_int ();
          A10_7 = __VERIFIER_nondet_int ();
          R3_7 = __VERIFIER_nondet_int ();
          A11_7 = __VERIFIER_nondet_int ();
          R4_7 = __VERIFIER_nondet_int ();
          A12_7 = __VERIFIER_nondet_int ();
          R5_7 = __VERIFIER_nondet_int ();
          R6_7 = __VERIFIER_nondet_int ();
          R7_7 = __VERIFIER_nondet_int ();
          R8_7 = __VERIFIER_nondet_int ();
          R9_7 = __VERIFIER_nondet_int ();
          I11_7 = __VERIFIER_nondet_int ();
          I10_7 = __VERIFIER_nondet_int ();
          B1_7 = __VERIFIER_nondet_int ();
          B2_7 = __VERIFIER_nondet_int ();
          Q11_7 = __VERIFIER_nondet_int ();
          B3_7 = __VERIFIER_nondet_int ();
          Q10_7 = __VERIFIER_nondet_int ();
          B4_7 = __VERIFIER_nondet_int ();
          B5_7 = __VERIFIER_nondet_int ();
          B6_7 = __VERIFIER_nondet_int ();
          B7_7 = __VERIFIER_nondet_int ();
          B8_7 = __VERIFIER_nondet_int ();
          B9_7 = __VERIFIER_nondet_int ();
          Y11_7 = __VERIFIER_nondet_int ();
          Y10_7 = __VERIFIER_nondet_int ();
          S1_7 = __VERIFIER_nondet_int ();
          S2_7 = __VERIFIER_nondet_int ();
          S3_7 = __VERIFIER_nondet_int ();
          A_7 = __VERIFIER_nondet_int ();
          S4_7 = __VERIFIER_nondet_int ();
          S5_7 = __VERIFIER_nondet_int ();
          S6_7 = __VERIFIER_nondet_int ();
          S7_7 = __VERIFIER_nondet_int ();
          S8_7 = __VERIFIER_nondet_int ();
          S9_7 = __VERIFIER_nondet_int ();
          I_7 = __VERIFIER_nondet_int ();
          J_7 = __VERIFIER_nondet_int ();
          K_7 = __VERIFIER_nondet_int ();
          L_7 = __VERIFIER_nondet_int ();
          M_7 = __VERIFIER_nondet_int ();
          N_7 = __VERIFIER_nondet_int ();
          C1_7 = __VERIFIER_nondet_int ();
          O_7 = __VERIFIER_nondet_int ();
          C2_7 = __VERIFIER_nondet_int ();
          P_7 = __VERIFIER_nondet_int ();
          C3_7 = __VERIFIER_nondet_int ();
          Q_7 = __VERIFIER_nondet_int ();
          C4_7 = __VERIFIER_nondet_int ();
          R_7 = __VERIFIER_nondet_int ();
          C5_7 = __VERIFIER_nondet_int ();
          S_7 = __VERIFIER_nondet_int ();
          C6_7 = __VERIFIER_nondet_int ();
          T_7 = __VERIFIER_nondet_int ();
          C7_7 = __VERIFIER_nondet_int ();
          U_7 = __VERIFIER_nondet_int ();
          C8_7 = __VERIFIER_nondet_int ();
          V_7 = __VERIFIER_nondet_int ();
          C9_7 = __VERIFIER_nondet_int ();
          W_7 = __VERIFIER_nondet_int ();
          X_7 = __VERIFIER_nondet_int ();
          Y_7 = __VERIFIER_nondet_int ();
          Z_7 = __VERIFIER_nondet_int ();
          T1_7 = __VERIFIER_nondet_int ();
          T2_7 = __VERIFIER_nondet_int ();
          T3_7 = __VERIFIER_nondet_int ();
          T4_7 = __VERIFIER_nondet_int ();
          T5_7 = __VERIFIER_nondet_int ();
          T6_7 = __VERIFIER_nondet_int ();
          T7_7 = __VERIFIER_nondet_int ();
          T8_7 = __VERIFIER_nondet_int ();
          T9_7 = __VERIFIER_nondet_int ();
          H10_7 = __VERIFIER_nondet_int ();
          H11_7 = __VERIFIER_nondet_int ();
          D1_7 = __VERIFIER_nondet_int ();
          D2_7 = __VERIFIER_nondet_int ();
          P10_7 = __VERIFIER_nondet_int ();
          D3_7 = __VERIFIER_nondet_int ();
          D4_7 = __VERIFIER_nondet_int ();
          D5_7 = __VERIFIER_nondet_int ();
          P11_7 = __VERIFIER_nondet_int ();
          D6_7 = __VERIFIER_nondet_int ();
          D7_7 = __VERIFIER_nondet_int ();
          D8_7 = __VERIFIER_nondet_int ();
          D9_7 = __VERIFIER_nondet_int ();
          X10_7 = __VERIFIER_nondet_int ();
          X11_7 = __VERIFIER_nondet_int ();
          U1_7 = __VERIFIER_nondet_int ();
          U2_7 = __VERIFIER_nondet_int ();
          U3_7 = __VERIFIER_nondet_int ();
          U4_7 = __VERIFIER_nondet_int ();
          U5_7 = __VERIFIER_nondet_int ();
          U6_7 = __VERIFIER_nondet_int ();
          U7_7 = __VERIFIER_nondet_int ();
          U8_7 = __VERIFIER_nondet_int ();
          U9_7 = __VERIFIER_nondet_int ();
          E1_7 = __VERIFIER_nondet_int ();
          E2_7 = __VERIFIER_nondet_int ();
          E3_7 = __VERIFIER_nondet_int ();
          E4_7 = __VERIFIER_nondet_int ();
          E5_7 = __VERIFIER_nondet_int ();
          E6_7 = __VERIFIER_nondet_int ();
          E7_7 = __VERIFIER_nondet_int ();
          E8_7 = __VERIFIER_nondet_int ();
          E9_7 = __VERIFIER_nondet_int ();
          V1_7 = __VERIFIER_nondet_int ();
          V2_7 = __VERIFIER_nondet_int ();
          V3_7 = __VERIFIER_nondet_int ();
          V4_7 = __VERIFIER_nondet_int ();
          V5_7 = __VERIFIER_nondet_int ();
          V6_7 = __VERIFIER_nondet_int ();
          V7_7 = __VERIFIER_nondet_int ();
          V8_7 = __VERIFIER_nondet_int ();
          V9_7 = __VERIFIER_nondet_int ();
          G11_7 = __VERIFIER_nondet_int ();
          G10_7 = __VERIFIER_nondet_int ();
          F1_7 = __VERIFIER_nondet_int ();
          F2_7 = __VERIFIER_nondet_int ();
          F3_7 = __VERIFIER_nondet_int ();
          F4_7 = __VERIFIER_nondet_int ();
          O11_7 = __VERIFIER_nondet_int ();
          F5_7 = __VERIFIER_nondet_int ();
          O10_7 = __VERIFIER_nondet_int ();
          F6_7 = __VERIFIER_nondet_int ();
          F7_7 = __VERIFIER_nondet_int ();
          F8_7 = __VERIFIER_nondet_int ();
          F9_7 = __VERIFIER_nondet_int ();
          W11_7 = __VERIFIER_nondet_int ();
          W10_7 = __VERIFIER_nondet_int ();
          W1_7 = __VERIFIER_nondet_int ();
          W2_7 = __VERIFIER_nondet_int ();
          W3_7 = __VERIFIER_nondet_int ();
          W4_7 = __VERIFIER_nondet_int ();
          W5_7 = __VERIFIER_nondet_int ();
          W6_7 = __VERIFIER_nondet_int ();
          W7_7 = __VERIFIER_nondet_int ();
          W8_7 = __VERIFIER_nondet_int ();
          W9_7 = __VERIFIER_nondet_int ();
          G1_7 = __VERIFIER_nondet_int ();
          G2_7 = __VERIFIER_nondet_int ();
          G3_7 = __VERIFIER_nondet_int ();
          G4_7 = __VERIFIER_nondet_int ();
          G5_7 = __VERIFIER_nondet_int ();
          G6_7 = __VERIFIER_nondet_int ();
          G7_7 = __VERIFIER_nondet_int ();
          G8_7 = __VERIFIER_nondet_int ();
          G9_7 = __VERIFIER_nondet_int ();
          X1_7 = __VERIFIER_nondet_int ();
          X2_7 = __VERIFIER_nondet_int ();
          X3_7 = __VERIFIER_nondet_int ();
          X4_7 = __VERIFIER_nondet_int ();
          X5_7 = __VERIFIER_nondet_int ();
          X6_7 = __VERIFIER_nondet_int ();
          X7_7 = __VERIFIER_nondet_int ();
          X8_7 = __VERIFIER_nondet_int ();
          X9_7 = __VERIFIER_nondet_int ();
          F10_7 = __VERIFIER_nondet_int ();
          F12_7 = __VERIFIER_nondet_int ();
          F11_7 = __VERIFIER_nondet_int ();
          H1_7 = __VERIFIER_nondet_int ();
          H2_7 = __VERIFIER_nondet_int ();
          H3_7 = __VERIFIER_nondet_int ();
          H4_7 = __VERIFIER_nondet_int ();
          N10_7 = __VERIFIER_nondet_int ();
          H5_7 = __VERIFIER_nondet_int ();
          H6_7 = __VERIFIER_nondet_int ();
          H7_7 = __VERIFIER_nondet_int ();
          N11_7 = __VERIFIER_nondet_int ();
          H8_7 = __VERIFIER_nondet_int ();
          H9_7 = __VERIFIER_nondet_int ();
          V10_7 = __VERIFIER_nondet_int ();
          V11_7 = __VERIFIER_nondet_int ();
          Y1_7 = __VERIFIER_nondet_int ();
          Y2_7 = __VERIFIER_nondet_int ();
          Y3_7 = __VERIFIER_nondet_int ();
          Y4_7 = __VERIFIER_nondet_int ();
          Y5_7 = __VERIFIER_nondet_int ();
          Y6_7 = __VERIFIER_nondet_int ();
          Y7_7 = __VERIFIER_nondet_int ();
          Y8_7 = __VERIFIER_nondet_int ();
          Y9_7 = __VERIFIER_nondet_int ();
          I1_7 = __VERIFIER_nondet_int ();
          I2_7 = __VERIFIER_nondet_int ();
          I3_7 = __VERIFIER_nondet_int ();
          I4_7 = __VERIFIER_nondet_int ();
          I5_7 = __VERIFIER_nondet_int ();
          I6_7 = __VERIFIER_nondet_int ();
          I7_7 = __VERIFIER_nondet_int ();
          I8_7 = __VERIFIER_nondet_int ();
          I9_7 = __VERIFIER_nondet_int ();
          Z1_7 = __VERIFIER_nondet_int ();
          Z2_7 = __VERIFIER_nondet_int ();
          Z3_7 = __VERIFIER_nondet_int ();
          Z4_7 = __VERIFIER_nondet_int ();
          Z5_7 = __VERIFIER_nondet_int ();
          Z6_7 = __VERIFIER_nondet_int ();
          Z7_7 = __VERIFIER_nondet_int ();
          Z8_7 = __VERIFIER_nondet_int ();
          Z9_7 = __VERIFIER_nondet_int ();
          E11_7 = __VERIFIER_nondet_int ();
          E10_7 = __VERIFIER_nondet_int ();
          J1_7 = __VERIFIER_nondet_int ();
          E12_7 = __VERIFIER_nondet_int ();
          J2_7 = __VERIFIER_nondet_int ();
          J3_7 = __VERIFIER_nondet_int ();
          J4_7 = __VERIFIER_nondet_int ();
          J5_7 = __VERIFIER_nondet_int ();
          J6_7 = __VERIFIER_nondet_int ();
          M11_7 = __VERIFIER_nondet_int ();
          J7_7 = __VERIFIER_nondet_int ();
          M10_7 = __VERIFIER_nondet_int ();
          J8_7 = __VERIFIER_nondet_int ();
          J9_7 = __VERIFIER_nondet_int ();
          U11_7 = __VERIFIER_nondet_int ();
          U10_7 = __VERIFIER_nondet_int ();
          K1_7 = __VERIFIER_nondet_int ();
          K2_7 = __VERIFIER_nondet_int ();
          K3_7 = __VERIFIER_nondet_int ();
          K4_7 = __VERIFIER_nondet_int ();
          K5_7 = __VERIFIER_nondet_int ();
          K6_7 = __VERIFIER_nondet_int ();
          K7_7 = __VERIFIER_nondet_int ();
          K8_7 = __VERIFIER_nondet_int ();
          K9_7 = __VERIFIER_nondet_int ();
          D10_7 = __VERIFIER_nondet_int ();
          D12_7 = __VERIFIER_nondet_int ();
          L1_7 = __VERIFIER_nondet_int ();
          D11_7 = __VERIFIER_nondet_int ();
          L2_7 = __VERIFIER_nondet_int ();
          L3_7 = __VERIFIER_nondet_int ();
          L4_7 = __VERIFIER_nondet_int ();
          L5_7 = __VERIFIER_nondet_int ();
          L6_7 = __VERIFIER_nondet_int ();
          L10_7 = __VERIFIER_nondet_int ();
          L7_7 = __VERIFIER_nondet_int ();
          L8_7 = __VERIFIER_nondet_int ();
          L9_7 = __VERIFIER_nondet_int ();
          L11_7 = __VERIFIER_nondet_int ();
          T10_7 = __VERIFIER_nondet_int ();
          T11_7 = __VERIFIER_nondet_int ();
          M1_7 = __VERIFIER_nondet_int ();
          M2_7 = __VERIFIER_nondet_int ();
          M3_7 = __VERIFIER_nondet_int ();
          M4_7 = __VERIFIER_nondet_int ();
          M5_7 = __VERIFIER_nondet_int ();
          M6_7 = __VERIFIER_nondet_int ();
          M7_7 = __VERIFIER_nondet_int ();
          M8_7 = __VERIFIER_nondet_int ();
          M9_7 = __VERIFIER_nondet_int ();
          C11_7 = __VERIFIER_nondet_int ();
          N1_7 = __VERIFIER_nondet_int ();
          C10_7 = __VERIFIER_nondet_int ();
          N2_7 = __VERIFIER_nondet_int ();
          N3_7 = __VERIFIER_nondet_int ();
          C12_7 = __VERIFIER_nondet_int ();
          N4_7 = __VERIFIER_nondet_int ();
          N5_7 = __VERIFIER_nondet_int ();
          N6_7 = __VERIFIER_nondet_int ();
          N7_7 = __VERIFIER_nondet_int ();
          N8_7 = __VERIFIER_nondet_int ();
          K11_7 = __VERIFIER_nondet_int ();
          N9_7 = __VERIFIER_nondet_int ();
          K10_7 = __VERIFIER_nondet_int ();
          S11_7 = __VERIFIER_nondet_int ();
          S10_7 = __VERIFIER_nondet_int ();
          O1_7 = __VERIFIER_nondet_int ();
          O2_7 = __VERIFIER_nondet_int ();
          O3_7 = __VERIFIER_nondet_int ();
          O4_7 = __VERIFIER_nondet_int ();
          O5_7 = __VERIFIER_nondet_int ();
          O6_7 = __VERIFIER_nondet_int ();
          O7_7 = __VERIFIER_nondet_int ();
          O8_7 = __VERIFIER_nondet_int ();
          O9_7 = __VERIFIER_nondet_int ();
          P1_7 = __VERIFIER_nondet_int ();
          B10_7 = __VERIFIER_nondet_int ();
          P2_7 = __VERIFIER_nondet_int ();
          B11_7 = __VERIFIER_nondet_int ();
          P3_7 = __VERIFIER_nondet_int ();
          B12_7 = __VERIFIER_nondet_int ();
          P4_7 = __VERIFIER_nondet_int ();
          P5_7 = __VERIFIER_nondet_int ();
          P6_7 = __VERIFIER_nondet_int ();
          P7_7 = __VERIFIER_nondet_int ();
          P8_7 = __VERIFIER_nondet_int ();
          J10_7 = __VERIFIER_nondet_int ();
          P9_7 = __VERIFIER_nondet_int ();
          J11_7 = __VERIFIER_nondet_int ();
          R10_7 = __VERIFIER_nondet_int ();
          R11_7 = __VERIFIER_nondet_int ();
          Z10_7 = __VERIFIER_nondet_int ();
          Z11_7 = __VERIFIER_nondet_int ();
          E_7 = inv_main11_0;
          B_7 = inv_main11_1;
          C_7 = inv_main11_2;
          F_7 = inv_main11_3;
          D_7 = inv_main11_4;
          G_7 = inv_main11_5;
          H_7 = inv_main11_6;
          if (!
              ((-9 <= ((-1 * I4_7) + (10 * H4_7)))
               && (-9 <= ((-1 * A4_7) + (10 * Z3_7)))
               && (-9 <= ((-1 * B4_7) + (10 * A4_7)))
               && (-9 <= ((-1 * R2_7) + (10 * Q2_7)))
               && (-9 <= ((-1 * I2_7) + (10 * H2_7)))
               && (-9 <= ((-1 * J2_7) + (10 * I2_7)))
               && (-9 <= ((-1 * K2_7) + (10 * J2_7)))
               && (-9 <= ((-1 * A2_7) + (10 * Z1_7)))
               && (-9 <= ((-1 * R_7) + (10 * Q_7)))
               && (-9 <= ((-1 * J_7) + (10 * I_7)))
               && (-9 <= ((-1 * K_7) + (10 * J_7)))
               && (-9 <= ((-1 * C_7) + (10 * J4_7)))
               && (-9 <= ((-1 * C_7) + (10 * I4_7)))
               && (-9 <= ((-1 * C_7) + (10 * C4_7)))
               && (-9 <= ((-1 * C_7) + (10 * B4_7)))
               && (-9 <= ((-1 * C_7) + (10 * S2_7)))
               && (-9 <= ((-1 * C_7) + (10 * R2_7)))
               && (-9 <= ((-1 * C_7) + (10 * L2_7)))
               && (-9 <= ((-1 * C_7) + (10 * K2_7)))
               && (-9 <= ((-1 * C_7) + (10 * G2_7)))
               && (-9 <= ((-1 * C_7) + (10 * B2_7)))
               && (-9 <= ((-1 * C_7) + (10 * A2_7)))
               && (-9 <= ((-1 * C_7) + (10 * S_7)))
               && (-9 <= ((-1 * C_7) + (10 * R_7)))
               && (-9 <= ((-1 * C_7) + (10 * L_7)))
               && (-9 <= ((-1 * C_7) + (10 * K_7)))
               && (-9 <= (J4_7 + (-10 * H4_7)))
               && (-9 <= (H4_7 + (-10 * Z3_7)))
               && (-9 <= (Z3_7 + (-10 * H2_7)))
               && (-9 <= (C4_7 + (-10 * A4_7)))
               && (-9 <= (S2_7 + (-10 * Q2_7)))
               && (-9 <= (Q2_7 + (-10 * I2_7)))
               && (-9 <= (L2_7 + (-10 * J2_7)))
               && (-9 <= (B2_7 + (-10 * Z1_7))) && (-9 <= (S_7 + (-10 * Q_7)))
               && (-9 <= (Q_7 + (-10 * I_7))) && (-9 <= (L_7 + (-10 * J_7)))
               && (-9 <= (C_7 + (-10 * J4_7))) && (-9 <= (C_7 + (-10 * I4_7)))
               && (-9 <= (C_7 + (-10 * C4_7))) && (-9 <= (C_7 + (-10 * B4_7)))
               && (-9 <= (C_7 + (-10 * S2_7))) && (-9 <= (C_7 + (-10 * R2_7)))
               && (-9 <= (C_7 + (-10 * L2_7))) && (-9 <= (C_7 + (-10 * K2_7)))
               && (-9 <= (C_7 + (-10 * G2_7))) && (-9 <= (C_7 + (-10 * B2_7)))
               && (-9 <= (C_7 + (-10 * A2_7))) && (-9 <= (C_7 + (-10 * S_7)))
               && (-9 <= (C_7 + (-10 * R_7))) && (-9 <= (C_7 + (-10 * L_7)))
               && (-9 <= (C_7 + (-10 * K_7))) && (1 <= G2_7) && (1 <= Z1_7)
               && (1 <= I_7) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * J4_7)))))
               && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * I4_7)))))
               && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * C4_7)))))
               && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * B4_7)))))
               && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * S2_7)))))
               && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * R2_7)))))
               && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * L2_7)))))
               && ((!(1 <= (C_7 + (-10 * K2_7)))) || (1 <= C_7))
               && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * G2_7)))))
               && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * B2_7)))))
               && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * A2_7)))))
               && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * S_7))))) && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_7 +
                                                                        (-10 *
                                                                         R_7)))))
               && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * L_7))))) && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_7 +
                                                                        (-10 *
                                                                         K_7)))))
               && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * J4_7)))))
               && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * I4_7)))))
               && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * C4_7)))))
               && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * B4_7)))))
               && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * S2_7)))))
               && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * R2_7)))))
               && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * L2_7)))))
               && ((!(1 <= ((-1 * C_7) + (10 * K2_7)))) || (C_7 <= -1))
               && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * G2_7)))))
               && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * B2_7)))))
               && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * A2_7)))))
               && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * S_7)))))
               && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * R_7)))))
               && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * L_7)))))
               && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * K_7)))))
               &&
               ((((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * D9_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * D9_7)))))
                   && (1 <= D9_7) && (-9 <= (C_7 + (-10 * D9_7)))
                   && (-9 <= ((-1 * C_7) + (10 * D9_7)))) || (((C_7 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    C9_7)))))
                                                              && ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       C9_7)))))
                                                              &&
                                                              (!(1 <=
                                                                 (C9_7 +
                                                                  (-10 *
                                                                   Z8_7))))
                                                              && (-9 <=
                                                                  (C_7 +
                                                                   (-10 *
                                                                    C9_7)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    C9_7)))))
                 &&
                 ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * F9_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * F9_7)))))
                   && (F9_7 <= -1) && (-9 <= (C_7 + (-10 * F9_7)))
                   && (-9 <= ((-1 * C_7) + (10 * F9_7)))) || (((C_7 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    E9_7)))))
                                                              && ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       E9_7)))))
                                                              &&
                                                              (!(1 <=
                                                                 ((-1 *
                                                                   E9_7) +
                                                                  (10 *
                                                                   Z8_7))))
                                                              && (-9 <=
                                                                  (C_7 +
                                                                   (-10 *
                                                                    E9_7)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    E9_7)))))
                 &&
                 ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * K9_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * K9_7)))))
                   && (1 <= K9_7) && (-9 <= (C_7 + (-10 * K9_7)))
                   && (-9 <= ((-1 * C_7) + (10 * K9_7)))) || (((C_7 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    J9_7)))))
                                                              && ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       J9_7)))))
                                                              &&
                                                              (!(1 <=
                                                                 (J9_7 +
                                                                  (-10 *
                                                                   G9_7))))
                                                              && (-9 <=
                                                                  (C_7 +
                                                                   (-10 *
                                                                    J9_7)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    J9_7)))))
                 &&
                 ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * M9_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * M9_7)))))
                   && (M9_7 <= -1) && (-9 <= (C_7 + (-10 * M9_7)))
                   && (-9 <= ((-1 * C_7) + (10 * M9_7)))) || (((C_7 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    L9_7)))))
                                                              && ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       L9_7)))))
                                                              &&
                                                              (!(1 <=
                                                                 ((-1 *
                                                                   L9_7) +
                                                                  (10 *
                                                                   G9_7))))
                                                              && (-9 <=
                                                                  (C_7 +
                                                                   (-10 *
                                                                    L9_7)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    L9_7)))))
                 &&
                 ((((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * R9_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * R9_7)))))
                     && (1 <= R9_7) && (-9 <= (C_7 + (-10 * R9_7)))
                     && (-9 <= ((-1 * C_7) + (10 * R9_7)))) || (((C_7 <= -1)
                                                                 ||
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      Q9_7)))))
                                                                && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_7 +
                                                                        (-10 *
                                                                         Q9_7)))))
                                                                &&
                                                                (!(1 <=
                                                                   (Q9_7 +
                                                                    (-10 *
                                                                     N9_7))))
                                                                && (-9 <=
                                                                    (C_7 +
                                                                     (-10 *
                                                                      Q9_7)))
                                                                && (-9 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      Q9_7)))))
                   &&
                   ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * T9_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * T9_7)))))
                     && (T9_7 <= -1) && (-9 <= (C_7 + (-10 * T9_7)))
                     && (-9 <= ((-1 * C_7) + (10 * T9_7)))) || (((C_7 <= -1)
                                                                 ||
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      S9_7)))))
                                                                && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_7 +
                                                                        (-10 *
                                                                         S9_7)))))
                                                                &&
                                                                (!(1 <=
                                                                   ((-1 *
                                                                     S9_7) +
                                                                    (10 *
                                                                     N9_7))))
                                                                && (-9 <=
                                                                    (C_7 +
                                                                     (-10 *
                                                                      S9_7)))
                                                                && (-9 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      S9_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * O9_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * P9_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * O9_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * P9_7)))))
                   && (!(1 <= (N9_7 + (-10 * Y8_7))))
                   && (-9 <= (P9_7 + (-10 * N9_7)))
                   && (-9 <= (C_7 + (-10 * O9_7)))
                   && (-9 <= (C_7 + (-10 * P9_7)))
                   && (-9 <= ((-1 * O9_7) + (10 * N9_7)))
                   && (-9 <= ((-1 * C_7) + (10 * O9_7)))
                   && (-9 <= ((-1 * C_7) + (10 * P9_7))))
                  ||
                  (((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Y9_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Y9_7)))))
                     && (1 <= Y9_7) && (-9 <= (C_7 + (-10 * Y9_7)))
                     && (-9 <= ((-1 * C_7) + (10 * Y9_7)))) || (((C_7 <= -1)
                                                                 ||
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      X9_7)))))
                                                                && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_7 +
                                                                        (-10 *
                                                                         X9_7)))))
                                                                &&
                                                                (!(1 <=
                                                                   (X9_7 +
                                                                    (-10 *
                                                                     U9_7))))
                                                                && (-9 <=
                                                                    (C_7 +
                                                                     (-10 *
                                                                      X9_7)))
                                                                && (-9 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      X9_7)))))
                   &&
                   ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * A10_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * A10_7)))))
                     && (A10_7 <= -1) && (-9 <= (C_7 + (-10 * A10_7)))
                     && (-9 <= ((-1 * C_7) + (10 * A10_7)))) || (((C_7 <= -1)
                                                                  ||
                                                                  (!(1 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       Z9_7)))))
                                                                 &&
                                                                 ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       Z9_7)))))
                                                                 &&
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      Z9_7) +
                                                                     (10 *
                                                                      U9_7))))
                                                                 && (-9 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       Z9_7)))
                                                                 && (-9 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       Z9_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * V9_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * W9_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * V9_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * W9_7)))))
                   && (1 <= U9_7) && (-9 <= (W9_7 + (-10 * U9_7)))
                   && (-9 <= (C_7 + (-10 * V9_7)))
                   && (-9 <= (C_7 + (-10 * W9_7)))
                   && (-9 <= ((-1 * V9_7) + (10 * U9_7)))
                   && (-9 <= ((-1 * C_7) + (10 * V9_7)))
                   && (-9 <= ((-1 * C_7) + (10 * W9_7)))))
                 &&
                 ((((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * F10_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * F10_7)))))
                     && (1 <= F10_7) && (-9 <= (C_7 + (-10 * F10_7)))
                     && (-9 <= ((-1 * C_7) + (10 * F10_7)))) || (((C_7 <= -1)
                                                                  ||
                                                                  (!(1 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       E10_7)))))
                                                                 &&
                                                                 ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       E10_7)))))
                                                                 &&
                                                                 (!(1 <=
                                                                    (E10_7 +
                                                                     (-10 *
                                                                      B10_7))))
                                                                 && (-9 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       E10_7)))
                                                                 && (-9 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       E10_7)))))
                   &&
                   ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * H10_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * H10_7)))))
                     && (H10_7 <= -1) && (-9 <= (C_7 + (-10 * H10_7)))
                     && (-9 <= ((-1 * C_7) + (10 * H10_7)))) || (((C_7 <= -1)
                                                                  ||
                                                                  (!(1 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       G10_7)))))
                                                                 &&
                                                                 ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       G10_7)))))
                                                                 &&
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      G10_7) +
                                                                     (10 *
                                                                      B10_7))))
                                                                 && (-9 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       G10_7)))
                                                                 && (-9 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       G10_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * C10_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * D10_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * C10_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * D10_7)))))
                   && (!(1 <= ((-1 * B10_7) + (10 * Y8_7))))
                   && (-9 <= (D10_7 + (-10 * B10_7)))
                   && (-9 <= (C_7 + (-10 * C10_7)))
                   && (-9 <= (C_7 + (-10 * D10_7)))
                   && (-9 <= ((-1 * C10_7) + (10 * B10_7)))
                   && (-9 <= ((-1 * C_7) + (10 * C10_7)))
                   && (-9 <= ((-1 * C_7) + (10 * D10_7))))
                  ||
                  (((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * M10_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * M10_7)))))
                     && (1 <= M10_7) && (-9 <= (C_7 + (-10 * M10_7)))
                     && (-9 <= ((-1 * C_7) + (10 * M10_7)))) || (((C_7 <= -1)
                                                                  ||
                                                                  (!(1 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       L10_7)))))
                                                                 &&
                                                                 ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       L10_7)))))
                                                                 &&
                                                                 (!(1 <=
                                                                    (L10_7 +
                                                                     (-10 *
                                                                      I10_7))))
                                                                 && (-9 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       L10_7)))
                                                                 && (-9 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       L10_7)))))
                   &&
                   ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * O10_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * O10_7)))))
                     && (O10_7 <= -1) && (-9 <= (C_7 + (-10 * O10_7)))
                     && (-9 <= ((-1 * C_7) + (10 * O10_7)))) || (((C_7 <= -1)
                                                                  ||
                                                                  (!(1 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       N10_7)))))
                                                                 &&
                                                                 ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       N10_7)))))
                                                                 &&
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      N10_7) +
                                                                     (10 *
                                                                      I10_7))))
                                                                 && (-9 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       N10_7)))
                                                                 && (-9 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       N10_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * J10_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * K10_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * J10_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * K10_7)))))
                   && (I10_7 <= -1) && (-9 <= (K10_7 + (-10 * I10_7)))
                   && (-9 <= (C_7 + (-10 * J10_7)))
                   && (-9 <= (C_7 + (-10 * K10_7)))
                   && (-9 <= ((-1 * J10_7) + (10 * I10_7)))
                   && (-9 <= ((-1 * C_7) + (10 * J10_7)))
                   && (-9 <= ((-1 * C_7) + (10 * K10_7))))) && ((C_7 <= -1)
                                                                ||
                                                                (!(1 <=
                                                                   ((-1 *
                                                                     C_7) +
                                                                    (10 *
                                                                     A9_7)))))
                 && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * B9_7)))))
                 && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * H9_7)))))
                 && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * I9_7)))))
                 && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * A9_7)))))
                 && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * B9_7)))))
                 && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * H9_7)))))
                 && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * I9_7)))))
                 && (!(1 <= ((-1 * Y8_7) + (10 * H2_7))))
                 && (-9 <= (B9_7 + (-10 * Z8_7)))
                 && (-9 <= (G9_7 + (-10 * Y8_7)))
                 && (-9 <= (I9_7 + (-10 * G9_7)))
                 && (-9 <= (C_7 + (-10 * A9_7)))
                 && (-9 <= (C_7 + (-10 * B9_7)))
                 && (-9 <= (C_7 + (-10 * H9_7)))
                 && (-9 <= (C_7 + (-10 * I9_7)))
                 && (-9 <= ((-1 * Z8_7) + (10 * Y8_7)))
                 && (-9 <= ((-1 * A9_7) + (10 * Z8_7)))
                 && (-9 <= ((-1 * H9_7) + (10 * G9_7)))
                 && (-9 <= ((-1 * C_7) + (10 * A9_7)))
                 && (-9 <= ((-1 * C_7) + (10 * B9_7)))
                 && (-9 <= ((-1 * C_7) + (10 * H9_7)))
                 && (-9 <= ((-1 * C_7) + (10 * I9_7))))
                ||
                (((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * U10_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * U10_7)))))
                   && (1 <= U10_7) && (-9 <= (C_7 + (-10 * U10_7)))
                   && (-9 <= ((-1 * C_7) + (10 * U10_7)))) || (((C_7 <= -1)
                                                                ||
                                                                (!(1 <=
                                                                   ((-1 *
                                                                     C_7) +
                                                                    (10 *
                                                                     T10_7)))))
                                                               && ((1 <= C_7)
                                                                   ||
                                                                   (!(1 <=
                                                                      (C_7 +
                                                                       (-10 *
                                                                        T10_7)))))
                                                               &&
                                                               (!(1 <=
                                                                  (T10_7 +
                                                                   (-10 *
                                                                    Q10_7))))
                                                               && (-9 <=
                                                                   (C_7 +
                                                                    (-10 *
                                                                     T10_7)))
                                                               && (-9 <=
                                                                   ((-1 *
                                                                     C_7) +
                                                                    (10 *
                                                                     T10_7)))))
                 &&
                 ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * W10_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * W10_7)))))
                   && (W10_7 <= -1) && (-9 <= (C_7 + (-10 * W10_7)))
                   && (-9 <= ((-1 * C_7) + (10 * W10_7)))) || (((C_7 <= -1)
                                                                ||
                                                                (!(1 <=
                                                                   ((-1 *
                                                                     C_7) +
                                                                    (10 *
                                                                     V10_7)))))
                                                               && ((1 <= C_7)
                                                                   ||
                                                                   (!(1 <=
                                                                      (C_7 +
                                                                       (-10 *
                                                                        V10_7)))))
                                                               &&
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    V10_7) +
                                                                   (10 *
                                                                    Q10_7))))
                                                               && (-9 <=
                                                                   (C_7 +
                                                                    (-10 *
                                                                     V10_7)))
                                                               && (-9 <=
                                                                   ((-1 *
                                                                     C_7) +
                                                                    (10 *
                                                                     V10_7)))))
                 &&
                 ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * B11_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * B11_7)))))
                   && (1 <= B11_7) && (-9 <= (C_7 + (-10 * B11_7)))
                   && (-9 <= ((-1 * C_7) + (10 * B11_7)))) || (((C_7 <= -1)
                                                                ||
                                                                (!(1 <=
                                                                   ((-1 *
                                                                     C_7) +
                                                                    (10 *
                                                                     A11_7)))))
                                                               && ((1 <= C_7)
                                                                   ||
                                                                   (!(1 <=
                                                                      (C_7 +
                                                                       (-10 *
                                                                        A11_7)))))
                                                               &&
                                                               (!(1 <=
                                                                  (A11_7 +
                                                                   (-10 *
                                                                    X10_7))))
                                                               && (-9 <=
                                                                   (C_7 +
                                                                    (-10 *
                                                                     A11_7)))
                                                               && (-9 <=
                                                                   ((-1 *
                                                                     C_7) +
                                                                    (10 *
                                                                     A11_7)))))
                 &&
                 ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * D11_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * D11_7)))))
                   && (D11_7 <= -1) && (-9 <= (C_7 + (-10 * D11_7)))
                   && (-9 <= ((-1 * C_7) + (10 * D11_7)))) || (((C_7 <= -1)
                                                                ||
                                                                (!(1 <=
                                                                   ((-1 *
                                                                     C_7) +
                                                                    (10 *
                                                                     C11_7)))))
                                                               && ((1 <= C_7)
                                                                   ||
                                                                   (!(1 <=
                                                                      (C_7 +
                                                                       (-10 *
                                                                        C11_7)))))
                                                               &&
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C11_7) +
                                                                   (10 *
                                                                    X10_7))))
                                                               && (-9 <=
                                                                   (C_7 +
                                                                    (-10 *
                                                                     C11_7)))
                                                               && (-9 <=
                                                                   ((-1 *
                                                                     C_7) +
                                                                    (10 *
                                                                     C11_7)))))
                 &&
                 ((((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * I11_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * I11_7)))))
                     && (1 <= I11_7) && (-9 <= (C_7 + (-10 * I11_7)))
                     && (-9 <= ((-1 * C_7) + (10 * I11_7)))) || (((C_7 <= -1)
                                                                  ||
                                                                  (!(1 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       H11_7)))))
                                                                 &&
                                                                 ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       H11_7)))))
                                                                 &&
                                                                 (!(1 <=
                                                                    (H11_7 +
                                                                     (-10 *
                                                                      E11_7))))
                                                                 && (-9 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       H11_7)))
                                                                 && (-9 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       H11_7)))))
                   &&
                   ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * K11_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * K11_7)))))
                     && (K11_7 <= -1) && (-9 <= (C_7 + (-10 * K11_7)))
                     && (-9 <= ((-1 * C_7) + (10 * K11_7)))) || (((C_7 <= -1)
                                                                  ||
                                                                  (!(1 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       J11_7)))))
                                                                 &&
                                                                 ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       J11_7)))))
                                                                 &&
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      J11_7) +
                                                                     (10 *
                                                                      E11_7))))
                                                                 && (-9 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       J11_7)))
                                                                 && (-9 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       J11_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * F11_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * G11_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * F11_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * G11_7)))))
                   && (!(1 <= (E11_7 + (-10 * P10_7))))
                   && (-9 <= (G11_7 + (-10 * E11_7)))
                   && (-9 <= (C_7 + (-10 * F11_7)))
                   && (-9 <= (C_7 + (-10 * G11_7)))
                   && (-9 <= ((-1 * F11_7) + (10 * E11_7)))
                   && (-9 <= ((-1 * C_7) + (10 * F11_7)))
                   && (-9 <= ((-1 * C_7) + (10 * G11_7))))
                  ||
                  (((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * P11_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * P11_7)))))
                     && (1 <= P11_7) && (-9 <= (C_7 + (-10 * P11_7)))
                     && (-9 <= ((-1 * C_7) + (10 * P11_7)))) || (((C_7 <= -1)
                                                                  ||
                                                                  (!(1 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       O11_7)))))
                                                                 &&
                                                                 ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       O11_7)))))
                                                                 &&
                                                                 (!(1 <=
                                                                    (O11_7 +
                                                                     (-10 *
                                                                      L11_7))))
                                                                 && (-9 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       O11_7)))
                                                                 && (-9 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       O11_7)))))
                   &&
                   ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * R11_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * R11_7)))))
                     && (R11_7 <= -1) && (-9 <= (C_7 + (-10 * R11_7)))
                     && (-9 <= ((-1 * C_7) + (10 * R11_7)))) || (((C_7 <= -1)
                                                                  ||
                                                                  (!(1 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       Q11_7)))))
                                                                 &&
                                                                 ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       Q11_7)))))
                                                                 &&
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      Q11_7) +
                                                                     (10 *
                                                                      L11_7))))
                                                                 && (-9 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       Q11_7)))
                                                                 && (-9 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       Q11_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * N11_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * M11_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * N11_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * M11_7)))))
                   && (1 <= L11_7) && (-9 <= (N11_7 + (-10 * L11_7)))
                   && (-9 <= (C_7 + (-10 * N11_7)))
                   && (-9 <= (C_7 + (-10 * M11_7)))
                   && (-9 <= ((-1 * M11_7) + (10 * L11_7)))
                   && (-9 <= ((-1 * C_7) + (10 * N11_7)))
                   && (-9 <= ((-1 * C_7) + (10 * M11_7)))))
                 &&
                 ((((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * W11_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * W11_7)))))
                     && (1 <= W11_7) && (-9 <= (C_7 + (-10 * W11_7)))
                     && (-9 <= ((-1 * C_7) + (10 * W11_7)))) || (((C_7 <= -1)
                                                                  ||
                                                                  (!(1 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       V11_7)))))
                                                                 &&
                                                                 ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       V11_7)))))
                                                                 &&
                                                                 (!(1 <=
                                                                    (V11_7 +
                                                                     (-10 *
                                                                      S11_7))))
                                                                 && (-9 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       V11_7)))
                                                                 && (-9 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       V11_7)))))
                   &&
                   ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Y11_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Y11_7)))))
                     && (Y11_7 <= -1) && (-9 <= (C_7 + (-10 * Y11_7)))
                     && (-9 <= ((-1 * C_7) + (10 * Y11_7)))) || (((C_7 <= -1)
                                                                  ||
                                                                  (!(1 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       X11_7)))))
                                                                 &&
                                                                 ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       X11_7)))))
                                                                 &&
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      X11_7) +
                                                                     (10 *
                                                                      S11_7))))
                                                                 && (-9 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       X11_7)))
                                                                 && (-9 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       X11_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * T11_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * U11_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * T11_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * U11_7)))))
                   && (!(1 <= ((-1 * S11_7) + (10 * P10_7))))
                   && (-9 <= (U11_7 + (-10 * S11_7)))
                   && (-9 <= (C_7 + (-10 * T11_7)))
                   && (-9 <= (C_7 + (-10 * U11_7)))
                   && (-9 <= ((-1 * T11_7) + (10 * S11_7)))
                   && (-9 <= ((-1 * C_7) + (10 * T11_7)))
                   && (-9 <= ((-1 * C_7) + (10 * U11_7))))
                  ||
                  (((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * D12_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * D12_7)))))
                     && (1 <= D12_7) && (-9 <= (C_7 + (-10 * D12_7)))
                     && (-9 <= ((-1 * C_7) + (10 * D12_7)))) || (((C_7 <= -1)
                                                                  ||
                                                                  (!(1 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       C12_7)))))
                                                                 &&
                                                                 ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       C12_7)))))
                                                                 &&
                                                                 (!(1 <=
                                                                    (C12_7 +
                                                                     (-10 *
                                                                      Z11_7))))
                                                                 && (-9 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       C12_7)))
                                                                 && (-9 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       C12_7)))))
                   &&
                   ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * F12_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * F12_7)))))
                     && (F12_7 <= -1) && (-9 <= (C_7 + (-10 * F12_7)))
                     && (-9 <= ((-1 * C_7) + (10 * F12_7)))) || (((C_7 <= -1)
                                                                  ||
                                                                  (!(1 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       E12_7)))))
                                                                 &&
                                                                 ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       E12_7)))))
                                                                 &&
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      E12_7) +
                                                                     (10 *
                                                                      Z11_7))))
                                                                 && (-9 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       E12_7)))
                                                                 && (-9 <=
                                                                     ((-1 *
                                                                       C_7) +
                                                                      (10 *
                                                                       E12_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * A12_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * B12_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * A12_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * B12_7)))))
                   && (Z11_7 <= -1) && (-9 <= (B12_7 + (-10 * Z11_7)))
                   && (-9 <= (C_7 + (-10 * A12_7)))
                   && (-9 <= (C_7 + (-10 * B12_7)))
                   && (-9 <= ((-1 * A12_7) + (10 * Z11_7)))
                   && (-9 <= ((-1 * C_7) + (10 * A12_7)))
                   && (-9 <= ((-1 * C_7) + (10 * B12_7))))) && ((C_7 <= -1)
                                                                ||
                                                                (!(1 <=
                                                                   ((-1 *
                                                                     C_7) +
                                                                    (10 *
                                                                     R10_7)))))
                 && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * S10_7)))))
                 && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Y10_7)))))
                 && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Z10_7)))))
                 && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * R10_7)))))
                 && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * S10_7)))))
                 && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Y10_7)))))
                 && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Z10_7)))))
                 && (P10_7 <= -1) && (-9 <= (S10_7 + (-10 * Q10_7)))
                 && (-9 <= (X10_7 + (-10 * P10_7)))
                 && (-9 <= (Z10_7 + (-10 * X10_7)))
                 && (-9 <= (C_7 + (-10 * R10_7)))
                 && (-9 <= (C_7 + (-10 * S10_7)))
                 && (-9 <= (C_7 + (-10 * Y10_7)))
                 && (-9 <= (C_7 + (-10 * Z10_7)))
                 && (-9 <= ((-1 * R10_7) + (10 * Q10_7)))
                 && (-9 <= ((-1 * Q10_7) + (10 * P10_7)))
                 && (-9 <= ((-1 * Y10_7) + (10 * X10_7)))
                 && (-9 <= ((-1 * C_7) + (10 * R10_7)))
                 && (-9 <= ((-1 * C_7) + (10 * S10_7)))
                 && (-9 <= ((-1 * C_7) + (10 * Y10_7)))
                 && (-9 <= ((-1 * C_7) + (10 * Z10_7)))))
               &&
               ((((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * V5_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * V5_7)))))
                   && (1 <= V5_7) && (-9 <= (C_7 + (-10 * V5_7)))
                   && (-9 <= ((-1 * C_7) + (10 * V5_7)))) || (((C_7 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    U5_7)))))
                                                              && ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       U5_7)))))
                                                              &&
                                                              (!(1 <=
                                                                 (U5_7 +
                                                                  (-10 *
                                                                   R5_7))))
                                                              && (-9 <=
                                                                  (C_7 +
                                                                   (-10 *
                                                                    U5_7)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    U5_7)))))
                 &&
                 ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * X5_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * X5_7)))))
                   && (X5_7 <= -1) && (-9 <= (C_7 + (-10 * X5_7)))
                   && (-9 <= ((-1 * C_7) + (10 * X5_7)))) || (((C_7 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    W5_7)))))
                                                              && ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       W5_7)))))
                                                              &&
                                                              (!(1 <=
                                                                 ((-1 *
                                                                   W5_7) +
                                                                  (10 *
                                                                   R5_7))))
                                                              && (-9 <=
                                                                  (C_7 +
                                                                   (-10 *
                                                                    W5_7)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    W5_7)))))
                 &&
                 ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * C6_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * C6_7)))))
                   && (1 <= C6_7) && (-9 <= (C_7 + (-10 * C6_7)))
                   && (-9 <= ((-1 * C_7) + (10 * C6_7)))) || (((C_7 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    B6_7)))))
                                                              && ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       B6_7)))))
                                                              &&
                                                              (!(1 <=
                                                                 (B6_7 +
                                                                  (-10 *
                                                                   Y5_7))))
                                                              && (-9 <=
                                                                  (C_7 +
                                                                   (-10 *
                                                                    B6_7)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    B6_7)))))
                 &&
                 ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * E6_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * E6_7)))))
                   && (E6_7 <= -1) && (-9 <= (C_7 + (-10 * E6_7)))
                   && (-9 <= ((-1 * C_7) + (10 * E6_7)))) || (((C_7 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    D6_7)))))
                                                              && ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       D6_7)))))
                                                              &&
                                                              (!(1 <=
                                                                 ((-1 *
                                                                   D6_7) +
                                                                  (10 *
                                                                   Y5_7))))
                                                              && (-9 <=
                                                                  (C_7 +
                                                                   (-10 *
                                                                    D6_7)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    D6_7)))))
                 &&
                 ((((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * J6_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * J6_7)))))
                     && (1 <= J6_7) && (-9 <= (C_7 + (-10 * J6_7)))
                     && (-9 <= ((-1 * C_7) + (10 * J6_7)))) || (((C_7 <= -1)
                                                                 ||
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      I6_7)))))
                                                                && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_7 +
                                                                        (-10 *
                                                                         I6_7)))))
                                                                &&
                                                                (!(1 <=
                                                                   (I6_7 +
                                                                    (-10 *
                                                                     F6_7))))
                                                                && (-9 <=
                                                                    (C_7 +
                                                                     (-10 *
                                                                      I6_7)))
                                                                && (-9 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      I6_7)))))
                   &&
                   ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * L6_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * L6_7)))))
                     && (L6_7 <= -1) && (-9 <= (C_7 + (-10 * L6_7)))
                     && (-9 <= ((-1 * C_7) + (10 * L6_7)))) || (((C_7 <= -1)
                                                                 ||
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      K6_7)))))
                                                                && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_7 +
                                                                        (-10 *
                                                                         K6_7)))))
                                                                &&
                                                                (!(1 <=
                                                                   ((-1 *
                                                                     K6_7) +
                                                                    (10 *
                                                                     F6_7))))
                                                                && (-9 <=
                                                                    (C_7 +
                                                                     (-10 *
                                                                      K6_7)))
                                                                && (-9 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      K6_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * G6_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * H6_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * G6_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * H6_7)))))
                   && (!(1 <= (F6_7 + (-10 * Q5_7))))
                   && (-9 <= (C_7 + (-10 * G6_7)))
                   && (-9 <= (C_7 + (-10 * H6_7)))
                   && (-9 <= (H6_7 + (-10 * F6_7)))
                   && (-9 <= ((-1 * C_7) + (10 * G6_7)))
                   && (-9 <= ((-1 * C_7) + (10 * H6_7)))
                   && (-9 <= ((-1 * G6_7) + (10 * F6_7))))
                  ||
                  (((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Q6_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Q6_7)))))
                     && (1 <= Q6_7) && (-9 <= (C_7 + (-10 * Q6_7)))
                     && (-9 <= ((-1 * C_7) + (10 * Q6_7)))) || (((C_7 <= -1)
                                                                 ||
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      P6_7)))))
                                                                && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_7 +
                                                                        (-10 *
                                                                         P6_7)))))
                                                                &&
                                                                (!(1 <=
                                                                   (P6_7 +
                                                                    (-10 *
                                                                     M6_7))))
                                                                && (-9 <=
                                                                    (C_7 +
                                                                     (-10 *
                                                                      P6_7)))
                                                                && (-9 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      P6_7)))))
                   &&
                   ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * S6_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * S6_7)))))
                     && (S6_7 <= -1) && (-9 <= (C_7 + (-10 * S6_7)))
                     && (-9 <= ((-1 * C_7) + (10 * S6_7)))) || (((C_7 <= -1)
                                                                 ||
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      R6_7)))))
                                                                && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_7 +
                                                                        (-10 *
                                                                         R6_7)))))
                                                                &&
                                                                (!(1 <=
                                                                   ((-1 *
                                                                     R6_7) +
                                                                    (10 *
                                                                     M6_7))))
                                                                && (-9 <=
                                                                    (C_7 +
                                                                     (-10 *
                                                                      R6_7)))
                                                                && (-9 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      R6_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * N6_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * O6_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * N6_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * O6_7)))))
                   && (1 <= M6_7) && (-9 <= (C_7 + (-10 * N6_7)))
                   && (-9 <= (C_7 + (-10 * O6_7)))
                   && (-9 <= (O6_7 + (-10 * M6_7)))
                   && (-9 <= ((-1 * C_7) + (10 * N6_7)))
                   && (-9 <= ((-1 * C_7) + (10 * O6_7)))
                   && (-9 <= ((-1 * N6_7) + (10 * M6_7)))))
                 &&
                 ((((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * X6_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * X6_7)))))
                     && (1 <= X6_7) && (-9 <= (C_7 + (-10 * X6_7)))
                     && (-9 <= ((-1 * C_7) + (10 * X6_7)))) || (((C_7 <= -1)
                                                                 ||
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      W6_7)))))
                                                                && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_7 +
                                                                        (-10 *
                                                                         W6_7)))))
                                                                &&
                                                                (!(1 <=
                                                                   (W6_7 +
                                                                    (-10 *
                                                                     T6_7))))
                                                                && (-9 <=
                                                                    (C_7 +
                                                                     (-10 *
                                                                      W6_7)))
                                                                && (-9 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      W6_7)))))
                   &&
                   ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Z6_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Z6_7)))))
                     && (Z6_7 <= -1) && (-9 <= (C_7 + (-10 * Z6_7)))
                     && (-9 <= ((-1 * C_7) + (10 * Z6_7)))) || (((C_7 <= -1)
                                                                 ||
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      Y6_7)))))
                                                                && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_7 +
                                                                        (-10 *
                                                                         Y6_7)))))
                                                                &&
                                                                (!(1 <=
                                                                   ((-1 *
                                                                     Y6_7) +
                                                                    (10 *
                                                                     T6_7))))
                                                                && (-9 <=
                                                                    (C_7 +
                                                                     (-10 *
                                                                      Y6_7)))
                                                                && (-9 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      Y6_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * U6_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * V6_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * U6_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * V6_7)))))
                   && (!(1 <= ((-1 * T6_7) + (10 * Q5_7))))
                   && (-9 <= (C_7 + (-10 * U6_7)))
                   && (-9 <= (C_7 + (-10 * V6_7)))
                   && (-9 <= (V6_7 + (-10 * T6_7)))
                   && (-9 <= ((-1 * C_7) + (10 * U6_7)))
                   && (-9 <= ((-1 * C_7) + (10 * V6_7)))
                   && (-9 <= ((-1 * U6_7) + (10 * T6_7))))
                  ||
                  (((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * E7_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * E7_7)))))
                     && (1 <= E7_7) && (-9 <= (C_7 + (-10 * E7_7)))
                     && (-9 <= ((-1 * C_7) + (10 * E7_7)))) || (((C_7 <= -1)
                                                                 ||
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      D7_7)))))
                                                                && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_7 +
                                                                        (-10 *
                                                                         D7_7)))))
                                                                &&
                                                                (!(1 <=
                                                                   (D7_7 +
                                                                    (-10 *
                                                                     A7_7))))
                                                                && (-9 <=
                                                                    (C_7 +
                                                                     (-10 *
                                                                      D7_7)))
                                                                && (-9 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      D7_7)))))
                   &&
                   ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * G7_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * G7_7)))))
                     && (G7_7 <= -1) && (-9 <= (C_7 + (-10 * G7_7)))
                     && (-9 <= ((-1 * C_7) + (10 * G7_7)))) || (((C_7 <= -1)
                                                                 ||
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      F7_7)))))
                                                                && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_7 +
                                                                        (-10 *
                                                                         F7_7)))))
                                                                &&
                                                                (!(1 <=
                                                                   ((-1 *
                                                                     F7_7) +
                                                                    (10 *
                                                                     A7_7))))
                                                                && (-9 <=
                                                                    (C_7 +
                                                                     (-10 *
                                                                      F7_7)))
                                                                && (-9 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      F7_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * B7_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * C7_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * B7_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * C7_7)))))
                   && (A7_7 <= -1) && (-9 <= (C_7 + (-10 * B7_7)))
                   && (-9 <= (C_7 + (-10 * C7_7)))
                   && (-9 <= (C7_7 + (-10 * A7_7)))
                   && (-9 <= ((-1 * C_7) + (10 * B7_7)))
                   && (-9 <= ((-1 * C_7) + (10 * C7_7)))
                   && (-9 <= ((-1 * B7_7) + (10 * A7_7))))) && ((C_7 <= -1)
                                                                ||
                                                                (!(1 <=
                                                                   ((-1 *
                                                                     C_7) +
                                                                    (10 *
                                                                     S5_7)))))
                 && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * T5_7)))))
                 && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Z5_7)))))
                 && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * A6_7)))))
                 && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * S5_7)))))
                 && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * T5_7)))))
                 && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Z5_7)))))
                 && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * A6_7)))))
                 && (!(1 <= (Q5_7 + (-10 * H2_7))))
                 && (-9 <= (C_7 + (-10 * S5_7)))
                 && (-9 <= (C_7 + (-10 * T5_7)))
                 && (-9 <= (C_7 + (-10 * Z5_7)))
                 && (-9 <= (C_7 + (-10 * A6_7)))
                 && (-9 <= (T5_7 + (-10 * R5_7)))
                 && (-9 <= (Y5_7 + (-10 * Q5_7)))
                 && (-9 <= (A6_7 + (-10 * Y5_7)))
                 && (-9 <= ((-1 * C_7) + (10 * S5_7)))
                 && (-9 <= ((-1 * C_7) + (10 * T5_7)))
                 && (-9 <= ((-1 * C_7) + (10 * Z5_7)))
                 && (-9 <= ((-1 * C_7) + (10 * A6_7)))
                 && (-9 <= ((-1 * S5_7) + (10 * R5_7)))
                 && (-9 <= ((-1 * R5_7) + (10 * Q5_7)))
                 && (-9 <= ((-1 * Z5_7) + (10 * Y5_7))))
                ||
                (((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * M7_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * M7_7)))))
                   && (1 <= M7_7) && (-9 <= (C_7 + (-10 * M7_7)))
                   && (-9 <= ((-1 * C_7) + (10 * M7_7)))) || (((C_7 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    L7_7)))))
                                                              && ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       L7_7)))))
                                                              &&
                                                              (!(1 <=
                                                                 (L7_7 +
                                                                  (-10 *
                                                                   I7_7))))
                                                              && (-9 <=
                                                                  (C_7 +
                                                                   (-10 *
                                                                    L7_7)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    L7_7)))))
                 &&
                 ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * O7_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * O7_7)))))
                   && (O7_7 <= -1) && (-9 <= (C_7 + (-10 * O7_7)))
                   && (-9 <= ((-1 * C_7) + (10 * O7_7)))) || (((C_7 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    N7_7)))))
                                                              && ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       N7_7)))))
                                                              &&
                                                              (!(1 <=
                                                                 ((-1 *
                                                                   N7_7) +
                                                                  (10 *
                                                                   I7_7))))
                                                              && (-9 <=
                                                                  (C_7 +
                                                                   (-10 *
                                                                    N7_7)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    N7_7)))))
                 &&
                 ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * T7_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * T7_7)))))
                   && (1 <= T7_7) && (-9 <= (C_7 + (-10 * T7_7)))
                   && (-9 <= ((-1 * C_7) + (10 * T7_7)))) || (((C_7 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    S7_7)))))
                                                              && ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       S7_7)))))
                                                              &&
                                                              (!(1 <=
                                                                 (S7_7 +
                                                                  (-10 *
                                                                   P7_7))))
                                                              && (-9 <=
                                                                  (C_7 +
                                                                   (-10 *
                                                                    S7_7)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    S7_7)))))
                 &&
                 ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * V7_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * V7_7)))))
                   && (V7_7 <= -1) && (-9 <= (C_7 + (-10 * V7_7)))
                   && (-9 <= ((-1 * C_7) + (10 * V7_7)))) || (((C_7 <= -1)
                                                               ||
                                                               (!(1 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    U7_7)))))
                                                              && ((1 <= C_7)
                                                                  ||
                                                                  (!(1 <=
                                                                     (C_7 +
                                                                      (-10 *
                                                                       U7_7)))))
                                                              &&
                                                              (!(1 <=
                                                                 ((-1 *
                                                                   U7_7) +
                                                                  (10 *
                                                                   P7_7))))
                                                              && (-9 <=
                                                                  (C_7 +
                                                                   (-10 *
                                                                    U7_7)))
                                                              && (-9 <=
                                                                  ((-1 *
                                                                    C_7) +
                                                                   (10 *
                                                                    U7_7)))))
                 &&
                 ((((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * A8_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * A8_7)))))
                     && (1 <= A8_7) && (-9 <= (C_7 + (-10 * A8_7)))
                     && (-9 <= ((-1 * C_7) + (10 * A8_7)))) || (((C_7 <= -1)
                                                                 ||
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      Z7_7)))))
                                                                && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_7 +
                                                                        (-10 *
                                                                         Z7_7)))))
                                                                &&
                                                                (!(1 <=
                                                                   (Z7_7 +
                                                                    (-10 *
                                                                     W7_7))))
                                                                && (-9 <=
                                                                    (C_7 +
                                                                     (-10 *
                                                                      Z7_7)))
                                                                && (-9 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      Z7_7)))))
                   &&
                   ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * C8_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * C8_7)))))
                     && (C8_7 <= -1) && (-9 <= (C_7 + (-10 * C8_7)))
                     && (-9 <= ((-1 * C_7) + (10 * C8_7)))) || (((C_7 <= -1)
                                                                 ||
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      B8_7)))))
                                                                && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_7 +
                                                                        (-10 *
                                                                         B8_7)))))
                                                                &&
                                                                (!(1 <=
                                                                   ((-1 *
                                                                     B8_7) +
                                                                    (10 *
                                                                     W7_7))))
                                                                && (-9 <=
                                                                    (C_7 +
                                                                     (-10 *
                                                                      B8_7)))
                                                                && (-9 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      B8_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * X7_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Y7_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * X7_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Y7_7)))))
                   && (!(1 <= (W7_7 + (-10 * H7_7))))
                   && (-9 <= (C_7 + (-10 * X7_7)))
                   && (-9 <= (C_7 + (-10 * Y7_7)))
                   && (-9 <= (Y7_7 + (-10 * W7_7)))
                   && (-9 <= ((-1 * C_7) + (10 * X7_7)))
                   && (-9 <= ((-1 * C_7) + (10 * Y7_7)))
                   && (-9 <= ((-1 * X7_7) + (10 * W7_7))))
                  ||
                  (((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * H8_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * H8_7)))))
                     && (1 <= H8_7) && (-9 <= (C_7 + (-10 * H8_7)))
                     && (-9 <= ((-1 * C_7) + (10 * H8_7)))) || (((C_7 <= -1)
                                                                 ||
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      G8_7)))))
                                                                && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_7 +
                                                                        (-10 *
                                                                         G8_7)))))
                                                                &&
                                                                (!(1 <=
                                                                   (G8_7 +
                                                                    (-10 *
                                                                     D8_7))))
                                                                && (-9 <=
                                                                    (C_7 +
                                                                     (-10 *
                                                                      G8_7)))
                                                                && (-9 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      G8_7)))))
                   &&
                   ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * J8_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * J8_7)))))
                     && (J8_7 <= -1) && (-9 <= (C_7 + (-10 * J8_7)))
                     && (-9 <= ((-1 * C_7) + (10 * J8_7)))) || (((C_7 <= -1)
                                                                 ||
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      I8_7)))))
                                                                && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_7 +
                                                                        (-10 *
                                                                         I8_7)))))
                                                                &&
                                                                (!(1 <=
                                                                   ((-1 *
                                                                     I8_7) +
                                                                    (10 *
                                                                     D8_7))))
                                                                && (-9 <=
                                                                    (C_7 +
                                                                     (-10 *
                                                                      I8_7)))
                                                                && (-9 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      I8_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * E8_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * F8_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * E8_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * F8_7)))))
                   && (1 <= D8_7) && (-9 <= (C_7 + (-10 * E8_7)))
                   && (-9 <= (C_7 + (-10 * F8_7)))
                   && (-9 <= (F8_7 + (-10 * D8_7)))
                   && (-9 <= ((-1 * C_7) + (10 * E8_7)))
                   && (-9 <= ((-1 * C_7) + (10 * F8_7)))
                   && (-9 <= ((-1 * E8_7) + (10 * D8_7)))))
                 &&
                 ((((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * O8_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * O8_7)))))
                     && (1 <= O8_7) && (-9 <= (C_7 + (-10 * O8_7)))
                     && (-9 <= ((-1 * C_7) + (10 * O8_7)))) || (((C_7 <= -1)
                                                                 ||
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      N8_7)))))
                                                                && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_7 +
                                                                        (-10 *
                                                                         N8_7)))))
                                                                &&
                                                                (!(1 <=
                                                                   (N8_7 +
                                                                    (-10 *
                                                                     K8_7))))
                                                                && (-9 <=
                                                                    (C_7 +
                                                                     (-10 *
                                                                      N8_7)))
                                                                && (-9 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      N8_7)))))
                   &&
                   ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Q8_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Q8_7)))))
                     && (Q8_7 <= -1) && (-9 <= (C_7 + (-10 * Q8_7)))
                     && (-9 <= ((-1 * C_7) + (10 * Q8_7)))) || (((C_7 <= -1)
                                                                 ||
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      P8_7)))))
                                                                && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                       (C_7 +
                                                                        (-10 *
                                                                         P8_7)))))
                                                                &&
                                                                (!(1 <=
                                                                   ((-1 *
                                                                     P8_7) +
                                                                    (10 *
                                                                     K8_7))))
                                                                && (-9 <=
                                                                    (C_7 +
                                                                     (-10 *
                                                                      P8_7)))
                                                                && (-9 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      P8_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * M8_7)))))
                   && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * L8_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * M8_7)))))
                   && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * L8_7)))))
                   && (!(1 <= ((-1 * K8_7) + (10 * H7_7))))
                   && (-9 <= (M8_7 + (-10 * K8_7)))
                   && (-9 <= (C_7 + (-10 * M8_7)))
                   && (-9 <= (C_7 + (-10 * L8_7)))
                   && (-9 <= ((-1 * L8_7) + (10 * K8_7)))
                   && (-9 <= ((-1 * C_7) + (10 * M8_7)))
                   && (-9 <= ((-1 * C_7) + (10 * L8_7))))
                  ||
                  (((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * V8_7)))))
                     && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * V8_7)))))
                     && (1 <= V8_7) && (-9 <= (C_7 + (-10 * V8_7)))
                     && (-9 <= ((-1 * C_7) + (10 * V8_7)))) || (((C_7 <= -1)
                                                                 ||
                                                                 (!(1 <=
                                                                    ((-1 *
                                                                      C_7) +
                                                                     (10 *
                                                                      U8_7)))))
                                                                && ((1 <= C_7)
                                                                    ||
                                                                    (!(1 <=
                                                                (C_7 +
                                                          (-10 * U8_7)))))
                                                   &&
                                                   (!(1 <=
                            (U8_7 + (-10 * R8_7))))
                    && (-9 <= (C_7 + (-10 * U8_7)))
                    && (-9 <= ((-1 * C_7) + (10 * U8_7))))) && ((((C_7 <= -1)
|| (!(1 <= ((-1 * C_7) + (10 * X8_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * X8_7)))))
&& (X8_7 <= -1) && (-9 <= (C_7 + (-10 * X8_7))) && (-9 <= ((-1 * C_7) + (10 * X8_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * W8_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * W8_7)))))
&& (!(1 <= ((-1 * W8_7) + (10 * R8_7)))) && (-9 <= (C_7 + (-10 * W8_7))) && (-9 <= ((-1 * C_7) + (10 * W8_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * S8_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * T8_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * S8_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * T8_7))))) && (R8_7 <= -1)
&& (-9 <= (T8_7 + (-10 * R8_7))) && (-9 <= (C_7 + (-10 * S8_7))) && (-9 <= (C_7 + (-10 * T8_7))) && (-9 <= ((-1 * S8_7) + (10 * R8_7))) && (-9 <= ((-1 * C_7) + (10 * S8_7))) && (-9 <= ((-1 * C_7) + (10 * T8_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * J7_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * K7_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Q7_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * R7_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * J7_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * K7_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Q7_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * R7_7))))) && (1 <= H7_7) && (-9 <= (C_7 + (-10 * J7_7)))
&& (-9 <= (C_7 + (-10 * K7_7))) && (-9 <= (C_7 + (-10 * Q7_7))) && (-9 <= (C_7 + (-10 * R7_7))) && (-9 <= (K7_7 + (-10 * I7_7))) && (-9 <= (P7_7 + (-10 * H7_7))) && (-9 <= (R7_7 + (-10 * P7_7))) && (-9 <= ((-1 * C_7) + (10 * J7_7))) && (-9 <= ((-1 * C_7) + (10 * K7_7))) && (-9 <= ((-1 * C_7) + (10 * Q7_7))) && (-9 <= ((-1 * C_7) + (10 * R7_7))) && (-9 <= ((-1 * J7_7) + (10 * I7_7))) && (-9 <= ((-1 * I7_7) + (10 * H7_7))) && (-9 <= ((-1 * Q7_7) + (10 * P7_7))))) && ((((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * P1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * P1_7))))) && (1 <= P1_7) && (-9 <= (C_7 + (-10 * P1_7))) && (-9 <= ((-1 * C_7) + (10 * P1_7))))
|| (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * O1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * O1_7))))) && (!(1 <= (O1_7 + (-10 * L1_7)))) && (-9 <= (C_7 + (-10 * O1_7))) && (-9 <= ((-1 * C_7) + (10 * O1_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * R1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * R1_7))))) && (R1_7 <= -1) && (-9 <= (C_7 + (-10 * R1_7))) && (-9 <= ((-1 * C_7) + (10 * R1_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Q1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Q1_7))))) && (!(1 <= ((-1 * Q1_7) + (10 * L1_7)))) && (-9 <= (C_7 + (-10 * Q1_7))) && (-9 <= ((-1 * C_7) + (10 * Q1_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * M1_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * N1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * M1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * N1_7))))) && (!(1 <= ((-1 * L1_7) + (10 * I_7)))) && (-9 <= (C_7 + (-10 * M1_7))) && (-9 <= (C_7 + (-10 * N1_7))) && (-9 <= (N1_7 + (-10 * L1_7))) && (-9 <= ((-1 * C_7) + (10 * M1_7)))
&& (-9 <= ((-1 * C_7) + (10 * N1_7))) && (-9 <= ((-1 * M1_7) + (10 * L1_7)))) || (((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * W1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * W1_7))))) && (1 <= W1_7) && (-9 <= (C_7 + (-10 * W1_7))) && (-9 <= ((-1 * C_7) + (10 * W1_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * V1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * V1_7))))) && (!(1 <= (V1_7 + (-10 * S1_7)))) && (-9 <= (C_7 + (-10 * V1_7))) && (-9 <= ((-1 * C_7) + (10 * V1_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Y1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Y1_7))))) && (Y1_7 <= -1) && (-9 <= (C_7 + (-10 * Y1_7))) && (-9 <= ((-1 * C_7) + (10 * Y1_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * X1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * X1_7))))) && (!(1 <= ((-1 * X1_7) + (10 * S1_7)))) && (-9 <= (C_7 + (-10 * X1_7))) && (-9 <= ((-1 * C_7) + (10 * X1_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * T1_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * U1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * T1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * U1_7))))) && (S1_7 <= -1) && (-9 <= (C_7 + (-10 * T1_7))) && (-9 <= (C_7 + (-10 * U1_7))) && (-9 <= (U1_7 + (-10 * S1_7))) && (-9 <= ((-1 * C_7) + (10 * T1_7))) && (-9 <= ((-1 * C_7) + (10 * U1_7))) && (-9 <= ((-1 * T1_7) + (10 * S1_7))))) && ((((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * B1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * B1_7))))) && (1 <= B1_7) && (-9 <= (C_7 + (-10 * B1_7))) && (-9 <= ((-1 * C_7) + (10 * B1_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * A1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * A1_7))))) && (!(1 <= (A1_7 + (-10 * X_7)))) && (-9 <= (C_7 + (-10 * A1_7))) && (-9 <= ((-1 * C_7) + (10 * A1_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * D1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * D1_7))))) && (D1_7 <= -1) && (-9 <= (C_7 + (-10 * D1_7))) && (-9 <= ((-1 * C_7) + (10 * D1_7))))
|| (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * C1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * C1_7))))) && (!(1 <= ((-1 * C1_7) + (10 * X_7)))) && (-9 <= (C_7 + (-10 * C1_7))) && (-9 <= ((-1 * C_7) + (10 * C1_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Y_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Z_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Y_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Z_7))))) && (!(1 <= (X_7 + (-10 * I_7)))) && (-9 <= (C_7 + (-10 * Y_7))) && (-9 <= (C_7 + (-10 * Z_7))) && (-9 <= (Z_7 + (-10 * X_7))) && (-9 <= ((-1 * C_7) + (10 * Y_7))) && (-9 <= ((-1 * C_7) + (10 * Z_7))) && (-9 <= ((-1 * Y_7) + (10 * X_7)))) || (((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * I1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * I1_7))))) && (1 <= I1_7) && (-9 <= (C_7 + (-10 * I1_7))) && (-9 <= ((-1 * C_7) + (10 * I1_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * H1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * H1_7))))) && (!(1 <= (H1_7 + (-10 * E1_7)))) && (-9 <= (C_7 + (-10 * H1_7))) && (-9 <= ((-1 * C_7) + (10 * H1_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * K1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * K1_7))))) && (K1_7 <= -1) && (-9 <= (C_7 + (-10 * K1_7))) && (-9 <= ((-1 * C_7) + (10 * K1_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * J1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * J1_7))))) && (!(1 <= ((-1 * J1_7) + (10 * E1_7)))) && (-9 <= (C_7 + (-10 * J1_7))) && (-9 <= ((-1 * C_7) + (10 * J1_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * F1_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * G1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * F1_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * G1_7))))) && (1 <= E1_7) && (-9 <= (C_7 + (-10 * F1_7))) && (-9 <= (C_7 + (-10 * G1_7))) && (-9 <= (G1_7 + (-10 * E1_7))) && (-9 <= ((-1 * C_7) + (10 * F1_7))) && (-9 <= ((-1 * C_7) + (10 * G1_7))) && (-9 <= ((-1 * F1_7) + (10 * E1_7))))) && ((((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * G5_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * G5_7))))) && (1 <= G5_7) && (-9 <= (C_7 + (-10 * G5_7))) && (-9 <= ((-1 * C_7) + (10 * G5_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * F5_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * F5_7))))) && (!(1 <= (F5_7 + (-10 * C5_7)))) && (-9 <= (C_7 + (-10 * F5_7))) && (-9 <= ((-1 * C_7) + (10 * F5_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * I5_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * I5_7))))) && (I5_7 <= -1) && (-9 <= (C_7 + (-10 * I5_7))) && (-9 <= ((-1 * C_7) + (10 * I5_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * H5_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * H5_7))))) && (!(1 <= ((-1 * H5_7) + (10 * C5_7)))) && (-9 <= (C_7 + (-10 * H5_7))) && (-9 <= ((-1 * C_7) + (10 * H5_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * D5_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * E5_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * D5_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * E5_7))))) && (!(1 <= ((-1 * C5_7) + (10 * Z3_7)))) && (-9 <= (C_7 + (-10 * D5_7))) && (-9 <= (C_7 + (-10 * E5_7)))
&& (-9 <= (E5_7 + (-10 * C5_7))) && (-9 <= ((-1 * C_7) + (10 * D5_7))) && (-9 <= ((-1 * C_7) + (10 * E5_7))) && (-9 <= ((-1 * D5_7) + (10 * C5_7)))) || (((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * N5_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * N5_7))))) && (1 <= N5_7) && (-9 <= (C_7 + (-10 * N5_7))) && (-9 <= ((-1 * C_7) + (10 * N5_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * M5_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * M5_7))))) && (!(1 <= (M5_7 + (-10 * J5_7)))) && (-9 <= (C_7 + (-10 * M5_7))) && (-9 <= ((-1 * C_7) + (10 * M5_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * P5_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * P5_7))))) && (P5_7 <= -1) && (-9 <= (C_7 + (-10 * P5_7))) && (-9 <= ((-1 * C_7) + (10 * P5_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * O5_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * O5_7))))) && (!(1 <= ((-1 * O5_7) + (10 * J5_7)))) && (-9 <= (C_7 + (-10 * O5_7))) && (-9 <= ((-1 * C_7) + (10 * O5_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * K5_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * L5_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * K5_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * L5_7))))) && (J5_7 <= -1) && (-9 <= (C_7 + (-10 * K5_7))) && (-9 <= (C_7 + (-10 * L5_7))) && (-9 <= (L5_7 + (-10 * J5_7))) && (-9 <= ((-1 * C_7) + (10 * K5_7))) && (-9 <= ((-1 * C_7) + (10 * L5_7))) && (-9 <= ((-1 * K5_7) + (10 * J5_7))))) && ((((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * P3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * P3_7))))) && (1 <= P3_7) && (-9 <= (C_7 + (-10 * P3_7))) && (-9 <= ((-1 * C_7) + (10 * P3_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * O3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * O3_7))))) && (!(1 <= (O3_7 + (-10 * L3_7)))) && (-9 <= (C_7 + (-10 * O3_7))) && (-9 <= ((-1 * C_7) + (10 * O3_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * R3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * R3_7))))) && (R3_7 <= -1) && (-9 <= (C_7 + (-10 * R3_7))) && (-9 <= ((-1 * C_7) + (10 * R3_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Q3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Q3_7))))) && (!(1 <= ((-1 * Q3_7) + (10 * L3_7)))) && (-9 <= (C_7 + (-10 * Q3_7))) && (-9 <= ((-1 * C_7) + (10 * Q3_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * M3_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * N3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * M3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * N3_7))))) && (!(1 <= ((-1 * L3_7) + (10 * I2_7)))) && (-9 <= (C_7 + (-10 * M3_7))) && (-9 <= (C_7 + (-10 * N3_7))) && (-9 <= (N3_7 + (-10 * L3_7))) && (-9 <= ((-1 * C_7) + (10 * M3_7))) && (-9 <= ((-1 * C_7) + (10 * N3_7))) && (-9 <= ((-1 * M3_7) + (10 * L3_7)))) || (((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * W3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * W3_7))))) && (1 <= W3_7) && (-9 <= (C_7 + (-10 * W3_7))) && (-9 <= ((-1 * C_7) + (10 * W3_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * V3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * V3_7))))) && (!(1 <= (V3_7 + (-10 * S3_7)))) && (-9 <= (C_7 + (-10 * V3_7))) && (-9 <= ((-1 * C_7) + (10 * V3_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Y3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Y3_7))))) && (Y3_7 <= -1) && (-9 <= (C_7 + (-10 * Y3_7))) && (-9 <= ((-1 * C_7) + (10 * Y3_7))))
|| (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * X3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * X3_7))))) && (!(1 <= ((-1 * X3_7) + (10 * S3_7)))) && (-9 <= (C_7 + (-10 * X3_7))) && (-9 <= ((-1 * C_7) + (10 * X3_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * T3_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * U3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * T3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * U3_7))))) && (S3_7 <= -1) && (-9 <= (C_7 + (-10 * T3_7))) && (-9 <= (C_7 + (-10 * U3_7))) && (-9 <= (U3_7 + (-10 * S3_7))) && (-9 <= ((-1 * C_7) + (10 * T3_7))) && (-9 <= ((-1 * C_7) + (10 * U3_7))) && (-9 <= ((-1 * T3_7) + (10 * S3_7))))) && ((((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * S4_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * S4_7))))) && (1 <= S4_7) && (-9 <= (C_7 + (-10 * S4_7))) && (-9 <= ((-1 * C_7) + (10 * S4_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * R4_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * R4_7))))) && (!(1 <= (R4_7 + (-10 * O4_7)))) && (-9 <= (C_7 + (-10 * R4_7))) && (-9 <= ((-1 * C_7) + (10 * R4_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * U4_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * U4_7))))) && (U4_7 <= -1) && (-9 <= (C_7 + (-10 * U4_7))) && (-9 <= ((-1 * C_7) + (10 * U4_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * T4_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * T4_7))))) && (!(1 <= ((-1 * T4_7) + (10 * O4_7)))) && (-9 <= (C_7 + (-10 * T4_7))) && (-9 <= ((-1 * C_7) + (10 * T4_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * P4_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Q4_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * P4_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Q4_7))))) && (!(1 <= (O4_7 + (-10 * Z3_7)))) && (-9 <= (C_7 + (-10 * P4_7))) && (-9 <= (C_7 + (-10 * Q4_7))) && (-9 <= (Q4_7 + (-10 * O4_7))) && (-9 <= ((-1 * C_7) + (10 * P4_7))) && (-9 <= ((-1 * C_7) + (10 * Q4_7))) && (-9 <= ((-1 * P4_7) + (10 * O4_7)))) || (((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Z4_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Z4_7))))) && (1 <= Z4_7) && (-9 <= (C_7 + (-10 * Z4_7))) && (-9 <= ((-1 * C_7) + (10 * Z4_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Y4_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Y4_7))))) && (!(1 <= (Y4_7 + (-10 * V4_7)))) && (-9 <= (C_7 + (-10 * Y4_7))) && (-9 <= ((-1 * C_7) + (10 * Y4_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * B5_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * B5_7))))) && (B5_7 <= -1) && (-9 <= (C_7 + (-10 * B5_7))) && (-9 <= ((-1 * C_7) + (10 * B5_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * A5_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * A5_7))))) && (!(1 <= ((-1 * A5_7) + (10 * V4_7)))) && (-9 <= (C_7 + (-10 * A5_7))) && (-9 <= ((-1 * C_7) + (10 * A5_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * W4_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * X4_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * W4_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * X4_7))))) && (1 <= V4_7) && (-9 <= (C_7 + (-10 * W4_7))) && (-9 <= (C_7 + (-10 * X4_7))) && (-9 <= (X4_7 + (-10 * V4_7))) && (-9 <= ((-1 * C_7) + (10 * W4_7))) && (-9 <= ((-1 * C_7) + (10 * X4_7))) && (-9 <= ((-1 * W4_7) + (10 * V4_7))))) && ((((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * B3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * B3_7))))) && (1 <= B3_7) && (-9 <= (C_7 + (-10 * B3_7))) && (-9 <= ((-1 * C_7) + (10 * B3_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * A3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * A3_7))))) && (!(1 <= (A3_7 + (-10 * X2_7)))) && (-9 <= (C_7 + (-10 * A3_7))) && (-9 <= ((-1 * C_7) + (10 * A3_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * D3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * D3_7))))) && (D3_7 <= -1) && (-9 <= (C_7 + (-10 * D3_7))) && (-9 <= ((-1 * C_7) + (10 * D3_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * C3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * C3_7))))) && (!(1 <= ((-1 * C3_7) + (10 * X2_7)))) && (-9 <= (C_7 + (-10 * C3_7))) && (-9 <= ((-1 * C_7) + (10 * C3_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Y2_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * Z2_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Y2_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * Z2_7))))) && (!(1 <= (X2_7 + (-10 * I2_7)))) && (-9 <= (C_7 + (-10 * Y2_7))) && (-9 <= (C_7 + (-10 * Z2_7))) && (-9 <= (Z2_7 + (-10 * X2_7))) && (-9 <= ((-1 * C_7) + (10 * Y2_7))) && (-9 <= ((-1 * C_7) + (10 * Z2_7))) && (-9 <= ((-1 * Y2_7) + (10 * X2_7)))) || (((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * I3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * I3_7))))) && (1 <= I3_7) && (-9 <= (C_7 + (-10 * I3_7))) && (-9 <= ((-1 * C_7) + (10 * I3_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * H3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * H3_7))))) && (!(1 <= (H3_7 + (-10 * E3_7)))) && (-9 <= (C_7 + (-10 * H3_7))) && (-9 <= ((-1 * C_7) + (10 * H3_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * K3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * K3_7))))) && (K3_7 <= -1) && (-9 <= (C_7 + (-10 * K3_7))) && (-9 <= ((-1 * C_7) + (10 * K3_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * J3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * J3_7))))) && (!(1 <= ((-1 * J3_7) + (10 * E3_7)))) && (-9 <= (C_7 + (-10 * J3_7))) && (-9 <= ((-1 * C_7) + (10 * J3_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * F3_7))))) && ((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * G3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * F3_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * G3_7))))) && (1 <= E3_7) && (-9 <= (C_7 + (-10 * F3_7))) && (-9 <= (C_7 + (-10 * G3_7))) && (-9 <= (G3_7 + (-10 * E3_7))) && (-9 <= ((-1 * C_7) + (10 * F3_7))) && (-9 <= ((-1 * C_7) + (10 * G3_7))) && (-9 <= ((-1 * F3_7) + (10 * E3_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * F2_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * F2_7))))) && (F2_7 <= -1) && (-9 <= (C_7 + (-10 * F2_7))) && (-9 <= ((-1 * C_7) + (10 * F2_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * E2_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * E2_7))))) && (!(1 <= ((-1 * E2_7) + (10 * Z1_7)))) && (-9 <= (C_7 + (-10 * E2_7))) && (-9 <= ((-1 * C_7) + (10 * E2_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * D2_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * D2_7))))) && (1 <= D2_7) && (-9 <= (C_7 + (-10 * D2_7))) && (-9 <= ((-1 * C_7) + (10 * D2_7)))) || (((C_7 <= -1)
|| (!(1 <= ((-1 * C_7) + (10 * C2_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * C2_7))))) && (!(1 <= (C2_7 + (-10 * Z1_7)))) && (-9 <= (C_7 + (-10 * C2_7))) && (-9 <= ((-1 * C_7) + (10 * C2_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * W_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * W_7))))) && (W_7 <= -1) && (-9 <= (C_7 + (-10 * W_7))) && (-9 <= ((-1 * C_7) + (10 * W_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * V_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * V_7))))) && (!(1 <= ((-1 * V_7) + (10 * Q_7)))) && (-9 <= (C_7 + (-10 * V_7))) && (-9 <= ((-1 * C_7) + (10 * V_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * P_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * P_7))))) && (P_7 <= -1) && (-9 <= (C_7 + (-10 * P_7))) && (-9 <= ((-1 * C_7) + (10 * P_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * O_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * O_7))))) && (!(1 <= ((-1 * O_7) + (10 * J_7)))) && (-9 <= (C_7 + (-10 * O_7))) && (-9 <= ((-1 * C_7) + (10 * O_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * U_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * U_7))))) && (1 <= U_7) && (-9 <= (C_7 + (-10 * U_7))) && (-9 <= ((-1 * C_7) + (10 * U_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * T_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * T_7))))) && (!(1 <= (T_7 + (-10 * Q_7)))) && (-9 <= (C_7 + (-10 * T_7))) && (-9 <= ((-1 * C_7) + (10 * T_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * N4_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * N4_7))))) && (N4_7 <= -1) && (-9 <= (C_7 + (-10 * N4_7))) && (-9 <= ((-1 * C_7) + (10 * N4_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * M4_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * M4_7))))) && (!(1 <= ((-1 * M4_7) + (10 * H4_7)))) && (-9 <= (C_7 + (-10 * M4_7))) && (-9 <= ((-1 * C_7) + (10 * M4_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * N_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * N_7))))) && (1 <= N_7) && (-9 <= (C_7 + (-10 * N_7))) && (-9 <= ((-1 * C_7) + (10 * N_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * M_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * M_7))))) && (!(1 <= (M_7 + (-10 * J_7)))) && (-9 <= (C_7 + (-10 * M_7))) && (-9 <= ((-1 * C_7) + (10 * M_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * W2_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * W2_7))))) && (W2_7 <= -1) && (-9 <= (C_7 + (-10 * W2_7))) && (-9 <= ((-1 * C_7) + (10 * W2_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * V2_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * V2_7))))) && (!(1 <= ((-1 * V2_7) + (10 * Q2_7)))) && (-9 <= (C_7 + (-10 * V2_7))) && (-9 <= ((-1 * C_7) + (10 * V2_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * G4_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * G4_7))))) && (G4_7 <= -1) && (-9 <= (C_7 + (-10 * G4_7))) && (-9 <= ((-1 * C_7) + (10 * G4_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * F4_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * F4_7))))) && (!(1 <= ((-1 * F4_7) + (10 * A4_7)))) && (-9 <= (C_7 + (-10 * F4_7))) && (-9 <= ((-1 * C_7) + (10 * F4_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * L4_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * L4_7))))) && (1 <= L4_7) && (-9 <= (C_7 + (-10 * L4_7))) && (-9 <= ((-1 * C_7) + (10 * L4_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * K4_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * K4_7))))) && (!(1 <= (K4_7 + (-10 * H4_7)))) && (-9 <= (C_7 + (-10 * K4_7))) && (-9 <= ((-1 * C_7) + (10 * K4_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * P2_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * P2_7))))) && (P2_7 <= -1) && (-9 <= (C_7 + (-10 * P2_7))) && (-9 <= ((-1 * C_7) + (10 * P2_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * O2_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * O2_7))))) && (!(1 <= ((-1 * O2_7) + (10 * J2_7)))) && (-9 <= (C_7 + (-10 * O2_7))) && (-9 <= ((-1 * C_7) + (10 * O2_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * U2_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * U2_7))))) && (1 <= U2_7) && (-9 <= (C_7 + (-10 * U2_7))) && (-9 <= ((-1 * C_7) + (10 * U2_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * T2_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * T2_7))))) && (!(1 <= (T2_7 + (-10 * Q2_7)))) && (-9 <= (C_7 + (-10 * T2_7))) && (-9 <= ((-1 * C_7) + (10 * T2_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * E4_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * E4_7))))) && (1 <= E4_7) && (-9 <= (C_7 + (-10 * E4_7))) && (-9 <= ((-1 * C_7) + (10 * E4_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * D4_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * D4_7))))) && (!(1 <= (D4_7 + (-10 * A4_7)))) && (-9 <= (C_7 + (-10 * D4_7))) && (-9 <= ((-1 * C_7) + (10 * D4_7))))) && ((((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * N2_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * N2_7))))) && (1 <= N2_7) && (-9 <= (C_7 + (-10 * N2_7))) && (-9 <= ((-1 * C_7) + (10 * N2_7)))) || (((C_7 <= -1) || (!(1 <= ((-1 * C_7) + (10 * M2_7))))) && ((1 <= C_7) || (!(1 <= (C_7 + (-10 * M2_7))))) && (!(1 <= (M2_7 + (-10 * J2_7)))) && (-9 <= (C_7 + (-10 * M2_7))) && (-9 <= ((-1 * C_7) + (10 * M2_7))))) && (A_7 == (B_7 + 4))))
              abort ();
          inv_main9_0 = E_7;
          inv_main9_1 = A_7;
          inv_main9_2 = H2_7;
          inv_main9_3 = F_7;
          inv_main9_4 = D_7;
          inv_main9_5 = G_7;
          inv_main9_6 = H_7;
          D_1 = inv_main9_0;
          A_1 = inv_main9_1;
          B_1 = inv_main9_2;
          E_1 = inv_main9_3;
          C_1 = inv_main9_4;
          F_1 = inv_main9_5;
          G_1 = inv_main9_6;
          if (!(1 <= B_1))
              abort ();
          inv_main11_0 = D_1;
          inv_main11_1 = A_1;
          inv_main11_2 = B_1;
          inv_main11_3 = E_1;
          inv_main11_4 = C_1;
          inv_main11_5 = F_1;
          inv_main11_6 = G_1;
          goto inv_main11;

      default:
          abort ();
      }
  inv_main10:
    goto inv_main10;

    // return expression

}

