// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: vmt-chc-benchmarks/metros_1_e7_1255_e8_598_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "metros_1_e7_1255_e8_598_000_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    _Bool state_0;
    _Bool state_1;
    _Bool state_2;
    _Bool state_3;
    _Bool state_4;
    _Bool state_5;
    int state_6;
    int state_7;
    int state_8;
    _Bool state_9;
    _Bool state_10;
    _Bool state_11;
    _Bool state_12;
    int state_13;
    int state_14;
    int state_15;
    int state_16;
    _Bool state_17;
    _Bool state_18;
    int state_19;
    int state_20;
    _Bool state_21;
    _Bool state_22;
    _Bool state_23;
    _Bool state_24;
    _Bool state_25;
    _Bool state_26;
    _Bool state_27;
    _Bool state_28;
    int state_29;
    int state_30;
    int state_31;
    int state_32;
    int state_33;
    _Bool state_34;
    _Bool state_35;
    _Bool state_36;
    _Bool state_37;
    _Bool state_38;
    _Bool state_39;
    _Bool state_40;
    _Bool state_41;
    int state_42;
    int state_43;
    int state_44;
    int state_45;
    int state_46;
    _Bool state_47;
    _Bool state_48;
    _Bool state_49;
    _Bool state_50;
    _Bool state_51;
    int state_52;
    int state_53;
    int state_54;
    _Bool state_55;
    _Bool state_56;
    int state_57;
    int state_58;
    _Bool state_59;
    _Bool state_60;
    int state_61;
    int state_62;
    _Bool state_63;
    _Bool state_64;
    _Bool state_65;
    _Bool state_66;
    _Bool A_0;
    _Bool B_0;
    int C_0;
    int D_0;
    int E_0;
    _Bool F_0;
    int G_0;
    _Bool H_0;
    int I_0;
    _Bool J_0;
    _Bool K_0;
    _Bool L_0;
    _Bool M_0;
    _Bool N_0;
    _Bool O_0;
    _Bool P_0;
    _Bool Q_0;
    int R_0;
    int S_0;
    int T_0;
    int U_0;
    int V_0;
    _Bool W_0;
    _Bool X_0;
    _Bool Y_0;
    int Z_0;
    int A1_0;
    _Bool B1_0;
    _Bool C1_0;
    int D1_0;
    int E1_0;
    _Bool F1_0;
    _Bool G1_0;
    _Bool H1_0;
    _Bool I1_0;
    _Bool J1_0;
    int K1_0;
    int L1_0;
    int M1_0;
    int N1_0;
    int O1_0;
    int P1_0;
    _Bool Q1_0;
    _Bool R1_0;
    _Bool S1_0;
    _Bool T1_0;
    _Bool U1_0;
    _Bool V1_0;
    _Bool W1_0;
    _Bool X1_0;
    _Bool Y1_0;
    _Bool Z1_0;
    _Bool A2_0;
    _Bool B2_0;
    int C2_0;
    int D2_0;
    _Bool E2_0;
    _Bool F2_0;
    _Bool G2_0;
    _Bool H2_0;
    int I2_0;
    int J2_0;
    _Bool K2_0;
    _Bool L2_0;
    int M2_0;
    _Bool N2_0;
    int O2_0;
    _Bool A_1;
    _Bool B_1;
    int C_1;
    int D_1;
    int E_1;
    _Bool F_1;
    int G_1;
    _Bool H_1;
    int I_1;
    _Bool J_1;
    _Bool K_1;
    _Bool L_1;
    _Bool M_1;
    _Bool N_1;
    _Bool O_1;
    _Bool P_1;
    _Bool Q_1;
    int R_1;
    int S_1;
    int T_1;
    int U_1;
    int V_1;
    _Bool W_1;
    _Bool X_1;
    _Bool Y_1;
    int Z_1;
    int A1_1;
    _Bool B1_1;
    _Bool C1_1;
    int D1_1;
    int E1_1;
    _Bool F1_1;
    _Bool G1_1;
    _Bool H1_1;
    _Bool I1_1;
    _Bool J1_1;
    int K1_1;
    int L1_1;
    int M1_1;
    int N1_1;
    int O1_1;
    int P1_1;
    _Bool Q1_1;
    _Bool R1_1;
    _Bool S1_1;
    _Bool T1_1;
    _Bool U1_1;
    _Bool V1_1;
    _Bool W1_1;
    _Bool X1_1;
    _Bool Y1_1;
    _Bool Z1_1;
    _Bool A2_1;
    _Bool B2_1;
    int C2_1;
    int D2_1;
    _Bool E2_1;
    _Bool F2_1;
    _Bool G2_1;
    _Bool H2_1;
    _Bool I2_1;
    int J2_1;
    int K2_1;
    _Bool L2_1;
    _Bool M2_1;
    int N2_1;
    _Bool O2_1;
    _Bool P2_1;
    _Bool Q2_1;
    int R2_1;
    _Bool S2_1;
    _Bool T2_1;
    _Bool U2_1;
    _Bool V2_1;
    int W2_1;
    _Bool X2_1;
    int Y2_1;
    _Bool Z2_1;
    int A3_1;
    int B3_1;
    int C3_1;
    _Bool D3_1;
    int E3_1;
    _Bool F3_1;
    int G3_1;
    int H3_1;
    _Bool I3_1;
    _Bool J3_1;
    _Bool K3_1;
    _Bool L3_1;
    int M3_1;
    _Bool N3_1;
    _Bool O3_1;
    _Bool P3_1;
    _Bool Q3_1;
    int R3_1;
    _Bool S3_1;
    int T3_1;
    int U3_1;
    int V3_1;
    _Bool W3_1;
    _Bool X3_1;
    _Bool Y3_1;
    int Z3_1;
    int A4_1;
    int B4_1;
    int C4_1;
    int D4_1;
    int E4_1;
    _Bool F4_1;
    _Bool G4_1;
    _Bool H4_1;
    _Bool I4_1;
    _Bool J4_1;
    _Bool K4_1;
    _Bool L4_1;
    _Bool M4_1;
    _Bool N4_1;
    _Bool O4_1;
    _Bool P4_1;
    _Bool Q4_1;
    _Bool R4_1;
    _Bool S4_1;
    int T4_1;
    int U4_1;
    int V4_1;
    _Bool W4_1;
    int X4_1;
    int Y4_1;
    _Bool Z4_1;
    _Bool A5_1;
    int B5_1;
    _Bool C5_1;
    int D5_1;
    _Bool A_2;
    _Bool B_2;
    int C_2;
    int D_2;
    int E_2;
    _Bool F_2;
    int G_2;
    _Bool H_2;
    int I_2;
    _Bool J_2;
    _Bool K_2;
    _Bool L_2;
    _Bool M_2;
    _Bool N_2;
    _Bool O_2;
    _Bool P_2;
    _Bool Q_2;
    int R_2;
    int S_2;
    int T_2;
    int U_2;
    int V_2;
    _Bool W_2;
    _Bool X_2;
    _Bool Y_2;
    int Z_2;
    int A1_2;
    _Bool B1_2;
    _Bool C1_2;
    int D1_2;
    int E1_2;
    _Bool F1_2;
    _Bool G1_2;
    _Bool H1_2;
    _Bool I1_2;
    _Bool J1_2;
    int K1_2;
    int L1_2;
    int M1_2;
    int N1_2;
    int O1_2;
    int P1_2;
    _Bool Q1_2;
    _Bool R1_2;
    _Bool S1_2;
    _Bool T1_2;
    _Bool U1_2;
    _Bool V1_2;
    _Bool W1_2;
    _Bool X1_2;
    _Bool Y1_2;
    _Bool Z1_2;
    _Bool A2_2;
    _Bool B2_2;
    int C2_2;
    int D2_2;
    _Bool E2_2;
    _Bool F2_2;
    _Bool G2_2;
    _Bool H2_2;
    int I2_2;
    int J2_2;
    _Bool K2_2;
    _Bool L2_2;
    int M2_2;
    _Bool N2_2;
    int O2_2;



    // main logic
    goto main_init;

  main_init:
    if (!
        (((M1_0 + C2_0 + (-1 * K1_0)) == 0) && (D2_0 == P1_0)
         && (C2_0 == N1_0) && (I_0 == 0) && (I_0 == J2_0) && (G_0 == 0)
         && (G_0 == I2_0) && (E_0 == 0) && (D_0 == 0) && (C_0 == 0)
         && (K1_0 == M2_0) && (K1_0 == C_0) && (L1_0 == D_0) && (L1_0 == O2_0)
         && (M1_0 == E_0) && (M1_0 == S_0) && (N1_0 == U_0) && (P1_0 == O1_0)
         && (((!J1_0) || A_0) == N_0) && ((B2_0 && A2_0) == Z1_0)
         && (G2_0 == V1_0) && (F2_0 == T1_0) && (E2_0 == R1_0)
         && (M_0 == H2_0) && (L_0 == F2_0) && (J_0 == E2_0) && (H_0 == L2_0)
         && (L2_0 == B2_0) && (K2_0 == A2_0) && (B_0 == Y1_0) && (F_0 == K2_0)
         && (K_0 == G2_0)
         && (W_0 ==
             (N2_0 && (!(1 <= S_0)) && (!(1 <= M2_0)) && (!(1 <= O2_0))
              && (!(32767 <= U_0)))) && (W_0 == J1_0) && (R1_0 == Q1_0)
         && (T1_0 == S1_0) && (V1_0 == U1_0) && (X1_0 == W1_0)
         && (Z1_0 == N2_0) && (H2_0 == X1_0) && (!M_0) && (!L_0) && (!J_0)
         && H_0 && A_0 && B_0 && F_0 && (!K_0)
         && ((D2_0 + M1_0 + (-1 * L1_0)) == 0)))
        abort ();
    state_0 = W_0;
    state_1 = J1_0;
    state_2 = M_0;
    state_3 = H2_0;
    state_4 = L_0;
    state_5 = F2_0;
    state_6 = D2_0;
    state_7 = M1_0;
    state_8 = L1_0;
    state_9 = K_0;
    state_10 = G2_0;
    state_11 = J_0;
    state_12 = E2_0;
    state_13 = C2_0;
    state_14 = K1_0;
    state_15 = I_0;
    state_16 = J2_0;
    state_17 = H_0;
    state_18 = L2_0;
    state_19 = G_0;
    state_20 = I2_0;
    state_21 = F_0;
    state_22 = K2_0;
    state_23 = B2_0;
    state_24 = A2_0;
    state_25 = X1_0;
    state_26 = V1_0;
    state_27 = T1_0;
    state_28 = R1_0;
    state_29 = P1_0;
    state_30 = N1_0;
    state_31 = E_0;
    state_32 = D_0;
    state_33 = C_0;
    state_34 = Z1_0;
    state_35 = N2_0;
    state_36 = B_0;
    state_37 = Y1_0;
    state_38 = W1_0;
    state_39 = U1_0;
    state_40 = S1_0;
    state_41 = Q1_0;
    state_42 = O1_0;
    state_43 = U_0;
    state_44 = S_0;
    state_45 = O2_0;
    state_46 = M2_0;
    state_47 = A_0;
    state_48 = N_0;
    state_49 = O_0;
    state_50 = P_0;
    state_51 = Q_0;
    state_52 = R_0;
    state_53 = T_0;
    state_54 = V_0;
    state_55 = X_0;
    state_56 = Y_0;
    state_57 = Z_0;
    state_58 = A1_0;
    state_59 = B1_0;
    state_60 = C1_0;
    state_61 = D1_0;
    state_62 = E1_0;
    state_63 = F1_0;
    state_64 = G1_0;
    state_65 = H1_0;
    state_66 = I1_0;
    Q2_1 = __VERIFIER_nondet__Bool ();
    Q3_1 = __VERIFIER_nondet__Bool ();
    Q4_1 = __VERIFIER_nondet__Bool ();
    I2_1 = __VERIFIER_nondet__Bool ();
    I3_1 = __VERIFIER_nondet__Bool ();
    I4_1 = __VERIFIER_nondet__Bool ();
    A3_1 = __VERIFIER_nondet_int ();
    A4_1 = __VERIFIER_nondet_int ();
    Z2_1 = __VERIFIER_nondet__Bool ();
    Z3_1 = __VERIFIER_nondet_int ();
    R2_1 = __VERIFIER_nondet_int ();
    R3_1 = __VERIFIER_nondet_int ();
    R4_1 = __VERIFIER_nondet__Bool ();
    J2_1 = __VERIFIER_nondet_int ();
    J3_1 = __VERIFIER_nondet__Bool ();
    J4_1 = __VERIFIER_nondet__Bool ();
    B3_1 = __VERIFIER_nondet_int ();
    B4_1 = __VERIFIER_nondet_int ();
    S2_1 = __VERIFIER_nondet__Bool ();
    S3_1 = __VERIFIER_nondet__Bool ();
    S4_1 = __VERIFIER_nondet__Bool ();
    K2_1 = __VERIFIER_nondet_int ();
    K3_1 = __VERIFIER_nondet__Bool ();
    K4_1 = __VERIFIER_nondet__Bool ();
    C3_1 = __VERIFIER_nondet_int ();
    C4_1 = __VERIFIER_nondet_int ();
    T2_1 = __VERIFIER_nondet__Bool ();
    T3_1 = __VERIFIER_nondet_int ();
    T4_1 = __VERIFIER_nondet_int ();
    L2_1 = __VERIFIER_nondet__Bool ();
    L3_1 = __VERIFIER_nondet__Bool ();
    L4_1 = __VERIFIER_nondet__Bool ();
    D3_1 = __VERIFIER_nondet__Bool ();
    D4_1 = __VERIFIER_nondet_int ();
    U2_1 = __VERIFIER_nondet__Bool ();
    U3_1 = __VERIFIER_nondet_int ();
    U4_1 = __VERIFIER_nondet_int ();
    M2_1 = __VERIFIER_nondet__Bool ();
    M3_1 = __VERIFIER_nondet_int ();
    M4_1 = __VERIFIER_nondet__Bool ();
    E3_1 = __VERIFIER_nondet_int ();
    E4_1 = __VERIFIER_nondet_int ();
    V2_1 = __VERIFIER_nondet__Bool ();
    V3_1 = __VERIFIER_nondet_int ();
    V4_1 = __VERIFIER_nondet_int ();
    N2_1 = __VERIFIER_nondet_int ();
    N3_1 = __VERIFIER_nondet__Bool ();
    N4_1 = __VERIFIER_nondet__Bool ();
    F3_1 = __VERIFIER_nondet__Bool ();
    F4_1 = __VERIFIER_nondet__Bool ();
    W2_1 = __VERIFIER_nondet_int ();
    W3_1 = __VERIFIER_nondet__Bool ();
    W4_1 = __VERIFIER_nondet__Bool ();
    O2_1 = __VERIFIER_nondet__Bool ();
    O3_1 = __VERIFIER_nondet__Bool ();
    O4_1 = __VERIFIER_nondet__Bool ();
    G3_1 = __VERIFIER_nondet_int ();
    G4_1 = __VERIFIER_nondet__Bool ();
    X2_1 = __VERIFIER_nondet__Bool ();
    X3_1 = __VERIFIER_nondet__Bool ();
    P2_1 = __VERIFIER_nondet__Bool ();
    P3_1 = __VERIFIER_nondet__Bool ();
    P4_1 = __VERIFIER_nondet__Bool ();
    H3_1 = __VERIFIER_nondet_int ();
    H4_1 = __VERIFIER_nondet__Bool ();
    Y2_1 = __VERIFIER_nondet_int ();
    Y3_1 = __VERIFIER_nondet__Bool ();
    W_1 = state_0;
    J1_1 = state_1;
    M_1 = state_2;
    H2_1 = state_3;
    L_1 = state_4;
    F2_1 = state_5;
    D2_1 = state_6;
    M1_1 = state_7;
    L1_1 = state_8;
    K_1 = state_9;
    G2_1 = state_10;
    J_1 = state_11;
    E2_1 = state_12;
    C2_1 = state_13;
    K1_1 = state_14;
    I_1 = state_15;
    Y4_1 = state_16;
    H_1 = state_17;
    A5_1 = state_18;
    G_1 = state_19;
    X4_1 = state_20;
    F_1 = state_21;
    Z4_1 = state_22;
    B2_1 = state_23;
    A2_1 = state_24;
    X1_1 = state_25;
    V1_1 = state_26;
    T1_1 = state_27;
    R1_1 = state_28;
    P1_1 = state_29;
    N1_1 = state_30;
    E_1 = state_31;
    D_1 = state_32;
    C_1 = state_33;
    Z1_1 = state_34;
    C5_1 = state_35;
    B_1 = state_36;
    Y1_1 = state_37;
    W1_1 = state_38;
    U1_1 = state_39;
    S1_1 = state_40;
    Q1_1 = state_41;
    O1_1 = state_42;
    U_1 = state_43;
    S_1 = state_44;
    D5_1 = state_45;
    B5_1 = state_46;
    A_1 = state_47;
    N_1 = state_48;
    O_1 = state_49;
    P_1 = state_50;
    Q_1 = state_51;
    R_1 = state_52;
    T_1 = state_53;
    V_1 = state_54;
    X_1 = state_55;
    Y_1 = state_56;
    Z_1 = state_57;
    A1_1 = state_58;
    B1_1 = state_59;
    C1_1 = state_60;
    D1_1 = state_61;
    E1_1 = state_62;
    F1_1 = state_63;
    G1_1 = state_64;
    H1_1 = state_65;
    I1_1 = state_66;
    if (!
        (((B4_1 + H3_1 + (-1 * Z3_1)) == 0)
         && ((D2_1 + M1_1 + (-1 * L1_1)) == 0)
         && ((M1_1 + C2_1 + (-1 * K1_1)) == 0) && (A3_1 == N2_1)
         && (B3_1 == J2_1) && (C3_1 == R2_1) && (E3_1 == W2_1)
         && (G3_1 == Y2_1) && (H3_1 == C4_1) && (M3_1 == E4_1)
         && (Z3_1 == A3_1) && (Z3_1 == R3_1) && (A4_1 == B3_1)
         && (A4_1 == T3_1) && (B4_1 == C3_1) && (B4_1 == U3_1)
         && (C4_1 == V3_1) && (E4_1 == D4_1) && (T4_1 == E3_1)
         && (U4_1 == G3_1) && (D2_1 == P1_1) && (C2_1 == N1_1)
         && (P1_1 == O1_1) && (N1_1 == U_1) && (M1_1 == S_1) && (M1_1 == E_1)
         && (L1_1 == D5_1) && (L1_1 == D_1) && (K1_1 == B5_1) && (K1_1 == C_1)
         && (I_1 == Y4_1) && (G_1 == X4_1)
         && (!(((Q1_1 && S2_1) || (U1_1 && T2_1)) == U2_1))
         && (((!Y3_1) || V2_1) == X3_1) && ((A_1 || (!J1_1)) == N_1)
         && ((M4_1 && L4_1) == K4_1) && ((A2_1 && B2_1) == Z1_1)
         && (A5_1 == B2_1) && (Z4_1 == A2_1) && (X2_1 == T2_1)
         && (Z2_1 == F4_1) && (D3_1 == (Q2_1 && M2_1))
         && (F3_1 == (P2_1 && I2_1)) && (K3_1 == I3_1) && (L3_1 == J3_1)
         && (P3_1 == N3_1) && (Q3_1 == O3_1)
         && (W3_1 ==
             (J1_1 && S3_1 && (!(1 <= U3_1)) && (!(1 <= T3_1))
              && (!(1 <= R3_1)) && (!(32767 <= V3_1)))) && (Y3_1 == W3_1)
         && (G4_1 == S2_1) && (I4_1 == H4_1) && (J4_1 == U2_1)
         && (K4_1 == S3_1) && (N4_1 == X2_1) && (N4_1 == K3_1)
         && (O4_1 == Z2_1) && (O4_1 == P3_1) && (P4_1 == L3_1)
         && (P4_1 == G4_1) && (Q4_1 == Q3_1) && (Q4_1 == I4_1)
         && (R4_1 == D3_1) && (R4_1 == L4_1) && (S4_1 == F3_1)
         && (S4_1 == M4_1) && (H2_1 == X1_1) && (G2_1 == V1_1)
         && (F2_1 == T1_1) && (E2_1 == R1_1) && (Z1_1 == C5_1)
         && (Y1_1 == V2_1) && (X1_1 == W1_1) && (V1_1 == U1_1)
         && (T1_1 == S1_1) && (R1_1 == Q1_1) && (W_1 == J1_1) && (M_1 == H2_1)
         && (L_1 == F2_1) && (K_1 == G2_1) && (J_1 == E2_1) && (H_1 == A5_1)
         && (F_1 == Z4_1) && (B_1 == Y1_1) && ((Y2_1 == V4_1) || (!T1_1)
                                               || (!Z2_1)) && ((W2_1 == K2_1)
                                                               || (!R1_1)
                                                               || (!X2_1))
         && ((!(9 <= Y4_1)) || (!(W4_1 == I2_1))) && ((!(9 <= X4_1))
                                                      || (!(L2_1 == M2_1)))
         && ((Y2_1 == 0) || (T1_1 && Z2_1)) && ((W2_1 == 0) || (R1_1 && X2_1))
         && (I2_1 || (9 <= Y4_1)) && ((!L2_1) || ((X4_1 + (-1 * K2_1)) == -1))
         && ((!L2_1) || ((K1_1 + (-1 * N2_1)) == -1)) && (L2_1
                                                          || (X4_1 == K2_1))
         && (L2_1 || (K1_1 == N2_1)) && (M2_1 || (9 <= X4_1)) && ((!O2_1)
                                                                  ||
                                                                  ((M1_1 +
                                                                    (-1 *
                                                                     R2_1)) ==
                                                                   -1))
         && (O2_1 || (M1_1 == R2_1)) && ((!W4_1)
                                         || ((Y4_1 + (-1 * V4_1)) == -1))
         && ((!W4_1) || ((L1_1 + (-1 * J2_1)) == -1)) && (W4_1
                                                          || (Y4_1 == V4_1))
         && (W4_1 || (L1_1 == J2_1)) && ((!H2_1) || (!((0 <= M3_1) == O3_1)))
         && (H2_1 || (O3_1 == (M3_1 <= -10))) && ((!G2_1)
                                                  || (!((0 <= H3_1) == J3_1)))
         && (G2_1 || (J3_1 == (H3_1 <= -10))) && ((!F2_1)
                                                  || (!((M3_1 <= 0) == N3_1)))
         && (F2_1 || (N3_1 == (10 <= M3_1))) && ((!E2_1)
                                                 || (!((H3_1 <= 0) == I3_1)))
         && (E2_1 || (I3_1 == (10 <= H3_1))) && ((!X1_1) || (!(O2_1 == P2_1)))
         && (X1_1 || P2_1) && ((!V1_1) || (!(O2_1 == Q2_1))) && (V1_1 || Q2_1)
         && ((M3_1 + B4_1 + (-1 * A4_1)) == 0)))
        abort ();
    state_0 = W3_1;
    state_1 = Y3_1;
    state_2 = Q3_1;
    state_3 = Q4_1;
    state_4 = P3_1;
    state_5 = O4_1;
    state_6 = M3_1;
    state_7 = B4_1;
    state_8 = A4_1;
    state_9 = L3_1;
    state_10 = P4_1;
    state_11 = K3_1;
    state_12 = N4_1;
    state_13 = H3_1;
    state_14 = Z3_1;
    state_15 = G3_1;
    state_16 = U4_1;
    state_17 = F3_1;
    state_18 = S4_1;
    state_19 = E3_1;
    state_20 = T4_1;
    state_21 = D3_1;
    state_22 = R4_1;
    state_23 = M4_1;
    state_24 = L4_1;
    state_25 = I4_1;
    state_26 = G4_1;
    state_27 = Z2_1;
    state_28 = X2_1;
    state_29 = E4_1;
    state_30 = C4_1;
    state_31 = C3_1;
    state_32 = B3_1;
    state_33 = A3_1;
    state_34 = K4_1;
    state_35 = S3_1;
    state_36 = U2_1;
    state_37 = J4_1;
    state_38 = H4_1;
    state_39 = S2_1;
    state_40 = F4_1;
    state_41 = T2_1;
    state_42 = D4_1;
    state_43 = V3_1;
    state_44 = U3_1;
    state_45 = T3_1;
    state_46 = R3_1;
    state_47 = V2_1;
    state_48 = X3_1;
    state_49 = L2_1;
    state_50 = W4_1;
    state_51 = O2_1;
    state_52 = N2_1;
    state_53 = J2_1;
    state_54 = R2_1;
    state_55 = Q2_1;
    state_56 = M2_1;
    state_57 = K2_1;
    state_58 = W2_1;
    state_59 = P2_1;
    state_60 = I2_1;
    state_61 = V4_1;
    state_62 = Y2_1;
    state_63 = I3_1;
    state_64 = J3_1;
    state_65 = N3_1;
    state_66 = O3_1;
    goto state_0;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  state_0:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          W_2 = state_0;
          J1_2 = state_1;
          M_2 = state_2;
          H2_2 = state_3;
          L_2 = state_4;
          F2_2 = state_5;
          D2_2 = state_6;
          M1_2 = state_7;
          L1_2 = state_8;
          K_2 = state_9;
          G2_2 = state_10;
          J_2 = state_11;
          E2_2 = state_12;
          C2_2 = state_13;
          K1_2 = state_14;
          I_2 = state_15;
          J2_2 = state_16;
          H_2 = state_17;
          L2_2 = state_18;
          G_2 = state_19;
          I2_2 = state_20;
          F_2 = state_21;
          K2_2 = state_22;
          B2_2 = state_23;
          A2_2 = state_24;
          X1_2 = state_25;
          V1_2 = state_26;
          T1_2 = state_27;
          R1_2 = state_28;
          P1_2 = state_29;
          N1_2 = state_30;
          E_2 = state_31;
          D_2 = state_32;
          C_2 = state_33;
          Z1_2 = state_34;
          N2_2 = state_35;
          B_2 = state_36;
          Y1_2 = state_37;
          W1_2 = state_38;
          U1_2 = state_39;
          S1_2 = state_40;
          Q1_2 = state_41;
          O1_2 = state_42;
          U_2 = state_43;
          S_2 = state_44;
          O2_2 = state_45;
          M2_2 = state_46;
          A_2 = state_47;
          N_2 = state_48;
          O_2 = state_49;
          P_2 = state_50;
          Q_2 = state_51;
          R_2 = state_52;
          T_2 = state_53;
          V_2 = state_54;
          X_2 = state_55;
          Y_2 = state_56;
          Z_2 = state_57;
          A1_2 = state_58;
          B1_2 = state_59;
          C1_2 = state_60;
          D1_2 = state_61;
          E1_2 = state_62;
          F1_2 = state_63;
          G1_2 = state_64;
          H1_2 = state_65;
          I1_2 = state_66;
          if (!(!N_2))
              abort ();
          goto main_error;

      case 1:
          Q2_1 = __VERIFIER_nondet__Bool ();
          Q3_1 = __VERIFIER_nondet__Bool ();
          Q4_1 = __VERIFIER_nondet__Bool ();
          I2_1 = __VERIFIER_nondet__Bool ();
          I3_1 = __VERIFIER_nondet__Bool ();
          I4_1 = __VERIFIER_nondet__Bool ();
          A3_1 = __VERIFIER_nondet_int ();
          A4_1 = __VERIFIER_nondet_int ();
          Z2_1 = __VERIFIER_nondet__Bool ();
          Z3_1 = __VERIFIER_nondet_int ();
          R2_1 = __VERIFIER_nondet_int ();
          R3_1 = __VERIFIER_nondet_int ();
          R4_1 = __VERIFIER_nondet__Bool ();
          J2_1 = __VERIFIER_nondet_int ();
          J3_1 = __VERIFIER_nondet__Bool ();
          J4_1 = __VERIFIER_nondet__Bool ();
          B3_1 = __VERIFIER_nondet_int ();
          B4_1 = __VERIFIER_nondet_int ();
          S2_1 = __VERIFIER_nondet__Bool ();
          S3_1 = __VERIFIER_nondet__Bool ();
          S4_1 = __VERIFIER_nondet__Bool ();
          K2_1 = __VERIFIER_nondet_int ();
          K3_1 = __VERIFIER_nondet__Bool ();
          K4_1 = __VERIFIER_nondet__Bool ();
          C3_1 = __VERIFIER_nondet_int ();
          C4_1 = __VERIFIER_nondet_int ();
          T2_1 = __VERIFIER_nondet__Bool ();
          T3_1 = __VERIFIER_nondet_int ();
          T4_1 = __VERIFIER_nondet_int ();
          L2_1 = __VERIFIER_nondet__Bool ();
          L3_1 = __VERIFIER_nondet__Bool ();
          L4_1 = __VERIFIER_nondet__Bool ();
          D3_1 = __VERIFIER_nondet__Bool ();
          D4_1 = __VERIFIER_nondet_int ();
          U2_1 = __VERIFIER_nondet__Bool ();
          U3_1 = __VERIFIER_nondet_int ();
          U4_1 = __VERIFIER_nondet_int ();
          M2_1 = __VERIFIER_nondet__Bool ();
          M3_1 = __VERIFIER_nondet_int ();
          M4_1 = __VERIFIER_nondet__Bool ();
          E3_1 = __VERIFIER_nondet_int ();
          E4_1 = __VERIFIER_nondet_int ();
          V2_1 = __VERIFIER_nondet__Bool ();
          V3_1 = __VERIFIER_nondet_int ();
          V4_1 = __VERIFIER_nondet_int ();
          N2_1 = __VERIFIER_nondet_int ();
          N3_1 = __VERIFIER_nondet__Bool ();
          N4_1 = __VERIFIER_nondet__Bool ();
          F3_1 = __VERIFIER_nondet__Bool ();
          F4_1 = __VERIFIER_nondet__Bool ();
          W2_1 = __VERIFIER_nondet_int ();
          W3_1 = __VERIFIER_nondet__Bool ();
          W4_1 = __VERIFIER_nondet__Bool ();
          O2_1 = __VERIFIER_nondet__Bool ();
          O3_1 = __VERIFIER_nondet__Bool ();
          O4_1 = __VERIFIER_nondet__Bool ();
          G3_1 = __VERIFIER_nondet_int ();
          G4_1 = __VERIFIER_nondet__Bool ();
          X2_1 = __VERIFIER_nondet__Bool ();
          X3_1 = __VERIFIER_nondet__Bool ();
          P2_1 = __VERIFIER_nondet__Bool ();
          P3_1 = __VERIFIER_nondet__Bool ();
          P4_1 = __VERIFIER_nondet__Bool ();
          H3_1 = __VERIFIER_nondet_int ();
          H4_1 = __VERIFIER_nondet__Bool ();
          Y2_1 = __VERIFIER_nondet_int ();
          Y3_1 = __VERIFIER_nondet__Bool ();
          W_1 = state_0;
          J1_1 = state_1;
          M_1 = state_2;
          H2_1 = state_3;
          L_1 = state_4;
          F2_1 = state_5;
          D2_1 = state_6;
          M1_1 = state_7;
          L1_1 = state_8;
          K_1 = state_9;
          G2_1 = state_10;
          J_1 = state_11;
          E2_1 = state_12;
          C2_1 = state_13;
          K1_1 = state_14;
          I_1 = state_15;
          Y4_1 = state_16;
          H_1 = state_17;
          A5_1 = state_18;
          G_1 = state_19;
          X4_1 = state_20;
          F_1 = state_21;
          Z4_1 = state_22;
          B2_1 = state_23;
          A2_1 = state_24;
          X1_1 = state_25;
          V1_1 = state_26;
          T1_1 = state_27;
          R1_1 = state_28;
          P1_1 = state_29;
          N1_1 = state_30;
          E_1 = state_31;
          D_1 = state_32;
          C_1 = state_33;
          Z1_1 = state_34;
          C5_1 = state_35;
          B_1 = state_36;
          Y1_1 = state_37;
          W1_1 = state_38;
          U1_1 = state_39;
          S1_1 = state_40;
          Q1_1 = state_41;
          O1_1 = state_42;
          U_1 = state_43;
          S_1 = state_44;
          D5_1 = state_45;
          B5_1 = state_46;
          A_1 = state_47;
          N_1 = state_48;
          O_1 = state_49;
          P_1 = state_50;
          Q_1 = state_51;
          R_1 = state_52;
          T_1 = state_53;
          V_1 = state_54;
          X_1 = state_55;
          Y_1 = state_56;
          Z_1 = state_57;
          A1_1 = state_58;
          B1_1 = state_59;
          C1_1 = state_60;
          D1_1 = state_61;
          E1_1 = state_62;
          F1_1 = state_63;
          G1_1 = state_64;
          H1_1 = state_65;
          I1_1 = state_66;
          if (!
              (((B4_1 + H3_1 + (-1 * Z3_1)) == 0)
               && ((D2_1 + M1_1 + (-1 * L1_1)) == 0)
               && ((M1_1 + C2_1 + (-1 * K1_1)) == 0) && (A3_1 == N2_1)
               && (B3_1 == J2_1) && (C3_1 == R2_1) && (E3_1 == W2_1)
               && (G3_1 == Y2_1) && (H3_1 == C4_1) && (M3_1 == E4_1)
               && (Z3_1 == A3_1) && (Z3_1 == R3_1) && (A4_1 == B3_1)
               && (A4_1 == T3_1) && (B4_1 == C3_1) && (B4_1 == U3_1)
               && (C4_1 == V3_1) && (E4_1 == D4_1) && (T4_1 == E3_1)
               && (U4_1 == G3_1) && (D2_1 == P1_1) && (C2_1 == N1_1)
               && (P1_1 == O1_1) && (N1_1 == U_1) && (M1_1 == S_1)
               && (M1_1 == E_1) && (L1_1 == D5_1) && (L1_1 == D_1)
               && (K1_1 == B5_1) && (K1_1 == C_1) && (I_1 == Y4_1)
               && (G_1 == X4_1)
               && (!(((Q1_1 && S2_1) || (U1_1 && T2_1)) == U2_1))
               && (((!Y3_1) || V2_1) == X3_1) && ((A_1 || (!J1_1)) == N_1)
               && ((M4_1 && L4_1) == K4_1) && ((A2_1 && B2_1) == Z1_1)
               && (A5_1 == B2_1) && (Z4_1 == A2_1) && (X2_1 == T2_1)
               && (Z2_1 == F4_1) && (D3_1 == (Q2_1 && M2_1))
               && (F3_1 == (P2_1 && I2_1)) && (K3_1 == I3_1) && (L3_1 == J3_1)
               && (P3_1 == N3_1) && (Q3_1 == O3_1)
               && (W3_1 ==
                   (J1_1 && S3_1 && (!(1 <= U3_1)) && (!(1 <= T3_1))
                    && (!(1 <= R3_1)) && (!(32767 <= V3_1))))
               && (Y3_1 == W3_1) && (G4_1 == S2_1) && (I4_1 == H4_1)
               && (J4_1 == U2_1) && (K4_1 == S3_1) && (N4_1 == X2_1)
               && (N4_1 == K3_1) && (O4_1 == Z2_1) && (O4_1 == P3_1)
               && (P4_1 == L3_1) && (P4_1 == G4_1) && (Q4_1 == Q3_1)
               && (Q4_1 == I4_1) && (R4_1 == D3_1) && (R4_1 == L4_1)
               && (S4_1 == F3_1) && (S4_1 == M4_1) && (H2_1 == X1_1)
               && (G2_1 == V1_1) && (F2_1 == T1_1) && (E2_1 == R1_1)
               && (Z1_1 == C5_1) && (Y1_1 == V2_1) && (X1_1 == W1_1)
               && (V1_1 == U1_1) && (T1_1 == S1_1) && (R1_1 == Q1_1)
               && (W_1 == J1_1) && (M_1 == H2_1) && (L_1 == F2_1)
               && (K_1 == G2_1) && (J_1 == E2_1) && (H_1 == A5_1)
               && (F_1 == Z4_1) && (B_1 == Y1_1) && ((Y2_1 == V4_1) || (!T1_1)
                                                     || (!Z2_1))
               && ((W2_1 == K2_1) || (!R1_1) || (!X2_1)) && ((!(9 <= Y4_1))
                                                             ||
                                                             (!(W4_1 ==
                                                                I2_1)))
               && ((!(9 <= X4_1)) || (!(L2_1 == M2_1))) && ((Y2_1 == 0)
                                                            || (T1_1 && Z2_1))
               && ((W2_1 == 0) || (R1_1 && X2_1)) && (I2_1 || (9 <= Y4_1))
               && ((!L2_1) || ((X4_1 + (-1 * K2_1)) == -1)) && ((!L2_1)
                                                                ||
                                                                ((K1_1 +
                                                                  (-1 *
                                                                   N2_1)) ==
                                                                 -1)) && (L2_1
                                                                          ||
                                                                          (X4_1
                                                                           ==
                                                                           K2_1))
               && (L2_1 || (K1_1 == N2_1)) && (M2_1 || (9 <= X4_1))
               && ((!O2_1) || ((M1_1 + (-1 * R2_1)) == -1)) && (O2_1
                                                                || (M1_1 ==
                                                                    R2_1))
               && ((!W4_1) || ((Y4_1 + (-1 * V4_1)) == -1)) && ((!W4_1)
                                                                ||
                                                                ((L1_1 +
                                                                  (-1 *
                                                                   J2_1)) ==
                                                                 -1)) && (W4_1
                                                                          ||
                                                                          (Y4_1
                                                                           ==
                                                                           V4_1))
               && (W4_1 || (L1_1 == J2_1)) && ((!H2_1)
                                               || (!((0 <= M3_1) == O3_1)))
               && (H2_1 || (O3_1 == (M3_1 <= -10))) && ((!G2_1)
                                                        ||
                                                        (!((0 <= H3_1) ==
                                                           J3_1))) && (G2_1
                                                                       ||
                                                                       (J3_1
                                                                        ==
                                                                        (H3_1
                                                                         <=
                                                                         -10)))
               && ((!F2_1) || (!((M3_1 <= 0) == N3_1))) && (F2_1
                                                            || (N3_1 ==
                                                                (10 <= M3_1)))
               && ((!E2_1) || (!((H3_1 <= 0) == I3_1))) && (E2_1
                                                            || (I3_1 ==
                                                                (10 <= H3_1)))
               && ((!X1_1) || (!(O2_1 == P2_1))) && (X1_1 || P2_1) && ((!V1_1)
                                                                       ||
                                                                       (!(O2_1
                                                                          ==
                                                                          Q2_1)))
               && (V1_1 || Q2_1) && ((M3_1 + B4_1 + (-1 * A4_1)) == 0)))
              abort ();
          state_0 = W3_1;
          state_1 = Y3_1;
          state_2 = Q3_1;
          state_3 = Q4_1;
          state_4 = P3_1;
          state_5 = O4_1;
          state_6 = M3_1;
          state_7 = B4_1;
          state_8 = A4_1;
          state_9 = L3_1;
          state_10 = P4_1;
          state_11 = K3_1;
          state_12 = N4_1;
          state_13 = H3_1;
          state_14 = Z3_1;
          state_15 = G3_1;
          state_16 = U4_1;
          state_17 = F3_1;
          state_18 = S4_1;
          state_19 = E3_1;
          state_20 = T4_1;
          state_21 = D3_1;
          state_22 = R4_1;
          state_23 = M4_1;
          state_24 = L4_1;
          state_25 = I4_1;
          state_26 = G4_1;
          state_27 = Z2_1;
          state_28 = X2_1;
          state_29 = E4_1;
          state_30 = C4_1;
          state_31 = C3_1;
          state_32 = B3_1;
          state_33 = A3_1;
          state_34 = K4_1;
          state_35 = S3_1;
          state_36 = U2_1;
          state_37 = J4_1;
          state_38 = H4_1;
          state_39 = S2_1;
          state_40 = F4_1;
          state_41 = T2_1;
          state_42 = D4_1;
          state_43 = V3_1;
          state_44 = U3_1;
          state_45 = T3_1;
          state_46 = R3_1;
          state_47 = V2_1;
          state_48 = X3_1;
          state_49 = L2_1;
          state_50 = W4_1;
          state_51 = O2_1;
          state_52 = N2_1;
          state_53 = J2_1;
          state_54 = R2_1;
          state_55 = Q2_1;
          state_56 = M2_1;
          state_57 = K2_1;
          state_58 = W2_1;
          state_59 = P2_1;
          state_60 = I2_1;
          state_61 = V4_1;
          state_62 = Y2_1;
          state_63 = I3_1;
          state_64 = J3_1;
          state_65 = N3_1;
          state_66 = O3_1;
          goto state_0;

      default:
          abort ();
      }

    // return expression

}

