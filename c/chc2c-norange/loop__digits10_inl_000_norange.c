// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: llreve-bench/loop__digits10_inl_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "loop__digits10_inl_000_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int INV_MAIN_42_0;
    int INV_MAIN_42_1;
    int INV_MAIN_42_2;
    int INV_MAIN_42_3;
    int INV_MAIN_42_4;
    int INV_MAIN_42_5;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    int v_4_0;
    int v_5_0;
    int v_6_0;
    int A_1;
    int B_1;
    int C_1;
    int D_1;
    int E_1;
    int F_1;
    int G_1;
    int H_1;
    int v_8_1;
    int v_9_1;
    int A_2;
    int B_2;
    int C_2;
    int D_2;
    int E_2;
    int F_2;
    int G_2;
    int H_2;
    int I_2;
    int v_9_2;
    int A_3;
    int B_3;
    int C_3;
    int D_3;
    int E_3;
    int F_3;
    int G_3;
    int H_3;
    int I_3;
    int v_9_3;
    int A_4;
    int B_4;
    int C_4;
    int D_4;
    int E_4;
    int F_4;
    int G_4;
    int H_4;
    int I_4;
    int v_9_4;
    int A_5;
    int B_5;
    int C_5;
    int D_5;
    int E_5;
    int F_5;
    int G_5;
    int H_5;
    int I_5;
    int J_5;
    int A_6;
    int B_6;
    int C_6;
    int D_6;
    int E_6;
    int F_6;
    int G_6;
    int H_6;
    int v_8_6;
    int v_9_6;
    int A_7;
    int B_7;
    int C_7;
    int D_7;
    int E_7;
    int F_7;
    int G_7;
    int H_7;
    int I_7;
    int v_9_7;
    int A_8;
    int B_8;
    int C_8;
    int D_8;
    int E_8;
    int F_8;
    int G_8;
    int H_8;
    int I_8;
    int v_9_8;
    int A_9;
    int B_9;
    int C_9;
    int D_9;
    int E_9;
    int F_9;
    int G_9;
    int H_9;
    int I_9;
    int v_9_9;
    int A_10;
    int B_10;
    int C_10;
    int D_10;
    int E_10;
    int F_10;
    int G_10;
    int H_10;
    int I_10;
    int J_10;
    int A_11;
    int B_11;
    int C_11;
    int D_11;
    int E_11;
    int F_11;
    int G_11;
    int H_11;
    int v_8_11;
    int v_9_11;
    int A_12;
    int B_12;
    int C_12;
    int D_12;
    int E_12;
    int F_12;
    int G_12;
    int H_12;
    int I_12;
    int v_9_12;
    int A_13;
    int B_13;
    int C_13;
    int D_13;
    int E_13;
    int F_13;
    int G_13;
    int H_13;
    int I_13;
    int v_9_13;
    int A_14;
    int B_14;
    int C_14;
    int D_14;
    int E_14;
    int F_14;
    int G_14;
    int H_14;
    int I_14;
    int v_9_14;
    int A_15;
    int B_15;
    int C_15;
    int D_15;
    int E_15;
    int F_15;
    int G_15;
    int H_15;
    int I_15;
    int J_15;
    int A_16;
    int B_16;
    int C_16;
    int D_16;
    int E_16;
    int F_16;
    int G_16;
    int H_16;
    int v_8_16;
    int v_9_16;
    int A_17;
    int B_17;
    int C_17;
    int D_17;
    int E_17;
    int F_17;
    int G_17;
    int H_17;
    int I_17;
    int v_9_17;
    int A_18;
    int B_18;
    int C_18;
    int D_18;
    int E_18;
    int F_18;
    int G_18;
    int H_18;
    int I_18;
    int v_9_18;
    int A_19;
    int B_19;
    int C_19;
    int D_19;
    int E_19;
    int F_19;
    int G_19;
    int H_19;
    int I_19;
    int v_9_19;
    int A_20;
    int B_20;
    int C_20;
    int D_20;
    int E_20;
    int F_20;
    int G_20;
    int H_20;
    int I_20;
    int J_20;
    int A_21;
    int B_21;
    int C_21;
    int D_21;
    int E_21;
    int F_21;
    int G_21;
    int H_21;
    int A_22;
    int B_22;
    int C_22;
    int D_22;
    int E_22;
    int F_22;
    int G_22;
    int H_22;
    int A_23;
    int B_23;
    int C_23;
    int D_23;
    int E_23;
    int F_23;
    int G_23;
    int H_23;
    int A_24;
    int B_24;
    int C_24;
    int D_24;
    int E_24;
    int F_24;
    int G_24;
    int H_24;
    int A_25;
    int B_25;
    int C_25;
    int D_25;
    int E_25;
    int F_25;
    int v_6_25;
    int v_7_25;
    int A_26;
    int B_26;
    int C_26;
    int D_26;
    int E_26;
    int F_26;
    int G_26;
    int v_7_26;
    int A_27;
    int B_27;
    int C_27;
    int D_27;
    int E_27;
    int F_27;
    int G_27;
    int v_7_27;
    int A_28;
    int B_28;
    int C_28;
    int D_28;
    int E_28;
    int F_28;
    int G_28;
    int v_7_28;
    int A_29;
    int B_29;
    int C_29;
    int D_29;
    int E_29;
    int F_29;
    int G_29;
    int H_29;
    int A_30;
    int B_30;
    int C_30;
    int D_30;
    int E_30;
    int F_30;



    // main logic
    goto main_init;

  main_init:
    if (!
        ((A_0 == -1) && (C_0 == D_0) && (B_0 == (C_0 / 10)) && (1 == v_4_0)
         && (1 == v_5_0) && (1 == v_6_0)))
        abort ();
    INV_MAIN_42_0 = B_0;
    INV_MAIN_42_1 = v_4_0;
    INV_MAIN_42_2 = v_5_0;
    INV_MAIN_42_3 = D_0;
    INV_MAIN_42_4 = v_6_0;
    INV_MAIN_42_5 = A_0;
    A_18 = __VERIFIER_nondet_int ();
    v_9_18 = __VERIFIER_nondet_int ();
    B_18 = __VERIFIER_nondet_int ();
    C_18 = __VERIFIER_nondet_int ();
    D_18 = INV_MAIN_42_0;
    E_18 = INV_MAIN_42_1;
    F_18 = INV_MAIN_42_2;
    G_18 = INV_MAIN_42_3;
    H_18 = INV_MAIN_42_4;
    I_18 = INV_MAIN_42_5;
    if (!
        ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
         && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
         && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
         && (0 == v_9_18)))
        abort ();
    INV_MAIN_42_0 = C_18;
    INV_MAIN_42_1 = B_18;
    INV_MAIN_42_2 = v_9_18;
    INV_MAIN_42_3 = G_18;
    INV_MAIN_42_4 = H_18;
    INV_MAIN_42_5 = A_18;
    goto INV_MAIN_42_28;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  INV_MAIN_42_0:
    switch (__VERIFIER_nondet_int ())
      {
      case 0:
          A_30 = INV_MAIN_42_0;
          B_30 = INV_MAIN_42_1;
          C_30 = INV_MAIN_42_2;
          D_30 = INV_MAIN_42_3;
          E_30 = INV_MAIN_42_4;
          F_30 = INV_MAIN_42_5;
          if (!((!(B_30 == F_30)) && (A_30 <= 0) && (C_30 == 0)))
              abort ();
          goto main_error;

      case 1:
          v_6_25 = __VERIFIER_nondet_int ();
          v_7_25 = __VERIFIER_nondet_int ();
          A_25 = INV_MAIN_42_0;
          B_25 = INV_MAIN_42_1;
          C_25 = INV_MAIN_42_2;
          D_25 = INV_MAIN_42_3;
          E_25 = INV_MAIN_42_4;
          F_25 = INV_MAIN_42_5;
          if (!
              ((!(10 <= D_25))
               && ((A_25 <= 0) || ((A_25 / 10) <= 0)
                   || (((A_25 / 10) / 10) <= 0)
                   || ((((A_25 / 10) / 10) / 10) <= 0))
               && ((!((((A_25 / 10) / 10) / 10) <= 0))
                   || (((A_25 / 10) / 10) <= 0) || ((A_25 / 10) <= 0)
                   || (A_25 <= 0)) && ((!(((A_25 / 10) / 10) <= 0))
                                       || ((A_25 / 10) <= 0) || (A_25 <= 0))
               && ((!((A_25 / 10) <= 0)) || (A_25 <= 0)) && (!(C_25 == 0))
               && (0 == v_6_25) && (v_7_25 == E_25)))
              abort ();
          INV_MAIN_42_0 = A_25;
          INV_MAIN_42_1 = B_25;
          INV_MAIN_42_2 = v_6_25;
          INV_MAIN_42_3 = D_25;
          INV_MAIN_42_4 = E_25;
          INV_MAIN_42_5 = v_7_25;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 2:
          A_1 = __VERIFIER_nondet_int ();
          v_9_1 = __VERIFIER_nondet_int ();
          B_1 = __VERIFIER_nondet_int ();
          v_8_1 = __VERIFIER_nondet_int ();
          C_1 = INV_MAIN_42_0;
          D_1 = INV_MAIN_42_1;
          E_1 = INV_MAIN_42_2;
          F_1 = INV_MAIN_42_3;
          G_1 = INV_MAIN_42_4;
          H_1 = INV_MAIN_42_5;
          if (!
              ((B_1 == ((((C_1 / 10) / 10) / 10) / 10)) && (!(E_1 == 0))
               && (!(10 <= F_1)) && (!((((C_1 / 10) / 10) / 10) <= 0))
               && (!(((C_1 / 10) / 10) <= 0)) && (!((C_1 / 10) <= 0))
               && (!(C_1 <= 0)) && (A_1 == (D_1 + 4)) && (0 == v_8_1)
               && (v_9_1 == G_1)))
              abort ();
          INV_MAIN_42_0 = B_1;
          INV_MAIN_42_1 = A_1;
          INV_MAIN_42_2 = v_8_1;
          INV_MAIN_42_3 = F_1;
          INV_MAIN_42_4 = G_1;
          INV_MAIN_42_5 = v_9_1;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 3:
          A_2 = __VERIFIER_nondet_int ();
          v_9_2 = __VERIFIER_nondet_int ();
          B_2 = __VERIFIER_nondet_int ();
          C_2 = __VERIFIER_nondet_int ();
          D_2 = INV_MAIN_42_0;
          E_2 = INV_MAIN_42_1;
          F_2 = INV_MAIN_42_2;
          G_2 = INV_MAIN_42_3;
          H_2 = INV_MAIN_42_4;
          I_2 = INV_MAIN_42_5;
          if (!
              ((B_2 == (E_2 + 4)) && (C_2 == ((((D_2 / 10) / 10) / 10) / 10))
               && (!(F_2 == 0)) && (!(100 <= G_2)) && (10 <= G_2)
               && (!((((D_2 / 10) / 10) / 10) <= 0))
               && (!(((D_2 / 10) / 10) <= 0)) && (!((D_2 / 10) <= 0))
               && (!(D_2 <= 0)) && (A_2 == (H_2 + 1)) && (0 == v_9_2)))
              abort ();
          INV_MAIN_42_0 = C_2;
          INV_MAIN_42_1 = B_2;
          INV_MAIN_42_2 = v_9_2;
          INV_MAIN_42_3 = G_2;
          INV_MAIN_42_4 = H_2;
          INV_MAIN_42_5 = A_2;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 4:
          A_4 = __VERIFIER_nondet_int ();
          v_9_4 = __VERIFIER_nondet_int ();
          B_4 = __VERIFIER_nondet_int ();
          C_4 = __VERIFIER_nondet_int ();
          D_4 = INV_MAIN_42_0;
          E_4 = INV_MAIN_42_1;
          F_4 = INV_MAIN_42_2;
          G_4 = INV_MAIN_42_3;
          H_4 = INV_MAIN_42_4;
          I_4 = INV_MAIN_42_5;
          if (!
              ((B_4 == (E_4 + 4)) && (C_4 == ((((D_4 / 10) / 10) / 10) / 10))
               && (!(F_4 == 0)) && (!(10000 <= G_4)) && (1000 <= G_4)
               && (100 <= G_4) && (10 <= G_4)
               && (!((((D_4 / 10) / 10) / 10) <= 0))
               && (!(((D_4 / 10) / 10) <= 0)) && (!((D_4 / 10) <= 0))
               && (!(D_4 <= 0)) && (A_4 == (H_4 + 3)) && (0 == v_9_4)))
              abort ();
          INV_MAIN_42_0 = C_4;
          INV_MAIN_42_1 = B_4;
          INV_MAIN_42_2 = v_9_4;
          INV_MAIN_42_3 = G_4;
          INV_MAIN_42_4 = H_4;
          INV_MAIN_42_5 = A_4;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 5:
          A_5 = __VERIFIER_nondet_int ();
          B_5 = __VERIFIER_nondet_int ();
          C_5 = __VERIFIER_nondet_int ();
          D_5 = __VERIFIER_nondet_int ();
          E_5 = INV_MAIN_42_0;
          F_5 = INV_MAIN_42_1;
          G_5 = INV_MAIN_42_2;
          H_5 = INV_MAIN_42_3;
          I_5 = INV_MAIN_42_4;
          J_5 = INV_MAIN_42_5;
          if (!
              ((B_5 == (H_5 / 10000)) && (C_5 == (F_5 + 4))
               && (D_5 == ((((E_5 / 10) / 10) / 10) / 10)) && (!(G_5 == 0))
               && (10000 <= H_5) && (1000 <= H_5) && (100 <= H_5)
               && (10 <= H_5) && (!((((E_5 / 10) / 10) / 10) <= 0))
               && (!(((E_5 / 10) / 10) <= 0)) && (!((E_5 / 10) <= 0))
               && (!(E_5 <= 0)) && (A_5 == (I_5 + 4))))
              abort ();
          INV_MAIN_42_0 = D_5;
          INV_MAIN_42_1 = C_5;
          INV_MAIN_42_2 = G_5;
          INV_MAIN_42_3 = B_5;
          INV_MAIN_42_4 = A_5;
          INV_MAIN_42_5 = J_5;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 6:
          A_6 = __VERIFIER_nondet_int ();
          v_9_6 = __VERIFIER_nondet_int ();
          B_6 = __VERIFIER_nondet_int ();
          v_8_6 = __VERIFIER_nondet_int ();
          C_6 = INV_MAIN_42_0;
          D_6 = INV_MAIN_42_1;
          E_6 = INV_MAIN_42_2;
          F_6 = INV_MAIN_42_3;
          G_6 = INV_MAIN_42_4;
          H_6 = INV_MAIN_42_5;
          if (!
              ((B_6 == (((C_6 / 10) / 10) / 10)) && (!(E_6 == 0))
               && (!(10 <= F_6)) && ((((C_6 / 10) / 10) / 10) <= 0)
               && (!(((C_6 / 10) / 10) <= 0)) && (!((C_6 / 10) <= 0))
               && (!(C_6 <= 0)) && (A_6 == (D_6 + 3)) && (0 == v_8_6)
               && (v_9_6 == G_6)))
              abort ();
          INV_MAIN_42_0 = B_6;
          INV_MAIN_42_1 = A_6;
          INV_MAIN_42_2 = v_8_6;
          INV_MAIN_42_3 = F_6;
          INV_MAIN_42_4 = G_6;
          INV_MAIN_42_5 = v_9_6;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 7:
          A_7 = __VERIFIER_nondet_int ();
          v_9_7 = __VERIFIER_nondet_int ();
          B_7 = __VERIFIER_nondet_int ();
          C_7 = __VERIFIER_nondet_int ();
          D_7 = INV_MAIN_42_0;
          E_7 = INV_MAIN_42_1;
          F_7 = INV_MAIN_42_2;
          G_7 = INV_MAIN_42_3;
          H_7 = INV_MAIN_42_4;
          I_7 = INV_MAIN_42_5;
          if (!
              ((B_7 == (E_7 + 3)) && (C_7 == (((D_7 / 10) / 10) / 10))
               && (!(F_7 == 0)) && (!(100 <= G_7)) && (10 <= G_7)
               && ((((D_7 / 10) / 10) / 10) <= 0)
               && (!(((D_7 / 10) / 10) <= 0)) && (!((D_7 / 10) <= 0))
               && (!(D_7 <= 0)) && (A_7 == (H_7 + 1)) && (0 == v_9_7)))
              abort ();
          INV_MAIN_42_0 = C_7;
          INV_MAIN_42_1 = B_7;
          INV_MAIN_42_2 = v_9_7;
          INV_MAIN_42_3 = G_7;
          INV_MAIN_42_4 = H_7;
          INV_MAIN_42_5 = A_7;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 8:
          A_8 = __VERIFIER_nondet_int ();
          v_9_8 = __VERIFIER_nondet_int ();
          B_8 = __VERIFIER_nondet_int ();
          C_8 = __VERIFIER_nondet_int ();
          D_8 = INV_MAIN_42_0;
          E_8 = INV_MAIN_42_1;
          F_8 = INV_MAIN_42_2;
          G_8 = INV_MAIN_42_3;
          H_8 = INV_MAIN_42_4;
          I_8 = INV_MAIN_42_5;
          if (!
              ((B_8 == (E_8 + 3)) && (C_8 == (((D_8 / 10) / 10) / 10))
               && (!(F_8 == 0)) && (!(1000 <= G_8)) && (100 <= G_8)
               && (10 <= G_8) && ((((D_8 / 10) / 10) / 10) <= 0)
               && (!(((D_8 / 10) / 10) <= 0)) && (!((D_8 / 10) <= 0))
               && (!(D_8 <= 0)) && (A_8 == (H_8 + 2)) && (0 == v_9_8)))
              abort ();
          INV_MAIN_42_0 = C_8;
          INV_MAIN_42_1 = B_8;
          INV_MAIN_42_2 = v_9_8;
          INV_MAIN_42_3 = G_8;
          INV_MAIN_42_4 = H_8;
          INV_MAIN_42_5 = A_8;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 9:
          A_9 = __VERIFIER_nondet_int ();
          v_9_9 = __VERIFIER_nondet_int ();
          B_9 = __VERIFIER_nondet_int ();
          C_9 = __VERIFIER_nondet_int ();
          D_9 = INV_MAIN_42_0;
          E_9 = INV_MAIN_42_1;
          F_9 = INV_MAIN_42_2;
          G_9 = INV_MAIN_42_3;
          H_9 = INV_MAIN_42_4;
          I_9 = INV_MAIN_42_5;
          if (!
              ((B_9 == (E_9 + 3)) && (C_9 == (((D_9 / 10) / 10) / 10))
               && (!(F_9 == 0)) && (!(10000 <= G_9)) && (1000 <= G_9)
               && (100 <= G_9) && (10 <= G_9)
               && ((((D_9 / 10) / 10) / 10) <= 0)
               && (!(((D_9 / 10) / 10) <= 0)) && (!((D_9 / 10) <= 0))
               && (!(D_9 <= 0)) && (A_9 == (H_9 + 3)) && (0 == v_9_9)))
              abort ();
          INV_MAIN_42_0 = C_9;
          INV_MAIN_42_1 = B_9;
          INV_MAIN_42_2 = v_9_9;
          INV_MAIN_42_3 = G_9;
          INV_MAIN_42_4 = H_9;
          INV_MAIN_42_5 = A_9;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 10:
          A_10 = __VERIFIER_nondet_int ();
          B_10 = __VERIFIER_nondet_int ();
          C_10 = __VERIFIER_nondet_int ();
          D_10 = __VERIFIER_nondet_int ();
          E_10 = INV_MAIN_42_0;
          F_10 = INV_MAIN_42_1;
          G_10 = INV_MAIN_42_2;
          H_10 = INV_MAIN_42_3;
          I_10 = INV_MAIN_42_4;
          J_10 = INV_MAIN_42_5;
          if (!
              ((B_10 == (H_10 / 10000)) && (C_10 == (F_10 + 3))
               && (D_10 == (((E_10 / 10) / 10) / 10)) && (!(G_10 == 0))
               && (10000 <= H_10) && (1000 <= H_10) && (100 <= H_10)
               && (10 <= H_10) && ((((E_10 / 10) / 10) / 10) <= 0)
               && (!(((E_10 / 10) / 10) <= 0)) && (!((E_10 / 10) <= 0))
               && (!(E_10 <= 0)) && (A_10 == (I_10 + 4))))
              abort ();
          INV_MAIN_42_0 = D_10;
          INV_MAIN_42_1 = C_10;
          INV_MAIN_42_2 = G_10;
          INV_MAIN_42_3 = B_10;
          INV_MAIN_42_4 = A_10;
          INV_MAIN_42_5 = J_10;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 11:
          A_11 = __VERIFIER_nondet_int ();
          v_9_11 = __VERIFIER_nondet_int ();
          B_11 = __VERIFIER_nondet_int ();
          v_8_11 = __VERIFIER_nondet_int ();
          C_11 = INV_MAIN_42_0;
          D_11 = INV_MAIN_42_1;
          E_11 = INV_MAIN_42_2;
          F_11 = INV_MAIN_42_3;
          G_11 = INV_MAIN_42_4;
          H_11 = INV_MAIN_42_5;
          if (!
              ((B_11 == ((C_11 / 10) / 10)) && (!(E_11 == 0))
               && (!(10 <= F_11)) && (((C_11 / 10) / 10) <= 0)
               && (!((C_11 / 10) <= 0)) && (!(C_11 <= 0))
               && (A_11 == (D_11 + 2)) && (0 == v_8_11) && (v_9_11 == G_11)))
              abort ();
          INV_MAIN_42_0 = B_11;
          INV_MAIN_42_1 = A_11;
          INV_MAIN_42_2 = v_8_11;
          INV_MAIN_42_3 = F_11;
          INV_MAIN_42_4 = G_11;
          INV_MAIN_42_5 = v_9_11;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 12:
          A_12 = __VERIFIER_nondet_int ();
          v_9_12 = __VERIFIER_nondet_int ();
          B_12 = __VERIFIER_nondet_int ();
          C_12 = __VERIFIER_nondet_int ();
          D_12 = INV_MAIN_42_0;
          E_12 = INV_MAIN_42_1;
          F_12 = INV_MAIN_42_2;
          G_12 = INV_MAIN_42_3;
          H_12 = INV_MAIN_42_4;
          I_12 = INV_MAIN_42_5;
          if (!
              ((B_12 == (E_12 + 2)) && (C_12 == ((D_12 / 10) / 10))
               && (!(F_12 == 0)) && (!(100 <= G_12)) && (10 <= G_12)
               && (((D_12 / 10) / 10) <= 0) && (!((D_12 / 10) <= 0))
               && (!(D_12 <= 0)) && (A_12 == (H_12 + 1)) && (0 == v_9_12)))
              abort ();
          INV_MAIN_42_0 = C_12;
          INV_MAIN_42_1 = B_12;
          INV_MAIN_42_2 = v_9_12;
          INV_MAIN_42_3 = G_12;
          INV_MAIN_42_4 = H_12;
          INV_MAIN_42_5 = A_12;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 13:
          A_13 = __VERIFIER_nondet_int ();
          v_9_13 = __VERIFIER_nondet_int ();
          B_13 = __VERIFIER_nondet_int ();
          C_13 = __VERIFIER_nondet_int ();
          D_13 = INV_MAIN_42_0;
          E_13 = INV_MAIN_42_1;
          F_13 = INV_MAIN_42_2;
          G_13 = INV_MAIN_42_3;
          H_13 = INV_MAIN_42_4;
          I_13 = INV_MAIN_42_5;
          if (!
              ((B_13 == (E_13 + 2)) && (C_13 == ((D_13 / 10) / 10))
               && (!(F_13 == 0)) && (!(1000 <= G_13)) && (100 <= G_13)
               && (10 <= G_13) && (((D_13 / 10) / 10) <= 0)
               && (!((D_13 / 10) <= 0)) && (!(D_13 <= 0))
               && (A_13 == (H_13 + 2)) && (0 == v_9_13)))
              abort ();
          INV_MAIN_42_0 = C_13;
          INV_MAIN_42_1 = B_13;
          INV_MAIN_42_2 = v_9_13;
          INV_MAIN_42_3 = G_13;
          INV_MAIN_42_4 = H_13;
          INV_MAIN_42_5 = A_13;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 14:
          A_14 = __VERIFIER_nondet_int ();
          v_9_14 = __VERIFIER_nondet_int ();
          B_14 = __VERIFIER_nondet_int ();
          C_14 = __VERIFIER_nondet_int ();
          D_14 = INV_MAIN_42_0;
          E_14 = INV_MAIN_42_1;
          F_14 = INV_MAIN_42_2;
          G_14 = INV_MAIN_42_3;
          H_14 = INV_MAIN_42_4;
          I_14 = INV_MAIN_42_5;
          if (!
              ((B_14 == (E_14 + 2)) && (C_14 == ((D_14 / 10) / 10))
               && (!(F_14 == 0)) && (!(10000 <= G_14)) && (1000 <= G_14)
               && (100 <= G_14) && (10 <= G_14) && (((D_14 / 10) / 10) <= 0)
               && (!((D_14 / 10) <= 0)) && (!(D_14 <= 0))
               && (A_14 == (H_14 + 3)) && (0 == v_9_14)))
              abort ();
          INV_MAIN_42_0 = C_14;
          INV_MAIN_42_1 = B_14;
          INV_MAIN_42_2 = v_9_14;
          INV_MAIN_42_3 = G_14;
          INV_MAIN_42_4 = H_14;
          INV_MAIN_42_5 = A_14;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 15:
          A_15 = __VERIFIER_nondet_int ();
          B_15 = __VERIFIER_nondet_int ();
          C_15 = __VERIFIER_nondet_int ();
          D_15 = __VERIFIER_nondet_int ();
          E_15 = INV_MAIN_42_0;
          F_15 = INV_MAIN_42_1;
          G_15 = INV_MAIN_42_2;
          H_15 = INV_MAIN_42_3;
          I_15 = INV_MAIN_42_4;
          J_15 = INV_MAIN_42_5;
          if (!
              ((B_15 == (H_15 / 10000)) && (C_15 == (F_15 + 2))
               && (D_15 == ((E_15 / 10) / 10)) && (!(G_15 == 0))
               && (10000 <= H_15) && (1000 <= H_15) && (100 <= H_15)
               && (10 <= H_15) && (((E_15 / 10) / 10) <= 0)
               && (!((E_15 / 10) <= 0)) && (!(E_15 <= 0))
               && (A_15 == (I_15 + 4))))
              abort ();
          INV_MAIN_42_0 = D_15;
          INV_MAIN_42_1 = C_15;
          INV_MAIN_42_2 = G_15;
          INV_MAIN_42_3 = B_15;
          INV_MAIN_42_4 = A_15;
          INV_MAIN_42_5 = J_15;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 16:
          A_16 = __VERIFIER_nondet_int ();
          v_9_16 = __VERIFIER_nondet_int ();
          B_16 = __VERIFIER_nondet_int ();
          v_8_16 = __VERIFIER_nondet_int ();
          C_16 = INV_MAIN_42_0;
          D_16 = INV_MAIN_42_1;
          E_16 = INV_MAIN_42_2;
          F_16 = INV_MAIN_42_3;
          G_16 = INV_MAIN_42_4;
          H_16 = INV_MAIN_42_5;
          if (!
              ((B_16 == (C_16 / 10)) && (!(E_16 == 0)) && (!(10 <= F_16))
               && ((C_16 / 10) <= 0) && (!(C_16 <= 0)) && (A_16 == (D_16 + 1))
               && (0 == v_8_16) && (v_9_16 == G_16)))
              abort ();
          INV_MAIN_42_0 = B_16;
          INV_MAIN_42_1 = A_16;
          INV_MAIN_42_2 = v_8_16;
          INV_MAIN_42_3 = F_16;
          INV_MAIN_42_4 = G_16;
          INV_MAIN_42_5 = v_9_16;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 17:
          A_17 = __VERIFIER_nondet_int ();
          v_9_17 = __VERIFIER_nondet_int ();
          B_17 = __VERIFIER_nondet_int ();
          C_17 = __VERIFIER_nondet_int ();
          D_17 = INV_MAIN_42_0;
          E_17 = INV_MAIN_42_1;
          F_17 = INV_MAIN_42_2;
          G_17 = INV_MAIN_42_3;
          H_17 = INV_MAIN_42_4;
          I_17 = INV_MAIN_42_5;
          if (!
              ((B_17 == (E_17 + 1)) && (C_17 == (D_17 / 10)) && (!(F_17 == 0))
               && (!(100 <= G_17)) && (10 <= G_17) && ((D_17 / 10) <= 0)
               && (!(D_17 <= 0)) && (A_17 == (H_17 + 1)) && (0 == v_9_17)))
              abort ();
          INV_MAIN_42_0 = C_17;
          INV_MAIN_42_1 = B_17;
          INV_MAIN_42_2 = v_9_17;
          INV_MAIN_42_3 = G_17;
          INV_MAIN_42_4 = H_17;
          INV_MAIN_42_5 = A_17;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 18:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 19:
          A_19 = __VERIFIER_nondet_int ();
          v_9_19 = __VERIFIER_nondet_int ();
          B_19 = __VERIFIER_nondet_int ();
          C_19 = __VERIFIER_nondet_int ();
          D_19 = INV_MAIN_42_0;
          E_19 = INV_MAIN_42_1;
          F_19 = INV_MAIN_42_2;
          G_19 = INV_MAIN_42_3;
          H_19 = INV_MAIN_42_4;
          I_19 = INV_MAIN_42_5;
          if (!
              ((B_19 == (E_19 + 1)) && (C_19 == (D_19 / 10)) && (!(F_19 == 0))
               && (!(10000 <= G_19)) && (1000 <= G_19) && (100 <= G_19)
               && (10 <= G_19) && ((D_19 / 10) <= 0) && (!(D_19 <= 0))
               && (A_19 == (H_19 + 3)) && (0 == v_9_19)))
              abort ();
          INV_MAIN_42_0 = C_19;
          INV_MAIN_42_1 = B_19;
          INV_MAIN_42_2 = v_9_19;
          INV_MAIN_42_3 = G_19;
          INV_MAIN_42_4 = H_19;
          INV_MAIN_42_5 = A_19;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 20:
          A_20 = __VERIFIER_nondet_int ();
          B_20 = __VERIFIER_nondet_int ();
          C_20 = __VERIFIER_nondet_int ();
          D_20 = __VERIFIER_nondet_int ();
          E_20 = INV_MAIN_42_0;
          F_20 = INV_MAIN_42_1;
          G_20 = INV_MAIN_42_2;
          H_20 = INV_MAIN_42_3;
          I_20 = INV_MAIN_42_4;
          J_20 = INV_MAIN_42_5;
          if (!
              ((B_20 == (H_20 / 10000)) && (C_20 == (F_20 + 1))
               && (D_20 == (E_20 / 10)) && (!(G_20 == 0)) && (10000 <= H_20)
               && (1000 <= H_20) && (100 <= H_20) && (10 <= H_20)
               && ((E_20 / 10) <= 0) && (!(E_20 <= 0))
               && (A_20 == (I_20 + 4))))
              abort ();
          INV_MAIN_42_0 = D_20;
          INV_MAIN_42_1 = C_20;
          INV_MAIN_42_2 = G_20;
          INV_MAIN_42_3 = B_20;
          INV_MAIN_42_4 = A_20;
          INV_MAIN_42_5 = J_20;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 21:
          A_21 = __VERIFIER_nondet_int ();
          B_21 = __VERIFIER_nondet_int ();
          C_21 = INV_MAIN_42_0;
          D_21 = INV_MAIN_42_1;
          E_21 = INV_MAIN_42_2;
          F_21 = INV_MAIN_42_3;
          G_21 = INV_MAIN_42_4;
          H_21 = INV_MAIN_42_5;
          if (!
              ((B_21 == ((((C_21 / 10) / 10) / 10) / 10))
               && (!((((C_21 / 10) / 10) / 10) <= 0))
               && (!(((C_21 / 10) / 10) <= 0)) && (!((C_21 / 10) <= 0))
               && (!(C_21 <= 0)) && ((!(10 <= F_21)) || (!(100 <= F_21))
                                     || (!(1000 <= F_21)) || (10000 <= F_21)
                                     || (E_21 == 0)) && ((!(10 <= F_21))
                                                         || (!(100 <= F_21))
                                                         || (!(1000 <= F_21))
                                                         || (!(10000 <= F_21))
                                                         || (E_21 == 0))
               && ((!(10 <= F_21)) || (!(100 <= F_21)) || (1000 <= F_21)
                   || (E_21 == 0)) && ((!(10 <= F_21)) || (100 <= F_21)
                                       || (E_21 == 0)) && ((10 <= F_21)
                                                           || (E_21 == 0))
               && (A_21 == (D_21 + 4))))
              abort ();
          INV_MAIN_42_0 = B_21;
          INV_MAIN_42_1 = A_21;
          INV_MAIN_42_2 = E_21;
          INV_MAIN_42_3 = F_21;
          INV_MAIN_42_4 = G_21;
          INV_MAIN_42_5 = H_21;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 22:
          A_22 = __VERIFIER_nondet_int ();
          B_22 = __VERIFIER_nondet_int ();
          C_22 = INV_MAIN_42_0;
          D_22 = INV_MAIN_42_1;
          E_22 = INV_MAIN_42_2;
          F_22 = INV_MAIN_42_3;
          G_22 = INV_MAIN_42_4;
          H_22 = INV_MAIN_42_5;
          if (!
              ((B_22 == (((C_22 / 10) / 10) / 10))
               && ((((C_22 / 10) / 10) / 10) <= 0)
               && (!(((C_22 / 10) / 10) <= 0)) && (!((C_22 / 10) <= 0))
               && (!(C_22 <= 0)) && ((!(10 <= F_22)) || (!(100 <= F_22))
                                     || (!(1000 <= F_22)) || (10000 <= F_22)
                                     || (E_22 == 0)) && ((!(10 <= F_22))
                                                         || (!(100 <= F_22))
                                                         || (!(1000 <= F_22))
                                                         || (!(10000 <= F_22))
                                                         || (E_22 == 0))
               && ((!(10 <= F_22)) || (!(100 <= F_22)) || (1000 <= F_22)
                   || (E_22 == 0)) && ((!(10 <= F_22)) || (100 <= F_22)
                                       || (E_22 == 0)) && ((10 <= F_22)
                                                           || (E_22 == 0))
               && (A_22 == (D_22 + 3))))
              abort ();
          INV_MAIN_42_0 = B_22;
          INV_MAIN_42_1 = A_22;
          INV_MAIN_42_2 = E_22;
          INV_MAIN_42_3 = F_22;
          INV_MAIN_42_4 = G_22;
          INV_MAIN_42_5 = H_22;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 23:
          A_23 = __VERIFIER_nondet_int ();
          B_23 = __VERIFIER_nondet_int ();
          C_23 = INV_MAIN_42_0;
          D_23 = INV_MAIN_42_1;
          E_23 = INV_MAIN_42_2;
          F_23 = INV_MAIN_42_3;
          G_23 = INV_MAIN_42_4;
          H_23 = INV_MAIN_42_5;
          if (!
              ((B_23 == ((C_23 / 10) / 10)) && (((C_23 / 10) / 10) <= 0)
               && (!((C_23 / 10) <= 0)) && (!(C_23 <= 0)) && ((!(10 <= F_23))
                                                              ||
                                                              (!(100 <= F_23))
                                                              ||
                                                              (!(1000 <=
                                                                 F_23))
                                                              || (10000 <=
                                                                  F_23)
                                                              || (E_23 == 0))
               && ((!(10 <= F_23)) || (!(100 <= F_23)) || (!(1000 <= F_23))
                   || (!(10000 <= F_23)) || (E_23 == 0)) && ((!(10 <= F_23))
                                                             ||
                                                             (!(100 <= F_23))
                                                             || (1000 <= F_23)
                                                             || (E_23 == 0))
               && ((!(10 <= F_23)) || (100 <= F_23) || (E_23 == 0))
               && ((10 <= F_23) || (E_23 == 0)) && (A_23 == (D_23 + 2))))
              abort ();
          INV_MAIN_42_0 = B_23;
          INV_MAIN_42_1 = A_23;
          INV_MAIN_42_2 = E_23;
          INV_MAIN_42_3 = F_23;
          INV_MAIN_42_4 = G_23;
          INV_MAIN_42_5 = H_23;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 24:
          A_24 = __VERIFIER_nondet_int ();
          B_24 = __VERIFIER_nondet_int ();
          C_24 = INV_MAIN_42_0;
          D_24 = INV_MAIN_42_1;
          E_24 = INV_MAIN_42_2;
          F_24 = INV_MAIN_42_3;
          G_24 = INV_MAIN_42_4;
          H_24 = INV_MAIN_42_5;
          if (!
              ((B_24 == (C_24 / 10)) && ((C_24 / 10) <= 0) && (!(C_24 <= 0))
               && ((!(10 <= F_24)) || (!(100 <= F_24)) || (!(1000 <= F_24))
                   || (10000 <= F_24) || (E_24 == 0)) && ((!(10 <= F_24))
                                                          || (!(100 <= F_24))
                                                          || (!(1000 <= F_24))
                                                          ||
                                                          (!(10000 <= F_24))
                                                          || (E_24 == 0))
               && ((!(10 <= F_24)) || (!(100 <= F_24)) || (1000 <= F_24)
                   || (E_24 == 0)) && ((!(10 <= F_24)) || (100 <= F_24)
                                       || (E_24 == 0)) && ((10 <= F_24)
                                                           || (E_24 == 0))
               && (A_24 == (D_24 + 1))))
              abort ();
          INV_MAIN_42_0 = B_24;
          INV_MAIN_42_1 = A_24;
          INV_MAIN_42_2 = E_24;
          INV_MAIN_42_3 = F_24;
          INV_MAIN_42_4 = G_24;
          INV_MAIN_42_5 = H_24;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 25:
          A_26 = __VERIFIER_nondet_int ();
          v_7_26 = __VERIFIER_nondet_int ();
          B_26 = INV_MAIN_42_0;
          C_26 = INV_MAIN_42_1;
          D_26 = INV_MAIN_42_2;
          E_26 = INV_MAIN_42_3;
          F_26 = INV_MAIN_42_4;
          G_26 = INV_MAIN_42_5;
          if (!
              ((!(D_26 == 0)) && (!(100 <= E_26)) && (10 <= E_26)
               && ((B_26 <= 0) || ((B_26 / 10) <= 0)
                   || (((B_26 / 10) / 10) <= 0)
                   || ((((B_26 / 10) / 10) / 10) <= 0))
               && ((!((((B_26 / 10) / 10) / 10) <= 0))
                   || (((B_26 / 10) / 10) <= 0) || ((B_26 / 10) <= 0)
                   || (B_26 <= 0)) && ((!(((B_26 / 10) / 10) <= 0))
                                       || ((B_26 / 10) <= 0) || (B_26 <= 0))
               && ((!((B_26 / 10) <= 0)) || (B_26 <= 0))
               && (A_26 == (F_26 + 1)) && (0 == v_7_26)))
              abort ();
          INV_MAIN_42_0 = B_26;
          INV_MAIN_42_1 = C_26;
          INV_MAIN_42_2 = v_7_26;
          INV_MAIN_42_3 = E_26;
          INV_MAIN_42_4 = F_26;
          INV_MAIN_42_5 = A_26;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 26:
          A_27 = __VERIFIER_nondet_int ();
          v_7_27 = __VERIFIER_nondet_int ();
          B_27 = INV_MAIN_42_0;
          C_27 = INV_MAIN_42_1;
          D_27 = INV_MAIN_42_2;
          E_27 = INV_MAIN_42_3;
          F_27 = INV_MAIN_42_4;
          G_27 = INV_MAIN_42_5;
          if (!
              ((!(D_27 == 0)) && (!(1000 <= E_27)) && (100 <= E_27)
               && (10 <= E_27) && ((B_27 <= 0) || ((B_27 / 10) <= 0)
                                   || (((B_27 / 10) / 10) <= 0)
                                   || ((((B_27 / 10) / 10) / 10) <= 0))
               && ((!((((B_27 / 10) / 10) / 10) <= 0))
                   || (((B_27 / 10) / 10) <= 0) || ((B_27 / 10) <= 0)
                   || (B_27 <= 0)) && ((!(((B_27 / 10) / 10) <= 0))
                                       || ((B_27 / 10) <= 0) || (B_27 <= 0))
               && ((!((B_27 / 10) <= 0)) || (B_27 <= 0))
               && (A_27 == (F_27 + 2)) && (0 == v_7_27)))
              abort ();
          INV_MAIN_42_0 = B_27;
          INV_MAIN_42_1 = C_27;
          INV_MAIN_42_2 = v_7_27;
          INV_MAIN_42_3 = E_27;
          INV_MAIN_42_4 = F_27;
          INV_MAIN_42_5 = A_27;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 27:
          A_28 = __VERIFIER_nondet_int ();
          v_7_28 = __VERIFIER_nondet_int ();
          B_28 = INV_MAIN_42_0;
          C_28 = INV_MAIN_42_1;
          D_28 = INV_MAIN_42_2;
          E_28 = INV_MAIN_42_3;
          F_28 = INV_MAIN_42_4;
          G_28 = INV_MAIN_42_5;
          if (!
              ((!(D_28 == 0)) && (!(10000 <= E_28)) && (1000 <= E_28)
               && (100 <= E_28) && (10 <= E_28) && ((B_28 <= 0)
                                                    || ((B_28 / 10) <= 0)
                                                    || (((B_28 / 10) / 10) <=
                                                        0)
                                                    ||
                                                    ((((B_28 / 10) / 10) /
                                                      10) <= 0))
               && ((!((((B_28 / 10) / 10) / 10) <= 0))
                   || (((B_28 / 10) / 10) <= 0) || ((B_28 / 10) <= 0)
                   || (B_28 <= 0)) && ((!(((B_28 / 10) / 10) <= 0))
                                       || ((B_28 / 10) <= 0) || (B_28 <= 0))
               && ((!((B_28 / 10) <= 0)) || (B_28 <= 0))
               && (A_28 == (F_28 + 3)) && (0 == v_7_28)))
              abort ();
          INV_MAIN_42_0 = B_28;
          INV_MAIN_42_1 = C_28;
          INV_MAIN_42_2 = v_7_28;
          INV_MAIN_42_3 = E_28;
          INV_MAIN_42_4 = F_28;
          INV_MAIN_42_5 = A_28;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 28:
          A_29 = __VERIFIER_nondet_int ();
          B_29 = __VERIFIER_nondet_int ();
          C_29 = INV_MAIN_42_0;
          D_29 = INV_MAIN_42_1;
          E_29 = INV_MAIN_42_2;
          F_29 = INV_MAIN_42_3;
          G_29 = INV_MAIN_42_4;
          H_29 = INV_MAIN_42_5;
          if (!
              ((B_29 == (F_29 / 10000)) && (!(E_29 == 0)) && (10000 <= F_29)
               && (1000 <= F_29) && (100 <= F_29) && (10 <= F_29)
               && ((C_29 <= 0) || ((C_29 / 10) <= 0)
                   || (((C_29 / 10) / 10) <= 0)
                   || ((((C_29 / 10) / 10) / 10) <= 0))
               && ((!((((C_29 / 10) / 10) / 10) <= 0))
                   || (((C_29 / 10) / 10) <= 0) || ((C_29 / 10) <= 0)
                   || (C_29 <= 0)) && ((!(((C_29 / 10) / 10) <= 0))
                                       || ((C_29 / 10) <= 0) || (C_29 <= 0))
               && ((!((C_29 / 10) <= 0)) || (C_29 <= 0))
               && (A_29 == (G_29 + 4))))
              abort ();
          INV_MAIN_42_0 = C_29;
          INV_MAIN_42_1 = D_29;
          INV_MAIN_42_2 = E_29;
          INV_MAIN_42_3 = B_29;
          INV_MAIN_42_4 = A_29;
          INV_MAIN_42_5 = H_29;
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      case 29:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_1:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_3 = __VERIFIER_nondet_int ();
          v_9_3 = __VERIFIER_nondet_int ();
          B_3 = __VERIFIER_nondet_int ();
          C_3 = __VERIFIER_nondet_int ();
          D_3 = INV_MAIN_42_0;
          E_3 = INV_MAIN_42_1;
          F_3 = INV_MAIN_42_2;
          G_3 = INV_MAIN_42_3;
          H_3 = INV_MAIN_42_4;
          I_3 = INV_MAIN_42_5;
          if (!
              ((B_3 == (E_3 + 4)) && (C_3 == ((((D_3 / 10) / 10) / 10) / 10))
               && (!(F_3 == 0)) && (!(1000 <= G_3)) && (100 <= G_3)
               && (10 <= G_3) && (!((((D_3 / 10) / 10) / 10) <= 0))
               && (!(((D_3 / 10) / 10) <= 0)) && (!((D_3 / 10) <= 0))
               && (!(D_3 <= 0)) && (A_3 == (H_3 + 2)) && (0 == v_9_3)))
              abort ();
          INV_MAIN_42_0 = C_3;
          INV_MAIN_42_1 = B_3;
          INV_MAIN_42_2 = v_9_3;
          INV_MAIN_42_3 = G_3;
          INV_MAIN_42_4 = H_3;
          INV_MAIN_42_5 = A_3;
          goto INV_MAIN_42_0;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_2:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_16 = __VERIFIER_nondet_int ();
          v_9_16 = __VERIFIER_nondet_int ();
          B_16 = __VERIFIER_nondet_int ();
          v_8_16 = __VERIFIER_nondet_int ();
          C_16 = INV_MAIN_42_0;
          D_16 = INV_MAIN_42_1;
          E_16 = INV_MAIN_42_2;
          F_16 = INV_MAIN_42_3;
          G_16 = INV_MAIN_42_4;
          H_16 = INV_MAIN_42_5;
          if (!
              ((B_16 == (C_16 / 10)) && (!(E_16 == 0)) && (!(10 <= F_16))
               && ((C_16 / 10) <= 0) && (!(C_16 <= 0)) && (A_16 == (D_16 + 1))
               && (0 == v_8_16) && (v_9_16 == G_16)))
              abort ();
          INV_MAIN_42_0 = B_16;
          INV_MAIN_42_1 = A_16;
          INV_MAIN_42_2 = v_8_16;
          INV_MAIN_42_3 = F_16;
          INV_MAIN_42_4 = G_16;
          INV_MAIN_42_5 = v_9_16;
          goto INV_MAIN_42_1;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_3:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_29 = __VERIFIER_nondet_int ();
          B_29 = __VERIFIER_nondet_int ();
          C_29 = INV_MAIN_42_0;
          D_29 = INV_MAIN_42_1;
          E_29 = INV_MAIN_42_2;
          F_29 = INV_MAIN_42_3;
          G_29 = INV_MAIN_42_4;
          H_29 = INV_MAIN_42_5;
          if (!
              ((B_29 == (F_29 / 10000)) && (!(E_29 == 0)) && (10000 <= F_29)
               && (1000 <= F_29) && (100 <= F_29) && (10 <= F_29)
               && ((C_29 <= 0) || ((C_29 / 10) <= 0)
                   || (((C_29 / 10) / 10) <= 0)
                   || ((((C_29 / 10) / 10) / 10) <= 0))
               && ((!((((C_29 / 10) / 10) / 10) <= 0))
                   || (((C_29 / 10) / 10) <= 0) || ((C_29 / 10) <= 0)
                   || (C_29 <= 0)) && ((!(((C_29 / 10) / 10) <= 0))
                                       || ((C_29 / 10) <= 0) || (C_29 <= 0))
               && ((!((C_29 / 10) <= 0)) || (C_29 <= 0))
               && (A_29 == (G_29 + 4))))
              abort ();
          INV_MAIN_42_0 = C_29;
          INV_MAIN_42_1 = D_29;
          INV_MAIN_42_2 = E_29;
          INV_MAIN_42_3 = B_29;
          INV_MAIN_42_4 = A_29;
          INV_MAIN_42_5 = H_29;
          goto INV_MAIN_42_2;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_4:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_27 = __VERIFIER_nondet_int ();
          v_7_27 = __VERIFIER_nondet_int ();
          B_27 = INV_MAIN_42_0;
          C_27 = INV_MAIN_42_1;
          D_27 = INV_MAIN_42_2;
          E_27 = INV_MAIN_42_3;
          F_27 = INV_MAIN_42_4;
          G_27 = INV_MAIN_42_5;
          if (!
              ((!(D_27 == 0)) && (!(1000 <= E_27)) && (100 <= E_27)
               && (10 <= E_27) && ((B_27 <= 0) || ((B_27 / 10) <= 0)
                                   || (((B_27 / 10) / 10) <= 0)
                                   || ((((B_27 / 10) / 10) / 10) <= 0))
               && ((!((((B_27 / 10) / 10) / 10) <= 0))
                   || (((B_27 / 10) / 10) <= 0) || ((B_27 / 10) <= 0)
                   || (B_27 <= 0)) && ((!(((B_27 / 10) / 10) <= 0))
                                       || ((B_27 / 10) <= 0) || (B_27 <= 0))
               && ((!((B_27 / 10) <= 0)) || (B_27 <= 0))
               && (A_27 == (F_27 + 2)) && (0 == v_7_27)))
              abort ();
          INV_MAIN_42_0 = B_27;
          INV_MAIN_42_1 = C_27;
          INV_MAIN_42_2 = v_7_27;
          INV_MAIN_42_3 = E_27;
          INV_MAIN_42_4 = F_27;
          INV_MAIN_42_5 = A_27;
          goto INV_MAIN_42_3;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_5:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_10 = __VERIFIER_nondet_int ();
          B_10 = __VERIFIER_nondet_int ();
          C_10 = __VERIFIER_nondet_int ();
          D_10 = __VERIFIER_nondet_int ();
          E_10 = INV_MAIN_42_0;
          F_10 = INV_MAIN_42_1;
          G_10 = INV_MAIN_42_2;
          H_10 = INV_MAIN_42_3;
          I_10 = INV_MAIN_42_4;
          J_10 = INV_MAIN_42_5;
          if (!
              ((B_10 == (H_10 / 10000)) && (C_10 == (F_10 + 3))
               && (D_10 == (((E_10 / 10) / 10) / 10)) && (!(G_10 == 0))
               && (10000 <= H_10) && (1000 <= H_10) && (100 <= H_10)
               && (10 <= H_10) && ((((E_10 / 10) / 10) / 10) <= 0)
               && (!(((E_10 / 10) / 10) <= 0)) && (!((E_10 / 10) <= 0))
               && (!(E_10 <= 0)) && (A_10 == (I_10 + 4))))
              abort ();
          INV_MAIN_42_0 = D_10;
          INV_MAIN_42_1 = C_10;
          INV_MAIN_42_2 = G_10;
          INV_MAIN_42_3 = B_10;
          INV_MAIN_42_4 = A_10;
          INV_MAIN_42_5 = J_10;
          goto INV_MAIN_42_4;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_6:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_17 = __VERIFIER_nondet_int ();
          v_9_17 = __VERIFIER_nondet_int ();
          B_17 = __VERIFIER_nondet_int ();
          C_17 = __VERIFIER_nondet_int ();
          D_17 = INV_MAIN_42_0;
          E_17 = INV_MAIN_42_1;
          F_17 = INV_MAIN_42_2;
          G_17 = INV_MAIN_42_3;
          H_17 = INV_MAIN_42_4;
          I_17 = INV_MAIN_42_5;
          if (!
              ((B_17 == (E_17 + 1)) && (C_17 == (D_17 / 10)) && (!(F_17 == 0))
               && (!(100 <= G_17)) && (10 <= G_17) && ((D_17 / 10) <= 0)
               && (!(D_17 <= 0)) && (A_17 == (H_17 + 1)) && (0 == v_9_17)))
              abort ();
          INV_MAIN_42_0 = C_17;
          INV_MAIN_42_1 = B_17;
          INV_MAIN_42_2 = v_9_17;
          INV_MAIN_42_3 = G_17;
          INV_MAIN_42_4 = H_17;
          INV_MAIN_42_5 = A_17;
          goto INV_MAIN_42_5;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_7:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_7 = __VERIFIER_nondet_int ();
          v_9_7 = __VERIFIER_nondet_int ();
          B_7 = __VERIFIER_nondet_int ();
          C_7 = __VERIFIER_nondet_int ();
          D_7 = INV_MAIN_42_0;
          E_7 = INV_MAIN_42_1;
          F_7 = INV_MAIN_42_2;
          G_7 = INV_MAIN_42_3;
          H_7 = INV_MAIN_42_4;
          I_7 = INV_MAIN_42_5;
          if (!
              ((B_7 == (E_7 + 3)) && (C_7 == (((D_7 / 10) / 10) / 10))
               && (!(F_7 == 0)) && (!(100 <= G_7)) && (10 <= G_7)
               && ((((D_7 / 10) / 10) / 10) <= 0)
               && (!(((D_7 / 10) / 10) <= 0)) && (!((D_7 / 10) <= 0))
               && (!(D_7 <= 0)) && (A_7 == (H_7 + 1)) && (0 == v_9_7)))
              abort ();
          INV_MAIN_42_0 = C_7;
          INV_MAIN_42_1 = B_7;
          INV_MAIN_42_2 = v_9_7;
          INV_MAIN_42_3 = G_7;
          INV_MAIN_42_4 = H_7;
          INV_MAIN_42_5 = A_7;
          goto INV_MAIN_42_6;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_8:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_26 = __VERIFIER_nondet_int ();
          v_7_26 = __VERIFIER_nondet_int ();
          B_26 = INV_MAIN_42_0;
          C_26 = INV_MAIN_42_1;
          D_26 = INV_MAIN_42_2;
          E_26 = INV_MAIN_42_3;
          F_26 = INV_MAIN_42_4;
          G_26 = INV_MAIN_42_5;
          if (!
              ((!(D_26 == 0)) && (!(100 <= E_26)) && (10 <= E_26)
               && ((B_26 <= 0) || ((B_26 / 10) <= 0)
                   || (((B_26 / 10) / 10) <= 0)
                   || ((((B_26 / 10) / 10) / 10) <= 0))
               && ((!((((B_26 / 10) / 10) / 10) <= 0))
                   || (((B_26 / 10) / 10) <= 0) || ((B_26 / 10) <= 0)
                   || (B_26 <= 0)) && ((!(((B_26 / 10) / 10) <= 0))
                                       || ((B_26 / 10) <= 0) || (B_26 <= 0))
               && ((!((B_26 / 10) <= 0)) || (B_26 <= 0))
               && (A_26 == (F_26 + 1)) && (0 == v_7_26)))
              abort ();
          INV_MAIN_42_0 = B_26;
          INV_MAIN_42_1 = C_26;
          INV_MAIN_42_2 = v_7_26;
          INV_MAIN_42_3 = E_26;
          INV_MAIN_42_4 = F_26;
          INV_MAIN_42_5 = A_26;
          goto INV_MAIN_42_7;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_9:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_2 = __VERIFIER_nondet_int ();
          v_9_2 = __VERIFIER_nondet_int ();
          B_2 = __VERIFIER_nondet_int ();
          C_2 = __VERIFIER_nondet_int ();
          D_2 = INV_MAIN_42_0;
          E_2 = INV_MAIN_42_1;
          F_2 = INV_MAIN_42_2;
          G_2 = INV_MAIN_42_3;
          H_2 = INV_MAIN_42_4;
          I_2 = INV_MAIN_42_5;
          if (!
              ((B_2 == (E_2 + 4)) && (C_2 == ((((D_2 / 10) / 10) / 10) / 10))
               && (!(F_2 == 0)) && (!(100 <= G_2)) && (10 <= G_2)
               && (!((((D_2 / 10) / 10) / 10) <= 0))
               && (!(((D_2 / 10) / 10) <= 0)) && (!((D_2 / 10) <= 0))
               && (!(D_2 <= 0)) && (A_2 == (H_2 + 1)) && (0 == v_9_2)))
              abort ();
          INV_MAIN_42_0 = C_2;
          INV_MAIN_42_1 = B_2;
          INV_MAIN_42_2 = v_9_2;
          INV_MAIN_42_3 = G_2;
          INV_MAIN_42_4 = H_2;
          INV_MAIN_42_5 = A_2;
          goto INV_MAIN_42_8;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_10:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_20 = __VERIFIER_nondet_int ();
          B_20 = __VERIFIER_nondet_int ();
          C_20 = __VERIFIER_nondet_int ();
          D_20 = __VERIFIER_nondet_int ();
          E_20 = INV_MAIN_42_0;
          F_20 = INV_MAIN_42_1;
          G_20 = INV_MAIN_42_2;
          H_20 = INV_MAIN_42_3;
          I_20 = INV_MAIN_42_4;
          J_20 = INV_MAIN_42_5;
          if (!
              ((B_20 == (H_20 / 10000)) && (C_20 == (F_20 + 1))
               && (D_20 == (E_20 / 10)) && (!(G_20 == 0)) && (10000 <= H_20)
               && (1000 <= H_20) && (100 <= H_20) && (10 <= H_20)
               && ((E_20 / 10) <= 0) && (!(E_20 <= 0))
               && (A_20 == (I_20 + 4))))
              abort ();
          INV_MAIN_42_0 = D_20;
          INV_MAIN_42_1 = C_20;
          INV_MAIN_42_2 = G_20;
          INV_MAIN_42_3 = B_20;
          INV_MAIN_42_4 = A_20;
          INV_MAIN_42_5 = J_20;
          goto INV_MAIN_42_9;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_11:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_19 = __VERIFIER_nondet_int ();
          v_9_19 = __VERIFIER_nondet_int ();
          B_19 = __VERIFIER_nondet_int ();
          C_19 = __VERIFIER_nondet_int ();
          D_19 = INV_MAIN_42_0;
          E_19 = INV_MAIN_42_1;
          F_19 = INV_MAIN_42_2;
          G_19 = INV_MAIN_42_3;
          H_19 = INV_MAIN_42_4;
          I_19 = INV_MAIN_42_5;
          if (!
              ((B_19 == (E_19 + 1)) && (C_19 == (D_19 / 10)) && (!(F_19 == 0))
               && (!(10000 <= G_19)) && (1000 <= G_19) && (100 <= G_19)
               && (10 <= G_19) && ((D_19 / 10) <= 0) && (!(D_19 <= 0))
               && (A_19 == (H_19 + 3)) && (0 == v_9_19)))
              abort ();
          INV_MAIN_42_0 = C_19;
          INV_MAIN_42_1 = B_19;
          INV_MAIN_42_2 = v_9_19;
          INV_MAIN_42_3 = G_19;
          INV_MAIN_42_4 = H_19;
          INV_MAIN_42_5 = A_19;
          goto INV_MAIN_42_10;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_12:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_4 = __VERIFIER_nondet_int ();
          v_9_4 = __VERIFIER_nondet_int ();
          B_4 = __VERIFIER_nondet_int ();
          C_4 = __VERIFIER_nondet_int ();
          D_4 = INV_MAIN_42_0;
          E_4 = INV_MAIN_42_1;
          F_4 = INV_MAIN_42_2;
          G_4 = INV_MAIN_42_3;
          H_4 = INV_MAIN_42_4;
          I_4 = INV_MAIN_42_5;
          if (!
              ((B_4 == (E_4 + 4)) && (C_4 == ((((D_4 / 10) / 10) / 10) / 10))
               && (!(F_4 == 0)) && (!(10000 <= G_4)) && (1000 <= G_4)
               && (100 <= G_4) && (10 <= G_4)
               && (!((((D_4 / 10) / 10) / 10) <= 0))
               && (!(((D_4 / 10) / 10) <= 0)) && (!((D_4 / 10) <= 0))
               && (!(D_4 <= 0)) && (A_4 == (H_4 + 3)) && (0 == v_9_4)))
              abort ();
          INV_MAIN_42_0 = C_4;
          INV_MAIN_42_1 = B_4;
          INV_MAIN_42_2 = v_9_4;
          INV_MAIN_42_3 = G_4;
          INV_MAIN_42_4 = H_4;
          INV_MAIN_42_5 = A_4;
          goto INV_MAIN_42_11;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_13:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_28 = __VERIFIER_nondet_int ();
          v_7_28 = __VERIFIER_nondet_int ();
          B_28 = INV_MAIN_42_0;
          C_28 = INV_MAIN_42_1;
          D_28 = INV_MAIN_42_2;
          E_28 = INV_MAIN_42_3;
          F_28 = INV_MAIN_42_4;
          G_28 = INV_MAIN_42_5;
          if (!
              ((!(D_28 == 0)) && (!(10000 <= E_28)) && (1000 <= E_28)
               && (100 <= E_28) && (10 <= E_28) && ((B_28 <= 0)
                                                    || ((B_28 / 10) <= 0)
                                                    || (((B_28 / 10) / 10) <=
                                                        0)
                                                    ||
                                                    ((((B_28 / 10) / 10) /
                                                      10) <= 0))
               && ((!((((B_28 / 10) / 10) / 10) <= 0))
                   || (((B_28 / 10) / 10) <= 0) || ((B_28 / 10) <= 0)
                   || (B_28 <= 0)) && ((!(((B_28 / 10) / 10) <= 0))
                                       || ((B_28 / 10) <= 0) || (B_28 <= 0))
               && ((!((B_28 / 10) <= 0)) || (B_28 <= 0))
               && (A_28 == (F_28 + 3)) && (0 == v_7_28)))
              abort ();
          INV_MAIN_42_0 = B_28;
          INV_MAIN_42_1 = C_28;
          INV_MAIN_42_2 = v_7_28;
          INV_MAIN_42_3 = E_28;
          INV_MAIN_42_4 = F_28;
          INV_MAIN_42_5 = A_28;
          goto INV_MAIN_42_12;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_14:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_11 = __VERIFIER_nondet_int ();
          v_9_11 = __VERIFIER_nondet_int ();
          B_11 = __VERIFIER_nondet_int ();
          v_8_11 = __VERIFIER_nondet_int ();
          C_11 = INV_MAIN_42_0;
          D_11 = INV_MAIN_42_1;
          E_11 = INV_MAIN_42_2;
          F_11 = INV_MAIN_42_3;
          G_11 = INV_MAIN_42_4;
          H_11 = INV_MAIN_42_5;
          if (!
              ((B_11 == ((C_11 / 10) / 10)) && (!(E_11 == 0))
               && (!(10 <= F_11)) && (((C_11 / 10) / 10) <= 0)
               && (!((C_11 / 10) <= 0)) && (!(C_11 <= 0))
               && (A_11 == (D_11 + 2)) && (0 == v_8_11) && (v_9_11 == G_11)))
              abort ();
          INV_MAIN_42_0 = B_11;
          INV_MAIN_42_1 = A_11;
          INV_MAIN_42_2 = v_8_11;
          INV_MAIN_42_3 = F_11;
          INV_MAIN_42_4 = G_11;
          INV_MAIN_42_5 = v_9_11;
          goto INV_MAIN_42_13;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_15:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_1 = __VERIFIER_nondet_int ();
          v_9_1 = __VERIFIER_nondet_int ();
          B_1 = __VERIFIER_nondet_int ();
          v_8_1 = __VERIFIER_nondet_int ();
          C_1 = INV_MAIN_42_0;
          D_1 = INV_MAIN_42_1;
          E_1 = INV_MAIN_42_2;
          F_1 = INV_MAIN_42_3;
          G_1 = INV_MAIN_42_4;
          H_1 = INV_MAIN_42_5;
          if (!
              ((B_1 == ((((C_1 / 10) / 10) / 10) / 10)) && (!(E_1 == 0))
               && (!(10 <= F_1)) && (!((((C_1 / 10) / 10) / 10) <= 0))
               && (!(((C_1 / 10) / 10) <= 0)) && (!((C_1 / 10) <= 0))
               && (!(C_1 <= 0)) && (A_1 == (D_1 + 4)) && (0 == v_8_1)
               && (v_9_1 == G_1)))
              abort ();
          INV_MAIN_42_0 = B_1;
          INV_MAIN_42_1 = A_1;
          INV_MAIN_42_2 = v_8_1;
          INV_MAIN_42_3 = F_1;
          INV_MAIN_42_4 = G_1;
          INV_MAIN_42_5 = v_9_1;
          goto INV_MAIN_42_14;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_16:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_23 = __VERIFIER_nondet_int ();
          B_23 = __VERIFIER_nondet_int ();
          C_23 = INV_MAIN_42_0;
          D_23 = INV_MAIN_42_1;
          E_23 = INV_MAIN_42_2;
          F_23 = INV_MAIN_42_3;
          G_23 = INV_MAIN_42_4;
          H_23 = INV_MAIN_42_5;
          if (!
              ((B_23 == ((C_23 / 10) / 10)) && (((C_23 / 10) / 10) <= 0)
               && (!((C_23 / 10) <= 0)) && (!(C_23 <= 0)) && ((!(10 <= F_23))
                                                              ||
                                                              (!(100 <= F_23))
                                                              ||
                                                              (!(1000 <=
                                                                 F_23))
                                                              || (10000 <=
                                                                  F_23)
                                                              || (E_23 == 0))
               && ((!(10 <= F_23)) || (!(100 <= F_23)) || (!(1000 <= F_23))
                   || (!(10000 <= F_23)) || (E_23 == 0)) && ((!(10 <= F_23))
                                                             ||
                                                             (!(100 <= F_23))
                                                             || (1000 <= F_23)
                                                             || (E_23 == 0))
               && ((!(10 <= F_23)) || (100 <= F_23) || (E_23 == 0))
               && ((10 <= F_23) || (E_23 == 0)) && (A_23 == (D_23 + 2))))
              abort ();
          INV_MAIN_42_0 = B_23;
          INV_MAIN_42_1 = A_23;
          INV_MAIN_42_2 = E_23;
          INV_MAIN_42_3 = F_23;
          INV_MAIN_42_4 = G_23;
          INV_MAIN_42_5 = H_23;
          goto INV_MAIN_42_15;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_17:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_13 = __VERIFIER_nondet_int ();
          v_9_13 = __VERIFIER_nondet_int ();
          B_13 = __VERIFIER_nondet_int ();
          C_13 = __VERIFIER_nondet_int ();
          D_13 = INV_MAIN_42_0;
          E_13 = INV_MAIN_42_1;
          F_13 = INV_MAIN_42_2;
          G_13 = INV_MAIN_42_3;
          H_13 = INV_MAIN_42_4;
          I_13 = INV_MAIN_42_5;
          if (!
              ((B_13 == (E_13 + 2)) && (C_13 == ((D_13 / 10) / 10))
               && (!(F_13 == 0)) && (!(1000 <= G_13)) && (100 <= G_13)
               && (10 <= G_13) && (((D_13 / 10) / 10) <= 0)
               && (!((D_13 / 10) <= 0)) && (!(D_13 <= 0))
               && (A_13 == (H_13 + 2)) && (0 == v_9_13)))
              abort ();
          INV_MAIN_42_0 = C_13;
          INV_MAIN_42_1 = B_13;
          INV_MAIN_42_2 = v_9_13;
          INV_MAIN_42_3 = G_13;
          INV_MAIN_42_4 = H_13;
          INV_MAIN_42_5 = A_13;
          goto INV_MAIN_42_16;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_18:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_5 = __VERIFIER_nondet_int ();
          B_5 = __VERIFIER_nondet_int ();
          C_5 = __VERIFIER_nondet_int ();
          D_5 = __VERIFIER_nondet_int ();
          E_5 = INV_MAIN_42_0;
          F_5 = INV_MAIN_42_1;
          G_5 = INV_MAIN_42_2;
          H_5 = INV_MAIN_42_3;
          I_5 = INV_MAIN_42_4;
          J_5 = INV_MAIN_42_5;
          if (!
              ((B_5 == (H_5 / 10000)) && (C_5 == (F_5 + 4))
               && (D_5 == ((((E_5 / 10) / 10) / 10) / 10)) && (!(G_5 == 0))
               && (10000 <= H_5) && (1000 <= H_5) && (100 <= H_5)
               && (10 <= H_5) && (!((((E_5 / 10) / 10) / 10) <= 0))
               && (!(((E_5 / 10) / 10) <= 0)) && (!((E_5 / 10) <= 0))
               && (!(E_5 <= 0)) && (A_5 == (I_5 + 4))))
              abort ();
          INV_MAIN_42_0 = D_5;
          INV_MAIN_42_1 = C_5;
          INV_MAIN_42_2 = G_5;
          INV_MAIN_42_3 = B_5;
          INV_MAIN_42_4 = A_5;
          INV_MAIN_42_5 = J_5;
          goto INV_MAIN_42_17;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_19:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_24 = __VERIFIER_nondet_int ();
          B_24 = __VERIFIER_nondet_int ();
          C_24 = INV_MAIN_42_0;
          D_24 = INV_MAIN_42_1;
          E_24 = INV_MAIN_42_2;
          F_24 = INV_MAIN_42_3;
          G_24 = INV_MAIN_42_4;
          H_24 = INV_MAIN_42_5;
          if (!
              ((B_24 == (C_24 / 10)) && ((C_24 / 10) <= 0) && (!(C_24 <= 0))
               && ((!(10 <= F_24)) || (!(100 <= F_24)) || (!(1000 <= F_24))
                   || (10000 <= F_24) || (E_24 == 0)) && ((!(10 <= F_24))
                                                          || (!(100 <= F_24))
                                                          || (!(1000 <= F_24))
                                                          ||
                                                          (!(10000 <= F_24))
                                                          || (E_24 == 0))
               && ((!(10 <= F_24)) || (!(100 <= F_24)) || (1000 <= F_24)
                   || (E_24 == 0)) && ((!(10 <= F_24)) || (100 <= F_24)
                                       || (E_24 == 0)) && ((10 <= F_24)
                                                           || (E_24 == 0))
               && (A_24 == (D_24 + 1))))
              abort ();
          INV_MAIN_42_0 = B_24;
          INV_MAIN_42_1 = A_24;
          INV_MAIN_42_2 = E_24;
          INV_MAIN_42_3 = F_24;
          INV_MAIN_42_4 = G_24;
          INV_MAIN_42_5 = H_24;
          goto INV_MAIN_42_18;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_20:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_14 = __VERIFIER_nondet_int ();
          v_9_14 = __VERIFIER_nondet_int ();
          B_14 = __VERIFIER_nondet_int ();
          C_14 = __VERIFIER_nondet_int ();
          D_14 = INV_MAIN_42_0;
          E_14 = INV_MAIN_42_1;
          F_14 = INV_MAIN_42_2;
          G_14 = INV_MAIN_42_3;
          H_14 = INV_MAIN_42_4;
          I_14 = INV_MAIN_42_5;
          if (!
              ((B_14 == (E_14 + 2)) && (C_14 == ((D_14 / 10) / 10))
               && (!(F_14 == 0)) && (!(10000 <= G_14)) && (1000 <= G_14)
               && (100 <= G_14) && (10 <= G_14) && (((D_14 / 10) / 10) <= 0)
               && (!((D_14 / 10) <= 0)) && (!(D_14 <= 0))
               && (A_14 == (H_14 + 3)) && (0 == v_9_14)))
              abort ();
          INV_MAIN_42_0 = C_14;
          INV_MAIN_42_1 = B_14;
          INV_MAIN_42_2 = v_9_14;
          INV_MAIN_42_3 = G_14;
          INV_MAIN_42_4 = H_14;
          INV_MAIN_42_5 = A_14;
          goto INV_MAIN_42_19;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_21:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_22 = __VERIFIER_nondet_int ();
          B_22 = __VERIFIER_nondet_int ();
          C_22 = INV_MAIN_42_0;
          D_22 = INV_MAIN_42_1;
          E_22 = INV_MAIN_42_2;
          F_22 = INV_MAIN_42_3;
          G_22 = INV_MAIN_42_4;
          H_22 = INV_MAIN_42_5;
          if (!
              ((B_22 == (((C_22 / 10) / 10) / 10))
               && ((((C_22 / 10) / 10) / 10) <= 0)
               && (!(((C_22 / 10) / 10) <= 0)) && (!((C_22 / 10) <= 0))
               && (!(C_22 <= 0)) && ((!(10 <= F_22)) || (!(100 <= F_22))
                                     || (!(1000 <= F_22)) || (10000 <= F_22)
                                     || (E_22 == 0)) && ((!(10 <= F_22))
                                                         || (!(100 <= F_22))
                                                         || (!(1000 <= F_22))
                                                         || (!(10000 <= F_22))
                                                         || (E_22 == 0))
               && ((!(10 <= F_22)) || (!(100 <= F_22)) || (1000 <= F_22)
                   || (E_22 == 0)) && ((!(10 <= F_22)) || (100 <= F_22)
                                       || (E_22 == 0)) && ((10 <= F_22)
                                                           || (E_22 == 0))
               && (A_22 == (D_22 + 3))))
              abort ();
          INV_MAIN_42_0 = B_22;
          INV_MAIN_42_1 = A_22;
          INV_MAIN_42_2 = E_22;
          INV_MAIN_42_3 = F_22;
          INV_MAIN_42_4 = G_22;
          INV_MAIN_42_5 = H_22;
          goto INV_MAIN_42_20;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_22:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_12 = __VERIFIER_nondet_int ();
          v_9_12 = __VERIFIER_nondet_int ();
          B_12 = __VERIFIER_nondet_int ();
          C_12 = __VERIFIER_nondet_int ();
          D_12 = INV_MAIN_42_0;
          E_12 = INV_MAIN_42_1;
          F_12 = INV_MAIN_42_2;
          G_12 = INV_MAIN_42_3;
          H_12 = INV_MAIN_42_4;
          I_12 = INV_MAIN_42_5;
          if (!
              ((B_12 == (E_12 + 2)) && (C_12 == ((D_12 / 10) / 10))
               && (!(F_12 == 0)) && (!(100 <= G_12)) && (10 <= G_12)
               && (((D_12 / 10) / 10) <= 0) && (!((D_12 / 10) <= 0))
               && (!(D_12 <= 0)) && (A_12 == (H_12 + 1)) && (0 == v_9_12)))
              abort ();
          INV_MAIN_42_0 = C_12;
          INV_MAIN_42_1 = B_12;
          INV_MAIN_42_2 = v_9_12;
          INV_MAIN_42_3 = G_12;
          INV_MAIN_42_4 = H_12;
          INV_MAIN_42_5 = A_12;
          goto INV_MAIN_42_21;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_23:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_9 = __VERIFIER_nondet_int ();
          v_9_9 = __VERIFIER_nondet_int ();
          B_9 = __VERIFIER_nondet_int ();
          C_9 = __VERIFIER_nondet_int ();
          D_9 = INV_MAIN_42_0;
          E_9 = INV_MAIN_42_1;
          F_9 = INV_MAIN_42_2;
          G_9 = INV_MAIN_42_3;
          H_9 = INV_MAIN_42_4;
          I_9 = INV_MAIN_42_5;
          if (!
              ((B_9 == (E_9 + 3)) && (C_9 == (((D_9 / 10) / 10) / 10))
               && (!(F_9 == 0)) && (!(10000 <= G_9)) && (1000 <= G_9)
               && (100 <= G_9) && (10 <= G_9)
               && ((((D_9 / 10) / 10) / 10) <= 0)
               && (!(((D_9 / 10) / 10) <= 0)) && (!((D_9 / 10) <= 0))
               && (!(D_9 <= 0)) && (A_9 == (H_9 + 3)) && (0 == v_9_9)))
              abort ();
          INV_MAIN_42_0 = C_9;
          INV_MAIN_42_1 = B_9;
          INV_MAIN_42_2 = v_9_9;
          INV_MAIN_42_3 = G_9;
          INV_MAIN_42_4 = H_9;
          INV_MAIN_42_5 = A_9;
          goto INV_MAIN_42_22;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_24:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_6 = __VERIFIER_nondet_int ();
          v_9_6 = __VERIFIER_nondet_int ();
          B_6 = __VERIFIER_nondet_int ();
          v_8_6 = __VERIFIER_nondet_int ();
          C_6 = INV_MAIN_42_0;
          D_6 = INV_MAIN_42_1;
          E_6 = INV_MAIN_42_2;
          F_6 = INV_MAIN_42_3;
          G_6 = INV_MAIN_42_4;
          H_6 = INV_MAIN_42_5;
          if (!
              ((B_6 == (((C_6 / 10) / 10) / 10)) && (!(E_6 == 0))
               && (!(10 <= F_6)) && ((((C_6 / 10) / 10) / 10) <= 0)
               && (!(((C_6 / 10) / 10) <= 0)) && (!((C_6 / 10) <= 0))
               && (!(C_6 <= 0)) && (A_6 == (D_6 + 3)) && (0 == v_8_6)
               && (v_9_6 == G_6)))
              abort ();
          INV_MAIN_42_0 = B_6;
          INV_MAIN_42_1 = A_6;
          INV_MAIN_42_2 = v_8_6;
          INV_MAIN_42_3 = F_6;
          INV_MAIN_42_4 = G_6;
          INV_MAIN_42_5 = v_9_6;
          goto INV_MAIN_42_23;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_25:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_15 = __VERIFIER_nondet_int ();
          B_15 = __VERIFIER_nondet_int ();
          C_15 = __VERIFIER_nondet_int ();
          D_15 = __VERIFIER_nondet_int ();
          E_15 = INV_MAIN_42_0;
          F_15 = INV_MAIN_42_1;
          G_15 = INV_MAIN_42_2;
          H_15 = INV_MAIN_42_3;
          I_15 = INV_MAIN_42_4;
          J_15 = INV_MAIN_42_5;
          if (!
              ((B_15 == (H_15 / 10000)) && (C_15 == (F_15 + 2))
               && (D_15 == ((E_15 / 10) / 10)) && (!(G_15 == 0))
               && (10000 <= H_15) && (1000 <= H_15) && (100 <= H_15)
               && (10 <= H_15) && (((E_15 / 10) / 10) <= 0)
               && (!((E_15 / 10) <= 0)) && (!(E_15 <= 0))
               && (A_15 == (I_15 + 4))))
              abort ();
          INV_MAIN_42_0 = D_15;
          INV_MAIN_42_1 = C_15;
          INV_MAIN_42_2 = G_15;
          INV_MAIN_42_3 = B_15;
          INV_MAIN_42_4 = A_15;
          INV_MAIN_42_5 = J_15;
          goto INV_MAIN_42_24;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_26:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          v_6_25 = __VERIFIER_nondet_int ();
          v_7_25 = __VERIFIER_nondet_int ();
          A_25 = INV_MAIN_42_0;
          B_25 = INV_MAIN_42_1;
          C_25 = INV_MAIN_42_2;
          D_25 = INV_MAIN_42_3;
          E_25 = INV_MAIN_42_4;
          F_25 = INV_MAIN_42_5;
          if (!
              ((!(10 <= D_25))
               && ((A_25 <= 0) || ((A_25 / 10) <= 0)
                   || (((A_25 / 10) / 10) <= 0)
                   || ((((A_25 / 10) / 10) / 10) <= 0))
               && ((!((((A_25 / 10) / 10) / 10) <= 0))
                   || (((A_25 / 10) / 10) <= 0) || ((A_25 / 10) <= 0)
                   || (A_25 <= 0)) && ((!(((A_25 / 10) / 10) <= 0))
                                       || ((A_25 / 10) <= 0) || (A_25 <= 0))
               && ((!((A_25 / 10) <= 0)) || (A_25 <= 0)) && (!(C_25 == 0))
               && (0 == v_6_25) && (v_7_25 == E_25)))
              abort ();
          INV_MAIN_42_0 = A_25;
          INV_MAIN_42_1 = B_25;
          INV_MAIN_42_2 = v_6_25;
          INV_MAIN_42_3 = D_25;
          INV_MAIN_42_4 = E_25;
          INV_MAIN_42_5 = v_7_25;
          goto INV_MAIN_42_25;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_27:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_21 = __VERIFIER_nondet_int ();
          B_21 = __VERIFIER_nondet_int ();
          C_21 = INV_MAIN_42_0;
          D_21 = INV_MAIN_42_1;
          E_21 = INV_MAIN_42_2;
          F_21 = INV_MAIN_42_3;
          G_21 = INV_MAIN_42_4;
          H_21 = INV_MAIN_42_5;
          if (!
              ((B_21 == ((((C_21 / 10) / 10) / 10) / 10))
               && (!((((C_21 / 10) / 10) / 10) <= 0))
               && (!(((C_21 / 10) / 10) <= 0)) && (!((C_21 / 10) <= 0))
               && (!(C_21 <= 0)) && ((!(10 <= F_21)) || (!(100 <= F_21))
                                     || (!(1000 <= F_21)) || (10000 <= F_21)
                                     || (E_21 == 0)) && ((!(10 <= F_21))
                                                         || (!(100 <= F_21))
                                                         || (!(1000 <= F_21))
                                                         || (!(10000 <= F_21))
                                                         || (E_21 == 0))
               && ((!(10 <= F_21)) || (!(100 <= F_21)) || (1000 <= F_21)
                   || (E_21 == 0)) && ((!(10 <= F_21)) || (100 <= F_21)
                                       || (E_21 == 0)) && ((10 <= F_21)
                                                           || (E_21 == 0))
               && (A_21 == (D_21 + 4))))
              abort ();
          INV_MAIN_42_0 = B_21;
          INV_MAIN_42_1 = A_21;
          INV_MAIN_42_2 = E_21;
          INV_MAIN_42_3 = F_21;
          INV_MAIN_42_4 = G_21;
          INV_MAIN_42_5 = H_21;
          goto INV_MAIN_42_26;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }
  INV_MAIN_42_28:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_8 = __VERIFIER_nondet_int ();
          v_9_8 = __VERIFIER_nondet_int ();
          B_8 = __VERIFIER_nondet_int ();
          C_8 = __VERIFIER_nondet_int ();
          D_8 = INV_MAIN_42_0;
          E_8 = INV_MAIN_42_1;
          F_8 = INV_MAIN_42_2;
          G_8 = INV_MAIN_42_3;
          H_8 = INV_MAIN_42_4;
          I_8 = INV_MAIN_42_5;
          if (!
              ((B_8 == (E_8 + 3)) && (C_8 == (((D_8 / 10) / 10) / 10))
               && (!(F_8 == 0)) && (!(1000 <= G_8)) && (100 <= G_8)
               && (10 <= G_8) && ((((D_8 / 10) / 10) / 10) <= 0)
               && (!(((D_8 / 10) / 10) <= 0)) && (!((D_8 / 10) <= 0))
               && (!(D_8 <= 0)) && (A_8 == (H_8 + 2)) && (0 == v_9_8)))
              abort ();
          INV_MAIN_42_0 = C_8;
          INV_MAIN_42_1 = B_8;
          INV_MAIN_42_2 = v_9_8;
          INV_MAIN_42_3 = G_8;
          INV_MAIN_42_4 = H_8;
          INV_MAIN_42_5 = A_8;
          goto INV_MAIN_42_27;

      case 1:
          A_18 = __VERIFIER_nondet_int ();
          v_9_18 = __VERIFIER_nondet_int ();
          B_18 = __VERIFIER_nondet_int ();
          C_18 = __VERIFIER_nondet_int ();
          D_18 = INV_MAIN_42_0;
          E_18 = INV_MAIN_42_1;
          F_18 = INV_MAIN_42_2;
          G_18 = INV_MAIN_42_3;
          H_18 = INV_MAIN_42_4;
          I_18 = INV_MAIN_42_5;
          if (!
              ((B_18 == (E_18 + 1)) && (C_18 == (D_18 / 10)) && (!(F_18 == 0))
               && (!(1000 <= G_18)) && (100 <= G_18) && (10 <= G_18)
               && ((D_18 / 10) <= 0) && (!(D_18 <= 0)) && (A_18 == (H_18 + 2))
               && (0 == v_9_18)))
              abort ();
          INV_MAIN_42_0 = C_18;
          INV_MAIN_42_1 = B_18;
          INV_MAIN_42_2 = v_9_18;
          INV_MAIN_42_3 = G_18;
          INV_MAIN_42_4 = H_18;
          INV_MAIN_42_5 = A_18;
          goto INV_MAIN_42_28;

      default:
          abort ();
      }

    // return expression

}

