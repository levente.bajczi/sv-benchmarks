// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: tricera-benchmarks/apache-get-tag.i.v+lhb-reducer.c-1.smt2.gz_010.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "apache-get-tag.i.v+lhb-reducer.c-1.smt2.gz_010_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int inv_main148_0;
    int inv_main148_1;
    int inv_main148_2;
    int inv_main148_3;
    int inv_main148_4;
    int inv_main148_5;
    int inv_main148_6;
    int inv_main148_7;
    int inv_main148_8;
    int inv_main148_9;
    int inv_main148_10;
    int inv_main4_0;
    int inv_main4_1;
    int inv_main4_2;
    int inv_main4_3;
    int inv_main4_4;
    int inv_main4_5;
    int inv_main132_0;
    int inv_main132_1;
    int inv_main132_2;
    int inv_main132_3;
    int inv_main132_4;
    int inv_main132_5;
    int inv_main132_6;
    int inv_main132_7;
    int inv_main132_8;
    int inv_main132_9;
    int inv_main132_10;
    int inv_main132_11;
    int inv_main132_12;
    int inv_main132_13;
    int inv_main132_14;
    int inv_main132_15;
    int inv_main132_16;
    int inv_main132_17;
    int inv_main36_0;
    int inv_main36_1;
    int inv_main36_2;
    int inv_main36_3;
    int inv_main36_4;
    int inv_main36_5;
    int inv_main36_6;
    int inv_main36_7;
    int inv_main36_8;
    int inv_main36_9;
    int inv_main36_10;
    int inv_main36_11;
    int inv_main98_0;
    int inv_main98_1;
    int inv_main98_2;
    int inv_main98_3;
    int inv_main98_4;
    int inv_main98_5;
    int inv_main98_6;
    int inv_main98_7;
    int inv_main98_8;
    int inv_main98_9;
    int inv_main98_10;
    int inv_main98_11;
    int inv_main98_12;
    int inv_main98_13;
    int inv_main98_14;
    int inv_main98_15;
    int inv_main98_16;
    int inv_main98_17;
    int inv_main98_18;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    int E_0;
    int F_0;
    int A_3;
    int B_3;
    int C_3;
    int D_3;
    int E_3;
    int F_3;
    int G_3;
    int H_3;
    int I_3;
    int J_3;
    int K_3;
    int L_3;
    int M_3;
    int N_3;
    int O_3;
    int P_3;
    int Q_3;
    int R_3;
    int S_3;
    int T_3;
    int U_3;
    int V_3;
    int W_3;
    int X_3;
    int Y_3;
    int Z_3;
    int A1_3;
    int B1_3;
    int C1_3;
    int D1_3;
    int E1_3;
    int F1_3;
    int G1_3;
    int H1_3;
    int I1_3;
    int J1_3;
    int K1_3;
    int L1_3;
    int M1_3;
    int N1_3;
    int O1_3;
    int P1_3;
    int Q1_3;
    int R1_3;
    int S1_3;
    int T1_3;
    int U1_3;
    int V1_3;
    int W1_3;
    int X1_3;
    int Y1_3;
    int Z1_3;
    int A2_3;
    int B2_3;
    int C2_3;
    int D2_3;
    int E2_3;
    int F2_3;
    int G2_3;
    int H2_3;
    int I2_3;
    int J2_3;
    int A_4;
    int B_4;
    int C_4;
    int D_4;
    int E_4;
    int F_4;
    int G_4;
    int H_4;
    int I_4;
    int J_4;
    int K_4;
    int L_4;
    int M_4;
    int N_4;
    int O_4;
    int P_4;
    int Q_4;
    int R_4;
    int S_4;
    int T_4;
    int U_4;
    int V_4;
    int W_4;
    int X_4;
    int Y_4;
    int Z_4;
    int A1_4;
    int B1_4;
    int C1_4;
    int D1_4;
    int E1_4;
    int F1_4;
    int G1_4;
    int H1_4;
    int I1_4;
    int J1_4;
    int K1_4;
    int L1_4;
    int M1_4;
    int N1_4;
    int O1_4;
    int P1_4;
    int Q1_4;
    int R1_4;
    int S1_4;
    int T1_4;
    int U1_4;
    int v_47_4;
    int A_5;
    int B_5;
    int C_5;
    int D_5;
    int E_5;
    int F_5;
    int G_5;
    int H_5;
    int I_5;
    int J_5;
    int K_5;
    int L_5;
    int M_5;
    int N_5;
    int O_5;
    int P_5;
    int Q_5;
    int R_5;
    int S_5;
    int T_5;
    int U_5;
    int V_5;
    int W_5;
    int X_5;
    int Y_5;
    int Z_5;
    int A1_5;
    int v_27_5;
    int v_28_5;
    int A_10;
    int B_10;
    int C_10;
    int D_10;
    int E_10;
    int F_10;
    int G_10;
    int H_10;
    int I_10;
    int J_10;
    int K_10;
    int L_10;
    int M_10;
    int N_10;
    int O_10;
    int P_10;
    int Q_10;
    int R_10;
    int S_10;
    int T_10;
    int U_10;
    int V_10;
    int W_10;
    int X_10;
    int Y_10;
    int Z_10;
    int A1_10;
    int B1_10;
    int C1_10;
    int D1_10;
    int E1_10;
    int F1_10;
    int G1_10;
    int H1_10;
    int I1_10;
    int J1_10;
    int K1_10;
    int L1_10;
    int M1_10;
    int N1_10;
    int O1_10;
    int P1_10;
    int Q1_10;
    int R1_10;
    int S1_10;
    int T1_10;
    int U1_10;
    int v_47_10;
    int A_11;
    int B_11;
    int C_11;
    int D_11;
    int E_11;
    int F_11;
    int G_11;
    int H_11;
    int I_11;
    int J_11;
    int K_11;
    int L_11;
    int M_11;
    int N_11;
    int O_11;
    int P_11;
    int Q_11;
    int R_11;
    int S_11;
    int A_12;
    int B_12;
    int C_12;
    int D_12;
    int E_12;
    int F_12;
    int G_12;
    int H_12;
    int I_12;
    int J_12;
    int K_12;
    int L_12;
    int M_12;
    int N_12;
    int O_12;
    int P_12;
    int Q_12;
    int R_12;
    int S_12;
    int T_12;
    int A_20;
    int B_20;
    int C_20;
    int D_20;
    int E_20;
    int F_20;
    int G_20;
    int H_20;
    int I_20;
    int J_20;
    int K_20;
    int L_20;
    int M_20;
    int N_20;
    int O_20;
    int P_20;
    int Q_20;
    int R_20;
    int S_20;
    int T_20;
    int U_20;
    int V_20;
    int W_20;
    int X_20;
    int Y_20;
    int Z_20;
    int A1_20;
    int B1_20;
    int C1_20;
    int D1_20;
    int E1_20;
    int F1_20;
    int G1_20;
    int H1_20;
    int I1_20;
    int J1_20;
    int K1_20;
    int L1_20;
    int M1_20;
    int N1_20;
    int O1_20;
    int P1_20;
    int A_21;
    int B_21;
    int C_21;
    int D_21;
    int E_21;
    int F_21;
    int G_21;
    int H_21;
    int I_21;
    int J_21;
    int K_21;
    int L_21;
    int M_21;
    int N_21;
    int O_21;
    int P_21;
    int Q_21;
    int R_21;
    int S_21;
    int T_21;
    int U_21;
    int V_21;
    int W_21;
    int X_21;
    int Y_21;
    int Z_21;
    int v_26_21;
    int v_27_21;
    int A_26;
    int B_26;
    int C_26;
    int D_26;
    int E_26;
    int F_26;
    int G_26;
    int H_26;
    int I_26;
    int J_26;
    int K_26;
    int L_26;
    int M_26;
    int N_26;
    int O_26;
    int P_26;
    int Q_26;
    int R_26;
    int S_26;
    int T_26;
    int U_26;
    int V_26;
    int W_26;
    int X_26;
    int Y_26;
    int Z_26;
    int A1_26;
    int B1_26;
    int C1_26;
    int D1_26;
    int E1_26;
    int F1_26;
    int G1_26;
    int H1_26;
    int I1_26;
    int J1_26;
    int K1_26;
    int L1_26;
    int M1_26;
    int N1_26;
    int O1_26;
    int P1_26;
    int Q1_26;
    int R1_26;
    int S1_26;
    int T1_26;
    int v_46_26;
    int A_27;
    int B_27;
    int C_27;
    int D_27;
    int E_27;
    int F_27;
    int G_27;
    int H_27;
    int I_27;
    int J_27;
    int K_27;
    int L_27;
    int M_27;
    int N_27;
    int O_27;
    int P_27;
    int Q_27;
    int R_27;



    // main logic
    goto main_init;

  main_init:
    if (!((E_0 == 0) && (D_0 == 0) && (A_0 == 0) && (F_0 == 0)))
        abort ();
    inv_main4_0 = F_0;
    inv_main4_1 = D_0;
    inv_main4_2 = A_0;
    inv_main4_3 = E_0;
    inv_main4_4 = B_0;
    inv_main4_5 = C_0;
    goto inv_main4;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  inv_main125:
    goto inv_main125;
  inv_main179:
    goto inv_main179;
  inv_main186:
    goto inv_main186;
  inv_main160:
    goto inv_main160;
  inv_main44:
    goto inv_main44;
  inv_main4:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_5 = __VERIFIER_nondet_int ();
          B_5 = __VERIFIER_nondet_int ();
          C_5 = __VERIFIER_nondet_int ();
          D_5 = __VERIFIER_nondet_int ();
          F_5 = __VERIFIER_nondet_int ();
          G_5 = __VERIFIER_nondet_int ();
          I_5 = __VERIFIER_nondet_int ();
          J_5 = __VERIFIER_nondet_int ();
          M_5 = __VERIFIER_nondet_int ();
          N_5 = __VERIFIER_nondet_int ();
          O_5 = __VERIFIER_nondet_int ();
          P_5 = __VERIFIER_nondet_int ();
          A1_5 = __VERIFIER_nondet_int ();
          Q_5 = __VERIFIER_nondet_int ();
          T_5 = __VERIFIER_nondet_int ();
          U_5 = __VERIFIER_nondet_int ();
          V_5 = __VERIFIER_nondet_int ();
          W_5 = __VERIFIER_nondet_int ();
          X_5 = __VERIFIER_nondet_int ();
          v_28_5 = __VERIFIER_nondet_int ();
          Y_5 = __VERIFIER_nondet_int ();
          v_27_5 = __VERIFIER_nondet_int ();
          Z_5 = __VERIFIER_nondet_int ();
          R_5 = inv_main4_0;
          L_5 = inv_main4_1;
          E_5 = inv_main4_2;
          H_5 = inv_main4_3;
          S_5 = inv_main4_4;
          K_5 = inv_main4_5;
          if (!
              ((U_5 == G_5) && (T_5 == B_5) && (Q_5 == X_5) && (P_5 == M_5)
               && (O_5 == (I_5 + -1)) && (N_5 == F_5) && (M_5 == 1)
               && (!(M_5 == 0)) && (!(J_5 == 0)) && (G_5 == L_5)
               && (F_5 == E_5) && (D_5 == 0) && (C_5 == D_5) && (B_5 == R_5)
               && (A_5 == M_5) && (!(A1_5 == 0)) && (Z_5 == H_5)
               && (Y_5 == (C_5 + 1)) && (X_5 == A1_5) && (W_5 == Z_5)
               && (V_5 == O_5) && (1 <= I_5)
               && (((!(0 <= (O_5 + (-1 * D_5)))) && (J_5 == 0))
                   || ((0 <= (O_5 + (-1 * D_5))) && (J_5 == 1)))
               && (!(1 == I_5)) && (v_27_5 == Q_5) && (v_28_5 == J_5)))
              abort ();
          inv_main36_0 = T_5;
          inv_main36_1 = Q_5;
          inv_main36_2 = N_5;
          inv_main36_3 = W_5;
          inv_main36_4 = V_5;
          inv_main36_5 = Y_5;
          inv_main36_6 = v_27_5;
          inv_main36_7 = P_5;
          inv_main36_8 = A_5;
          inv_main36_9 = J_5;
          inv_main36_10 = v_28_5;
          inv_main36_11 = C_5;
          goto inv_main36;

      case 1:
          A_21 = __VERIFIER_nondet_int ();
          B_21 = __VERIFIER_nondet_int ();
          C_21 = __VERIFIER_nondet_int ();
          D_21 = __VERIFIER_nondet_int ();
          E_21 = __VERIFIER_nondet_int ();
          F_21 = __VERIFIER_nondet_int ();
          G_21 = __VERIFIER_nondet_int ();
          I_21 = __VERIFIER_nondet_int ();
          J_21 = __VERIFIER_nondet_int ();
          K_21 = __VERIFIER_nondet_int ();
          L_21 = __VERIFIER_nondet_int ();
          M_21 = __VERIFIER_nondet_int ();
          N_21 = __VERIFIER_nondet_int ();
          O_21 = __VERIFIER_nondet_int ();
          Q_21 = __VERIFIER_nondet_int ();
          R_21 = __VERIFIER_nondet_int ();
          T_21 = __VERIFIER_nondet_int ();
          U_21 = __VERIFIER_nondet_int ();
          Y_21 = __VERIFIER_nondet_int ();
          v_27_21 = __VERIFIER_nondet_int ();
          Z_21 = __VERIFIER_nondet_int ();
          v_26_21 = __VERIFIER_nondet_int ();
          S_21 = inv_main4_0;
          H_21 = inv_main4_1;
          P_21 = inv_main4_2;
          W_21 = inv_main4_3;
          X_21 = inv_main4_4;
          V_21 = inv_main4_5;
          if (!
              ((T_21 == D_21) && (R_21 == Q_21) && (Q_21 == 0)
               && (O_21 == M_21) && (N_21 == U_21) && (M_21 == P_21)
               && (L_21 == R_21) && (K_21 == E_21) && (I_21 == A_21)
               && (G_21 == S_21) && (F_21 == B_21) && (E_21 == 1)
               && (!(E_21 == 0)) && (D_21 == H_21) && (C_21 == E_21)
               && (B_21 == 0) && (A_21 == W_21) && (Z_21 == G_21)
               && (!(Y_21 == 0)) && (U_21 == (J_21 + -1)) && (1 <= J_21)
               && (((0 <= (U_21 + (-1 * B_21))) && (Y_21 == 1))
                   || ((!(0 <= (U_21 + (-1 * B_21)))) && (Y_21 == 0)))
               && (!(1 == J_21)) && (v_26_21 == L_21) && (v_27_21 == Y_21)))
              abort ();
          inv_main148_0 = Z_21;
          inv_main148_1 = T_21;
          inv_main148_2 = O_21;
          inv_main148_3 = L_21;
          inv_main148_4 = N_21;
          inv_main148_5 = F_21;
          inv_main148_6 = v_26_21;
          inv_main148_7 = K_21;
          inv_main148_8 = C_21;
          inv_main148_9 = Y_21;
          inv_main148_10 = v_27_21;
          A_20 = __VERIFIER_nondet_int ();
          B_20 = __VERIFIER_nondet_int ();
          O1_20 = __VERIFIER_nondet_int ();
          C_20 = __VERIFIER_nondet_int ();
          D_20 = __VERIFIER_nondet_int ();
          M1_20 = __VERIFIER_nondet_int ();
          F_20 = __VERIFIER_nondet_int ();
          K1_20 = __VERIFIER_nondet_int ();
          G_20 = __VERIFIER_nondet_int ();
          J_20 = __VERIFIER_nondet_int ();
          G1_20 = __VERIFIER_nondet_int ();
          L_20 = __VERIFIER_nondet_int ();
          E1_20 = __VERIFIER_nondet_int ();
          M_20 = __VERIFIER_nondet_int ();
          N_20 = __VERIFIER_nondet_int ();
          C1_20 = __VERIFIER_nondet_int ();
          O_20 = __VERIFIER_nondet_int ();
          A1_20 = __VERIFIER_nondet_int ();
          Q_20 = __VERIFIER_nondet_int ();
          R_20 = __VERIFIER_nondet_int ();
          S_20 = __VERIFIER_nondet_int ();
          T_20 = __VERIFIER_nondet_int ();
          V_20 = __VERIFIER_nondet_int ();
          W_20 = __VERIFIER_nondet_int ();
          Z_20 = __VERIFIER_nondet_int ();
          P1_20 = __VERIFIER_nondet_int ();
          L1_20 = __VERIFIER_nondet_int ();
          J1_20 = __VERIFIER_nondet_int ();
          H1_20 = __VERIFIER_nondet_int ();
          D1_20 = __VERIFIER_nondet_int ();
          B1_20 = __VERIFIER_nondet_int ();
          I_20 = inv_main148_0;
          H_20 = inv_main148_1;
          K_20 = inv_main148_2;
          I1_20 = inv_main148_3;
          U_20 = inv_main148_4;
          E_20 = inv_main148_5;
          N1_20 = inv_main148_6;
          F1_20 = inv_main148_7;
          Y_20 = inv_main148_8;
          X_20 = inv_main148_9;
          P_20 = inv_main148_10;
          if (!
              ((A_20 == D_20) && (J1_20 == I1_20) && (H1_20 == I1_20)
               && (G1_20 == Z_20) && (!(E1_20 == 0)) && (D1_20 == (E_20 + 1))
               && (C1_20 == W_20) && (B1_20 == Q_20) && (A1_20 == Y_20)
               && (!(Z_20 == 0)) && (W_20 == E_20) && (V_20 == P_20)
               && (T_20 == V_20) && (S_20 == L_20) && (R_20 == H1_20)
               && (Q_20 == H_20) && (O_20 == J_20) && (N_20 == L1_20)
               && (M_20 == Z_20) && (L_20 == K_20) && (J_20 == I_20)
               && (G_20 == X_20) && (F_20 == A1_20)
               && (!(E_20 == (U_20 + -1))) && (D_20 == K1_20)
               && (C_20 == F1_20) && (P1_20 == J1_20) && (O1_20 == D1_20)
               && (M1_20 == G_20) && (L1_20 == U_20) && (K1_20 == 0)
               && (((-1 <= E_20) && (Z_20 == 1))
                   || ((!(-1 <= E_20)) && (Z_20 == 0)))
               && (((0 <= (L1_20 + (-1 * D1_20))) && (E1_20 == 1))
                   || ((!(0 <= (L1_20 + (-1 * D1_20)))) && (E1_20 == 0)))
               && (B_20 == C_20)))
              abort ();
          inv_main148_0 = O_20;
          inv_main148_1 = B1_20;
          inv_main148_2 = S_20;
          inv_main148_3 = A_20;
          inv_main148_4 = N_20;
          inv_main148_5 = O1_20;
          inv_main148_6 = R_20;
          inv_main148_7 = B_20;
          inv_main148_8 = F_20;
          inv_main148_9 = M1_20;
          inv_main148_10 = T_20;
          goto inv_main148_0;

      default:
          abort ();
      }
  inv_main51:
    goto inv_main51;
  inv_main77:
    goto inv_main77;
  inv_main104:
    goto inv_main104;
  inv_main194:
    goto inv_main194;
  inv_main95:
    goto inv_main95;
  inv_main167:
    goto inv_main167;
  inv_main70:
    goto inv_main70;
  inv_main111:
    goto inv_main111;
  inv_main201:
    goto inv_main201;
  inv_main88:
    goto inv_main88;
  inv_main36:
    switch (__VERIFIER_nondet_int ())
      {
      case 0:
          Q1_26 = __VERIFIER_nondet_int ();
          A_26 = __VERIFIER_nondet_int ();
          B_26 = __VERIFIER_nondet_int ();
          C_26 = __VERIFIER_nondet_int ();
          M1_26 = __VERIFIER_nondet_int ();
          E_26 = __VERIFIER_nondet_int ();
          F_26 = __VERIFIER_nondet_int ();
          K1_26 = __VERIFIER_nondet_int ();
          I1_26 = __VERIFIER_nondet_int ();
          I_26 = __VERIFIER_nondet_int ();
          J_26 = __VERIFIER_nondet_int ();
          K_26 = __VERIFIER_nondet_int ();
          L_26 = __VERIFIER_nondet_int ();
          M_26 = __VERIFIER_nondet_int ();
          N_26 = __VERIFIER_nondet_int ();
          C1_26 = __VERIFIER_nondet_int ();
          P_26 = __VERIFIER_nondet_int ();
          Q_26 = __VERIFIER_nondet_int ();
          R_26 = __VERIFIER_nondet_int ();
          S_26 = __VERIFIER_nondet_int ();
          T_26 = __VERIFIER_nondet_int ();
          U_26 = __VERIFIER_nondet_int ();
          W_26 = __VERIFIER_nondet_int ();
          Z_26 = __VERIFIER_nondet_int ();
          v_46_26 = __VERIFIER_nondet_int ();
          T1_26 = __VERIFIER_nondet_int ();
          R1_26 = __VERIFIER_nondet_int ();
          P1_26 = __VERIFIER_nondet_int ();
          N1_26 = __VERIFIER_nondet_int ();
          L1_26 = __VERIFIER_nondet_int ();
          J1_26 = __VERIFIER_nondet_int ();
          F1_26 = __VERIFIER_nondet_int ();
          D1_26 = __VERIFIER_nondet_int ();
          B1_26 = __VERIFIER_nondet_int ();
          S1_26 = __VERIFIER_nondet_int ();
          G1_26 = inv_main36_0;
          Y_26 = inv_main36_1;
          G_26 = inv_main36_2;
          H1_26 = inv_main36_3;
          A1_26 = inv_main36_4;
          H_26 = inv_main36_5;
          O_26 = inv_main36_6;
          V_26 = inv_main36_7;
          D_26 = inv_main36_8;
          E1_26 = inv_main36_9;
          O1_26 = inv_main36_10;
          X_26 = inv_main36_11;
          if (!
              ((E_26 == V_26) && (C_26 == K_26) && (B_26 == E_26)
               && (A_26 == S1_26) && (!(N1_26 == 0)) && (M1_26 == Q_26)
               && (L1_26 == H_26) && (K1_26 == E1_26) && (J1_26 == 0)
               && (I1_26 == W_26) && (F1_26 == I_26) && (D1_26 == D_26)
               && (C1_26 == H1_26) && (B1_26 == N1_26) && (Z_26 == Y_26)
               && (W_26 == G1_26) && (U_26 == C1_26) && (T_26 == L1_26)
               && (S_26 == A1_26) && (R_26 == D1_26) && (Q_26 == O_26)
               && (P_26 == J_26) && (N_26 == Z_26) && (M_26 == L_26)
               && (L_26 == O1_26) && (!(K_26 == 0)) && (J_26 == T1_26)
               && (I_26 == G_26) && (!(H_26 == A1_26)) && (T1_26 == 0)
               && (S1_26 == X_26) && (R1_26 == K1_26) && (Q1_26 == B1_26)
               && (P1_26 == K_26)
               && (((0 <= (S_26 + (-1 * L1_26))) && (J1_26 == 1))
                   || ((!(0 <= (S_26 + (-1 * L1_26)))) && (J1_26 == 0)))
               && (((0 <= H_26) && (K_26 == 1))
                   || ((!(0 <= H_26)) && (K_26 == 0))) && (F_26 == S_26)
               && (v_46_26 == J1_26)))
              abort ();
          inv_main132_0 = I1_26;
          inv_main132_1 = N_26;
          inv_main132_2 = F1_26;
          inv_main132_3 = U_26;
          inv_main132_4 = F_26;
          inv_main132_5 = T_26;
          inv_main132_6 = M1_26;
          inv_main132_7 = B_26;
          inv_main132_8 = R_26;
          inv_main132_9 = R1_26;
          inv_main132_10 = M_26;
          inv_main132_11 = A_26;
          inv_main132_12 = P_26;
          inv_main132_13 = Q1_26;
          inv_main132_14 = P1_26;
          inv_main132_15 = C_26;
          inv_main132_16 = J1_26;
          inv_main132_17 = v_46_26;
          G_27 = inv_main132_0;
          K_27 = inv_main132_1;
          I_27 = inv_main132_2;
          O_27 = inv_main132_3;
          C_27 = inv_main132_4;
          L_27 = inv_main132_5;
          P_27 = inv_main132_6;
          B_27 = inv_main132_7;
          N_27 = inv_main132_8;
          F_27 = inv_main132_9;
          R_27 = inv_main132_10;
          Q_27 = inv_main132_11;
          J_27 = inv_main132_12;
          H_27 = inv_main132_13;
          A_27 = inv_main132_14;
          E_27 = inv_main132_15;
          M_27 = inv_main132_16;
          D_27 = inv_main132_17;
          if (!1)
              abort ();
          goto main_error;

      case 1:
          D_11 = __VERIFIER_nondet_int ();
          G_11 = __VERIFIER_nondet_int ();
          L_11 = __VERIFIER_nondet_int ();
          M_11 = __VERIFIER_nondet_int ();
          P_11 = __VERIFIER_nondet_int ();
          Q_11 = __VERIFIER_nondet_int ();
          R_11 = __VERIFIER_nondet_int ();
          O_11 = inv_main36_0;
          N_11 = inv_main36_1;
          F_11 = inv_main36_2;
          S_11 = inv_main36_3;
          I_11 = inv_main36_4;
          E_11 = inv_main36_5;
          J_11 = inv_main36_6;
          H_11 = inv_main36_7;
          C_11 = inv_main36_8;
          K_11 = inv_main36_9;
          B_11 = inv_main36_10;
          A_11 = inv_main36_11;
          if (!((!(E_11 == I_11)) && (!(Q_11 == 0)) && (M_11 == 0)))
              abort ();
          inv_main98_0 = O_11;
          inv_main98_1 = N_11;
          inv_main98_2 = F_11;
          inv_main98_3 = S_11;
          inv_main98_4 = I_11;
          inv_main98_5 = E_11;
          inv_main98_6 = J_11;
          inv_main98_7 = H_11;
          inv_main98_8 = C_11;
          inv_main98_9 = K_11;
          inv_main98_10 = B_11;
          inv_main98_11 = A_11;
          inv_main98_12 = Q_11;
          inv_main98_13 = M_11;
          inv_main98_14 = D_11;
          inv_main98_15 = R_11;
          inv_main98_16 = L_11;
          inv_main98_17 = P_11;
          inv_main98_18 = G_11;
          I1_3 = __VERIFIER_nondet_int ();
          E1_3 = __VERIFIER_nondet_int ();
          E2_3 = __VERIFIER_nondet_int ();
          A1_3 = __VERIFIER_nondet_int ();
          R1_3 = __VERIFIER_nondet_int ();
          N1_3 = __VERIFIER_nondet_int ();
          J1_3 = __VERIFIER_nondet_int ();
          J2_3 = __VERIFIER_nondet_int ();
          F1_3 = __VERIFIER_nondet_int ();
          F2_3 = __VERIFIER_nondet_int ();
          B1_3 = __VERIFIER_nondet_int ();
          S1_3 = __VERIFIER_nondet_int ();
          B_3 = __VERIFIER_nondet_int ();
          O1_3 = __VERIFIER_nondet_int ();
          E_3 = __VERIFIER_nondet_int ();
          K1_3 = __VERIFIER_nondet_int ();
          G_3 = __VERIFIER_nondet_int ();
          H_3 = __VERIFIER_nondet_int ();
          I_3 = __VERIFIER_nondet_int ();
          J_3 = __VERIFIER_nondet_int ();
          K_3 = __VERIFIER_nondet_int ();
          G2_3 = __VERIFIER_nondet_int ();
          L_3 = __VERIFIER_nondet_int ();
          M_3 = __VERIFIER_nondet_int ();
          N_3 = __VERIFIER_nondet_int ();
          C1_3 = __VERIFIER_nondet_int ();
          O_3 = __VERIFIER_nondet_int ();
          C2_3 = __VERIFIER_nondet_int ();
          Q_3 = __VERIFIER_nondet_int ();
          R_3 = __VERIFIER_nondet_int ();
          U_3 = __VERIFIER_nondet_int ();
          V_3 = __VERIFIER_nondet_int ();
          W_3 = __VERIFIER_nondet_int ();
          X_3 = __VERIFIER_nondet_int ();
          X1_3 = __VERIFIER_nondet_int ();
          T1_3 = __VERIFIER_nondet_int ();
          P1_3 = __VERIFIER_nondet_int ();
          L1_3 = __VERIFIER_nondet_int ();
          H1_3 = __VERIFIER_nondet_int ();
          H2_3 = __VERIFIER_nondet_int ();
          D2_3 = __VERIFIER_nondet_int ();
          Y1_3 = __VERIFIER_nondet_int ();
          U1_3 = __VERIFIER_nondet_int ();
          Q1_3 = inv_main98_0;
          T_3 = inv_main98_1;
          M1_3 = inv_main98_2;
          Z_3 = inv_main98_3;
          I2_3 = inv_main98_4;
          B2_3 = inv_main98_5;
          C_3 = inv_main98_6;
          A2_3 = inv_main98_7;
          D1_3 = inv_main98_8;
          W1_3 = inv_main98_9;
          D_3 = inv_main98_10;
          S_3 = inv_main98_11;
          F_3 = inv_main98_12;
          V1_3 = inv_main98_13;
          P_3 = inv_main98_14;
          A_3 = inv_main98_15;
          Z1_3 = inv_main98_16;
          Y_3 = inv_main98_17;
          G1_3 = inv_main98_18;
          if (!
              ((U_3 == N1_3) && (R_3 == A2_3) && (Q_3 == U1_3) && (O_3 == T_3)
               && (N_3 == F_3) && (M_3 == N_3) && (L_3 == K_3)
               && (K_3 == D1_3) && (J_3 == D_3) && (I_3 == Q1_3)
               && (H_3 == S_3) && (G_3 == C_3) && (E_3 == T1_3)
               && (B_3 == G_3) && (D2_3 == M1_3) && (C2_3 == C1_3)
               && (Y1_3 == I2_3) && (X1_3 == Z1_3) && (U1_3 == Z_3)
               && (T1_3 == A_3) && (S1_3 == W1_3) && (R1_3 == O_3)
               && (!(P1_3 == 0)) && (O1_3 == F2_3) && (N1_3 == P_3)
               && (L1_3 == W_3) && (K1_3 == W_3) && (J1_3 == R_3)
               && (I1_3 == H_3) && (H1_3 == X1_3) && (F1_3 == V1_3)
               && (E1_3 == D2_3) && (C1_3 == Y_3) && (B1_3 == (A1_3 + 1))
               && (A1_3 == J2_3) && (X_3 == S1_3) && (!(W_3 == 0))
               && (J2_3 == B2_3) && (H2_3 == I_3) && (G2_3 == F1_3)
               && (F2_3 == G1_3) && (E2_3 == J_3)
               && (((0 <= (Y1_3 + (-1 * J2_3))) && (P1_3 == 1))
                   || ((!(0 <= (Y1_3 + (-1 * J2_3)))) && (P1_3 == 0)))
               && (((0 <= B2_3) && (W_3 == 1))
                   || ((!(0 <= B2_3)) && (W_3 == 0))) && (V_3 == Y1_3)))
              abort ();
          inv_main36_0 = H2_3;
          inv_main36_1 = R1_3;
          inv_main36_2 = E1_3;
          inv_main36_3 = Q_3;
          inv_main36_4 = V_3;
          inv_main36_5 = B1_3;
          inv_main36_6 = B_3;
          inv_main36_7 = J1_3;
          inv_main36_8 = L_3;
          inv_main36_9 = X_3;
          inv_main36_10 = E2_3;
          inv_main36_11 = I1_3;
          goto inv_main36;

      case 2:
          B_12 = __VERIFIER_nondet_int ();
          C_12 = __VERIFIER_nondet_int ();
          J_12 = __VERIFIER_nondet_int ();
          K_12 = __VERIFIER_nondet_int ();
          N_12 = __VERIFIER_nondet_int ();
          Q_12 = __VERIFIER_nondet_int ();
          R_12 = __VERIFIER_nondet_int ();
          S_12 = __VERIFIER_nondet_int ();
          M_12 = inv_main36_0;
          F_12 = inv_main36_1;
          I_12 = inv_main36_2;
          H_12 = inv_main36_3;
          L_12 = inv_main36_4;
          P_12 = inv_main36_5;
          O_12 = inv_main36_6;
          E_12 = inv_main36_7;
          T_12 = inv_main36_8;
          G_12 = inv_main36_9;
          D_12 = inv_main36_10;
          A_12 = inv_main36_11;
          if (!((C_12 == 0) && (!(P_12 == L_12)) && (K_12 == 0)))
              abort ();
          inv_main98_0 = M_12;
          inv_main98_1 = F_12;
          inv_main98_2 = I_12;
          inv_main98_3 = H_12;
          inv_main98_4 = L_12;
          inv_main98_5 = P_12;
          inv_main98_6 = O_12;
          inv_main98_7 = E_12;
          inv_main98_8 = T_12;
          inv_main98_9 = G_12;
          inv_main98_10 = D_12;
          inv_main98_11 = A_12;
          inv_main98_12 = K_12;
          inv_main98_13 = B_12;
          inv_main98_14 = N_12;
          inv_main98_15 = R_12;
          inv_main98_16 = S_12;
          inv_main98_17 = Q_12;
          inv_main98_18 = J_12;
          I1_3 = __VERIFIER_nondet_int ();
          E1_3 = __VERIFIER_nondet_int ();
          E2_3 = __VERIFIER_nondet_int ();
          A1_3 = __VERIFIER_nondet_int ();
          R1_3 = __VERIFIER_nondet_int ();
          N1_3 = __VERIFIER_nondet_int ();
          J1_3 = __VERIFIER_nondet_int ();
          J2_3 = __VERIFIER_nondet_int ();
          F1_3 = __VERIFIER_nondet_int ();
          F2_3 = __VERIFIER_nondet_int ();
          B1_3 = __VERIFIER_nondet_int ();
          S1_3 = __VERIFIER_nondet_int ();
          B_3 = __VERIFIER_nondet_int ();
          O1_3 = __VERIFIER_nondet_int ();
          E_3 = __VERIFIER_nondet_int ();
          K1_3 = __VERIFIER_nondet_int ();
          G_3 = __VERIFIER_nondet_int ();
          H_3 = __VERIFIER_nondet_int ();
          I_3 = __VERIFIER_nondet_int ();
          J_3 = __VERIFIER_nondet_int ();
          K_3 = __VERIFIER_nondet_int ();
          G2_3 = __VERIFIER_nondet_int ();
          L_3 = __VERIFIER_nondet_int ();
          M_3 = __VERIFIER_nondet_int ();
          N_3 = __VERIFIER_nondet_int ();
          C1_3 = __VERIFIER_nondet_int ();
          O_3 = __VERIFIER_nondet_int ();
          C2_3 = __VERIFIER_nondet_int ();
          Q_3 = __VERIFIER_nondet_int ();
          R_3 = __VERIFIER_nondet_int ();
          U_3 = __VERIFIER_nondet_int ();
          V_3 = __VERIFIER_nondet_int ();
          W_3 = __VERIFIER_nondet_int ();
          X_3 = __VERIFIER_nondet_int ();
          X1_3 = __VERIFIER_nondet_int ();
          T1_3 = __VERIFIER_nondet_int ();
          P1_3 = __VERIFIER_nondet_int ();
          L1_3 = __VERIFIER_nondet_int ();
          H1_3 = __VERIFIER_nondet_int ();
          H2_3 = __VERIFIER_nondet_int ();
          D2_3 = __VERIFIER_nondet_int ();
          Y1_3 = __VERIFIER_nondet_int ();
          U1_3 = __VERIFIER_nondet_int ();
          Q1_3 = inv_main98_0;
          T_3 = inv_main98_1;
          M1_3 = inv_main98_2;
          Z_3 = inv_main98_3;
          I2_3 = inv_main98_4;
          B2_3 = inv_main98_5;
          C_3 = inv_main98_6;
          A2_3 = inv_main98_7;
          D1_3 = inv_main98_8;
          W1_3 = inv_main98_9;
          D_3 = inv_main98_10;
          S_3 = inv_main98_11;
          F_3 = inv_main98_12;
          V1_3 = inv_main98_13;
          P_3 = inv_main98_14;
          A_3 = inv_main98_15;
          Z1_3 = inv_main98_16;
          Y_3 = inv_main98_17;
          G1_3 = inv_main98_18;
          if (!
              ((U_3 == N1_3) && (R_3 == A2_3) && (Q_3 == U1_3) && (O_3 == T_3)
               && (N_3 == F_3) && (M_3 == N_3) && (L_3 == K_3)
               && (K_3 == D1_3) && (J_3 == D_3) && (I_3 == Q1_3)
               && (H_3 == S_3) && (G_3 == C_3) && (E_3 == T1_3)
               && (B_3 == G_3) && (D2_3 == M1_3) && (C2_3 == C1_3)
               && (Y1_3 == I2_3) && (X1_3 == Z1_3) && (U1_3 == Z_3)
               && (T1_3 == A_3) && (S1_3 == W1_3) && (R1_3 == O_3)
               && (!(P1_3 == 0)) && (O1_3 == F2_3) && (N1_3 == P_3)
               && (L1_3 == W_3) && (K1_3 == W_3) && (J1_3 == R_3)
               && (I1_3 == H_3) && (H1_3 == X1_3) && (F1_3 == V1_3)
               && (E1_3 == D2_3) && (C1_3 == Y_3) && (B1_3 == (A1_3 + 1))
               && (A1_3 == J2_3) && (X_3 == S1_3) && (!(W_3 == 0))
               && (J2_3 == B2_3) && (H2_3 == I_3) && (G2_3 == F1_3)
               && (F2_3 == G1_3) && (E2_3 == J_3)
               && (((0 <= (Y1_3 + (-1 * J2_3))) && (P1_3 == 1))
                   || ((!(0 <= (Y1_3 + (-1 * J2_3)))) && (P1_3 == 0)))
               && (((0 <= B2_3) && (W_3 == 1))
                   || ((!(0 <= B2_3)) && (W_3 == 0))) && (V_3 == Y1_3)))
              abort ();
          inv_main36_0 = H2_3;
          inv_main36_1 = R1_3;
          inv_main36_2 = E1_3;
          inv_main36_3 = Q_3;
          inv_main36_4 = V_3;
          inv_main36_5 = B1_3;
          inv_main36_6 = B_3;
          inv_main36_7 = J1_3;
          inv_main36_8 = L_3;
          inv_main36_9 = X_3;
          inv_main36_10 = E2_3;
          inv_main36_11 = I1_3;
          goto inv_main36;

      case 3:
          Q1_10 = __VERIFIER_nondet_int ();
          A_10 = __VERIFIER_nondet_int ();
          B_10 = __VERIFIER_nondet_int ();
          O1_10 = __VERIFIER_nondet_int ();
          C_10 = __VERIFIER_nondet_int ();
          D_10 = __VERIFIER_nondet_int ();
          M1_10 = __VERIFIER_nondet_int ();
          E_10 = __VERIFIER_nondet_int ();
          F_10 = __VERIFIER_nondet_int ();
          K1_10 = __VERIFIER_nondet_int ();
          H_10 = __VERIFIER_nondet_int ();
          I1_10 = __VERIFIER_nondet_int ();
          I_10 = __VERIFIER_nondet_int ();
          J_10 = __VERIFIER_nondet_int ();
          K_10 = __VERIFIER_nondet_int ();
          E1_10 = __VERIFIER_nondet_int ();
          M_10 = __VERIFIER_nondet_int ();
          C1_10 = __VERIFIER_nondet_int ();
          O_10 = __VERIFIER_nondet_int ();
          A1_10 = __VERIFIER_nondet_int ();
          R_10 = __VERIFIER_nondet_int ();
          S_10 = __VERIFIER_nondet_int ();
          T_10 = __VERIFIER_nondet_int ();
          U_10 = __VERIFIER_nondet_int ();
          W_10 = __VERIFIER_nondet_int ();
          X_10 = __VERIFIER_nondet_int ();
          Y_10 = __VERIFIER_nondet_int ();
          Z_10 = __VERIFIER_nondet_int ();
          v_47_10 = __VERIFIER_nondet_int ();
          R1_10 = __VERIFIER_nondet_int ();
          L1_10 = __VERIFIER_nondet_int ();
          J1_10 = __VERIFIER_nondet_int ();
          H1_10 = __VERIFIER_nondet_int ();
          F1_10 = __VERIFIER_nondet_int ();
          D1_10 = __VERIFIER_nondet_int ();
          B1_10 = __VERIFIER_nondet_int ();
          V_10 = inv_main36_0;
          N1_10 = inv_main36_1;
          G1_10 = inv_main36_2;
          G_10 = inv_main36_3;
          N_10 = inv_main36_4;
          S1_10 = inv_main36_5;
          U1_10 = inv_main36_6;
          Q_10 = inv_main36_7;
          L_10 = inv_main36_8;
          P_10 = inv_main36_9;
          T1_10 = inv_main36_10;
          P1_10 = inv_main36_11;
          if (!
              ((E_10 == X_10) && (D_10 == G_10) && (C_10 == O_10)
               && (B_10 == N1_10) && (A_10 == Q1_10)
               && (!(O1_10 == (K_10 + -1))) && (O1_10 == Z_10)
               && (M1_10 == G1_10) && (L1_10 == U1_10) && (K1_10 == L_10)
               && (J1_10 == A_10) && (I1_10 == M1_10) && (H1_10 == T_10)
               && (F1_10 == T1_10) && (E1_10 == B_10) && (D1_10 == R1_10)
               && (C1_10 == K1_10) && (B1_10 == E_10) && (A1_10 == V_10)
               && (Z_10 == S1_10) && (!(Y_10 == 0)) && (!(X_10 == 0))
               && (W_10 == (O1_10 + 1)) && (U_10 == D_10) && (T_10 == Q_10)
               && (S_10 == P_10) && (R_10 == R1_10) && (O_10 == P1_10)
               && (M_10 == L1_10) && (K_10 == I_10) && (J_10 == A1_10)
               && (I_10 == N_10) && (H_10 == S_10) && (!(S1_10 == N_10))
               && (!(R1_10 == 0)) && (!(Q1_10 == 0))
               && (((0 <= (I_10 + (-1 * Z_10))) && (Y_10 == 1))
                   || ((!(0 <= (I_10 + (-1 * Z_10)))) && (Y_10 == 0)))
               && (((0 <= S1_10) && (R1_10 == 1))
                   || ((!(0 <= S1_10)) && (R1_10 == 0))) && (F_10 == F1_10)
               && (v_47_10 == Y_10)))
              abort ();
          inv_main98_0 = J_10;
          inv_main98_1 = E1_10;
          inv_main98_2 = I1_10;
          inv_main98_3 = U_10;
          inv_main98_4 = K_10;
          inv_main98_5 = W_10;
          inv_main98_6 = M_10;
          inv_main98_7 = H1_10;
          inv_main98_8 = C1_10;
          inv_main98_9 = H_10;
          inv_main98_10 = F_10;
          inv_main98_11 = C_10;
          inv_main98_12 = J1_10;
          inv_main98_13 = B1_10;
          inv_main98_14 = D1_10;
          inv_main98_15 = R_10;
          inv_main98_16 = Y_10;
          inv_main98_17 = v_47_10;
          inv_main98_18 = O1_10;
          I1_3 = __VERIFIER_nondet_int ();
          E1_3 = __VERIFIER_nondet_int ();
          E2_3 = __VERIFIER_nondet_int ();
          A1_3 = __VERIFIER_nondet_int ();
          R1_3 = __VERIFIER_nondet_int ();
          N1_3 = __VERIFIER_nondet_int ();
          J1_3 = __VERIFIER_nondet_int ();
          J2_3 = __VERIFIER_nondet_int ();
          F1_3 = __VERIFIER_nondet_int ();
          F2_3 = __VERIFIER_nondet_int ();
          B1_3 = __VERIFIER_nondet_int ();
          S1_3 = __VERIFIER_nondet_int ();
          B_3 = __VERIFIER_nondet_int ();
          O1_3 = __VERIFIER_nondet_int ();
          E_3 = __VERIFIER_nondet_int ();
          K1_3 = __VERIFIER_nondet_int ();
          G_3 = __VERIFIER_nondet_int ();
          H_3 = __VERIFIER_nondet_int ();
          I_3 = __VERIFIER_nondet_int ();
          J_3 = __VERIFIER_nondet_int ();
          K_3 = __VERIFIER_nondet_int ();
          G2_3 = __VERIFIER_nondet_int ();
          L_3 = __VERIFIER_nondet_int ();
          M_3 = __VERIFIER_nondet_int ();
          N_3 = __VERIFIER_nondet_int ();
          C1_3 = __VERIFIER_nondet_int ();
          O_3 = __VERIFIER_nondet_int ();
          C2_3 = __VERIFIER_nondet_int ();
          Q_3 = __VERIFIER_nondet_int ();
          R_3 = __VERIFIER_nondet_int ();
          U_3 = __VERIFIER_nondet_int ();
          V_3 = __VERIFIER_nondet_int ();
          W_3 = __VERIFIER_nondet_int ();
          X_3 = __VERIFIER_nondet_int ();
          X1_3 = __VERIFIER_nondet_int ();
          T1_3 = __VERIFIER_nondet_int ();
          P1_3 = __VERIFIER_nondet_int ();
          L1_3 = __VERIFIER_nondet_int ();
          H1_3 = __VERIFIER_nondet_int ();
          H2_3 = __VERIFIER_nondet_int ();
          D2_3 = __VERIFIER_nondet_int ();
          Y1_3 = __VERIFIER_nondet_int ();
          U1_3 = __VERIFIER_nondet_int ();
          Q1_3 = inv_main98_0;
          T_3 = inv_main98_1;
          M1_3 = inv_main98_2;
          Z_3 = inv_main98_3;
          I2_3 = inv_main98_4;
          B2_3 = inv_main98_5;
          C_3 = inv_main98_6;
          A2_3 = inv_main98_7;
          D1_3 = inv_main98_8;
          W1_3 = inv_main98_9;
          D_3 = inv_main98_10;
          S_3 = inv_main98_11;
          F_3 = inv_main98_12;
          V1_3 = inv_main98_13;
          P_3 = inv_main98_14;
          A_3 = inv_main98_15;
          Z1_3 = inv_main98_16;
          Y_3 = inv_main98_17;
          G1_3 = inv_main98_18;
          if (!
              ((U_3 == N1_3) && (R_3 == A2_3) && (Q_3 == U1_3) && (O_3 == T_3)
               && (N_3 == F_3) && (M_3 == N_3) && (L_3 == K_3)
               && (K_3 == D1_3) && (J_3 == D_3) && (I_3 == Q1_3)
               && (H_3 == S_3) && (G_3 == C_3) && (E_3 == T1_3)
               && (B_3 == G_3) && (D2_3 == M1_3) && (C2_3 == C1_3)
               && (Y1_3 == I2_3) && (X1_3 == Z1_3) && (U1_3 == Z_3)
               && (T1_3 == A_3) && (S1_3 == W1_3) && (R1_3 == O_3)
               && (!(P1_3 == 0)) && (O1_3 == F2_3) && (N1_3 == P_3)
               && (L1_3 == W_3) && (K1_3 == W_3) && (J1_3 == R_3)
               && (I1_3 == H_3) && (H1_3 == X1_3) && (F1_3 == V1_3)
               && (E1_3 == D2_3) && (C1_3 == Y_3) && (B1_3 == (A1_3 + 1))
               && (A1_3 == J2_3) && (X_3 == S1_3) && (!(W_3 == 0))
               && (J2_3 == B2_3) && (H2_3 == I_3) && (G2_3 == F1_3)
               && (F2_3 == G1_3) && (E2_3 == J_3)
               && (((0 <= (Y1_3 + (-1 * J2_3))) && (P1_3 == 1))
                   || ((!(0 <= (Y1_3 + (-1 * J2_3)))) && (P1_3 == 0)))
               && (((0 <= B2_3) && (W_3 == 1))
                   || ((!(0 <= B2_3)) && (W_3 == 0))) && (V_3 == Y1_3)))
              abort ();
          inv_main36_0 = H2_3;
          inv_main36_1 = R1_3;
          inv_main36_2 = E1_3;
          inv_main36_3 = Q_3;
          inv_main36_4 = V_3;
          inv_main36_5 = B1_3;
          inv_main36_6 = B_3;
          inv_main36_7 = J1_3;
          inv_main36_8 = L_3;
          inv_main36_9 = X_3;
          inv_main36_10 = E2_3;
          inv_main36_11 = I1_3;
          goto inv_main36;

      default:
          abort ();
      }
  inv_main145:
    goto inv_main145;
  inv_main28:
    goto inv_main28;
  inv_main148_0:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          Q1_4 = __VERIFIER_nondet_int ();
          A_4 = __VERIFIER_nondet_int ();
          O1_4 = __VERIFIER_nondet_int ();
          C_4 = __VERIFIER_nondet_int ();
          D_4 = __VERIFIER_nondet_int ();
          E_4 = __VERIFIER_nondet_int ();
          F_4 = __VERIFIER_nondet_int ();
          K1_4 = __VERIFIER_nondet_int ();
          G_4 = __VERIFIER_nondet_int ();
          I1_4 = __VERIFIER_nondet_int ();
          I_4 = __VERIFIER_nondet_int ();
          J_4 = __VERIFIER_nondet_int ();
          K_4 = __VERIFIER_nondet_int ();
          E1_4 = __VERIFIER_nondet_int ();
          M_4 = __VERIFIER_nondet_int ();
          N_4 = __VERIFIER_nondet_int ();
          C1_4 = __VERIFIER_nondet_int ();
          O_4 = __VERIFIER_nondet_int ();
          P_4 = __VERIFIER_nondet_int ();
          Q_4 = __VERIFIER_nondet_int ();
          R_4 = __VERIFIER_nondet_int ();
          S_4 = __VERIFIER_nondet_int ();
          T_4 = __VERIFIER_nondet_int ();
          V_4 = __VERIFIER_nondet_int ();
          X_4 = __VERIFIER_nondet_int ();
          Y_4 = __VERIFIER_nondet_int ();
          v_47_4 = __VERIFIER_nondet_int ();
          T1_4 = __VERIFIER_nondet_int ();
          R1_4 = __VERIFIER_nondet_int ();
          N1_4 = __VERIFIER_nondet_int ();
          L1_4 = __VERIFIER_nondet_int ();
          J1_4 = __VERIFIER_nondet_int ();
          H1_4 = __VERIFIER_nondet_int ();
          F1_4 = __VERIFIER_nondet_int ();
          B1_4 = __VERIFIER_nondet_int ();
          U1_4 = __VERIFIER_nondet_int ();
          S1_4 = __VERIFIER_nondet_int ();
          Z_4 = inv_main148_0;
          U_4 = inv_main148_1;
          A1_4 = inv_main148_2;
          M1_4 = inv_main148_3;
          B_4 = inv_main148_4;
          P1_4 = inv_main148_5;
          H_4 = inv_main148_6;
          G1_4 = inv_main148_7;
          W_4 = inv_main148_8;
          D1_4 = inv_main148_9;
          L_4 = inv_main148_10;
          if (!
              ((F_4 == D1_4) && (E_4 == O_4) && (D_4 == M1_4) && (C_4 == X_4)
               && (A_4 == M_4) && (O1_4 == (P1_4 + 1)) && (!(N1_4 == 0))
               && (K1_4 == B_4) && (H1_4 == (R_4 + 1)) && (F1_4 == C1_4)
               && (E1_4 == F_4) && (C1_4 == G1_4) && (B1_4 == T1_4)
               && (Y_4 == Z_4) && (!(X_4 == 0)) && (V_4 == P1_4)
               && (T_4 == Y_4) && (S_4 == R1_4) && (R_4 == O1_4)
               && (Q_4 == D_4) && (P_4 == V_4) && (O_4 == W_4) && (N_4 == S_4)
               && (M_4 == M1_4) && (K_4 == X_4) && (J_4 == I_4)
               && (I_4 == U_4) && (U1_4 == Q1_4) && (T1_4 == A1_4)
               && (!(R1_4 == 0)) && (Q1_4 == L_4) && (!(P1_4 == (B_4 + -1)))
               && (((!(-1 <= P1_4)) && (X_4 == 0))
                   || ((-1 <= P1_4) && (X_4 == 1)))
               && (((!(0 <= (K1_4 + (-1 * O1_4)))) && (N1_4 == 0))
                   || ((0 <= (K1_4 + (-1 * O1_4))) && (N1_4 == 1)))
               && (G_4 == K1_4) && (v_47_4 == N_4)))
              abort ();
          inv_main36_0 = T_4;
          inv_main36_1 = N_4;
          inv_main36_2 = B1_4;
          inv_main36_3 = A_4;
          inv_main36_4 = G_4;
          inv_main36_5 = H1_4;
          inv_main36_6 = v_47_4;
          inv_main36_7 = I1_4;
          inv_main36_8 = S1_4;
          inv_main36_9 = J1_4;
          inv_main36_10 = L1_4;
          inv_main36_11 = R_4;
          goto inv_main36;

      case 1:
          A_20 = __VERIFIER_nondet_int ();
          B_20 = __VERIFIER_nondet_int ();
          O1_20 = __VERIFIER_nondet_int ();
          C_20 = __VERIFIER_nondet_int ();
          D_20 = __VERIFIER_nondet_int ();
          M1_20 = __VERIFIER_nondet_int ();
          F_20 = __VERIFIER_nondet_int ();
          K1_20 = __VERIFIER_nondet_int ();
          G_20 = __VERIFIER_nondet_int ();
          J_20 = __VERIFIER_nondet_int ();
          G1_20 = __VERIFIER_nondet_int ();
          L_20 = __VERIFIER_nondet_int ();
          E1_20 = __VERIFIER_nondet_int ();
          M_20 = __VERIFIER_nondet_int ();
          N_20 = __VERIFIER_nondet_int ();
          C1_20 = __VERIFIER_nondet_int ();
          O_20 = __VERIFIER_nondet_int ();
          A1_20 = __VERIFIER_nondet_int ();
          Q_20 = __VERIFIER_nondet_int ();
          R_20 = __VERIFIER_nondet_int ();
          S_20 = __VERIFIER_nondet_int ();
          T_20 = __VERIFIER_nondet_int ();
          V_20 = __VERIFIER_nondet_int ();
          W_20 = __VERIFIER_nondet_int ();
          Z_20 = __VERIFIER_nondet_int ();
          P1_20 = __VERIFIER_nondet_int ();
          L1_20 = __VERIFIER_nondet_int ();
          J1_20 = __VERIFIER_nondet_int ();
          H1_20 = __VERIFIER_nondet_int ();
          D1_20 = __VERIFIER_nondet_int ();
          B1_20 = __VERIFIER_nondet_int ();
          I_20 = inv_main148_0;
          H_20 = inv_main148_1;
          K_20 = inv_main148_2;
          I1_20 = inv_main148_3;
          U_20 = inv_main148_4;
          E_20 = inv_main148_5;
          N1_20 = inv_main148_6;
          F1_20 = inv_main148_7;
          Y_20 = inv_main148_8;
          X_20 = inv_main148_9;
          P_20 = inv_main148_10;
          if (!
              ((A_20 == D_20) && (J1_20 == I1_20) && (H1_20 == I1_20)
               && (G1_20 == Z_20) && (!(E1_20 == 0)) && (D1_20 == (E_20 + 1))
               && (C1_20 == W_20) && (B1_20 == Q_20) && (A1_20 == Y_20)
               && (!(Z_20 == 0)) && (W_20 == E_20) && (V_20 == P_20)
               && (T_20 == V_20) && (S_20 == L_20) && (R_20 == H1_20)
               && (Q_20 == H_20) && (O_20 == J_20) && (N_20 == L1_20)
               && (M_20 == Z_20) && (L_20 == K_20) && (J_20 == I_20)
               && (G_20 == X_20) && (F_20 == A1_20)
               && (!(E_20 == (U_20 + -1))) && (D_20 == K1_20)
               && (C_20 == F1_20) && (P1_20 == J1_20) && (O1_20 == D1_20)
               && (M1_20 == G_20) && (L1_20 == U_20) && (K1_20 == 0)
               && (((-1 <= E_20) && (Z_20 == 1))
                   || ((!(-1 <= E_20)) && (Z_20 == 0)))
               && (((0 <= (L1_20 + (-1 * D1_20))) && (E1_20 == 1))
                   || ((!(0 <= (L1_20 + (-1 * D1_20)))) && (E1_20 == 0)))
               && (B_20 == C_20)))
              abort ();
          inv_main148_0 = O_20;
          inv_main148_1 = B1_20;
          inv_main148_2 = S_20;
          inv_main148_3 = A_20;
          inv_main148_4 = N_20;
          inv_main148_5 = O1_20;
          inv_main148_6 = R_20;
          inv_main148_7 = B_20;
          inv_main148_8 = F_20;
          inv_main148_9 = M1_20;
          inv_main148_10 = T_20;
          goto inv_main148_0;

      default:
          abort ();
      }

    // return expression

}

