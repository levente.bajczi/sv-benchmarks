// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: hopv/Fibonacci01_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "Fibonacci01_000_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int fib_1030_unknown_7_0;
    int fib_1030_unknown_7_1;
    int fib_1030_unknown_7_2;
    int fail_unknown_3_0;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    int E_0;
    int F_0;
    int G_0;
    int H_0;
    int I_0;
    int J_0;
    int K_0;
    int L_0;
    int M_0;
    int N_0;
    int O_0;
    int P_0;
    int A_1;
    int B_1;
    int C_1;
    int A_2;



    // main logic
    goto main_init;

  main_init:
    if (!((B_1 == 0) && (C_1 == 0)))
        abort ();
    fib_1030_unknown_7_0 = A_1;
    fib_1030_unknown_7_1 = C_1;
    fib_1030_unknown_7_2 = B_1;
    D_0 = __VERIFIER_nondet_int ();
    E_0 = __VERIFIER_nondet_int ();
    F_0 = __VERIFIER_nondet_int ();
    G_0 = __VERIFIER_nondet_int ();
    H_0 = __VERIFIER_nondet_int ();
    I_0 = __VERIFIER_nondet_int ();
    J_0 = __VERIFIER_nondet_int ();
    K_0 = __VERIFIER_nondet_int ();
    L_0 = __VERIFIER_nondet_int ();
    M_0 = __VERIFIER_nondet_int ();
    N_0 = __VERIFIER_nondet_int ();
    O_0 = __VERIFIER_nondet_int ();
    P_0 = __VERIFIER_nondet_int ();
    A_0 = fib_1030_unknown_7_0;
    C_0 = fib_1030_unknown_7_1;
    B_0 = fib_1030_unknown_7_2;
    if (!
        ((!((0 == O_0) == ((!(0 == N_0)) && (!(0 == J_0)))))
         && (!((0 == N_0) == (M_0 >= 0))) && (!(0 == B_0)) && (0 == O_0)
         && (L_0 == A_0) && (K_0 == 0) && (I_0 == (G_0 + H_0)) && (H_0 == A_0)
         && (G_0 == 0) && (F_0 == (D_0 + E_0)) && (E_0 == C_0) && (D_0 == 0)
         && (P_0 == 1) && (M_0 == (K_0 + L_0))
         && ((0 == J_0) == (F_0 <= I_0))))
        abort ();
    fail_unknown_3_0 = P_0;
    A_2 = fail_unknown_3_0;
    if (!1)
        abort ();
    goto main_error;
  main_error:
    reach_error ();
  main_final:
    goto main_final;

    // return expression

}

