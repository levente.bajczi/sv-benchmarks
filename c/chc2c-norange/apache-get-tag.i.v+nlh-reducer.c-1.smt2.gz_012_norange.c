// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: tricera-benchmarks/apache-get-tag.i.v+nlh-reducer.c-1.smt2.gz_012.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "apache-get-tag.i.v+nlh-reducer.c-1.smt2.gz_012_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int inv_main4_0;
    int inv_main4_1;
    int inv_main4_2;
    int inv_main4_3;
    int inv_main4_4;
    int inv_main4_5;
    int inv_main146_0;
    int inv_main146_1;
    int inv_main146_2;
    int inv_main146_3;
    int inv_main146_4;
    int inv_main146_5;
    int inv_main146_6;
    int inv_main146_7;
    int inv_main146_8;
    int inv_main146_9;
    int inv_main146_10;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    int E_0;
    int F_0;
    int A_6;
    int B_6;
    int C_6;
    int D_6;
    int E_6;
    int F_6;
    int G_6;
    int H_6;
    int I_6;
    int J_6;
    int K_6;
    int L_6;
    int M_6;
    int N_6;
    int O_6;
    int P_6;
    int Q_6;
    int R_6;
    int S_6;
    int T_6;
    int U_6;
    int V_6;
    int W_6;
    int X_6;
    int Y_6;
    int Z_6;
    int v_26_6;
    int A_27;
    int B_27;
    int C_27;
    int D_27;
    int E_27;
    int F_27;
    int G_27;
    int H_27;
    int I_27;
    int J_27;
    int K_27;



    // main logic
    goto main_init;

  main_init:
    if (!((E_0 == 0) && (C_0 == 0) && (B_0 == 0) && (F_0 == 0)))
        abort ();
    inv_main4_0 = E_0;
    inv_main4_1 = B_0;
    inv_main4_2 = F_0;
    inv_main4_3 = C_0;
    inv_main4_4 = A_0;
    inv_main4_5 = D_0;
    B_6 = __VERIFIER_nondet_int ();
    C_6 = __VERIFIER_nondet_int ();
    D_6 = __VERIFIER_nondet_int ();
    E_6 = __VERIFIER_nondet_int ();
    F_6 = __VERIFIER_nondet_int ();
    G_6 = __VERIFIER_nondet_int ();
    H_6 = __VERIFIER_nondet_int ();
    I_6 = __VERIFIER_nondet_int ();
    J_6 = __VERIFIER_nondet_int ();
    L_6 = __VERIFIER_nondet_int ();
    N_6 = __VERIFIER_nondet_int ();
    O_6 = __VERIFIER_nondet_int ();
    P_6 = __VERIFIER_nondet_int ();
    R_6 = __VERIFIER_nondet_int ();
    U_6 = __VERIFIER_nondet_int ();
    V_6 = __VERIFIER_nondet_int ();
    W_6 = __VERIFIER_nondet_int ();
    X_6 = __VERIFIER_nondet_int ();
    Y_6 = __VERIFIER_nondet_int ();
    Z_6 = __VERIFIER_nondet_int ();
    v_26_6 = __VERIFIER_nondet_int ();
    K_6 = inv_main4_0;
    Q_6 = inv_main4_1;
    S_6 = inv_main4_2;
    M_6 = inv_main4_3;
    A_6 = inv_main4_4;
    T_6 = inv_main4_5;
    if (!
        ((R_6 == K_6) && (P_6 == I_6) && (O_6 == 0) && (N_6 == V_6)
         && (L_6 == W_6) && (J_6 == X_6) && (I_6 == O_6) && (H_6 == W_6)
         && (G_6 == S_6) && (F_6 == 0) && (E_6 == G_6) && (D_6 == R_6)
         && (C_6 == (Y_6 + -1)) && (B_6 == U_6) && (Z_6 == C_6) && (X_6 == 0)
         && (W_6 == 1) && (!(W_6 == 0)) && (V_6 == M_6) && (U_6 == Q_6)
         && (1 <= Y_6) && (((0 <= (C_6 + (-1 * X_6))) && (F_6 == 1))
                           || ((!(0 <= (C_6 + (-1 * X_6)))) && (F_6 == 0)))
         && (!(1 == Y_6)) && (v_26_6 == F_6)))
        abort ();
    inv_main146_0 = D_6;
    inv_main146_1 = B_6;
    inv_main146_2 = E_6;
    inv_main146_3 = N_6;
    inv_main146_4 = Z_6;
    inv_main146_5 = J_6;
    inv_main146_6 = P_6;
    inv_main146_7 = L_6;
    inv_main146_8 = H_6;
    inv_main146_9 = F_6;
    inv_main146_10 = v_26_6;
    K_27 = inv_main146_0;
    D_27 = inv_main146_1;
    E_27 = inv_main146_2;
    F_27 = inv_main146_3;
    C_27 = inv_main146_4;
    A_27 = inv_main146_5;
    I_27 = inv_main146_6;
    B_27 = inv_main146_7;
    J_27 = inv_main146_8;
    H_27 = inv_main146_9;
    G_27 = inv_main146_10;
    if (!1)
        abort ();
    goto main_error;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  inv_main161:
    goto inv_main161;
  inv_main37:
    goto inv_main37;
  inv_main181:
    goto inv_main181;
  inv_main89:
    goto inv_main89;
  inv_main126:
    goto inv_main126;
  inv_main45:
    goto inv_main45;
  inv_main71:
    goto inv_main71;
  inv_main78:
    goto inv_main78;
  inv_main96:
    goto inv_main96;
  inv_main99:
    goto inv_main99;
  inv_main149:
    goto inv_main149;
  inv_main188:
    goto inv_main188;
  inv_main18:
    goto inv_main18;
  inv_main33:
    goto inv_main33;
  inv_main112:
    goto inv_main112;
  inv_main26:
    goto inv_main26;
  inv_main105:
    goto inv_main105;
  inv_main52:
    goto inv_main52;
  inv_main133:
    goto inv_main133;
  inv_main168:
    goto inv_main168;

    // return expression

}

