// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: vmt-chc-benchmarks/vmt-durationThm_1_e3_389_e4_294_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "vmt-durationThm_1_e3_389_e4_294_000_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int state_0;
    int state_1;
    _Bool state_2;
    _Bool state_3;
    int state_4;
    int state_5;
    int state_6;
    int state_7;
    _Bool state_8;
    int state_9;
    int state_10;
    int state_11;
    int state_12;
    _Bool state_13;
    _Bool state_14;
    int state_15;
    int state_16;
    _Bool state_17;
    _Bool state_18;
    _Bool state_19;
    int state_20;
    int state_21;
    int state_22;
    int A_0;
    _Bool B_0;
    int C_0;
    int D_0;
    int E_0;
    int F_0;
    int G_0;
    int H_0;
    int I_0;
    int J_0;
    _Bool K_0;
    int L_0;
    int M_0;
    int N_0;
    _Bool O_0;
    _Bool P_0;
    _Bool Q_0;
    int R_0;
    _Bool S_0;
    _Bool T_0;
    int U_0;
    int V_0;
    _Bool W_0;
    int A_1;
    _Bool B_1;
    int C_1;
    int D_1;
    int E_1;
    _Bool F_1;
    int G_1;
    int H_1;
    int I_1;
    int J_1;
    int K_1;
    int L_1;
    int M_1;
    _Bool N_1;
    int O_1;
    int P_1;
    int Q_1;
    _Bool R_1;
    _Bool S_1;
    _Bool T_1;
    int U_1;
    int V_1;
    int W_1;
    int X_1;
    int Y_1;
    int Z_1;
    _Bool A1_1;
    _Bool B1_1;
    int C1_1;
    _Bool D1_1;
    int E1_1;
    int F1_1;
    int G1_1;
    int H1_1;
    int I1_1;
    _Bool J1_1;
    int K1_1;
    _Bool L1_1;
    int M1_1;
    _Bool N1_1;
    _Bool O1_1;
    _Bool P1_1;
    _Bool Q1_1;
    int R1_1;
    int S1_1;
    _Bool T1_1;
    int A_2;
    _Bool B_2;
    int C_2;
    int D_2;
    int E_2;
    int F_2;
    int G_2;
    int H_2;
    int I_2;
    int J_2;
    _Bool K_2;
    int L_2;
    int M_2;
    int N_2;
    _Bool O_2;
    _Bool P_2;
    _Bool Q_2;
    int R_2;
    _Bool S_2;
    _Bool T_2;
    int U_2;
    int V_2;
    _Bool W_2;



    // main logic
    goto main_init;

  main_init:
    if (!
        ((M_0 == U_0) && (H_0 == G_0) && (H_0 == A_0) && (J_0 == V_0)
         && (J_0 == I_0) && (L_0 == 0) && (L_0 == F_0) && (N_0 == 0)
         && (N_0 == R_0) && (((!P_0) || K_0) == O_0)
         && (B_0 ==
             ((T_0 || (!(V_0 <= U_0))) && (W_0 || (!(A_0 <= F_0)))
              && (1 <= A_0) && (1 <= V_0))) && (B_0 == Q_0) && (Q_0 == P_0)
         && K_0 && (M_0 == 0)))
        abort ();
    state_0 = N_0;
    state_1 = R_0;
    state_2 = B_0;
    state_3 = Q_0;
    state_4 = M_0;
    state_5 = U_0;
    state_6 = L_0;
    state_7 = F_0;
    state_8 = P_0;
    state_9 = J_0;
    state_10 = V_0;
    state_11 = H_0;
    state_12 = A_0;
    state_13 = K_0;
    state_14 = O_0;
    state_15 = I_0;
    state_16 = G_0;
    state_17 = W_0;
    state_18 = T_0;
    state_19 = S_0;
    state_20 = C_0;
    state_21 = D_0;
    state_22 = E_0;
    O1_1 = __VERIFIER_nondet__Bool ();
    M1_1 = __VERIFIER_nondet_int ();
    F_1 = __VERIFIER_nondet__Bool ();
    K1_1 = __VERIFIER_nondet_int ();
    G_1 = __VERIFIER_nondet_int ();
    H_1 = __VERIFIER_nondet_int ();
    I1_1 = __VERIFIER_nondet_int ();
    G1_1 = __VERIFIER_nondet_int ();
    E1_1 = __VERIFIER_nondet_int ();
    C1_1 = __VERIFIER_nondet_int ();
    A1_1 = __VERIFIER_nondet__Bool ();
    V_1 = __VERIFIER_nondet_int ();
    W_1 = __VERIFIER_nondet_int ();
    X_1 = __VERIFIER_nondet_int ();
    Y_1 = __VERIFIER_nondet_int ();
    Z_1 = __VERIFIER_nondet_int ();
    P1_1 = __VERIFIER_nondet__Bool ();
    N1_1 = __VERIFIER_nondet__Bool ();
    L1_1 = __VERIFIER_nondet__Bool ();
    J1_1 = __VERIFIER_nondet__Bool ();
    H1_1 = __VERIFIER_nondet_int ();
    F1_1 = __VERIFIER_nondet_int ();
    B1_1 = __VERIFIER_nondet__Bool ();
    Q_1 = state_0;
    U_1 = state_1;
    B_1 = state_2;
    T_1 = state_3;
    P_1 = state_4;
    R1_1 = state_5;
    O_1 = state_6;
    I_1 = state_7;
    S_1 = state_8;
    M_1 = state_9;
    S1_1 = state_10;
    K_1 = state_11;
    A_1 = state_12;
    N_1 = state_13;
    R_1 = state_14;
    L_1 = state_15;
    J_1 = state_16;
    T1_1 = state_17;
    Q1_1 = state_18;
    D1_1 = state_19;
    C_1 = state_20;
    D_1 = state_21;
    E_1 = state_22;
    if (!
        ((X_1 == V_1) && (Y_1 == W_1) && (Z_1 == M1_1) && (G1_1 == C1_1)
         && (H1_1 == F1_1) && (I1_1 == H1_1) && (K1_1 == G1_1)
         && (M1_1 == E1_1) && (Q_1 == U_1) && (P_1 == R1_1) && (O_1 == I_1)
         && (M_1 == S1_1) && (K_1 == A_1) && (A_1 == V_1)
         && ((N_1 || (!S_1)) == R_1) && (((!O1_1) || B1_1) == N1_1)
         && (B1_1 == (A1_1 || (!(0 <= (Z_1 + (-1 * Y_1) + (-1 * X_1))))))
         && (L1_1 ==
             (T_1 && (J1_1 || (!(X_1 <= K1_1))) && (A1_1 || (!(Y_1 <= I1_1)))
              && (1 <= Y_1) && (1 <= X_1))) && (T_1 == S_1) && (P1_1 == L1_1)
         && (P1_1 == O1_1) && (B_1 == T_1) && ((!T1_1) || (R1_1 == F1_1))
         && (T1_1 || (F1_1 == 0)) && (D1_1 || (C1_1 == 0)) && (D1_1
                                                               || (E1_1 == 0))
         && ((!D1_1) || (U_1 == E1_1)) && ((!D1_1) || (I_1 == C1_1))
         && (S1_1 == W_1)))
        abort ();
    state_0 = M1_1;
    state_1 = Z_1;
    state_2 = L1_1;
    state_3 = P1_1;
    state_4 = H1_1;
    state_5 = I1_1;
    state_6 = G1_1;
    state_7 = K1_1;
    state_8 = O1_1;
    state_9 = W_1;
    state_10 = Y_1;
    state_11 = V_1;
    state_12 = X_1;
    state_13 = B1_1;
    state_14 = N1_1;
    state_15 = H_1;
    state_16 = G_1;
    state_17 = J1_1;
    state_18 = A1_1;
    state_19 = F_1;
    state_20 = C1_1;
    state_21 = F1_1;
    state_22 = E1_1;
    goto state_0;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  state_0:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          N_2 = state_0;
          R_2 = state_1;
          B_2 = state_2;
          Q_2 = state_3;
          M_2 = state_4;
          U_2 = state_5;
          L_2 = state_6;
          F_2 = state_7;
          P_2 = state_8;
          J_2 = state_9;
          V_2 = state_10;
          H_2 = state_11;
          A_2 = state_12;
          K_2 = state_13;
          O_2 = state_14;
          I_2 = state_15;
          G_2 = state_16;
          W_2 = state_17;
          T_2 = state_18;
          S_2 = state_19;
          C_2 = state_20;
          D_2 = state_21;
          E_2 = state_22;
          if (!(!O_2))
              abort ();
          goto main_error;

      case 1:
          O1_1 = __VERIFIER_nondet__Bool ();
          M1_1 = __VERIFIER_nondet_int ();
          F_1 = __VERIFIER_nondet__Bool ();
          K1_1 = __VERIFIER_nondet_int ();
          G_1 = __VERIFIER_nondet_int ();
          H_1 = __VERIFIER_nondet_int ();
          I1_1 = __VERIFIER_nondet_int ();
          G1_1 = __VERIFIER_nondet_int ();
          E1_1 = __VERIFIER_nondet_int ();
          C1_1 = __VERIFIER_nondet_int ();
          A1_1 = __VERIFIER_nondet__Bool ();
          V_1 = __VERIFIER_nondet_int ();
          W_1 = __VERIFIER_nondet_int ();
          X_1 = __VERIFIER_nondet_int ();
          Y_1 = __VERIFIER_nondet_int ();
          Z_1 = __VERIFIER_nondet_int ();
          P1_1 = __VERIFIER_nondet__Bool ();
          N1_1 = __VERIFIER_nondet__Bool ();
          L1_1 = __VERIFIER_nondet__Bool ();
          J1_1 = __VERIFIER_nondet__Bool ();
          H1_1 = __VERIFIER_nondet_int ();
          F1_1 = __VERIFIER_nondet_int ();
          B1_1 = __VERIFIER_nondet__Bool ();
          Q_1 = state_0;
          U_1 = state_1;
          B_1 = state_2;
          T_1 = state_3;
          P_1 = state_4;
          R1_1 = state_5;
          O_1 = state_6;
          I_1 = state_7;
          S_1 = state_8;
          M_1 = state_9;
          S1_1 = state_10;
          K_1 = state_11;
          A_1 = state_12;
          N_1 = state_13;
          R_1 = state_14;
          L_1 = state_15;
          J_1 = state_16;
          T1_1 = state_17;
          Q1_1 = state_18;
          D1_1 = state_19;
          C_1 = state_20;
          D_1 = state_21;
          E_1 = state_22;
          if (!
              ((X_1 == V_1) && (Y_1 == W_1) && (Z_1 == M1_1) && (G1_1 == C1_1)
               && (H1_1 == F1_1) && (I1_1 == H1_1) && (K1_1 == G1_1)
               && (M1_1 == E1_1) && (Q_1 == U_1) && (P_1 == R1_1)
               && (O_1 == I_1) && (M_1 == S1_1) && (K_1 == A_1)
               && (A_1 == V_1) && ((N_1 || (!S_1)) == R_1)
               && (((!O1_1) || B1_1) == N1_1)
               && (B1_1 ==
                   (A1_1 || (!(0 <= (Z_1 + (-1 * Y_1) + (-1 * X_1))))))
               && (L1_1 ==
                   (T_1 && (J1_1 || (!(X_1 <= K1_1)))
                    && (A1_1 || (!(Y_1 <= I1_1))) && (1 <= Y_1)
                    && (1 <= X_1))) && (T_1 == S_1) && (P1_1 == L1_1)
               && (P1_1 == O1_1) && (B_1 == T_1) && ((!T1_1)
                                                     || (R1_1 == F1_1))
               && (T1_1 || (F1_1 == 0)) && (D1_1 || (C1_1 == 0)) && (D1_1
                                                                     || (E1_1
                                                                         ==
                                                                         0))
               && ((!D1_1) || (U_1 == E1_1)) && ((!D1_1) || (I_1 == C1_1))
               && (S1_1 == W_1)))
              abort ();
          state_0 = M1_1;
          state_1 = Z_1;
          state_2 = L1_1;
          state_3 = P1_1;
          state_4 = H1_1;
          state_5 = I1_1;
          state_6 = G1_1;
          state_7 = K1_1;
          state_8 = O1_1;
          state_9 = W_1;
          state_10 = Y_1;
          state_11 = V_1;
          state_12 = X_1;
          state_13 = B1_1;
          state_14 = N1_1;
          state_15 = H_1;
          state_16 = G_1;
          state_17 = J1_1;
          state_18 = A1_1;
          state_19 = F_1;
          state_20 = C1_1;
          state_21 = F1_1;
          state_22 = E1_1;
          goto state_0;

      default:
          abort ();
      }

    // return expression

}

