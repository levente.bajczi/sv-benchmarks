// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: tricera-benchmarks/apache-get-tag.i.v+lhb-reducer.c-1.smt2.gz_016.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "apache-get-tag.i.v+lhb-reducer.c-1.smt2.gz_016_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int inv_main148_0;
    int inv_main148_1;
    int inv_main148_2;
    int inv_main148_3;
    int inv_main148_4;
    int inv_main148_5;
    int inv_main148_6;
    int inv_main148_7;
    int inv_main148_8;
    int inv_main148_9;
    int inv_main148_10;
    int inv_main4_0;
    int inv_main4_1;
    int inv_main4_2;
    int inv_main4_3;
    int inv_main4_4;
    int inv_main4_5;
    int inv_main194_0;
    int inv_main194_1;
    int inv_main194_2;
    int inv_main194_3;
    int inv_main194_4;
    int inv_main194_5;
    int inv_main194_6;
    int inv_main194_7;
    int inv_main194_8;
    int inv_main194_9;
    int inv_main194_10;
    int inv_main194_11;
    int inv_main194_12;
    int inv_main194_13;
    int inv_main194_14;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    int E_0;
    int F_0;
    int A_18;
    int B_18;
    int C_18;
    int D_18;
    int E_18;
    int F_18;
    int G_18;
    int H_18;
    int I_18;
    int J_18;
    int K_18;
    int L_18;
    int M_18;
    int N_18;
    int O_18;
    int P_18;
    int Q_18;
    int R_18;
    int S_18;
    int T_18;
    int U_18;
    int V_18;
    int W_18;
    int X_18;
    int Y_18;
    int Z_18;
    int v_26_18;
    int A_20;
    int B_20;
    int C_20;
    int D_20;
    int E_20;
    int F_20;
    int G_20;
    int H_20;
    int I_20;
    int J_20;
    int K_20;
    int L_20;
    int M_20;
    int N_20;
    int O_20;
    int P_20;
    int Q_20;
    int R_20;
    int S_20;
    int T_20;
    int U_20;
    int V_20;
    int W_20;
    int X_20;
    int Y_20;
    int Z_20;
    int A1_20;
    int B1_20;
    int C1_20;
    int D1_20;
    int E1_20;
    int F1_20;
    int G1_20;
    int H1_20;
    int I1_20;
    int J1_20;
    int K1_20;
    int L1_20;
    int M1_20;
    int N1_20;
    int O1_20;
    int P1_20;
    int A_21;
    int B_21;
    int C_21;
    int D_21;
    int E_21;
    int F_21;
    int G_21;
    int H_21;
    int I_21;
    int J_21;
    int K_21;
    int L_21;
    int M_21;
    int N_21;
    int O_21;
    int P_21;
    int Q_21;
    int R_21;
    int S_21;
    int T_21;
    int U_21;
    int V_21;
    int W_21;
    int X_21;
    int Y_21;
    int Z_21;
    int v_26_21;
    int v_27_21;
    int A_27;
    int B_27;
    int C_27;
    int D_27;
    int E_27;
    int F_27;
    int G_27;
    int H_27;
    int I_27;
    int J_27;
    int K_27;
    int L_27;
    int M_27;
    int N_27;
    int O_27;



    // main logic
    goto main_init;

  main_init:
    if (!((E_0 == 0) && (D_0 == 0) && (A_0 == 0) && (F_0 == 0)))
        abort ();
    inv_main4_0 = F_0;
    inv_main4_1 = D_0;
    inv_main4_2 = A_0;
    inv_main4_3 = E_0;
    inv_main4_4 = B_0;
    inv_main4_5 = C_0;
    A_21 = __VERIFIER_nondet_int ();
    B_21 = __VERIFIER_nondet_int ();
    C_21 = __VERIFIER_nondet_int ();
    D_21 = __VERIFIER_nondet_int ();
    E_21 = __VERIFIER_nondet_int ();
    F_21 = __VERIFIER_nondet_int ();
    G_21 = __VERIFIER_nondet_int ();
    I_21 = __VERIFIER_nondet_int ();
    J_21 = __VERIFIER_nondet_int ();
    K_21 = __VERIFIER_nondet_int ();
    L_21 = __VERIFIER_nondet_int ();
    M_21 = __VERIFIER_nondet_int ();
    N_21 = __VERIFIER_nondet_int ();
    O_21 = __VERIFIER_nondet_int ();
    Q_21 = __VERIFIER_nondet_int ();
    R_21 = __VERIFIER_nondet_int ();
    T_21 = __VERIFIER_nondet_int ();
    U_21 = __VERIFIER_nondet_int ();
    Y_21 = __VERIFIER_nondet_int ();
    v_27_21 = __VERIFIER_nondet_int ();
    Z_21 = __VERIFIER_nondet_int ();
    v_26_21 = __VERIFIER_nondet_int ();
    S_21 = inv_main4_0;
    H_21 = inv_main4_1;
    P_21 = inv_main4_2;
    W_21 = inv_main4_3;
    X_21 = inv_main4_4;
    V_21 = inv_main4_5;
    if (!
        ((T_21 == D_21) && (R_21 == Q_21) && (Q_21 == 0) && (O_21 == M_21)
         && (N_21 == U_21) && (M_21 == P_21) && (L_21 == R_21)
         && (K_21 == E_21) && (I_21 == A_21) && (G_21 == S_21)
         && (F_21 == B_21) && (E_21 == 1) && (!(E_21 == 0)) && (D_21 == H_21)
         && (C_21 == E_21) && (B_21 == 0) && (A_21 == W_21) && (Z_21 == G_21)
         && (!(Y_21 == 0)) && (U_21 == (J_21 + -1)) && (1 <= J_21)
         && (((0 <= (U_21 + (-1 * B_21))) && (Y_21 == 1))
             || ((!(0 <= (U_21 + (-1 * B_21)))) && (Y_21 == 0)))
         && (!(1 == J_21)) && (v_26_21 == L_21) && (v_27_21 == Y_21)))
        abort ();
    inv_main148_0 = Z_21;
    inv_main148_1 = T_21;
    inv_main148_2 = O_21;
    inv_main148_3 = L_21;
    inv_main148_4 = N_21;
    inv_main148_5 = F_21;
    inv_main148_6 = v_26_21;
    inv_main148_7 = K_21;
    inv_main148_8 = C_21;
    inv_main148_9 = Y_21;
    inv_main148_10 = v_27_21;
    A_20 = __VERIFIER_nondet_int ();
    B_20 = __VERIFIER_nondet_int ();
    O1_20 = __VERIFIER_nondet_int ();
    C_20 = __VERIFIER_nondet_int ();
    D_20 = __VERIFIER_nondet_int ();
    M1_20 = __VERIFIER_nondet_int ();
    F_20 = __VERIFIER_nondet_int ();
    K1_20 = __VERIFIER_nondet_int ();
    G_20 = __VERIFIER_nondet_int ();
    J_20 = __VERIFIER_nondet_int ();
    G1_20 = __VERIFIER_nondet_int ();
    L_20 = __VERIFIER_nondet_int ();
    E1_20 = __VERIFIER_nondet_int ();
    M_20 = __VERIFIER_nondet_int ();
    N_20 = __VERIFIER_nondet_int ();
    C1_20 = __VERIFIER_nondet_int ();
    O_20 = __VERIFIER_nondet_int ();
    A1_20 = __VERIFIER_nondet_int ();
    Q_20 = __VERIFIER_nondet_int ();
    R_20 = __VERIFIER_nondet_int ();
    S_20 = __VERIFIER_nondet_int ();
    T_20 = __VERIFIER_nondet_int ();
    V_20 = __VERIFIER_nondet_int ();
    W_20 = __VERIFIER_nondet_int ();
    Z_20 = __VERIFIER_nondet_int ();
    P1_20 = __VERIFIER_nondet_int ();
    L1_20 = __VERIFIER_nondet_int ();
    J1_20 = __VERIFIER_nondet_int ();
    H1_20 = __VERIFIER_nondet_int ();
    D1_20 = __VERIFIER_nondet_int ();
    B1_20 = __VERIFIER_nondet_int ();
    I_20 = inv_main148_0;
    H_20 = inv_main148_1;
    K_20 = inv_main148_2;
    I1_20 = inv_main148_3;
    U_20 = inv_main148_4;
    E_20 = inv_main148_5;
    N1_20 = inv_main148_6;
    F1_20 = inv_main148_7;
    Y_20 = inv_main148_8;
    X_20 = inv_main148_9;
    P_20 = inv_main148_10;
    if (!
        ((A_20 == D_20) && (J1_20 == I1_20) && (H1_20 == I1_20)
         && (G1_20 == Z_20) && (!(E1_20 == 0)) && (D1_20 == (E_20 + 1))
         && (C1_20 == W_20) && (B1_20 == Q_20) && (A1_20 == Y_20)
         && (!(Z_20 == 0)) && (W_20 == E_20) && (V_20 == P_20)
         && (T_20 == V_20) && (S_20 == L_20) && (R_20 == H1_20)
         && (Q_20 == H_20) && (O_20 == J_20) && (N_20 == L1_20)
         && (M_20 == Z_20) && (L_20 == K_20) && (J_20 == I_20)
         && (G_20 == X_20) && (F_20 == A1_20) && (!(E_20 == (U_20 + -1)))
         && (D_20 == K1_20) && (C_20 == F1_20) && (P1_20 == J1_20)
         && (O1_20 == D1_20) && (M1_20 == G_20) && (L1_20 == U_20)
         && (K1_20 == 0) && (((-1 <= E_20) && (Z_20 == 1))
                             || ((!(-1 <= E_20)) && (Z_20 == 0)))
         && (((0 <= (L1_20 + (-1 * D1_20))) && (E1_20 == 1))
             || ((!(0 <= (L1_20 + (-1 * D1_20)))) && (E1_20 == 0)))
         && (B_20 == C_20)))
        abort ();
    inv_main148_0 = O_20;
    inv_main148_1 = B1_20;
    inv_main148_2 = S_20;
    inv_main148_3 = A_20;
    inv_main148_4 = N_20;
    inv_main148_5 = O1_20;
    inv_main148_6 = R_20;
    inv_main148_7 = B_20;
    inv_main148_8 = F_20;
    inv_main148_9 = M1_20;
    inv_main148_10 = T_20;
    goto inv_main148_0;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  inv_main125:
    goto inv_main125;
  inv_main179:
    goto inv_main179;
  inv_main186:
    goto inv_main186;
  inv_main160:
    goto inv_main160;
  inv_main44:
    goto inv_main44;
  inv_main51:
    goto inv_main51;
  inv_main77:
    goto inv_main77;
  inv_main104:
    goto inv_main104;
  inv_main95:
    goto inv_main95;
  inv_main167:
    goto inv_main167;
  inv_main70:
    goto inv_main70;
  inv_main111:
    goto inv_main111;
  inv_main201:
    goto inv_main201;
  inv_main132:
    goto inv_main132;
  inv_main88:
    goto inv_main88;
  inv_main36:
    goto inv_main36;
  inv_main98:
    goto inv_main98;
  inv_main145:
    goto inv_main145;
  inv_main28:
    goto inv_main28;
  inv_main148_0:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_18 = __VERIFIER_nondet_int ();
          D_18 = __VERIFIER_nondet_int ();
          F_18 = __VERIFIER_nondet_int ();
          G_18 = __VERIFIER_nondet_int ();
          J_18 = __VERIFIER_nondet_int ();
          L_18 = __VERIFIER_nondet_int ();
          M_18 = __VERIFIER_nondet_int ();
          N_18 = __VERIFIER_nondet_int ();
          P_18 = __VERIFIER_nondet_int ();
          R_18 = __VERIFIER_nondet_int ();
          U_18 = __VERIFIER_nondet_int ();
          V_18 = __VERIFIER_nondet_int ();
          W_18 = __VERIFIER_nondet_int ();
          X_18 = __VERIFIER_nondet_int ();
          Y_18 = __VERIFIER_nondet_int ();
          v_26_18 = __VERIFIER_nondet_int ();
          I_18 = inv_main148_0;
          T_18 = inv_main148_1;
          O_18 = inv_main148_2;
          Q_18 = inv_main148_3;
          C_18 = inv_main148_4;
          S_18 = inv_main148_5;
          B_18 = inv_main148_6;
          H_18 = inv_main148_7;
          Z_18 = inv_main148_8;
          E_18 = inv_main148_9;
          K_18 = inv_main148_10;
          if (!
              ((R_18 == H_18) && (P_18 == C_18) && (N_18 == Q_18)
               && (M_18 == K_18) && (L_18 == 0) && (J_18 == O_18)
               && (G_18 == L_18) && (F_18 == 0) && (D_18 == (S_18 + 1))
               && (A_18 == Z_18) && (Y_18 == I_18) && (X_18 == Q_18)
               && (W_18 == E_18) && (V_18 == T_18) && (U_18 == S_18)
               && (((-1 <= S_18) && (F_18 == 1))
                   || ((!(-1 <= S_18)) && (F_18 == 0)))
               && (!(S_18 == (C_18 + -1))) && (v_26_18 == F_18)))
              abort ();
          inv_main194_0 = Y_18;
          inv_main194_1 = V_18;
          inv_main194_2 = J_18;
          inv_main194_3 = X_18;
          inv_main194_4 = P_18;
          inv_main194_5 = D_18;
          inv_main194_6 = N_18;
          inv_main194_7 = R_18;
          inv_main194_8 = A_18;
          inv_main194_9 = W_18;
          inv_main194_10 = M_18;
          inv_main194_11 = U_18;
          inv_main194_12 = G_18;
          inv_main194_13 = F_18;
          inv_main194_14 = v_26_18;
          J_27 = inv_main194_0;
          G_27 = inv_main194_1;
          N_27 = inv_main194_2;
          I_27 = inv_main194_3;
          B_27 = inv_main194_4;
          L_27 = inv_main194_5;
          C_27 = inv_main194_6;
          K_27 = inv_main194_7;
          E_27 = inv_main194_8;
          D_27 = inv_main194_9;
          H_27 = inv_main194_10;
          O_27 = inv_main194_11;
          F_27 = inv_main194_12;
          A_27 = inv_main194_13;
          M_27 = inv_main194_14;
          if (!1)
              abort ();
          goto main_error;

      case 1:
          A_20 = __VERIFIER_nondet_int ();
          B_20 = __VERIFIER_nondet_int ();
          O1_20 = __VERIFIER_nondet_int ();
          C_20 = __VERIFIER_nondet_int ();
          D_20 = __VERIFIER_nondet_int ();
          M1_20 = __VERIFIER_nondet_int ();
          F_20 = __VERIFIER_nondet_int ();
          K1_20 = __VERIFIER_nondet_int ();
          G_20 = __VERIFIER_nondet_int ();
          J_20 = __VERIFIER_nondet_int ();
          G1_20 = __VERIFIER_nondet_int ();
          L_20 = __VERIFIER_nondet_int ();
          E1_20 = __VERIFIER_nondet_int ();
          M_20 = __VERIFIER_nondet_int ();
          N_20 = __VERIFIER_nondet_int ();
          C1_20 = __VERIFIER_nondet_int ();
          O_20 = __VERIFIER_nondet_int ();
          A1_20 = __VERIFIER_nondet_int ();
          Q_20 = __VERIFIER_nondet_int ();
          R_20 = __VERIFIER_nondet_int ();
          S_20 = __VERIFIER_nondet_int ();
          T_20 = __VERIFIER_nondet_int ();
          V_20 = __VERIFIER_nondet_int ();
          W_20 = __VERIFIER_nondet_int ();
          Z_20 = __VERIFIER_nondet_int ();
          P1_20 = __VERIFIER_nondet_int ();
          L1_20 = __VERIFIER_nondet_int ();
          J1_20 = __VERIFIER_nondet_int ();
          H1_20 = __VERIFIER_nondet_int ();
          D1_20 = __VERIFIER_nondet_int ();
          B1_20 = __VERIFIER_nondet_int ();
          I_20 = inv_main148_0;
          H_20 = inv_main148_1;
          K_20 = inv_main148_2;
          I1_20 = inv_main148_3;
          U_20 = inv_main148_4;
          E_20 = inv_main148_5;
          N1_20 = inv_main148_6;
          F1_20 = inv_main148_7;
          Y_20 = inv_main148_8;
          X_20 = inv_main148_9;
          P_20 = inv_main148_10;
          if (!
              ((A_20 == D_20) && (J1_20 == I1_20) && (H1_20 == I1_20)
               && (G1_20 == Z_20) && (!(E1_20 == 0)) && (D1_20 == (E_20 + 1))
               && (C1_20 == W_20) && (B1_20 == Q_20) && (A1_20 == Y_20)
               && (!(Z_20 == 0)) && (W_20 == E_20) && (V_20 == P_20)
               && (T_20 == V_20) && (S_20 == L_20) && (R_20 == H1_20)
               && (Q_20 == H_20) && (O_20 == J_20) && (N_20 == L1_20)
               && (M_20 == Z_20) && (L_20 == K_20) && (J_20 == I_20)
               && (G_20 == X_20) && (F_20 == A1_20)
               && (!(E_20 == (U_20 + -1))) && (D_20 == K1_20)
               && (C_20 == F1_20) && (P1_20 == J1_20) && (O1_20 == D1_20)
               && (M1_20 == G_20) && (L1_20 == U_20) && (K1_20 == 0)
               && (((-1 <= E_20) && (Z_20 == 1))
                   || ((!(-1 <= E_20)) && (Z_20 == 0)))
               && (((0 <= (L1_20 + (-1 * D1_20))) && (E1_20 == 1))
                   || ((!(0 <= (L1_20 + (-1 * D1_20)))) && (E1_20 == 0)))
               && (B_20 == C_20)))
              abort ();
          inv_main148_0 = O_20;
          inv_main148_1 = B1_20;
          inv_main148_2 = S_20;
          inv_main148_3 = A_20;
          inv_main148_4 = N_20;
          inv_main148_5 = O1_20;
          inv_main148_6 = R_20;
          inv_main148_7 = B_20;
          inv_main148_8 = F_20;
          inv_main148_9 = M1_20;
          inv_main148_10 = T_20;
          goto inv_main148_0;

      default:
          abort ();
      }

    // return expression

}

