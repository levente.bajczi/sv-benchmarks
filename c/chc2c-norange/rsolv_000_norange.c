// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: eldarica-misc/rsolv_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "rsolv_000_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int h8_0;
    int h8_1;
    int h8_2;
    int h8_3;
    int h8_4;
    int h8_5;
    int h8_6;
    int h8_7;
    int h8_8;
    int h8_9;
    int h8_10;
    int h8_11;
    int h8_12;
    int h8_13;
    int h8_14;
    int h8_15;
    int h8_16;
    int h8_17;
    int h8_18;
    int h8_19;
    int h8_20;
    int h8_21;
    int h8_22;
    int h8_23;
    int h8_24;
    int h8_25;
    int h8_26;
    int h8_27;
    int h8_28;
    int h8_29;
    int h1_0;
    int h1_1;
    int h1_2;
    int h1_3;
    int h1_4;
    int h1_5;
    int h1_6;
    int h1_7;
    int h1_8;
    int h1_9;
    int h1_10;
    int h1_11;
    int h1_12;
    int h1_13;
    int h1_14;
    int h1_15;
    int h1_16;
    int h1_17;
    int h1_18;
    int h1_19;
    int h1_20;
    int h1_21;
    int h1_22;
    int h1_23;
    int h1_24;
    int h1_25;
    int h1_26;
    int h1_27;
    int h1_28;
    int h1_29;
    int h3_0;
    int h3_1;
    int h3_2;
    int h3_3;
    int h3_4;
    int h3_5;
    int h3_6;
    int h3_7;
    int h3_8;
    int h3_9;
    int h3_10;
    int h3_11;
    int h3_12;
    int h3_13;
    int h3_14;
    int h3_15;
    int h3_16;
    int h3_17;
    int h3_18;
    int h3_19;
    int h3_20;
    int h3_21;
    int h3_22;
    int h3_23;
    int h3_24;
    int h3_25;
    int h3_26;
    int h3_27;
    int h3_28;
    int h3_29;
    int h5_0;
    int h5_1;
    int h5_2;
    int h5_3;
    int h5_4;
    int h5_5;
    int h5_6;
    int h5_7;
    int h5_8;
    int h5_9;
    int h5_10;
    int h5_11;
    int h5_12;
    int h5_13;
    int h5_14;
    int h5_15;
    int h5_16;
    int h5_17;
    int h5_18;
    int h5_19;
    int h5_20;
    int h5_21;
    int h5_22;
    int h5_23;
    int h5_24;
    int h5_25;
    int h5_26;
    int h5_27;
    int h5_28;
    int h5_29;
    int h2_0;
    int h2_1;
    int h2_2;
    int h2_3;
    int h2_4;
    int h2_5;
    int h2_6;
    int h2_7;
    int h2_8;
    int h2_9;
    int h2_10;
    int h2_11;
    int h2_12;
    int h2_13;
    int h2_14;
    int h2_15;
    int h2_16;
    int h2_17;
    int h2_18;
    int h2_19;
    int h2_20;
    int h2_21;
    int h2_22;
    int h2_23;
    int h2_24;
    int h2_25;
    int h2_26;
    int h2_27;
    int h2_28;
    int h2_29;
    int h4_0;
    int h4_1;
    int h4_2;
    int h4_3;
    int h4_4;
    int h4_5;
    int h4_6;
    int h4_7;
    int h4_8;
    int h4_9;
    int h4_10;
    int h4_11;
    int h4_12;
    int h4_13;
    int h4_14;
    int h4_15;
    int h4_16;
    int h4_17;
    int h4_18;
    int h4_19;
    int h4_20;
    int h4_21;
    int h4_22;
    int h4_23;
    int h4_24;
    int h4_25;
    int h4_26;
    int h4_27;
    int h4_28;
    int h4_29;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    int E_0;
    int F_0;
    int G_0;
    int H_0;
    int I_0;
    int J_0;
    int K_0;
    int L_0;
    int M_0;
    int N_0;
    int O_0;
    int v_15_0;
    int v_16_0;
    int v_17_0;
    int v_18_0;
    int v_19_0;
    int v_20_0;
    int v_21_0;
    int v_22_0;
    int v_23_0;
    int v_24_0;
    int v_25_0;
    int v_26_0;
    int v_27_0;
    int v_28_0;
    int v_29_0;
    int A_1;
    int B_1;
    int C_1;
    int D_1;
    int E_1;
    int F_1;
    int G_1;
    int H_1;
    int I_1;
    int J_1;
    int K_1;
    int L_1;
    int M_1;
    int N_1;
    int O_1;
    int P_1;
    int Q_1;
    int R_1;
    int S_1;
    int T_1;
    int U_1;
    int V_1;
    int W_1;
    int X_1;
    int Y_1;
    int Z_1;
    int A1_1;
    int B1_1;
    int C1_1;
    int D1_1;
    int A_2;
    int B_2;
    int C_2;
    int D_2;
    int E_2;
    int F_2;
    int G_2;
    int H_2;
    int I_2;
    int J_2;
    int K_2;
    int L_2;
    int M_2;
    int N_2;
    int O_2;
    int P_2;
    int Q_2;
    int R_2;
    int S_2;
    int T_2;
    int U_2;
    int V_2;
    int W_2;
    int X_2;
    int Y_2;
    int Z_2;
    int A1_2;
    int B1_2;
    int C1_2;
    int D1_2;
    int A_3;
    int B_3;
    int C_3;
    int D_3;
    int E_3;
    int F_3;
    int G_3;
    int H_3;
    int I_3;
    int J_3;
    int K_3;
    int L_3;
    int M_3;
    int N_3;
    int O_3;
    int P_3;
    int Q_3;
    int R_3;
    int S_3;
    int T_3;
    int U_3;
    int V_3;
    int W_3;
    int X_3;
    int Y_3;
    int Z_3;
    int A1_3;
    int B1_3;
    int C1_3;
    int D1_3;
    int E1_3;
    int F1_3;
    int G1_3;
    int H1_3;
    int I1_3;
    int J1_3;
    int K1_3;
    int L1_3;
    int A_4;
    int B_4;
    int C_4;
    int D_4;
    int E_4;
    int F_4;
    int G_4;
    int H_4;
    int I_4;
    int J_4;
    int K_4;
    int L_4;
    int M_4;
    int N_4;
    int O_4;
    int P_4;
    int Q_4;
    int R_4;
    int S_4;
    int T_4;
    int U_4;
    int V_4;
    int W_4;
    int X_4;
    int Y_4;
    int Z_4;
    int A1_4;
    int B1_4;
    int C1_4;
    int D1_4;
    int A_7;
    int B_7;
    int C_7;
    int D_7;
    int E_7;
    int F_7;
    int G_7;
    int H_7;
    int I_7;
    int J_7;
    int K_7;
    int L_7;
    int M_7;
    int N_7;
    int O_7;
    int P_7;
    int Q_7;
    int R_7;
    int S_7;
    int T_7;
    int U_7;
    int V_7;
    int W_7;
    int X_7;
    int Y_7;
    int Z_7;
    int A1_7;
    int B1_7;
    int C1_7;
    int D1_7;
    int A_77;
    int B_77;
    int C_77;
    int D_77;
    int E_77;
    int F_77;
    int G_77;
    int H_77;
    int I_77;
    int J_77;
    int K_77;
    int L_77;
    int M_77;
    int N_77;
    int O_77;
    int P_77;
    int Q_77;
    int R_77;
    int S_77;
    int T_77;
    int U_77;
    int V_77;
    int W_77;
    int X_77;
    int Y_77;
    int Z_77;
    int A1_77;
    int B1_77;
    int C1_77;
    int D1_77;



    // main logic
    goto main_init;

  main_init:
    if (!
        ((v_15_0 == A_0) && (v_16_0 == B_0) && (v_17_0 == C_0)
         && (v_18_0 == D_0) && (v_19_0 == E_0) && (v_20_0 == F_0)
         && (v_21_0 == G_0) && (v_22_0 == H_0) && (v_23_0 == I_0)
         && (v_24_0 == J_0) && (v_25_0 == K_0) && (v_26_0 == L_0)
         && (v_27_0 == M_0) && (v_28_0 == N_0) && (v_29_0 == O_0)))
        abort ();
    h1_0 = A_0;
    h1_1 = B_0;
    h1_2 = C_0;
    h1_3 = D_0;
    h1_4 = E_0;
    h1_5 = F_0;
    h1_6 = G_0;
    h1_7 = H_0;
    h1_8 = I_0;
    h1_9 = J_0;
    h1_10 = K_0;
    h1_11 = L_0;
    h1_12 = M_0;
    h1_13 = N_0;
    h1_14 = O_0;
    h1_15 = v_15_0;
    h1_16 = v_16_0;
    h1_17 = v_17_0;
    h1_18 = v_18_0;
    h1_19 = v_19_0;
    h1_20 = v_20_0;
    h1_21 = v_21_0;
    h1_22 = v_22_0;
    h1_23 = v_23_0;
    h1_24 = v_24_0;
    h1_25 = v_25_0;
    h1_26 = v_26_0;
    h1_27 = v_27_0;
    h1_28 = v_28_0;
    h1_29 = v_29_0;
    A_1 = h1_0;
    C_1 = h1_1;
    E_1 = h1_2;
    G_1 = h1_3;
    I_1 = h1_4;
    J_1 = h1_5;
    K_1 = h1_6;
    L_1 = h1_7;
    M_1 = h1_8;
    N_1 = h1_9;
    O_1 = h1_10;
    P_1 = h1_11;
    Q_1 = h1_12;
    R_1 = h1_13;
    S_1 = h1_14;
    T_1 = h1_15;
    U_1 = h1_16;
    V_1 = h1_17;
    W_1 = h1_18;
    X_1 = h1_19;
    Y_1 = h1_20;
    Z_1 = h1_21;
    A1_1 = h1_22;
    B1_1 = h1_23;
    C1_1 = h1_24;
    D1_1 = h1_25;
    B_1 = h1_26;
    D_1 = h1_27;
    F_1 = h1_28;
    H_1 = h1_29;
    if (!1)
        abort ();
    h2_0 = A_1;
    h2_1 = C_1;
    h2_2 = E_1;
    h2_3 = G_1;
    h2_4 = I_1;
    h2_5 = J_1;
    h2_6 = K_1;
    h2_7 = L_1;
    h2_8 = M_1;
    h2_9 = N_1;
    h2_10 = O_1;
    h2_11 = P_1;
    h2_12 = Q_1;
    h2_13 = R_1;
    h2_14 = S_1;
    h2_15 = T_1;
    h2_16 = U_1;
    h2_17 = V_1;
    h2_18 = W_1;
    h2_19 = X_1;
    h2_20 = Y_1;
    h2_21 = Z_1;
    h2_22 = A1_1;
    h2_23 = B1_1;
    h2_24 = C1_1;
    h2_25 = D1_1;
    h2_26 = B_1;
    h2_27 = D_1;
    h2_28 = F_1;
    h2_29 = H_1;
    A_2 = h2_0;
    C_2 = h2_1;
    E_2 = h2_2;
    G_2 = h2_3;
    I_2 = h2_4;
    J_2 = h2_5;
    K_2 = h2_6;
    L_2 = h2_7;
    M_2 = h2_8;
    N_2 = h2_9;
    O_2 = h2_10;
    P_2 = h2_11;
    Q_2 = h2_12;
    R_2 = h2_13;
    S_2 = h2_14;
    T_2 = h2_15;
    U_2 = h2_16;
    V_2 = h2_17;
    W_2 = h2_18;
    X_2 = h2_19;
    Y_2 = h2_20;
    Z_2 = h2_21;
    A1_2 = h2_22;
    B1_2 = h2_23;
    C1_2 = h2_24;
    D1_2 = h2_25;
    B_2 = h2_26;
    D_2 = h2_27;
    F_2 = h2_28;
    H_2 = h2_29;
    if (!(U_2 >= 11))
        abort ();
    h3_0 = A_2;
    h3_1 = C_2;
    h3_2 = E_2;
    h3_3 = G_2;
    h3_4 = I_2;
    h3_5 = J_2;
    h3_6 = K_2;
    h3_7 = L_2;
    h3_8 = M_2;
    h3_9 = N_2;
    h3_10 = O_2;
    h3_11 = P_2;
    h3_12 = Q_2;
    h3_13 = R_2;
    h3_14 = S_2;
    h3_15 = T_2;
    h3_16 = U_2;
    h3_17 = V_2;
    h3_18 = W_2;
    h3_19 = X_2;
    h3_20 = Y_2;
    h3_21 = Z_2;
    h3_22 = A1_2;
    h3_23 = B1_2;
    h3_24 = C1_2;
    h3_25 = D1_2;
    h3_26 = B_2;
    h3_27 = D_2;
    h3_28 = F_2;
    h3_29 = H_2;
    B_3 = __VERIFIER_nondet_int ();
    D_3 = __VERIFIER_nondet_int ();
    F_3 = __VERIFIER_nondet_int ();
    K1_3 = __VERIFIER_nondet_int ();
    H_3 = __VERIFIER_nondet_int ();
    I1_3 = __VERIFIER_nondet_int ();
    L1_3 = __VERIFIER_nondet_int ();
    J1_3 = __VERIFIER_nondet_int ();
    A_3 = h3_0;
    C_3 = h3_1;
    E_3 = h3_2;
    G_3 = h3_3;
    I_3 = h3_4;
    K_3 = h3_5;
    M_3 = h3_6;
    O_3 = h3_7;
    Q_3 = h3_8;
    S_3 = h3_9;
    U_3 = h3_10;
    W_3 = h3_11;
    Y_3 = h3_12;
    Z_3 = h3_13;
    A1_3 = h3_14;
    B1_3 = h3_15;
    C1_3 = h3_16;
    D1_3 = h3_17;
    E1_3 = h3_18;
    F1_3 = h3_19;
    G1_3 = h3_20;
    H1_3 = h3_21;
    J_3 = h3_22;
    L_3 = h3_23;
    N_3 = h3_24;
    P_3 = h3_25;
    R_3 = h3_26;
    T_3 = h3_27;
    V_3 = h3_28;
    X_3 = h3_29;
    if (!
        ((D_3 == H_3) && (B_3 == 1) && (L1_3 == H_3) && (K1_3 == 1)
         && (J1_3 == H_3) && (I1_3 == 1) && (C1_3 == H_3) && (F_3 == 1)))
        abort ();
    h4_0 = A_3;
    h4_1 = C_3;
    h4_2 = E_3;
    h4_3 = G_3;
    h4_4 = I_3;
    h4_5 = K_3;
    h4_6 = M_3;
    h4_7 = O_3;
    h4_8 = Q_3;
    h4_9 = S_3;
    h4_10 = U_3;
    h4_11 = W_3;
    h4_12 = Y_3;
    h4_13 = Z_3;
    h4_14 = A1_3;
    h4_15 = B1_3;
    h4_16 = C1_3;
    h4_17 = D1_3;
    h4_18 = E1_3;
    h4_19 = F1_3;
    h4_20 = G1_3;
    h4_21 = H1_3;
    h4_22 = I1_3;
    h4_23 = J1_3;
    h4_24 = K1_3;
    h4_25 = L1_3;
    h4_26 = B_3;
    h4_27 = D_3;
    h4_28 = F_3;
    h4_29 = H_3;
    A_4 = h4_0;
    C_4 = h4_1;
    E_4 = h4_2;
    G_4 = h4_3;
    I_4 = h4_4;
    J_4 = h4_5;
    K_4 = h4_6;
    L_4 = h4_7;
    M_4 = h4_8;
    N_4 = h4_9;
    O_4 = h4_10;
    P_4 = h4_11;
    Q_4 = h4_12;
    R_4 = h4_13;
    S_4 = h4_14;
    T_4 = h4_15;
    U_4 = h4_16;
    V_4 = h4_17;
    W_4 = h4_18;
    X_4 = h4_19;
    Y_4 = h4_20;
    Z_4 = h4_21;
    A1_4 = h4_22;
    B1_4 = h4_23;
    C1_4 = h4_24;
    D1_4 = h4_25;
    B_4 = h4_26;
    D_4 = h4_27;
    F_4 = h4_28;
    H_4 = h4_29;
    if (!1)
        abort ();
    h5_0 = A_4;
    h5_1 = C_4;
    h5_2 = E_4;
    h5_3 = G_4;
    h5_4 = I_4;
    h5_5 = J_4;
    h5_6 = K_4;
    h5_7 = L_4;
    h5_8 = M_4;
    h5_9 = N_4;
    h5_10 = O_4;
    h5_11 = P_4;
    h5_12 = Q_4;
    h5_13 = R_4;
    h5_14 = S_4;
    h5_15 = T_4;
    h5_16 = U_4;
    h5_17 = V_4;
    h5_18 = W_4;
    h5_19 = X_4;
    h5_20 = Y_4;
    h5_21 = Z_4;
    h5_22 = A1_4;
    h5_23 = B1_4;
    h5_24 = C1_4;
    h5_25 = D1_4;
    h5_26 = B_4;
    h5_27 = D_4;
    h5_28 = F_4;
    h5_29 = H_4;
    A_7 = h5_0;
    C_7 = h5_1;
    E_7 = h5_2;
    G_7 = h5_3;
    I_7 = h5_4;
    J_7 = h5_5;
    K_7 = h5_6;
    L_7 = h5_7;
    M_7 = h5_8;
    N_7 = h5_9;
    O_7 = h5_10;
    P_7 = h5_11;
    Q_7 = h5_12;
    R_7 = h5_13;
    S_7 = h5_14;
    T_7 = h5_15;
    U_7 = h5_16;
    V_7 = h5_17;
    W_7 = h5_18;
    X_7 = h5_19;
    Y_7 = h5_20;
    Z_7 = h5_21;
    A1_7 = h5_22;
    B1_7 = h5_23;
    C1_7 = h5_24;
    D1_7 = h5_25;
    B_7 = h5_26;
    D_7 = h5_27;
    F_7 = h5_28;
    H_7 = h5_29;
    if (!((U_7 + (-1 * F_7)) <= -1))
        abort ();
    h8_0 = A_7;
    h8_1 = C_7;
    h8_2 = E_7;
    h8_3 = G_7;
    h8_4 = I_7;
    h8_5 = J_7;
    h8_6 = K_7;
    h8_7 = L_7;
    h8_8 = M_7;
    h8_9 = N_7;
    h8_10 = O_7;
    h8_11 = P_7;
    h8_12 = Q_7;
    h8_13 = R_7;
    h8_14 = S_7;
    h8_15 = T_7;
    h8_16 = U_7;
    h8_17 = V_7;
    h8_18 = W_7;
    h8_19 = X_7;
    h8_20 = Y_7;
    h8_21 = Z_7;
    h8_22 = A1_7;
    h8_23 = B1_7;
    h8_24 = C1_7;
    h8_25 = D1_7;
    h8_26 = B_7;
    h8_27 = D_7;
    h8_28 = F_7;
    h8_29 = H_7;
    A_77 = h8_0;
    C_77 = h8_1;
    E_77 = h8_2;
    G_77 = h8_3;
    I_77 = h8_4;
    J_77 = h8_5;
    K_77 = h8_6;
    L_77 = h8_7;
    M_77 = h8_8;
    N_77 = h8_9;
    O_77 = h8_10;
    P_77 = h8_11;
    Q_77 = h8_12;
    R_77 = h8_13;
    S_77 = h8_14;
    T_77 = h8_15;
    U_77 = h8_16;
    V_77 = h8_17;
    W_77 = h8_18;
    X_77 = h8_19;
    Y_77 = h8_20;
    Z_77 = h8_21;
    A1_77 = h8_22;
    B1_77 = h8_23;
    C1_77 = h8_24;
    D1_77 = h8_25;
    B_77 = h8_26;
    D_77 = h8_27;
    F_77 = h8_28;
    H_77 = h8_29;
    if (!1)
        abort ();
    goto main_error;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  h15:
    goto h15;
  h71:
    goto h71;
  h12:
    goto h12;
  h38:
    goto h38;
  h74:
    goto h74;
  h73:
    goto h73;
  h76:
    goto h76;
  h25:
    goto h25;
  h36:
    goto h36;
  h40:
    goto h40;
  h29:
    goto h29;
  h22:
    goto h22;
  h24:
    goto h24;
  h17:
    goto h17;
  h54:
    goto h54;
  h37:
    goto h37;
  h61:
    goto h61;
  h66:
    goto h66;
  h64:
    goto h64;
  h9:
    goto h9;
  h28:
    goto h28;
  h18:
    goto h18;
  h35:
    goto h35;
  h39:
    goto h39;
  h33:
    goto h33;
  h44:
    goto h44;
  h56:
    goto h56;
  h67:
    goto h67;
  h59:
    goto h59;
  h68:
    goto h68;
  h69:
    goto h69;
  h21:
    goto h21;
  h72:
    goto h72;
  h13:
    goto h13;
  h11:
    goto h11;
  h48:
    goto h48;
  h45:
    goto h45;
  h52:
    goto h52;
  h47:
    goto h47;
  h19:
    goto h19;
  h62:
    goto h62;
  h26:
    goto h26;
  h65:
    goto h65;
  h75:
    goto h75;
  h50:
    goto h50;
  h20:
    goto h20;
  h46:
    goto h46;
  h60:
    goto h60;
  h78:
    goto h78;
  h70:
    goto h70;
  h53:
    goto h53;
  h10:
    goto h10;
  h58:
    goto h58;
  h14:
    goto h14;
  h16:
    goto h16;
  h49:
    goto h49;
  h41:
    goto h41;
  h57:
    goto h57;
  h42:
    goto h42;
  h6:
    goto h6;
  h23:
    goto h23;
  h7:
    goto h7;
  h27:
    goto h27;
  h77:
    goto h77;
  h63:
    goto h63;
  h43:
    goto h43;
  h34:
    goto h34;
  h51:
    goto h51;
  h55:
    goto h55;

    // return expression

}

