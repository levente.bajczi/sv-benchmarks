// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: tricera-benchmarks/test_locks_6.c-1.smt2.gz_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "test_locks_6.c-1.smt2.gz_000_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int inv_main77_0;
    int inv_main77_1;
    int inv_main77_2;
    int inv_main77_3;
    int inv_main77_4;
    int inv_main77_5;
    int inv_main77_6;
    int inv_main77_7;
    int inv_main77_8;
    int inv_main77_9;
    int inv_main77_10;
    int inv_main77_11;
    int inv_main77_12;
    int inv_main71_0;
    int inv_main71_1;
    int inv_main71_2;
    int inv_main71_3;
    int inv_main71_4;
    int inv_main71_5;
    int inv_main71_6;
    int inv_main71_7;
    int inv_main71_8;
    int inv_main71_9;
    int inv_main71_10;
    int inv_main71_11;
    int inv_main71_12;
    int inv_main21_0;
    int inv_main21_1;
    int inv_main21_2;
    int inv_main21_3;
    int inv_main21_4;
    int inv_main21_5;
    int inv_main21_6;
    int inv_main21_7;
    int inv_main21_8;
    int inv_main21_9;
    int inv_main21_10;
    int inv_main21_11;
    int inv_main21_12;
    int inv_main59_0;
    int inv_main59_1;
    int inv_main59_2;
    int inv_main59_3;
    int inv_main59_4;
    int inv_main59_5;
    int inv_main59_6;
    int inv_main59_7;
    int inv_main59_8;
    int inv_main59_9;
    int inv_main59_10;
    int inv_main59_11;
    int inv_main59_12;
    int inv_main50_0;
    int inv_main50_1;
    int inv_main50_2;
    int inv_main50_3;
    int inv_main50_4;
    int inv_main50_5;
    int inv_main50_6;
    int inv_main50_7;
    int inv_main50_8;
    int inv_main50_9;
    int inv_main50_10;
    int inv_main50_11;
    int inv_main50_12;
    int inv_main88_0;
    int inv_main88_1;
    int inv_main88_2;
    int inv_main88_3;
    int inv_main88_4;
    int inv_main88_5;
    int inv_main88_6;
    int inv_main88_7;
    int inv_main88_8;
    int inv_main88_9;
    int inv_main88_10;
    int inv_main88_11;
    int inv_main88_12;
    int inv_main53_0;
    int inv_main53_1;
    int inv_main53_2;
    int inv_main53_3;
    int inv_main53_4;
    int inv_main53_5;
    int inv_main53_6;
    int inv_main53_7;
    int inv_main53_8;
    int inv_main53_9;
    int inv_main53_10;
    int inv_main53_11;
    int inv_main53_12;
    int inv_main38_0;
    int inv_main38_1;
    int inv_main38_2;
    int inv_main38_3;
    int inv_main38_4;
    int inv_main38_5;
    int inv_main38_6;
    int inv_main38_7;
    int inv_main38_8;
    int inv_main38_9;
    int inv_main38_10;
    int inv_main38_11;
    int inv_main38_12;
    int inv_main65_0;
    int inv_main65_1;
    int inv_main65_2;
    int inv_main65_3;
    int inv_main65_4;
    int inv_main65_5;
    int inv_main65_6;
    int inv_main65_7;
    int inv_main65_8;
    int inv_main65_9;
    int inv_main65_10;
    int inv_main65_11;
    int inv_main65_12;
    int inv_main44_0;
    int inv_main44_1;
    int inv_main44_2;
    int inv_main44_3;
    int inv_main44_4;
    int inv_main44_5;
    int inv_main44_6;
    int inv_main44_7;
    int inv_main44_8;
    int inv_main44_9;
    int inv_main44_10;
    int inv_main44_11;
    int inv_main44_12;
    int A_1;
    int B_1;
    int C_1;
    int D_1;
    int E_1;
    int F_1;
    int G_1;
    int H_1;
    int I_1;
    int J_1;
    int K_1;
    int L_1;
    int M_1;
    int A_2;
    int B_2;
    int C_2;
    int D_2;
    int E_2;
    int F_2;
    int G_2;
    int H_2;
    int I_2;
    int J_2;
    int K_2;
    int L_2;
    int M_2;
    int N_2;
    int A_3;
    int B_3;
    int C_3;
    int D_3;
    int E_3;
    int F_3;
    int G_3;
    int H_3;
    int I_3;
    int J_3;
    int K_3;
    int L_3;
    int M_3;
    int A_4;
    int B_4;
    int C_4;
    int D_4;
    int E_4;
    int F_4;
    int G_4;
    int H_4;
    int I_4;
    int J_4;
    int K_4;
    int L_4;
    int M_4;
    int N_4;
    int A_5;
    int B_5;
    int C_5;
    int D_5;
    int E_5;
    int F_5;
    int G_5;
    int H_5;
    int I_5;
    int J_5;
    int K_5;
    int L_5;
    int M_5;
    int A_6;
    int B_6;
    int C_6;
    int D_6;
    int E_6;
    int F_6;
    int G_6;
    int H_6;
    int I_6;
    int J_6;
    int K_6;
    int L_6;
    int M_6;
    int N_6;
    int A_7;
    int B_7;
    int C_7;
    int D_7;
    int E_7;
    int F_7;
    int G_7;
    int H_7;
    int I_7;
    int J_7;
    int K_7;
    int L_7;
    int M_7;
    int N_7;
    int O_7;
    int A_8;
    int B_8;
    int C_8;
    int D_8;
    int E_8;
    int F_8;
    int G_8;
    int H_8;
    int I_8;
    int J_8;
    int K_8;
    int L_8;
    int M_8;
    int N_8;
    int A_9;
    int B_9;
    int C_9;
    int D_9;
    int E_9;
    int F_9;
    int G_9;
    int H_9;
    int I_9;
    int J_9;
    int K_9;
    int L_9;
    int M_9;
    int N_9;
    int A_10;
    int B_10;
    int C_10;
    int D_10;
    int E_10;
    int F_10;
    int G_10;
    int H_10;
    int I_10;
    int J_10;
    int K_10;
    int L_10;
    int M_10;
    int A_11;
    int B_11;
    int C_11;
    int D_11;
    int E_11;
    int F_11;
    int G_11;
    int H_11;
    int I_11;
    int J_11;
    int K_11;
    int L_11;
    int M_11;
    int N_11;
    int O_11;
    int A_12;
    int B_12;
    int C_12;
    int D_12;
    int E_12;
    int F_12;
    int G_12;
    int H_12;
    int I_12;
    int J_12;
    int K_12;
    int L_12;
    int M_12;
    int N_12;
    int A_13;
    int B_13;
    int C_13;
    int D_13;
    int E_13;
    int F_13;
    int G_13;
    int H_13;
    int I_13;
    int J_13;
    int K_13;
    int L_13;
    int M_13;
    int N_13;
    int A_14;
    int B_14;
    int C_14;
    int D_14;
    int E_14;
    int F_14;
    int G_14;
    int H_14;
    int I_14;
    int J_14;
    int K_14;
    int L_14;
    int M_14;
    int A_15;
    int B_15;
    int C_15;
    int D_15;
    int E_15;
    int F_15;
    int G_15;
    int H_15;
    int I_15;
    int J_15;
    int K_15;
    int L_15;
    int M_15;
    int A_16;
    int B_16;
    int C_16;
    int D_16;
    int E_16;
    int F_16;
    int G_16;
    int H_16;
    int I_16;
    int J_16;
    int K_16;
    int L_16;
    int M_16;
    int N_16;
    int A_17;
    int B_17;
    int C_17;
    int D_17;
    int E_17;
    int F_17;
    int G_17;
    int H_17;
    int I_17;
    int J_17;
    int K_17;
    int L_17;
    int M_17;
    int A_18;
    int B_18;
    int C_18;
    int D_18;
    int E_18;
    int F_18;
    int G_18;
    int H_18;
    int I_18;
    int J_18;
    int K_18;
    int L_18;
    int M_18;
    int A_19;
    int B_19;
    int C_19;
    int D_19;
    int E_19;
    int F_19;
    int G_19;
    int H_19;
    int I_19;
    int J_19;
    int K_19;
    int L_19;
    int M_19;
    int A_20;
    int B_20;
    int C_20;
    int D_20;
    int E_20;
    int F_20;
    int G_20;
    int H_20;
    int I_20;
    int J_20;
    int K_20;
    int L_20;
    int M_20;
    int A_21;
    int B_21;
    int C_21;
    int D_21;
    int E_21;
    int F_21;
    int G_21;
    int H_21;
    int I_21;
    int J_21;
    int K_21;
    int L_21;
    int M_21;
    int A_22;
    int B_22;
    int C_22;
    int D_22;
    int E_22;
    int F_22;
    int G_22;
    int H_22;
    int I_22;
    int J_22;
    int K_22;
    int L_22;
    int M_22;
    int A_23;
    int B_23;
    int C_23;
    int D_23;
    int E_23;
    int F_23;
    int G_23;
    int H_23;
    int I_23;
    int J_23;
    int K_23;
    int L_23;
    int M_23;
    int N_23;
    int O_23;
    int P_23;
    int Q_23;
    int R_23;
    int S_23;
    int T_23;
    int A_24;
    int B_24;
    int C_24;
    int D_24;
    int E_24;
    int F_24;
    int G_24;
    int H_24;
    int I_24;
    int J_24;
    int K_24;
    int L_24;
    int M_24;
    int N_24;
    int O_24;
    int P_24;
    int Q_24;
    int R_24;
    int S_24;
    int T_24;
    int A_25;
    int B_25;
    int C_25;
    int D_25;
    int E_25;
    int F_25;
    int G_25;
    int H_25;
    int I_25;
    int J_25;
    int K_25;
    int L_25;
    int M_25;
    int N_25;
    int O_25;
    int P_25;
    int Q_25;
    int R_25;
    int S_25;
    int T_25;
    int A_26;
    int B_26;
    int C_26;
    int D_26;
    int E_26;
    int F_26;
    int G_26;
    int H_26;
    int I_26;
    int J_26;
    int K_26;
    int L_26;
    int M_26;
    int N_26;
    int O_26;
    int P_26;
    int Q_26;
    int R_26;
    int S_26;
    int T_26;
    int A_27;
    int B_27;
    int C_27;
    int D_27;
    int E_27;
    int F_27;
    int G_27;
    int H_27;
    int I_27;
    int J_27;
    int K_27;
    int L_27;
    int M_27;
    int A_28;
    int B_28;
    int C_28;
    int D_28;
    int E_28;
    int F_28;
    int G_28;
    int H_28;
    int I_28;
    int J_28;
    int K_28;
    int L_28;
    int M_28;
    int N_28;
    int A_29;
    int B_29;
    int C_29;
    int D_29;
    int E_29;
    int F_29;
    int G_29;
    int H_29;
    int I_29;
    int J_29;
    int K_29;
    int L_29;
    int M_29;
    int A_30;
    int B_30;
    int C_30;
    int D_30;
    int E_30;
    int F_30;
    int G_30;
    int H_30;
    int I_30;
    int J_30;
    int K_30;
    int L_30;
    int M_30;
    int A_31;
    int B_31;
    int C_31;
    int D_31;
    int E_31;
    int F_31;
    int G_31;
    int H_31;
    int I_31;
    int J_31;
    int K_31;
    int L_31;
    int M_31;
    int N_31;
    int A_32;
    int B_32;
    int C_32;
    int D_32;
    int E_32;
    int F_32;
    int G_32;
    int H_32;
    int I_32;
    int J_32;
    int K_32;
    int L_32;
    int M_32;



    // main logic
    goto main_init;

  main_init:
    if (!1)
        abort ();
    A_29 = __VERIFIER_nondet_int ();
    B_29 = __VERIFIER_nondet_int ();
    C_29 = __VERIFIER_nondet_int ();
    D_29 = __VERIFIER_nondet_int ();
    E_29 = __VERIFIER_nondet_int ();
    F_29 = __VERIFIER_nondet_int ();
    G_29 = __VERIFIER_nondet_int ();
    H_29 = __VERIFIER_nondet_int ();
    I_29 = __VERIFIER_nondet_int ();
    J_29 = __VERIFIER_nondet_int ();
    K_29 = __VERIFIER_nondet_int ();
    L_29 = __VERIFIER_nondet_int ();
    M_29 = __VERIFIER_nondet_int ();
    if (!1)
        abort ();
    inv_main21_0 = E_29;
    inv_main21_1 = B_29;
    inv_main21_2 = L_29;
    inv_main21_3 = I_29;
    inv_main21_4 = H_29;
    inv_main21_5 = M_29;
    inv_main21_6 = A_29;
    inv_main21_7 = J_29;
    inv_main21_8 = D_29;
    inv_main21_9 = G_29;
    inv_main21_10 = F_29;
    inv_main21_11 = C_29;
    inv_main21_12 = K_29;
    goto inv_main21;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  inv_main77:
    switch (__VERIFIER_nondet_int ())
      {
      case 0:
          C_27 = inv_main77_0;
          D_27 = inv_main77_1;
          M_27 = inv_main77_2;
          L_27 = inv_main77_3;
          K_27 = inv_main77_4;
          J_27 = inv_main77_5;
          I_27 = inv_main77_6;
          A_27 = inv_main77_7;
          H_27 = inv_main77_8;
          E_27 = inv_main77_9;
          B_27 = inv_main77_10;
          F_27 = inv_main77_11;
          G_27 = inv_main77_12;
          if (!(B_27 == 0))
              abort ();
          inv_main21_0 = C_27;
          inv_main21_1 = D_27;
          inv_main21_2 = M_27;
          inv_main21_3 = L_27;
          inv_main21_4 = K_27;
          inv_main21_5 = J_27;
          inv_main21_6 = I_27;
          inv_main21_7 = A_27;
          inv_main21_8 = H_27;
          inv_main21_9 = E_27;
          inv_main21_10 = B_27;
          inv_main21_11 = F_27;
          inv_main21_12 = G_27;
          goto inv_main21;

      case 1:
          A_28 = __VERIFIER_nondet_int ();
          I_28 = inv_main77_0;
          F_28 = inv_main77_1;
          M_28 = inv_main77_2;
          C_28 = inv_main77_3;
          G_28 = inv_main77_4;
          N_28 = inv_main77_5;
          L_28 = inv_main77_6;
          D_28 = inv_main77_7;
          K_28 = inv_main77_8;
          J_28 = inv_main77_9;
          B_28 = inv_main77_10;
          H_28 = inv_main77_11;
          E_28 = inv_main77_12;
          if (!((H_28 == 1) && (!(B_28 == 0)) && (A_28 == 0)))
              abort ();
          inv_main21_0 = I_28;
          inv_main21_1 = F_28;
          inv_main21_2 = M_28;
          inv_main21_3 = C_28;
          inv_main21_4 = G_28;
          inv_main21_5 = N_28;
          inv_main21_6 = L_28;
          inv_main21_7 = D_28;
          inv_main21_8 = K_28;
          inv_main21_9 = J_28;
          inv_main21_10 = B_28;
          inv_main21_11 = A_28;
          inv_main21_12 = E_28;
          goto inv_main21;

      case 2:
          E_22 = inv_main77_0;
          F_22 = inv_main77_1;
          A_22 = inv_main77_2;
          I_22 = inv_main77_3;
          J_22 = inv_main77_4;
          H_22 = inv_main77_5;
          D_22 = inv_main77_6;
          M_22 = inv_main77_7;
          C_22 = inv_main77_8;
          G_22 = inv_main77_9;
          K_22 = inv_main77_10;
          B_22 = inv_main77_11;
          L_22 = inv_main77_12;
          if (!((!(B_22 == 1)) && (!(K_22 == 0))))
              abort ();
          inv_main88_0 = E_22;
          inv_main88_1 = F_22;
          inv_main88_2 = A_22;
          inv_main88_3 = I_22;
          inv_main88_4 = J_22;
          inv_main88_5 = H_22;
          inv_main88_6 = D_22;
          inv_main88_7 = M_22;
          inv_main88_8 = C_22;
          inv_main88_9 = G_22;
          inv_main88_10 = K_22;
          inv_main88_11 = B_22;
          inv_main88_12 = L_22;
          C_32 = inv_main88_0;
          H_32 = inv_main88_1;
          D_32 = inv_main88_2;
          E_32 = inv_main88_3;
          K_32 = inv_main88_4;
          L_32 = inv_main88_5;
          A_32 = inv_main88_6;
          B_32 = inv_main88_7;
          I_32 = inv_main88_8;
          J_32 = inv_main88_9;
          F_32 = inv_main88_10;
          G_32 = inv_main88_11;
          M_32 = inv_main88_12;
          if (!1)
              abort ();
          goto main_error;

      default:
          abort ();
      }
  inv_main71:
    switch (__VERIFIER_nondet_int ())
      {
      case 0:
          I_1 = inv_main71_0;
          K_1 = inv_main71_1;
          C_1 = inv_main71_2;
          D_1 = inv_main71_3;
          G_1 = inv_main71_4;
          A_1 = inv_main71_5;
          B_1 = inv_main71_6;
          H_1 = inv_main71_7;
          F_1 = inv_main71_8;
          M_1 = inv_main71_9;
          E_1 = inv_main71_10;
          L_1 = inv_main71_11;
          J_1 = inv_main71_12;
          if (!(F_1 == 0))
              abort ();
          inv_main77_0 = I_1;
          inv_main77_1 = K_1;
          inv_main77_2 = C_1;
          inv_main77_3 = D_1;
          inv_main77_4 = G_1;
          inv_main77_5 = A_1;
          inv_main77_6 = B_1;
          inv_main77_7 = H_1;
          inv_main77_8 = F_1;
          inv_main77_9 = M_1;
          inv_main77_10 = E_1;
          inv_main77_11 = L_1;
          inv_main77_12 = J_1;
          goto inv_main77;

      case 1:
          M_2 = __VERIFIER_nondet_int ();
          I_2 = inv_main71_0;
          F_2 = inv_main71_1;
          J_2 = inv_main71_2;
          G_2 = inv_main71_3;
          N_2 = inv_main71_4;
          E_2 = inv_main71_5;
          C_2 = inv_main71_6;
          D_2 = inv_main71_7;
          L_2 = inv_main71_8;
          H_2 = inv_main71_9;
          A_2 = inv_main71_10;
          B_2 = inv_main71_11;
          K_2 = inv_main71_12;
          if (!((!(L_2 == 0)) && (H_2 == 1) && (M_2 == 0)))
              abort ();
          inv_main77_0 = I_2;
          inv_main77_1 = F_2;
          inv_main77_2 = J_2;
          inv_main77_3 = G_2;
          inv_main77_4 = N_2;
          inv_main77_5 = E_2;
          inv_main77_6 = C_2;
          inv_main77_7 = D_2;
          inv_main77_8 = L_2;
          inv_main77_9 = M_2;
          inv_main77_10 = A_2;
          inv_main77_11 = B_2;
          inv_main77_12 = K_2;
          goto inv_main77;

      case 2:
          H_21 = inv_main71_0;
          M_21 = inv_main71_1;
          G_21 = inv_main71_2;
          A_21 = inv_main71_3;
          K_21 = inv_main71_4;
          J_21 = inv_main71_5;
          I_21 = inv_main71_6;
          F_21 = inv_main71_7;
          B_21 = inv_main71_8;
          L_21 = inv_main71_9;
          E_21 = inv_main71_10;
          D_21 = inv_main71_11;
          C_21 = inv_main71_12;
          if (!((!(B_21 == 0)) && (!(L_21 == 1))))
              abort ();
          inv_main88_0 = H_21;
          inv_main88_1 = M_21;
          inv_main88_2 = G_21;
          inv_main88_3 = A_21;
          inv_main88_4 = K_21;
          inv_main88_5 = J_21;
          inv_main88_6 = I_21;
          inv_main88_7 = F_21;
          inv_main88_8 = B_21;
          inv_main88_9 = L_21;
          inv_main88_10 = E_21;
          inv_main88_11 = D_21;
          inv_main88_12 = C_21;
          C_32 = inv_main88_0;
          H_32 = inv_main88_1;
          D_32 = inv_main88_2;
          E_32 = inv_main88_3;
          K_32 = inv_main88_4;
          L_32 = inv_main88_5;
          A_32 = inv_main88_6;
          B_32 = inv_main88_7;
          I_32 = inv_main88_8;
          J_32 = inv_main88_9;
          F_32 = inv_main88_10;
          G_32 = inv_main88_11;
          M_32 = inv_main88_12;
          if (!1)
              abort ();
          goto main_error;

      default:
          abort ();
      }
  inv_main21:
    switch (__VERIFIER_nondet_int ())
      {
      case 0:
          B_23 = __VERIFIER_nondet_int ();
          F_23 = __VERIFIER_nondet_int ();
          H_23 = __VERIFIER_nondet_int ();
          I_23 = __VERIFIER_nondet_int ();
          K_23 = __VERIFIER_nondet_int ();
          O_23 = __VERIFIER_nondet_int ();
          Q_23 = __VERIFIER_nondet_int ();
          J_23 = inv_main21_0;
          T_23 = inv_main21_1;
          C_23 = inv_main21_2;
          D_23 = inv_main21_3;
          R_23 = inv_main21_4;
          N_23 = inv_main21_5;
          S_23 = inv_main21_6;
          G_23 = inv_main21_7;
          E_23 = inv_main21_8;
          A_23 = inv_main21_9;
          P_23 = inv_main21_10;
          M_23 = inv_main21_11;
          L_23 = inv_main21_12;
          if (!
              ((B_23 == 0) && (F_23 == 1) && (Q_23 == 0) && (O_23 == 0)
               && (K_23 == 1) && (!(J_23 == 0)) && (I_23 == 0)
               && (!(H_23 == 0)) && (!(C_23 == 0))))
              abort ();
          inv_main38_0 = J_23;
          inv_main38_1 = K_23;
          inv_main38_2 = C_23;
          inv_main38_3 = F_23;
          inv_main38_4 = R_23;
          inv_main38_5 = I_23;
          inv_main38_6 = S_23;
          inv_main38_7 = Q_23;
          inv_main38_8 = E_23;
          inv_main38_9 = B_23;
          inv_main38_10 = P_23;
          inv_main38_11 = O_23;
          inv_main38_12 = H_23;
          goto inv_main38;

      case 1:
          E_24 = __VERIFIER_nondet_int ();
          F_24 = __VERIFIER_nondet_int ();
          J_24 = __VERIFIER_nondet_int ();
          K_24 = __VERIFIER_nondet_int ();
          O_24 = __VERIFIER_nondet_int ();
          R_24 = __VERIFIER_nondet_int ();
          T_24 = __VERIFIER_nondet_int ();
          M_24 = inv_main21_0;
          P_24 = inv_main21_1;
          I_24 = inv_main21_2;
          Q_24 = inv_main21_3;
          L_24 = inv_main21_4;
          G_24 = inv_main21_5;
          B_24 = inv_main21_6;
          D_24 = inv_main21_7;
          C_24 = inv_main21_8;
          H_24 = inv_main21_9;
          A_24 = inv_main21_10;
          N_24 = inv_main21_11;
          S_24 = inv_main21_12;
          if (!
              ((F_24 == 0) && (T_24 == 0) && (R_24 == 0) && (!(O_24 == 0))
               && (!(M_24 == 0)) && (K_24 == 0) && (J_24 == 0) && (I_24 == 0)
               && (E_24 == 1)))
              abort ();
          inv_main38_0 = M_24;
          inv_main38_1 = E_24;
          inv_main38_2 = I_24;
          inv_main38_3 = J_24;
          inv_main38_4 = L_24;
          inv_main38_5 = T_24;
          inv_main38_6 = B_24;
          inv_main38_7 = F_24;
          inv_main38_8 = C_24;
          inv_main38_9 = K_24;
          inv_main38_10 = A_24;
          inv_main38_11 = R_24;
          inv_main38_12 = O_24;
          goto inv_main38;

      case 2:
          A_25 = __VERIFIER_nondet_int ();
          E_25 = __VERIFIER_nondet_int ();
          N_25 = __VERIFIER_nondet_int ();
          O_25 = __VERIFIER_nondet_int ();
          R_25 = __VERIFIER_nondet_int ();
          S_25 = __VERIFIER_nondet_int ();
          T_25 = __VERIFIER_nondet_int ();
          P_25 = inv_main21_0;
          H_25 = inv_main21_1;
          C_25 = inv_main21_2;
          M_25 = inv_main21_3;
          L_25 = inv_main21_4;
          G_25 = inv_main21_5;
          B_25 = inv_main21_6;
          D_25 = inv_main21_7;
          K_25 = inv_main21_8;
          J_25 = inv_main21_9;
          F_25 = inv_main21_10;
          Q_25 = inv_main21_11;
          I_25 = inv_main21_12;
          if (!
              ((!(C_25 == 0)) && (A_25 == 0) && (T_25 == 0) && (S_25 == 0)
               && (R_25 == 0) && (P_25 == 0) && (O_25 == 0) && (N_25 == 1)
               && (!(E_25 == 0))))
              abort ();
          inv_main38_0 = P_25;
          inv_main38_1 = A_25;
          inv_main38_2 = C_25;
          inv_main38_3 = N_25;
          inv_main38_4 = L_25;
          inv_main38_5 = R_25;
          inv_main38_6 = B_25;
          inv_main38_7 = O_25;
          inv_main38_8 = K_25;
          inv_main38_9 = T_25;
          inv_main38_10 = F_25;
          inv_main38_11 = S_25;
          inv_main38_12 = E_25;
          goto inv_main38;

      case 3:
          H_26 = __VERIFIER_nondet_int ();
          I_26 = __VERIFIER_nondet_int ();
          J_26 = __VERIFIER_nondet_int ();
          N_26 = __VERIFIER_nondet_int ();
          R_26 = __VERIFIER_nondet_int ();
          S_26 = __VERIFIER_nondet_int ();
          T_26 = __VERIFIER_nondet_int ();
          L_26 = inv_main21_0;
          B_26 = inv_main21_1;
          F_26 = inv_main21_2;
          P_26 = inv_main21_3;
          K_26 = inv_main21_4;
          O_26 = inv_main21_5;
          D_26 = inv_main21_6;
          G_26 = inv_main21_7;
          E_26 = inv_main21_8;
          M_26 = inv_main21_9;
          C_26 = inv_main21_10;
          Q_26 = inv_main21_11;
          A_26 = inv_main21_12;
          if (!
              ((T_26 == 0) && (S_26 == 0) && (!(R_26 == 0)) && (N_26 == 0)
               && (L_26 == 0) && (J_26 == 0) && (I_26 == 0) && (H_26 == 0)
               && (F_26 == 0)))
              abort ();
          inv_main38_0 = L_26;
          inv_main38_1 = J_26;
          inv_main38_2 = F_26;
          inv_main38_3 = H_26;
          inv_main38_4 = K_26;
          inv_main38_5 = N_26;
          inv_main38_6 = D_26;
          inv_main38_7 = I_26;
          inv_main38_8 = E_26;
          inv_main38_9 = T_26;
          inv_main38_10 = C_26;
          inv_main38_11 = S_26;
          inv_main38_12 = R_26;
          goto inv_main38;

      default:
          abort ();
      }
  inv_main59:
    switch (__VERIFIER_nondet_int ())
      {
      case 0:
          K_3 = inv_main59_0;
          A_3 = inv_main59_1;
          H_3 = inv_main59_2;
          M_3 = inv_main59_3;
          J_3 = inv_main59_4;
          D_3 = inv_main59_5;
          L_3 = inv_main59_6;
          B_3 = inv_main59_7;
          G_3 = inv_main59_8;
          F_3 = inv_main59_9;
          I_3 = inv_main59_10;
          E_3 = inv_main59_11;
          C_3 = inv_main59_12;
          if (!(J_3 == 0))
              abort ();
          inv_main65_0 = K_3;
          inv_main65_1 = A_3;
          inv_main65_2 = H_3;
          inv_main65_3 = M_3;
          inv_main65_4 = J_3;
          inv_main65_5 = D_3;
          inv_main65_6 = L_3;
          inv_main65_7 = B_3;
          inv_main65_8 = G_3;
          inv_main65_9 = F_3;
          inv_main65_10 = I_3;
          inv_main65_11 = E_3;
          inv_main65_12 = C_3;
          goto inv_main65;

      case 1:
          E_4 = __VERIFIER_nondet_int ();
          L_4 = inv_main59_0;
          J_4 = inv_main59_1;
          F_4 = inv_main59_2;
          N_4 = inv_main59_3;
          I_4 = inv_main59_4;
          M_4 = inv_main59_5;
          H_4 = inv_main59_6;
          D_4 = inv_main59_7;
          A_4 = inv_main59_8;
          K_4 = inv_main59_9;
          G_4 = inv_main59_10;
          C_4 = inv_main59_11;
          B_4 = inv_main59_12;
          if (!((!(I_4 == 0)) && (E_4 == 0) && (M_4 == 1)))
              abort ();
          inv_main65_0 = L_4;
          inv_main65_1 = J_4;
          inv_main65_2 = F_4;
          inv_main65_3 = N_4;
          inv_main65_4 = I_4;
          inv_main65_5 = E_4;
          inv_main65_6 = H_4;
          inv_main65_7 = D_4;
          inv_main65_8 = A_4;
          inv_main65_9 = K_4;
          inv_main65_10 = G_4;
          inv_main65_11 = C_4;
          inv_main65_12 = B_4;
          goto inv_main65;

      case 2:
          I_19 = inv_main59_0;
          L_19 = inv_main59_1;
          M_19 = inv_main59_2;
          E_19 = inv_main59_3;
          G_19 = inv_main59_4;
          F_19 = inv_main59_5;
          J_19 = inv_main59_6;
          B_19 = inv_main59_7;
          K_19 = inv_main59_8;
          D_19 = inv_main59_9;
          H_19 = inv_main59_10;
          C_19 = inv_main59_11;
          A_19 = inv_main59_12;
          if (!((!(F_19 == 1)) && (!(G_19 == 0))))
              abort ();
          inv_main88_0 = I_19;
          inv_main88_1 = L_19;
          inv_main88_2 = M_19;
          inv_main88_3 = E_19;
          inv_main88_4 = G_19;
          inv_main88_5 = F_19;
          inv_main88_6 = J_19;
          inv_main88_7 = B_19;
          inv_main88_8 = K_19;
          inv_main88_9 = D_19;
          inv_main88_10 = H_19;
          inv_main88_11 = C_19;
          inv_main88_12 = A_19;
          C_32 = inv_main88_0;
          H_32 = inv_main88_1;
          D_32 = inv_main88_2;
          E_32 = inv_main88_3;
          K_32 = inv_main88_4;
          L_32 = inv_main88_5;
          A_32 = inv_main88_6;
          B_32 = inv_main88_7;
          I_32 = inv_main88_8;
          J_32 = inv_main88_9;
          F_32 = inv_main88_10;
          G_32 = inv_main88_11;
          M_32 = inv_main88_12;
          if (!1)
              abort ();
          goto main_error;

      default:
          abort ();
      }
  inv_main50:
    switch (__VERIFIER_nondet_int ())
      {
      case 0:
          M_15 = inv_main50_0;
          G_15 = inv_main50_1;
          A_15 = inv_main50_2;
          L_15 = inv_main50_3;
          B_15 = inv_main50_4;
          H_15 = inv_main50_5;
          D_15 = inv_main50_6;
          K_15 = inv_main50_7;
          E_15 = inv_main50_8;
          F_15 = inv_main50_9;
          I_15 = inv_main50_10;
          J_15 = inv_main50_11;
          C_15 = inv_main50_12;
          if (!(M_15 == 0))
              abort ();
          inv_main53_0 = M_15;
          inv_main53_1 = G_15;
          inv_main53_2 = A_15;
          inv_main53_3 = L_15;
          inv_main53_4 = B_15;
          inv_main53_5 = H_15;
          inv_main53_6 = D_15;
          inv_main53_7 = K_15;
          inv_main53_8 = E_15;
          inv_main53_9 = F_15;
          inv_main53_10 = I_15;
          inv_main53_11 = J_15;
          inv_main53_12 = C_15;
          goto inv_main53;

      case 1:
          L_16 = __VERIFIER_nondet_int ();
          H_16 = inv_main50_0;
          F_16 = inv_main50_1;
          B_16 = inv_main50_2;
          M_16 = inv_main50_3;
          D_16 = inv_main50_4;
          J_16 = inv_main50_5;
          G_16 = inv_main50_6;
          I_16 = inv_main50_7;
          E_16 = inv_main50_8;
          C_16 = inv_main50_9;
          N_16 = inv_main50_10;
          A_16 = inv_main50_11;
          K_16 = inv_main50_12;
          if (!((!(H_16 == 0)) && (F_16 == 1) && (L_16 == 0)))
              abort ();
          inv_main53_0 = H_16;
          inv_main53_1 = L_16;
          inv_main53_2 = B_16;
          inv_main53_3 = M_16;
          inv_main53_4 = D_16;
          inv_main53_5 = J_16;
          inv_main53_6 = G_16;
          inv_main53_7 = I_16;
          inv_main53_8 = E_16;
          inv_main53_9 = C_16;
          inv_main53_10 = N_16;
          inv_main53_11 = A_16;
          inv_main53_12 = K_16;
          goto inv_main53;

      case 2:
          A_17 = inv_main50_0;
          K_17 = inv_main50_1;
          J_17 = inv_main50_2;
          G_17 = inv_main50_3;
          I_17 = inv_main50_4;
          F_17 = inv_main50_5;
          E_17 = inv_main50_6;
          D_17 = inv_main50_7;
          B_17 = inv_main50_8;
          C_17 = inv_main50_9;
          H_17 = inv_main50_10;
          L_17 = inv_main50_11;
          M_17 = inv_main50_12;
          if (!((!(A_17 == 0)) && (!(K_17 == 1))))
              abort ();
          inv_main88_0 = A_17;
          inv_main88_1 = K_17;
          inv_main88_2 = J_17;
          inv_main88_3 = G_17;
          inv_main88_4 = I_17;
          inv_main88_5 = F_17;
          inv_main88_6 = E_17;
          inv_main88_7 = D_17;
          inv_main88_8 = B_17;
          inv_main88_9 = C_17;
          inv_main88_10 = H_17;
          inv_main88_11 = L_17;
          inv_main88_12 = M_17;
          C_32 = inv_main88_0;
          H_32 = inv_main88_1;
          D_32 = inv_main88_2;
          E_32 = inv_main88_3;
          K_32 = inv_main88_4;
          L_32 = inv_main88_5;
          A_32 = inv_main88_6;
          B_32 = inv_main88_7;
          I_32 = inv_main88_8;
          J_32 = inv_main88_9;
          F_32 = inv_main88_10;
          G_32 = inv_main88_11;
          M_32 = inv_main88_12;
          if (!1)
              abort ();
          goto main_error;

      default:
          abort ();
      }
  inv_main53:
    switch (__VERIFIER_nondet_int ())
      {
      case 0:
          K_30 = inv_main53_0;
          B_30 = inv_main53_1;
          L_30 = inv_main53_2;
          E_30 = inv_main53_3;
          J_30 = inv_main53_4;
          D_30 = inv_main53_5;
          H_30 = inv_main53_6;
          I_30 = inv_main53_7;
          M_30 = inv_main53_8;
          G_30 = inv_main53_9;
          A_30 = inv_main53_10;
          F_30 = inv_main53_11;
          C_30 = inv_main53_12;
          if (!(L_30 == 0))
              abort ();
          inv_main59_0 = K_30;
          inv_main59_1 = B_30;
          inv_main59_2 = L_30;
          inv_main59_3 = E_30;
          inv_main59_4 = J_30;
          inv_main59_5 = D_30;
          inv_main59_6 = H_30;
          inv_main59_7 = I_30;
          inv_main59_8 = M_30;
          inv_main59_9 = G_30;
          inv_main59_10 = A_30;
          inv_main59_11 = F_30;
          inv_main59_12 = C_30;
          goto inv_main59;

      case 1:
          E_31 = __VERIFIER_nondet_int ();
          G_31 = inv_main53_0;
          N_31 = inv_main53_1;
          F_31 = inv_main53_2;
          K_31 = inv_main53_3;
          H_31 = inv_main53_4;
          M_31 = inv_main53_5;
          A_31 = inv_main53_6;
          C_31 = inv_main53_7;
          B_31 = inv_main53_8;
          D_31 = inv_main53_9;
          I_31 = inv_main53_10;
          L_31 = inv_main53_11;
          J_31 = inv_main53_12;
          if (!((!(F_31 == 0)) && (E_31 == 0) && (K_31 == 1)))
              abort ();
          inv_main59_0 = G_31;
          inv_main59_1 = N_31;
          inv_main59_2 = F_31;
          inv_main59_3 = E_31;
          inv_main59_4 = H_31;
          inv_main59_5 = M_31;
          inv_main59_6 = A_31;
          inv_main59_7 = C_31;
          inv_main59_8 = B_31;
          inv_main59_9 = D_31;
          inv_main59_10 = I_31;
          inv_main59_11 = L_31;
          inv_main59_12 = J_31;
          goto inv_main59;

      case 2:
          C_18 = inv_main53_0;
          L_18 = inv_main53_1;
          B_18 = inv_main53_2;
          A_18 = inv_main53_3;
          K_18 = inv_main53_4;
          J_18 = inv_main53_5;
          D_18 = inv_main53_6;
          F_18 = inv_main53_7;
          E_18 = inv_main53_8;
          I_18 = inv_main53_9;
          H_18 = inv_main53_10;
          M_18 = inv_main53_11;
          G_18 = inv_main53_12;
          if (!((!(A_18 == 1)) && (!(B_18 == 0))))
              abort ();
          inv_main88_0 = C_18;
          inv_main88_1 = L_18;
          inv_main88_2 = B_18;
          inv_main88_3 = A_18;
          inv_main88_4 = K_18;
          inv_main88_5 = J_18;
          inv_main88_6 = D_18;
          inv_main88_7 = F_18;
          inv_main88_8 = E_18;
          inv_main88_9 = I_18;
          inv_main88_10 = H_18;
          inv_main88_11 = M_18;
          inv_main88_12 = G_18;
          C_32 = inv_main88_0;
          H_32 = inv_main88_1;
          D_32 = inv_main88_2;
          E_32 = inv_main88_3;
          K_32 = inv_main88_4;
          L_32 = inv_main88_5;
          A_32 = inv_main88_6;
          B_32 = inv_main88_7;
          I_32 = inv_main88_8;
          J_32 = inv_main88_9;
          F_32 = inv_main88_10;
          G_32 = inv_main88_11;
          M_32 = inv_main88_12;
          if (!1)
              abort ();
          goto main_error;

      default:
          abort ();
      }
  inv_main38:
    switch (__VERIFIER_nondet_int ())
      {
      case 0:
          A_11 = __VERIFIER_nondet_int ();
          J_11 = __VERIFIER_nondet_int ();
          M_11 = inv_main38_0;
          D_11 = inv_main38_1;
          O_11 = inv_main38_2;
          G_11 = inv_main38_3;
          B_11 = inv_main38_4;
          I_11 = inv_main38_5;
          L_11 = inv_main38_6;
          E_11 = inv_main38_7;
          F_11 = inv_main38_8;
          H_11 = inv_main38_9;
          C_11 = inv_main38_10;
          N_11 = inv_main38_11;
          K_11 = inv_main38_12;
          if (!
              ((!(B_11 == 0)) && (!(L_11 == 0)) && (J_11 == 1)
               && (A_11 == 1)))
              abort ();
          inv_main44_0 = M_11;
          inv_main44_1 = D_11;
          inv_main44_2 = O_11;
          inv_main44_3 = G_11;
          inv_main44_4 = B_11;
          inv_main44_5 = J_11;
          inv_main44_6 = L_11;
          inv_main44_7 = A_11;
          inv_main44_8 = F_11;
          inv_main44_9 = H_11;
          inv_main44_10 = C_11;
          inv_main44_11 = N_11;
          inv_main44_12 = K_11;
          goto inv_main44;

      case 1:
          C_12 = __VERIFIER_nondet_int ();
          F_12 = inv_main38_0;
          D_12 = inv_main38_1;
          M_12 = inv_main38_2;
          J_12 = inv_main38_3;
          E_12 = inv_main38_4;
          N_12 = inv_main38_5;
          K_12 = inv_main38_6;
          B_12 = inv_main38_7;
          L_12 = inv_main38_8;
          A_12 = inv_main38_9;
          H_12 = inv_main38_10;
          I_12 = inv_main38_11;
          G_12 = inv_main38_12;
          if (!((!(E_12 == 0)) && (C_12 == 1) && (K_12 == 0)))
              abort ();
          inv_main44_0 = F_12;
          inv_main44_1 = D_12;
          inv_main44_2 = M_12;
          inv_main44_3 = J_12;
          inv_main44_4 = E_12;
          inv_main44_5 = C_12;
          inv_main44_6 = K_12;
          inv_main44_7 = B_12;
          inv_main44_8 = L_12;
          inv_main44_9 = A_12;
          inv_main44_10 = H_12;
          inv_main44_11 = I_12;
          inv_main44_12 = G_12;
          goto inv_main44;

      case 2:
          H_13 = __VERIFIER_nondet_int ();
          K_13 = inv_main38_0;
          C_13 = inv_main38_1;
          L_13 = inv_main38_2;
          N_13 = inv_main38_3;
          E_13 = inv_main38_4;
          F_13 = inv_main38_5;
          D_13 = inv_main38_6;
          M_13 = inv_main38_7;
          A_13 = inv_main38_8;
          J_13 = inv_main38_9;
          G_13 = inv_main38_10;
          I_13 = inv_main38_11;
          B_13 = inv_main38_12;
          if (!((E_13 == 0) && (!(D_13 == 0)) && (H_13 == 1)))
              abort ();
          inv_main44_0 = K_13;
          inv_main44_1 = C_13;
          inv_main44_2 = L_13;
          inv_main44_3 = N_13;
          inv_main44_4 = E_13;
          inv_main44_5 = F_13;
          inv_main44_6 = D_13;
          inv_main44_7 = H_13;
          inv_main44_8 = A_13;
          inv_main44_9 = J_13;
          inv_main44_10 = G_13;
          inv_main44_11 = I_13;
          inv_main44_12 = B_13;
          goto inv_main44;

      case 3:
          C_14 = inv_main38_0;
          D_14 = inv_main38_1;
          F_14 = inv_main38_2;
          E_14 = inv_main38_3;
          M_14 = inv_main38_4;
          J_14 = inv_main38_5;
          I_14 = inv_main38_6;
          K_14 = inv_main38_7;
          L_14 = inv_main38_8;
          G_14 = inv_main38_9;
          B_14 = inv_main38_10;
          A_14 = inv_main38_11;
          H_14 = inv_main38_12;
          if (!((I_14 == 0) && (M_14 == 0)))
              abort ();
          inv_main44_0 = C_14;
          inv_main44_1 = D_14;
          inv_main44_2 = F_14;
          inv_main44_3 = E_14;
          inv_main44_4 = M_14;
          inv_main44_5 = J_14;
          inv_main44_6 = I_14;
          inv_main44_7 = K_14;
          inv_main44_8 = L_14;
          inv_main44_9 = G_14;
          inv_main44_10 = B_14;
          inv_main44_11 = A_14;
          inv_main44_12 = H_14;
          goto inv_main44;

      default:
          abort ();
      }
  inv_main65:
    switch (__VERIFIER_nondet_int ())
      {
      case 0:
          M_5 = inv_main65_0;
          G_5 = inv_main65_1;
          L_5 = inv_main65_2;
          A_5 = inv_main65_3;
          K_5 = inv_main65_4;
          B_5 = inv_main65_5;
          D_5 = inv_main65_6;
          F_5 = inv_main65_7;
          I_5 = inv_main65_8;
          J_5 = inv_main65_9;
          C_5 = inv_main65_10;
          H_5 = inv_main65_11;
          E_5 = inv_main65_12;
          if (!(D_5 == 0))
              abort ();
          inv_main71_0 = M_5;
          inv_main71_1 = G_5;
          inv_main71_2 = L_5;
          inv_main71_3 = A_5;
          inv_main71_4 = K_5;
          inv_main71_5 = B_5;
          inv_main71_6 = D_5;
          inv_main71_7 = F_5;
          inv_main71_8 = I_5;
          inv_main71_9 = J_5;
          inv_main71_10 = C_5;
          inv_main71_11 = H_5;
          inv_main71_12 = E_5;
          goto inv_main71;

      case 1:
          K_6 = __VERIFIER_nondet_int ();
          M_6 = inv_main65_0;
          E_6 = inv_main65_1;
          N_6 = inv_main65_2;
          A_6 = inv_main65_3;
          F_6 = inv_main65_4;
          D_6 = inv_main65_5;
          I_6 = inv_main65_6;
          J_6 = inv_main65_7;
          H_6 = inv_main65_8;
          C_6 = inv_main65_9;
          B_6 = inv_main65_10;
          L_6 = inv_main65_11;
          G_6 = inv_main65_12;
          if (!((J_6 == 1) && (!(I_6 == 0)) && (K_6 == 0)))
              abort ();
          inv_main71_0 = M_6;
          inv_main71_1 = E_6;
          inv_main71_2 = N_6;
          inv_main71_3 = A_6;
          inv_main71_4 = F_6;
          inv_main71_5 = D_6;
          inv_main71_6 = I_6;
          inv_main71_7 = K_6;
          inv_main71_8 = H_6;
          inv_main71_9 = C_6;
          inv_main71_10 = B_6;
          inv_main71_11 = L_6;
          inv_main71_12 = G_6;
          goto inv_main71;

      case 2:
          F_20 = inv_main65_0;
          G_20 = inv_main65_1;
          J_20 = inv_main65_2;
          C_20 = inv_main65_3;
          I_20 = inv_main65_4;
          E_20 = inv_main65_5;
          M_20 = inv_main65_6;
          D_20 = inv_main65_7;
          L_20 = inv_main65_8;
          K_20 = inv_main65_9;
          B_20 = inv_main65_10;
          H_20 = inv_main65_11;
          A_20 = inv_main65_12;
          if (!((!(D_20 == 1)) && (!(M_20 == 0))))
              abort ();
          inv_main88_0 = F_20;
          inv_main88_1 = G_20;
          inv_main88_2 = J_20;
          inv_main88_3 = C_20;
          inv_main88_4 = I_20;
          inv_main88_5 = E_20;
          inv_main88_6 = M_20;
          inv_main88_7 = D_20;
          inv_main88_8 = L_20;
          inv_main88_9 = K_20;
          inv_main88_10 = B_20;
          inv_main88_11 = H_20;
          inv_main88_12 = A_20;
          C_32 = inv_main88_0;
          H_32 = inv_main88_1;
          D_32 = inv_main88_2;
          E_32 = inv_main88_3;
          K_32 = inv_main88_4;
          L_32 = inv_main88_5;
          A_32 = inv_main88_6;
          B_32 = inv_main88_7;
          I_32 = inv_main88_8;
          J_32 = inv_main88_9;
          F_32 = inv_main88_10;
          G_32 = inv_main88_11;
          M_32 = inv_main88_12;
          if (!1)
              abort ();
          goto main_error;

      default:
          abort ();
      }
  inv_main44:
    switch (__VERIFIER_nondet_int ())
      {
      case 0:
          L_7 = __VERIFIER_nondet_int ();
          M_7 = __VERIFIER_nondet_int ();
          B_7 = inv_main44_0;
          I_7 = inv_main44_1;
          F_7 = inv_main44_2;
          O_7 = inv_main44_3;
          H_7 = inv_main44_4;
          N_7 = inv_main44_5;
          A_7 = inv_main44_6;
          G_7 = inv_main44_7;
          K_7 = inv_main44_8;
          C_7 = inv_main44_9;
          D_7 = inv_main44_10;
          J_7 = inv_main44_11;
          E_7 = inv_main44_12;
          if (!((L_7 == 1) && (!(K_7 == 0)) && (!(D_7 == 0)) && (M_7 == 1)))
              abort ();
          inv_main50_0 = B_7;
          inv_main50_1 = I_7;
          inv_main50_2 = F_7;
          inv_main50_3 = O_7;
          inv_main50_4 = H_7;
          inv_main50_5 = N_7;
          inv_main50_6 = A_7;
          inv_main50_7 = G_7;
          inv_main50_8 = K_7;
          inv_main50_9 = M_7;
          inv_main50_10 = D_7;
          inv_main50_11 = L_7;
          inv_main50_12 = E_7;
          goto inv_main50;

      case 1:
          M_8 = __VERIFIER_nondet_int ();
          E_8 = inv_main44_0;
          D_8 = inv_main44_1;
          G_8 = inv_main44_2;
          I_8 = inv_main44_3;
          H_8 = inv_main44_4;
          B_8 = inv_main44_5;
          A_8 = inv_main44_6;
          F_8 = inv_main44_7;
          N_8 = inv_main44_8;
          J_8 = inv_main44_9;
          K_8 = inv_main44_10;
          L_8 = inv_main44_11;
          C_8 = inv_main44_12;
          if (!((M_8 == 1) && (K_8 == 0) && (!(N_8 == 0))))
              abort ();
          inv_main50_0 = E_8;
          inv_main50_1 = D_8;
          inv_main50_2 = G_8;
          inv_main50_3 = I_8;
          inv_main50_4 = H_8;
          inv_main50_5 = B_8;
          inv_main50_6 = A_8;
          inv_main50_7 = F_8;
          inv_main50_8 = N_8;
          inv_main50_9 = M_8;
          inv_main50_10 = K_8;
          inv_main50_11 = L_8;
          inv_main50_12 = C_8;
          goto inv_main50;

      case 2:
          L_9 = __VERIFIER_nondet_int ();
          C_9 = inv_main44_0;
          K_9 = inv_main44_1;
          J_9 = inv_main44_2;
          G_9 = inv_main44_3;
          H_9 = inv_main44_4;
          M_9 = inv_main44_5;
          N_9 = inv_main44_6;
          D_9 = inv_main44_7;
          A_9 = inv_main44_8;
          I_9 = inv_main44_9;
          F_9 = inv_main44_10;
          E_9 = inv_main44_11;
          B_9 = inv_main44_12;
          if (!((L_9 == 1) && (!(F_9 == 0)) && (A_9 == 0)))
              abort ();
          inv_main50_0 = C_9;
          inv_main50_1 = K_9;
          inv_main50_2 = J_9;
          inv_main50_3 = G_9;
          inv_main50_4 = H_9;
          inv_main50_5 = M_9;
          inv_main50_6 = N_9;
          inv_main50_7 = D_9;
          inv_main50_8 = A_9;
          inv_main50_9 = I_9;
          inv_main50_10 = F_9;
          inv_main50_11 = L_9;
          inv_main50_12 = B_9;
          goto inv_main50;

      case 3:
          E_10 = inv_main44_0;
          I_10 = inv_main44_1;
          H_10 = inv_main44_2;
          M_10 = inv_main44_3;
          G_10 = inv_main44_4;
          B_10 = inv_main44_5;
          D_10 = inv_main44_6;
          K_10 = inv_main44_7;
          L_10 = inv_main44_8;
          J_10 = inv_main44_9;
          A_10 = inv_main44_10;
          F_10 = inv_main44_11;
          C_10 = inv_main44_12;
          if (!((A_10 == 0) && (L_10 == 0)))
              abort ();
          inv_main50_0 = E_10;
          inv_main50_1 = I_10;
          inv_main50_2 = H_10;
          inv_main50_3 = M_10;
          inv_main50_4 = G_10;
          inv_main50_5 = B_10;
          inv_main50_6 = D_10;
          inv_main50_7 = K_10;
          inv_main50_8 = L_10;
          inv_main50_9 = J_10;
          inv_main50_10 = A_10;
          inv_main50_11 = F_10;
          inv_main50_12 = C_10;
          goto inv_main50;

      default:
          abort ();
      }

    // return expression

}

