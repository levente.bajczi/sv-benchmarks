// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: tricera-benchmarks/id_build.i.v+lhb-reducer.c-1.smt2.gz_002.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "id_build.i.v+lhb-reducer.c-1.smt2.gz_002_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int inv_main32_0;
    int inv_main32_1;
    int inv_main32_2;
    int inv_main32_3;
    int inv_main32_4;
    int inv_main32_5;
    int inv_main32_6;
    int inv_main32_7;
    int inv_main32_8;
    int inv_main32_9;
    int inv_main32_10;
    int inv_main40_0;
    int inv_main40_1;
    int inv_main40_2;
    int inv_main40_3;
    int inv_main40_4;
    int inv_main40_5;
    int inv_main40_6;
    int inv_main40_7;
    int inv_main40_8;
    int inv_main40_9;
    int inv_main40_10;
    int inv_main40_11;
    int inv_main40_12;
    int inv_main5_0;
    int inv_main5_1;
    int inv_main5_2;
    int inv_main5_3;
    int inv_main5_4;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    int E_0;
    int A_1;
    int B_1;
    int C_1;
    int D_1;
    int E_1;
    int F_1;
    int G_1;
    int H_1;
    int I_1;
    int J_1;
    int K_1;
    int L_1;
    int M_1;
    int A_2;
    int B_2;
    int C_2;
    int D_2;
    int E_2;
    int F_2;
    int G_2;
    int H_2;
    int I_2;
    int J_2;
    int K_2;
    int L_2;
    int M_2;
    int N_2;
    int O_2;
    int P_2;
    int Q_2;
    int R_2;
    int S_2;
    int T_2;
    int U_2;
    int V_2;
    int W_2;
    int X_2;
    int Y_2;
    int Z_2;
    int A1_2;
    int B1_2;
    int C1_2;
    int D1_2;
    int E1_2;
    int F1_2;
    int G1_2;
    int H1_2;
    int v_34_2;
    int A_3;
    int B_3;
    int C_3;
    int D_3;
    int E_3;
    int F_3;
    int G_3;
    int H_3;
    int I_3;
    int J_3;
    int K_3;
    int L_3;
    int M_3;
    int N_3;
    int O_3;
    int P_3;
    int Q_3;
    int R_3;
    int S_3;
    int T_3;
    int U_3;
    int V_3;
    int W_3;
    int X_3;
    int Y_3;
    int v_25_3;
    int A_5;
    int B_5;
    int C_5;
    int D_5;
    int E_5;
    int F_5;
    int G_5;
    int H_5;
    int I_5;
    int J_5;
    int K_5;
    int L_5;
    int M_5;
    int N_5;
    int O_5;
    int P_5;
    int Q_5;
    int R_5;
    int S_5;
    int T_5;
    int U_5;
    int V_5;
    int W_5;
    int v_23_5;
    int A_8;
    int B_8;
    int C_8;
    int D_8;
    int E_8;
    int F_8;
    int G_8;
    int H_8;
    int I_8;
    int J_8;
    int K_8;
    int L_8;
    int M_8;



    // main logic
    goto main_init;

  main_init:
    if (!((D_0 == 0) && (E_0 == 0)))
        abort ();
    inv_main5_0 = E_0;
    inv_main5_1 = D_0;
    inv_main5_2 = C_0;
    inv_main5_3 = B_0;
    inv_main5_4 = A_0;
    A_3 = __VERIFIER_nondet_int ();
    B_3 = __VERIFIER_nondet_int ();
    C_3 = __VERIFIER_nondet_int ();
    E_3 = __VERIFIER_nondet_int ();
    F_3 = __VERIFIER_nondet_int ();
    G_3 = __VERIFIER_nondet_int ();
    H_3 = __VERIFIER_nondet_int ();
    I_3 = __VERIFIER_nondet_int ();
    L_3 = __VERIFIER_nondet_int ();
    M_3 = __VERIFIER_nondet_int ();
    N_3 = __VERIFIER_nondet_int ();
    O_3 = __VERIFIER_nondet_int ();
    P_3 = __VERIFIER_nondet_int ();
    Q_3 = __VERIFIER_nondet_int ();
    R_3 = __VERIFIER_nondet_int ();
    S_3 = __VERIFIER_nondet_int ();
    T_3 = __VERIFIER_nondet_int ();
    U_3 = __VERIFIER_nondet_int ();
    V_3 = __VERIFIER_nondet_int ();
    W_3 = __VERIFIER_nondet_int ();
    v_25_3 = __VERIFIER_nondet_int ();
    K_3 = inv_main5_0;
    X_3 = inv_main5_1;
    J_3 = inv_main5_2;
    D_3 = inv_main5_3;
    Y_3 = inv_main5_4;
    if (!
        ((I_3 == X_3) && (H_3 == 0) && (G_3 == (O_3 + 1)) && (F_3 == I_3)
         && (E_3 == A_3) && (C_3 == R_3) && (B_3 == W_3) && (!(A_3 == 0))
         && (S_3 == D_3) && (R_3 == T_3) && (!(Q_3 == 0)) && (P_3 == H_3)
         && (O_3 == N_3) && (N_3 == 0) && (M_3 == A_3) && (W_3 == K_3)
         && (V_3 == U_3) && (U_3 == J_3) && (1 <= T_3)
         && (((1 <= T_3) && (A_3 == 1)) || ((!(1 <= T_3)) && (A_3 == 0)))
         && (((0 <= H_3) && (Q_3 == 1)) || ((!(0 <= H_3)) && (Q_3 == 0)))
         && (L_3 == S_3) && (v_25_3 == Q_3)))
        abort ();
    inv_main32_0 = B_3;
    inv_main32_1 = F_3;
    inv_main32_2 = V_3;
    inv_main32_3 = L_3;
    inv_main32_4 = C_3;
    inv_main32_5 = P_3;
    inv_main32_6 = G_3;
    inv_main32_7 = E_3;
    inv_main32_8 = M_3;
    inv_main32_9 = Q_3;
    inv_main32_10 = v_25_3;
    A_2 = __VERIFIER_nondet_int ();
    B_2 = __VERIFIER_nondet_int ();
    D_2 = __VERIFIER_nondet_int ();
    E_2 = __VERIFIER_nondet_int ();
    I_2 = __VERIFIER_nondet_int ();
    J_2 = __VERIFIER_nondet_int ();
    G1_2 = __VERIFIER_nondet_int ();
    K_2 = __VERIFIER_nondet_int ();
    L_2 = __VERIFIER_nondet_int ();
    E1_2 = __VERIFIER_nondet_int ();
    M_2 = __VERIFIER_nondet_int ();
    N_2 = __VERIFIER_nondet_int ();
    P_2 = __VERIFIER_nondet_int ();
    A1_2 = __VERIFIER_nondet_int ();
    Q_2 = __VERIFIER_nondet_int ();
    S_2 = __VERIFIER_nondet_int ();
    T_2 = __VERIFIER_nondet_int ();
    V_2 = __VERIFIER_nondet_int ();
    X_2 = __VERIFIER_nondet_int ();
    Y_2 = __VERIFIER_nondet_int ();
    H1_2 = __VERIFIER_nondet_int ();
    D1_2 = __VERIFIER_nondet_int ();
    B1_2 = __VERIFIER_nondet_int ();
    v_34_2 = __VERIFIER_nondet_int ();
    Z_2 = inv_main32_0;
    O_2 = inv_main32_1;
    U_2 = inv_main32_2;
    F1_2 = inv_main32_3;
    R_2 = inv_main32_4;
    W_2 = inv_main32_5;
    C1_2 = inv_main32_6;
    F_2 = inv_main32_7;
    C_2 = inv_main32_8;
    G_2 = inv_main32_9;
    H_2 = inv_main32_10;
    if (!
        ((S_2 == (Q_2 + 1)) && (Q_2 == T_2) && (P_2 == H_2) && (N_2 == K_2)
         && (M_2 == Y_2) && (L_2 == F1_2) && (K_2 == Z_2) && (J_2 == W_2)
         && (I_2 == C_2) && (!(E_2 == 0)) && (D_2 == D1_2) && (B_2 == J_2)
         && (A_2 == F_2) && (B1_2 == G_2) && (A1_2 == I_2) && (Y_2 == O_2)
         && (X_2 == A_2) && (V_2 == G1_2) && (!(H1_2 == 0)) && (G1_2 == U_2)
         && (E1_2 == L_2) && (D1_2 == R_2) && (C1_2 <= 7)
         && (((!(1 <= (R_2 + (-1 * W_2)))) && (H1_2 == 0))
             || ((1 <= (R_2 + (-1 * W_2))) && (H1_2 == 1))) && (((0 <= J_2)
                                                                 && (E_2 ==
                                                                     1))
                                                                ||
                                                                ((!(0 <= J_2))
                                                                 && (E_2 ==
                                                                     0)))
         && (T_2 == C1_2) && (v_34_2 == E_2)))
        abort ();
    inv_main32_0 = N_2;
    inv_main32_1 = M_2;
    inv_main32_2 = V_2;
    inv_main32_3 = E1_2;
    inv_main32_4 = D_2;
    inv_main32_5 = B_2;
    inv_main32_6 = S_2;
    inv_main32_7 = X_2;
    inv_main32_8 = A1_2;
    inv_main32_9 = E_2;
    inv_main32_10 = v_34_2;
    goto inv_main32_1;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  inv_main29:
    goto inv_main29;
  inv_main21:
    goto inv_main21;
  inv_main32_0:
    switch (__VERIFIER_nondet_int ())
      {
      case 0:
          A_5 = __VERIFIER_nondet_int ();
          B_5 = __VERIFIER_nondet_int ();
          C_5 = __VERIFIER_nondet_int ();
          D_5 = __VERIFIER_nondet_int ();
          E_5 = __VERIFIER_nondet_int ();
          G_5 = __VERIFIER_nondet_int ();
          H_5 = __VERIFIER_nondet_int ();
          J_5 = __VERIFIER_nondet_int ();
          M_5 = __VERIFIER_nondet_int ();
          N_5 = __VERIFIER_nondet_int ();
          O_5 = __VERIFIER_nondet_int ();
          V_5 = __VERIFIER_nondet_int ();
          v_23_5 = __VERIFIER_nondet_int ();
          R_5 = inv_main32_0;
          W_5 = inv_main32_1;
          K_5 = inv_main32_2;
          F_5 = inv_main32_3;
          P_5 = inv_main32_4;
          Q_5 = inv_main32_5;
          S_5 = inv_main32_6;
          I_5 = inv_main32_7;
          L_5 = inv_main32_8;
          T_5 = inv_main32_9;
          U_5 = inv_main32_10;
          if (!
              ((H_5 == L_5) && (G_5 == P_5) && (E_5 == I_5) && (D_5 == W_5)
               && (C_5 == F_5) && (B_5 == U_5) && (A_5 == T_5) && (O_5 == 0)
               && (N_5 == S_5) && (M_5 == K_5) && (V_5 == Q_5) && (S_5 <= 7)
               && (((1 <= (P_5 + (-1 * Q_5))) && (O_5 == 1))
                   || ((!(1 <= (P_5 + (-1 * Q_5)))) && (O_5 == 0)))
               && (J_5 == R_5) && (v_23_5 == O_5)))
              abort ();
          inv_main40_0 = J_5;
          inv_main40_1 = D_5;
          inv_main40_2 = M_5;
          inv_main40_3 = C_5;
          inv_main40_4 = G_5;
          inv_main40_5 = V_5;
          inv_main40_6 = N_5;
          inv_main40_7 = E_5;
          inv_main40_8 = H_5;
          inv_main40_9 = A_5;
          inv_main40_10 = B_5;
          inv_main40_11 = O_5;
          inv_main40_12 = v_23_5;
          D_8 = inv_main40_0;
          B_8 = inv_main40_1;
          C_8 = inv_main40_2;
          J_8 = inv_main40_3;
          G_8 = inv_main40_4;
          I_8 = inv_main40_5;
          M_8 = inv_main40_6;
          F_8 = inv_main40_7;
          E_8 = inv_main40_8;
          K_8 = inv_main40_9;
          H_8 = inv_main40_10;
          A_8 = inv_main40_11;
          L_8 = inv_main40_12;
          if (!1)
              abort ();
          goto main_error;

      case 1:
          A_2 = __VERIFIER_nondet_int ();
          B_2 = __VERIFIER_nondet_int ();
          D_2 = __VERIFIER_nondet_int ();
          E_2 = __VERIFIER_nondet_int ();
          I_2 = __VERIFIER_nondet_int ();
          J_2 = __VERIFIER_nondet_int ();
          G1_2 = __VERIFIER_nondet_int ();
          K_2 = __VERIFIER_nondet_int ();
          L_2 = __VERIFIER_nondet_int ();
          E1_2 = __VERIFIER_nondet_int ();
          M_2 = __VERIFIER_nondet_int ();
          N_2 = __VERIFIER_nondet_int ();
          P_2 = __VERIFIER_nondet_int ();
          A1_2 = __VERIFIER_nondet_int ();
          Q_2 = __VERIFIER_nondet_int ();
          S_2 = __VERIFIER_nondet_int ();
          T_2 = __VERIFIER_nondet_int ();
          V_2 = __VERIFIER_nondet_int ();
          X_2 = __VERIFIER_nondet_int ();
          Y_2 = __VERIFIER_nondet_int ();
          H1_2 = __VERIFIER_nondet_int ();
          D1_2 = __VERIFIER_nondet_int ();
          B1_2 = __VERIFIER_nondet_int ();
          v_34_2 = __VERIFIER_nondet_int ();
          Z_2 = inv_main32_0;
          O_2 = inv_main32_1;
          U_2 = inv_main32_2;
          F1_2 = inv_main32_3;
          R_2 = inv_main32_4;
          W_2 = inv_main32_5;
          C1_2 = inv_main32_6;
          F_2 = inv_main32_7;
          C_2 = inv_main32_8;
          G_2 = inv_main32_9;
          H_2 = inv_main32_10;
          if (!
              ((S_2 == (Q_2 + 1)) && (Q_2 == T_2) && (P_2 == H_2)
               && (N_2 == K_2) && (M_2 == Y_2) && (L_2 == F1_2)
               && (K_2 == Z_2) && (J_2 == W_2) && (I_2 == C_2)
               && (!(E_2 == 0)) && (D_2 == D1_2) && (B_2 == J_2)
               && (A_2 == F_2) && (B1_2 == G_2) && (A1_2 == I_2)
               && (Y_2 == O_2) && (X_2 == A_2) && (V_2 == G1_2)
               && (!(H1_2 == 0)) && (G1_2 == U_2) && (E1_2 == L_2)
               && (D1_2 == R_2) && (C1_2 <= 7)
               && (((!(1 <= (R_2 + (-1 * W_2)))) && (H1_2 == 0))
                   || ((1 <= (R_2 + (-1 * W_2))) && (H1_2 == 1)))
               && (((0 <= J_2) && (E_2 == 1))
                   || ((!(0 <= J_2)) && (E_2 == 0))) && (T_2 == C1_2)
               && (v_34_2 == E_2)))
              abort ();
          inv_main32_0 = N_2;
          inv_main32_1 = M_2;
          inv_main32_2 = V_2;
          inv_main32_3 = E1_2;
          inv_main32_4 = D_2;
          inv_main32_5 = B_2;
          inv_main32_6 = S_2;
          inv_main32_7 = X_2;
          inv_main32_8 = A1_2;
          inv_main32_9 = E_2;
          inv_main32_10 = v_34_2;
          A_2 = __VERIFIER_nondet_int ();
          B_2 = __VERIFIER_nondet_int ();
          D_2 = __VERIFIER_nondet_int ();
          E_2 = __VERIFIER_nondet_int ();
          I_2 = __VERIFIER_nondet_int ();
          J_2 = __VERIFIER_nondet_int ();
          G1_2 = __VERIFIER_nondet_int ();
          K_2 = __VERIFIER_nondet_int ();
          L_2 = __VERIFIER_nondet_int ();
          E1_2 = __VERIFIER_nondet_int ();
          M_2 = __VERIFIER_nondet_int ();
          N_2 = __VERIFIER_nondet_int ();
          P_2 = __VERIFIER_nondet_int ();
          A1_2 = __VERIFIER_nondet_int ();
          Q_2 = __VERIFIER_nondet_int ();
          S_2 = __VERIFIER_nondet_int ();
          T_2 = __VERIFIER_nondet_int ();
          V_2 = __VERIFIER_nondet_int ();
          X_2 = __VERIFIER_nondet_int ();
          Y_2 = __VERIFIER_nondet_int ();
          H1_2 = __VERIFIER_nondet_int ();
          D1_2 = __VERIFIER_nondet_int ();
          B1_2 = __VERIFIER_nondet_int ();
          v_34_2 = __VERIFIER_nondet_int ();
          Z_2 = inv_main32_0;
          O_2 = inv_main32_1;
          U_2 = inv_main32_2;
          F1_2 = inv_main32_3;
          R_2 = inv_main32_4;
          W_2 = inv_main32_5;
          C1_2 = inv_main32_6;
          F_2 = inv_main32_7;
          C_2 = inv_main32_8;
          G_2 = inv_main32_9;
          H_2 = inv_main32_10;
          if (!
              ((S_2 == (Q_2 + 1)) && (Q_2 == T_2) && (P_2 == H_2)
               && (N_2 == K_2) && (M_2 == Y_2) && (L_2 == F1_2)
               && (K_2 == Z_2) && (J_2 == W_2) && (I_2 == C_2)
               && (!(E_2 == 0)) && (D_2 == D1_2) && (B_2 == J_2)
               && (A_2 == F_2) && (B1_2 == G_2) && (A1_2 == I_2)
               && (Y_2 == O_2) && (X_2 == A_2) && (V_2 == G1_2)
               && (!(H1_2 == 0)) && (G1_2 == U_2) && (E1_2 == L_2)
               && (D1_2 == R_2) && (C1_2 <= 7)
               && (((!(1 <= (R_2 + (-1 * W_2)))) && (H1_2 == 0))
                   || ((1 <= (R_2 + (-1 * W_2))) && (H1_2 == 1)))
               && (((0 <= J_2) && (E_2 == 1))
                   || ((!(0 <= J_2)) && (E_2 == 0))) && (T_2 == C1_2)
               && (v_34_2 == E_2)))
              abort ();
          inv_main32_0 = N_2;
          inv_main32_1 = M_2;
          inv_main32_2 = V_2;
          inv_main32_3 = E1_2;
          inv_main32_4 = D_2;
          inv_main32_5 = B_2;
          inv_main32_6 = S_2;
          inv_main32_7 = X_2;
          inv_main32_8 = A1_2;
          inv_main32_9 = E_2;
          inv_main32_10 = v_34_2;
          goto inv_main32_1;

      case 2:
          A_2 = __VERIFIER_nondet_int ();
          B_2 = __VERIFIER_nondet_int ();
          D_2 = __VERIFIER_nondet_int ();
          E_2 = __VERIFIER_nondet_int ();
          I_2 = __VERIFIER_nondet_int ();
          J_2 = __VERIFIER_nondet_int ();
          G1_2 = __VERIFIER_nondet_int ();
          K_2 = __VERIFIER_nondet_int ();
          L_2 = __VERIFIER_nondet_int ();
          E1_2 = __VERIFIER_nondet_int ();
          M_2 = __VERIFIER_nondet_int ();
          N_2 = __VERIFIER_nondet_int ();
          P_2 = __VERIFIER_nondet_int ();
          A1_2 = __VERIFIER_nondet_int ();
          Q_2 = __VERIFIER_nondet_int ();
          S_2 = __VERIFIER_nondet_int ();
          T_2 = __VERIFIER_nondet_int ();
          V_2 = __VERIFIER_nondet_int ();
          X_2 = __VERIFIER_nondet_int ();
          Y_2 = __VERIFIER_nondet_int ();
          H1_2 = __VERIFIER_nondet_int ();
          D1_2 = __VERIFIER_nondet_int ();
          B1_2 = __VERIFIER_nondet_int ();
          v_34_2 = __VERIFIER_nondet_int ();
          Z_2 = inv_main32_0;
          O_2 = inv_main32_1;
          U_2 = inv_main32_2;
          F1_2 = inv_main32_3;
          R_2 = inv_main32_4;
          W_2 = inv_main32_5;
          C1_2 = inv_main32_6;
          F_2 = inv_main32_7;
          C_2 = inv_main32_8;
          G_2 = inv_main32_9;
          H_2 = inv_main32_10;
          if (!
              ((S_2 == (Q_2 + 1)) && (Q_2 == T_2) && (P_2 == H_2)
               && (N_2 == K_2) && (M_2 == Y_2) && (L_2 == F1_2)
               && (K_2 == Z_2) && (J_2 == W_2) && (I_2 == C_2)
               && (!(E_2 == 0)) && (D_2 == D1_2) && (B_2 == J_2)
               && (A_2 == F_2) && (B1_2 == G_2) && (A1_2 == I_2)
               && (Y_2 == O_2) && (X_2 == A_2) && (V_2 == G1_2)
               && (!(H1_2 == 0)) && (G1_2 == U_2) && (E1_2 == L_2)
               && (D1_2 == R_2) && (C1_2 <= 7)
               && (((!(1 <= (R_2 + (-1 * W_2)))) && (H1_2 == 0))
                   || ((1 <= (R_2 + (-1 * W_2))) && (H1_2 == 1)))
               && (((0 <= J_2) && (E_2 == 1))
                   || ((!(0 <= J_2)) && (E_2 == 0))) && (T_2 == C1_2)
               && (v_34_2 == E_2)))
              abort ();
          inv_main32_0 = N_2;
          inv_main32_1 = M_2;
          inv_main32_2 = V_2;
          inv_main32_3 = E1_2;
          inv_main32_4 = D_2;
          inv_main32_5 = B_2;
          inv_main32_6 = S_2;
          inv_main32_7 = X_2;
          inv_main32_8 = A1_2;
          inv_main32_9 = E_2;
          inv_main32_10 = v_34_2;
          goto inv_main32_1;

      default:
          abort ();
      }
  inv_main32_1:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          I_1 = __VERIFIER_nondet_int ();
          K_1 = __VERIFIER_nondet_int ();
          A_1 = inv_main32_0;
          E_1 = inv_main32_1;
          F_1 = inv_main32_2;
          G_1 = inv_main32_3;
          J_1 = inv_main32_4;
          D_1 = inv_main32_5;
          H_1 = inv_main32_6;
          L_1 = inv_main32_7;
          B_1 = inv_main32_8;
          C_1 = inv_main32_9;
          M_1 = inv_main32_10;
          if (!
              ((I_1 == 0) && (2 <= (J_1 + (-1 * D_1))) && (!(H_1 <= 7))
               && (K_1 == (D_1 + 1))))
              abort ();
          inv_main32_0 = A_1;
          inv_main32_1 = E_1;
          inv_main32_2 = F_1;
          inv_main32_3 = G_1;
          inv_main32_4 = J_1;
          inv_main32_5 = K_1;
          inv_main32_6 = I_1;
          inv_main32_7 = L_1;
          inv_main32_8 = B_1;
          inv_main32_9 = C_1;
          inv_main32_10 = M_1;
          goto inv_main32_0;

      case 1:
          A_2 = __VERIFIER_nondet_int ();
          B_2 = __VERIFIER_nondet_int ();
          D_2 = __VERIFIER_nondet_int ();
          E_2 = __VERIFIER_nondet_int ();
          I_2 = __VERIFIER_nondet_int ();
          J_2 = __VERIFIER_nondet_int ();
          G1_2 = __VERIFIER_nondet_int ();
          K_2 = __VERIFIER_nondet_int ();
          L_2 = __VERIFIER_nondet_int ();
          E1_2 = __VERIFIER_nondet_int ();
          M_2 = __VERIFIER_nondet_int ();
          N_2 = __VERIFIER_nondet_int ();
          P_2 = __VERIFIER_nondet_int ();
          A1_2 = __VERIFIER_nondet_int ();
          Q_2 = __VERIFIER_nondet_int ();
          S_2 = __VERIFIER_nondet_int ();
          T_2 = __VERIFIER_nondet_int ();
          V_2 = __VERIFIER_nondet_int ();
          X_2 = __VERIFIER_nondet_int ();
          Y_2 = __VERIFIER_nondet_int ();
          H1_2 = __VERIFIER_nondet_int ();
          D1_2 = __VERIFIER_nondet_int ();
          B1_2 = __VERIFIER_nondet_int ();
          v_34_2 = __VERIFIER_nondet_int ();
          Z_2 = inv_main32_0;
          O_2 = inv_main32_1;
          U_2 = inv_main32_2;
          F1_2 = inv_main32_3;
          R_2 = inv_main32_4;
          W_2 = inv_main32_5;
          C1_2 = inv_main32_6;
          F_2 = inv_main32_7;
          C_2 = inv_main32_8;
          G_2 = inv_main32_9;
          H_2 = inv_main32_10;
          if (!
              ((S_2 == (Q_2 + 1)) && (Q_2 == T_2) && (P_2 == H_2)
               && (N_2 == K_2) && (M_2 == Y_2) && (L_2 == F1_2)
               && (K_2 == Z_2) && (J_2 == W_2) && (I_2 == C_2)
               && (!(E_2 == 0)) && (D_2 == D1_2) && (B_2 == J_2)
               && (A_2 == F_2) && (B1_2 == G_2) && (A1_2 == I_2)
               && (Y_2 == O_2) && (X_2 == A_2) && (V_2 == G1_2)
               && (!(H1_2 == 0)) && (G1_2 == U_2) && (E1_2 == L_2)
               && (D1_2 == R_2) && (C1_2 <= 7)
               && (((!(1 <= (R_2 + (-1 * W_2)))) && (H1_2 == 0))
                   || ((1 <= (R_2 + (-1 * W_2))) && (H1_2 == 1)))
               && (((0 <= J_2) && (E_2 == 1))
                   || ((!(0 <= J_2)) && (E_2 == 0))) && (T_2 == C1_2)
               && (v_34_2 == E_2)))
              abort ();
          inv_main32_0 = N_2;
          inv_main32_1 = M_2;
          inv_main32_2 = V_2;
          inv_main32_3 = E1_2;
          inv_main32_4 = D_2;
          inv_main32_5 = B_2;
          inv_main32_6 = S_2;
          inv_main32_7 = X_2;
          inv_main32_8 = A1_2;
          inv_main32_9 = E_2;
          inv_main32_10 = v_34_2;
          goto inv_main32_1;

      default:
          abort ();
      }

    // return expression

}

