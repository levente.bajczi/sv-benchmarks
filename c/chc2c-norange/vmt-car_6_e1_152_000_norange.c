// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: vmt-chc-benchmarks/vmt-car_6_e1_152_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "vmt-car_6_e1_152_000_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    _Bool state_0;
    _Bool state_1;
    _Bool state_2;
    _Bool state_3;
    _Bool state_4;
    _Bool state_5;
    _Bool state_6;
    _Bool state_7;
    _Bool state_8;
    _Bool state_9;
    _Bool state_10;
    int state_11;
    int state_12;
    int state_13;
    int state_14;
    int state_15;
    int state_16;
    int state_17;
    int state_18;
    _Bool state_19;
    _Bool state_20;
    _Bool state_21;
    _Bool state_22;
    _Bool state_23;
    _Bool state_24;
    _Bool state_25;
    int state_26;
    int state_27;
    int state_28;
    _Bool state_29;
    _Bool state_30;
    _Bool state_31;
    _Bool state_32;
    _Bool state_33;
    int state_34;
    int state_35;
    _Bool A_0;
    _Bool B_0;
    _Bool C_0;
    _Bool D_0;
    _Bool E_0;
    _Bool F_0;
    int G_0;
    int H_0;
    int I_0;
    int J_0;
    int K_0;
    int L_0;
    int M_0;
    int N_0;
    _Bool O_0;
    _Bool P_0;
    _Bool Q_0;
    _Bool R_0;
    _Bool S_0;
    _Bool T_0;
    _Bool U_0;
    _Bool V_0;
    _Bool W_0;
    int X_0;
    int Y_0;
    _Bool Z_0;
    _Bool A1_0;
    _Bool B1_0;
    _Bool C1_0;
    _Bool D1_0;
    _Bool E1_0;
    int F1_0;
    int G1_0;
    int H1_0;
    _Bool I1_0;
    _Bool J1_0;
    _Bool A_1;
    _Bool B_1;
    _Bool C_1;
    _Bool D_1;
    _Bool E_1;
    _Bool F_1;
    int G_1;
    int H_1;
    int I_1;
    int J_1;
    int K_1;
    int L_1;
    int M_1;
    int N_1;
    _Bool O_1;
    _Bool P_1;
    _Bool Q_1;
    _Bool R_1;
    _Bool S_1;
    _Bool T_1;
    _Bool U_1;
    _Bool V_1;
    _Bool W_1;
    int X_1;
    int Y_1;
    _Bool Z_1;
    _Bool A1_1;
    int B1_1;
    _Bool C1_1;
    _Bool D1_1;
    _Bool E1_1;
    int F1_1;
    int G1_1;
    _Bool H1_1;
    _Bool I1_1;
    _Bool J1_1;
    _Bool K1_1;
    _Bool L1_1;
    _Bool M1_1;
    int N1_1;
    int O1_1;
    int P1_1;
    int Q1_1;
    _Bool R1_1;
    _Bool S1_1;
    _Bool T1_1;
    _Bool U1_1;
    _Bool V1_1;
    _Bool W1_1;
    _Bool X1_1;
    _Bool Y1_1;
    _Bool Z1_1;
    int A2_1;
    int B2_1;
    int C2_1;
    int D2_1;
    int E2_1;
    int F2_1;
    _Bool G2_1;
    _Bool H2_1;
    _Bool I2_1;
    _Bool J2_1;
    _Bool K2_1;
    _Bool L2_1;
    _Bool M2_1;
    _Bool N2_1;
    _Bool O2_1;
    int P2_1;
    int Q2_1;
    int R2_1;
    _Bool S2_1;
    _Bool T2_1;
    _Bool A_2;
    _Bool B_2;
    _Bool C_2;
    _Bool D_2;
    _Bool E_2;
    _Bool F_2;
    int G_2;
    int H_2;
    int I_2;
    int J_2;
    int K_2;
    int L_2;
    int M_2;
    int N_2;
    _Bool O_2;
    _Bool P_2;
    _Bool Q_2;
    _Bool R_2;
    _Bool S_2;
    _Bool T_2;
    _Bool U_2;
    _Bool V_2;
    _Bool W_2;
    int X_2;
    int Y_2;
    _Bool Z_2;
    _Bool A1_2;
    _Bool B1_2;
    _Bool C1_2;
    _Bool D1_2;
    _Bool E1_2;
    int F1_2;
    int G1_2;
    int H1_2;
    _Bool I1_2;
    _Bool J1_2;



    // main logic
    goto main_init;

  main_init:
    if (!
        (((4 <= K_0) == D_0) && ((3 <= M_0) == B_0)
         && (!((A1_0 && Z_0) == I1_0)) && (U_0 == T_0) && (S_0 == R_0)
         && (Q_0 == P_0) && (F_0 == E_0) && (D_0 == C_0) && (J1_0 == I1_0)
         && (J1_0 == W_0) && (E1_0 == Q_0) && (D1_0 == S_0) && (C1_0 == O_0)
         && (B1_0 == U_0) && (B_0 == A_0) && (W_0 == V_0) && (M_0 == I_0)
         && (K_0 == J_0) && (H_0 == G_0) && (H1_0 == 0) && (H1_0 == K_0)
         && (G1_0 == 0) && (G1_0 == M_0) && (F1_0 == 0) && (F1_0 == H_0)
         && ((Y_0 == X_0) || (!Q_0) || S_0) && ((Y_0 == 0) || (Q_0 && (!S_0)))
         && E1_0 && (!D1_0) && C1_0 && (!B1_0) && ((H_0 == 10) == F_0)))
        abort ();
    state_0 = J1_0;
    state_1 = W_0;
    state_2 = A1_0;
    state_3 = Z_0;
    state_4 = I1_0;
    state_5 = B1_0;
    state_6 = U_0;
    state_7 = D1_0;
    state_8 = S_0;
    state_9 = E1_0;
    state_10 = Q_0;
    state_11 = Y_0;
    state_12 = X_0;
    state_13 = H1_0;
    state_14 = K_0;
    state_15 = G1_0;
    state_16 = M_0;
    state_17 = F1_0;
    state_18 = H_0;
    state_19 = F_0;
    state_20 = D_0;
    state_21 = B_0;
    state_22 = V_0;
    state_23 = T_0;
    state_24 = R_0;
    state_25 = P_0;
    state_26 = J_0;
    state_27 = I_0;
    state_28 = G_0;
    state_29 = E_0;
    state_30 = C_0;
    state_31 = A_0;
    state_32 = C1_0;
    state_33 = O_0;
    state_34 = L_0;
    state_35 = N_0;
    Q1_1 = __VERIFIER_nondet_int ();
    M1_1 = __VERIFIER_nondet__Bool ();
    I1_1 = __VERIFIER_nondet__Bool ();
    I2_1 = __VERIFIER_nondet__Bool ();
    E1_1 = __VERIFIER_nondet__Bool ();
    E2_1 = __VERIFIER_nondet_int ();
    A2_1 = __VERIFIER_nondet_int ();
    Z1_1 = __VERIFIER_nondet__Bool ();
    V1_1 = __VERIFIER_nondet__Bool ();
    R1_1 = __VERIFIER_nondet__Bool ();
    N1_1 = __VERIFIER_nondet_int ();
    J1_1 = __VERIFIER_nondet__Bool ();
    J2_1 = __VERIFIER_nondet__Bool ();
    F1_1 = __VERIFIER_nondet_int ();
    F2_1 = __VERIFIER_nondet_int ();
    B1_1 = __VERIFIER_nondet_int ();
    B2_1 = __VERIFIER_nondet_int ();
    W1_1 = __VERIFIER_nondet__Bool ();
    S1_1 = __VERIFIER_nondet__Bool ();
    O1_1 = __VERIFIER_nondet_int ();
    K1_1 = __VERIFIER_nondet__Bool ();
    K2_1 = __VERIFIER_nondet__Bool ();
    G1_1 = __VERIFIER_nondet_int ();
    G2_1 = __VERIFIER_nondet__Bool ();
    C1_1 = __VERIFIER_nondet__Bool ();
    C2_1 = __VERIFIER_nondet_int ();
    X1_1 = __VERIFIER_nondet__Bool ();
    T1_1 = __VERIFIER_nondet__Bool ();
    P1_1 = __VERIFIER_nondet_int ();
    L1_1 = __VERIFIER_nondet__Bool ();
    H1_1 = __VERIFIER_nondet__Bool ();
    H2_1 = __VERIFIER_nondet__Bool ();
    D1_1 = __VERIFIER_nondet__Bool ();
    D2_1 = __VERIFIER_nondet_int ();
    Y1_1 = __VERIFIER_nondet__Bool ();
    U1_1 = __VERIFIER_nondet__Bool ();
    T2_1 = state_0;
    W_1 = state_1;
    A1_1 = state_2;
    Z_1 = state_3;
    S2_1 = state_4;
    L2_1 = state_5;
    U_1 = state_6;
    N2_1 = state_7;
    S_1 = state_8;
    O2_1 = state_9;
    Q_1 = state_10;
    Y_1 = state_11;
    X_1 = state_12;
    R2_1 = state_13;
    K_1 = state_14;
    Q2_1 = state_15;
    M_1 = state_16;
    P2_1 = state_17;
    H_1 = state_18;
    F_1 = state_19;
    D_1 = state_20;
    B_1 = state_21;
    V_1 = state_22;
    T_1 = state_23;
    R_1 = state_24;
    P_1 = state_25;
    J_1 = state_26;
    I_1 = state_27;
    G_1 = state_28;
    E_1 = state_29;
    C_1 = state_30;
    A_1 = state_31;
    M2_1 = state_32;
    O_1 = state_33;
    L_1 = state_34;
    N_1 = state_35;
    if (!
        (((H_1 == 10) == F_1) && ((4 <= F2_1) == X1_1) && ((4 <= K_1) == D_1)
         && ((3 <= D2_1) == V1_1) && ((3 <= M_1) == B_1)
         && (!((J1_1 && I1_1) == R1_1)) && (!((Z_1 && A1_1) == S2_1))
         && (T2_1 == W_1) && (O2_1 == Q_1) && (N2_1 == S_1) && (M2_1 == O_1)
         && (L2_1 == U_1) && (C1_1 == M1_1) && (C1_1 == G2_1)
         && (D1_1 == K1_1) && (D1_1 == I2_1) && (H1_1 == L1_1)
         && (H1_1 == H2_1) && (I1_1 == L1_1) && (K1_1 == (J1_1 && (!I1_1)))
         && (M1_1 == ((!B_1) && (!D_1) && (!F_1) && Q_1))
         && (S1_1 == (W_1 && R1_1)) && (T1_1 == E1_1) && (V1_1 == U1_1)
         && (X1_1 == W1_1) && (Z1_1 == Y1_1) && (K2_1 == S1_1)
         && (K2_1 == J2_1) && (W_1 == V_1) && (U_1 == T_1) && (S_1 == R_1)
         && (Q_1 == P_1) && (F_1 == E_1) && (!(E_1 == E1_1)) && (D_1 == C_1)
         && (B_1 == A_1) && (R2_1 == K_1) && (Q2_1 == M_1) && (P2_1 == H_1)
         && (N1_1 == B1_1) && (P1_1 == O1_1) && (Q1_1 == G1_1)
         && (B2_1 == N1_1) && (B2_1 == A2_1) && (D2_1 == O1_1)
         && (D2_1 == C2_1) && (F2_1 == Q1_1) && (F2_1 == E2_1) && (M_1 == I_1)
         && (K_1 == J_1) && (H_1 == G_1) && ((!D1_1) || (!C1_1)
                                             || ((M_1 + (-1 * F1_1)) == -2))
         && ((!D1_1) || (!C1_1) || ((H_1 + (-1 * B1_1)) == -1)) && (H1_1
                                                                    || (P1_1
                                                                        ==
                                                                        F1_1)
                                                                    ||
                                                                    (!C1_1))
         && (S_1 || (Y_1 == X_1) || (!Q_1)) && ((D1_1 && C1_1)
                                                || (M_1 == F1_1))
         && ((H_1 == B1_1) || (D1_1 && C1_1)) && ((P1_1 == 0)
                                                  || ((!H1_1) && C1_1))
         && ((Y_1 == 0) || (Q_1 && (!S_1))) && ((!H1_1)
                                                || ((K_1 + (-1 * G1_1)) ==
                                                    -1)) && (H1_1
                                                             || (K_1 == G1_1))
         && ((B2_1 == 10) == Z1_1)))
        abort ();
    state_0 = S1_1;
    state_1 = K2_1;
    state_2 = I1_1;
    state_3 = J1_1;
    state_4 = R1_1;
    state_5 = K1_1;
    state_6 = D1_1;
    state_7 = L1_1;
    state_8 = H1_1;
    state_9 = M1_1;
    state_10 = C1_1;
    state_11 = P1_1;
    state_12 = F1_1;
    state_13 = Q1_1;
    state_14 = F2_1;
    state_15 = O1_1;
    state_16 = D2_1;
    state_17 = N1_1;
    state_18 = B2_1;
    state_19 = Z1_1;
    state_20 = X1_1;
    state_21 = V1_1;
    state_22 = J2_1;
    state_23 = I2_1;
    state_24 = H2_1;
    state_25 = G2_1;
    state_26 = E2_1;
    state_27 = C2_1;
    state_28 = A2_1;
    state_29 = Y1_1;
    state_30 = W1_1;
    state_31 = U1_1;
    state_32 = E1_1;
    state_33 = T1_1;
    state_34 = B1_1;
    state_35 = G1_1;
    goto state_0;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  state_0:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          J1_2 = state_0;
          W_2 = state_1;
          A1_2 = state_2;
          Z_2 = state_3;
          I1_2 = state_4;
          B1_2 = state_5;
          U_2 = state_6;
          D1_2 = state_7;
          S_2 = state_8;
          E1_2 = state_9;
          Q_2 = state_10;
          Y_2 = state_11;
          X_2 = state_12;
          H1_2 = state_13;
          K_2 = state_14;
          G1_2 = state_15;
          M_2 = state_16;
          F1_2 = state_17;
          H_2 = state_18;
          F_2 = state_19;
          D_2 = state_20;
          B_2 = state_21;
          V_2 = state_22;
          T_2 = state_23;
          R_2 = state_24;
          P_2 = state_25;
          J_2 = state_26;
          I_2 = state_27;
          G_2 = state_28;
          E_2 = state_29;
          C_2 = state_30;
          A_2 = state_31;
          C1_2 = state_32;
          O_2 = state_33;
          L_2 = state_34;
          N_2 = state_35;
          if (!(!O_2))
              abort ();
          goto main_error;

      case 1:
          Q1_1 = __VERIFIER_nondet_int ();
          M1_1 = __VERIFIER_nondet__Bool ();
          I1_1 = __VERIFIER_nondet__Bool ();
          I2_1 = __VERIFIER_nondet__Bool ();
          E1_1 = __VERIFIER_nondet__Bool ();
          E2_1 = __VERIFIER_nondet_int ();
          A2_1 = __VERIFIER_nondet_int ();
          Z1_1 = __VERIFIER_nondet__Bool ();
          V1_1 = __VERIFIER_nondet__Bool ();
          R1_1 = __VERIFIER_nondet__Bool ();
          N1_1 = __VERIFIER_nondet_int ();
          J1_1 = __VERIFIER_nondet__Bool ();
          J2_1 = __VERIFIER_nondet__Bool ();
          F1_1 = __VERIFIER_nondet_int ();
          F2_1 = __VERIFIER_nondet_int ();
          B1_1 = __VERIFIER_nondet_int ();
          B2_1 = __VERIFIER_nondet_int ();
          W1_1 = __VERIFIER_nondet__Bool ();
          S1_1 = __VERIFIER_nondet__Bool ();
          O1_1 = __VERIFIER_nondet_int ();
          K1_1 = __VERIFIER_nondet__Bool ();
          K2_1 = __VERIFIER_nondet__Bool ();
          G1_1 = __VERIFIER_nondet_int ();
          G2_1 = __VERIFIER_nondet__Bool ();
          C1_1 = __VERIFIER_nondet__Bool ();
          C2_1 = __VERIFIER_nondet_int ();
          X1_1 = __VERIFIER_nondet__Bool ();
          T1_1 = __VERIFIER_nondet__Bool ();
          P1_1 = __VERIFIER_nondet_int ();
          L1_1 = __VERIFIER_nondet__Bool ();
          H1_1 = __VERIFIER_nondet__Bool ();
          H2_1 = __VERIFIER_nondet__Bool ();
          D1_1 = __VERIFIER_nondet__Bool ();
          D2_1 = __VERIFIER_nondet_int ();
          Y1_1 = __VERIFIER_nondet__Bool ();
          U1_1 = __VERIFIER_nondet__Bool ();
          T2_1 = state_0;
          W_1 = state_1;
          A1_1 = state_2;
          Z_1 = state_3;
          S2_1 = state_4;
          L2_1 = state_5;
          U_1 = state_6;
          N2_1 = state_7;
          S_1 = state_8;
          O2_1 = state_9;
          Q_1 = state_10;
          Y_1 = state_11;
          X_1 = state_12;
          R2_1 = state_13;
          K_1 = state_14;
          Q2_1 = state_15;
          M_1 = state_16;
          P2_1 = state_17;
          H_1 = state_18;
          F_1 = state_19;
          D_1 = state_20;
          B_1 = state_21;
          V_1 = state_22;
          T_1 = state_23;
          R_1 = state_24;
          P_1 = state_25;
          J_1 = state_26;
          I_1 = state_27;
          G_1 = state_28;
          E_1 = state_29;
          C_1 = state_30;
          A_1 = state_31;
          M2_1 = state_32;
          O_1 = state_33;
          L_1 = state_34;
          N_1 = state_35;
          if (!
              (((H_1 == 10) == F_1) && ((4 <= F2_1) == X1_1)
               && ((4 <= K_1) == D_1) && ((3 <= D2_1) == V1_1)
               && ((3 <= M_1) == B_1) && (!((J1_1 && I1_1) == R1_1))
               && (!((Z_1 && A1_1) == S2_1)) && (T2_1 == W_1) && (O2_1 == Q_1)
               && (N2_1 == S_1) && (M2_1 == O_1) && (L2_1 == U_1)
               && (C1_1 == M1_1) && (C1_1 == G2_1) && (D1_1 == K1_1)
               && (D1_1 == I2_1) && (H1_1 == L1_1) && (H1_1 == H2_1)
               && (I1_1 == L1_1) && (K1_1 == (J1_1 && (!I1_1)))
               && (M1_1 == ((!B_1) && (!D_1) && (!F_1) && Q_1))
               && (S1_1 == (W_1 && R1_1)) && (T1_1 == E1_1) && (V1_1 == U1_1)
               && (X1_1 == W1_1) && (Z1_1 == Y1_1) && (K2_1 == S1_1)
               && (K2_1 == J2_1) && (W_1 == V_1) && (U_1 == T_1)
               && (S_1 == R_1) && (Q_1 == P_1) && (F_1 == E_1)
               && (!(E_1 == E1_1)) && (D_1 == C_1) && (B_1 == A_1)
               && (R2_1 == K_1) && (Q2_1 == M_1) && (P2_1 == H_1)
               && (N1_1 == B1_1) && (P1_1 == O1_1) && (Q1_1 == G1_1)
               && (B2_1 == N1_1) && (B2_1 == A2_1) && (D2_1 == O1_1)
               && (D2_1 == C2_1) && (F2_1 == Q1_1) && (F2_1 == E2_1)
               && (M_1 == I_1) && (K_1 == J_1) && (H_1 == G_1) && ((!D1_1)
                                                                   || (!C1_1)
                                                                   ||
                                                                   ((M_1 +
                                                                     (-1 *
                                                                      F1_1))
                                                                    == -2))
               && ((!D1_1) || (!C1_1) || ((H_1 + (-1 * B1_1)) == -1)) && (H1_1
                                                                          ||
                                                                          (P1_1
                                                                           ==
                                                                           F1_1)
                                                                          ||
                                                                          (!C1_1))
               && (S_1 || (Y_1 == X_1) || (!Q_1)) && ((D1_1 && C1_1)
                                                      || (M_1 == F1_1))
               && ((H_1 == B1_1) || (D1_1 && C1_1)) && ((P1_1 == 0)
                                                        || ((!H1_1) && C1_1))
               && ((Y_1 == 0) || (Q_1 && (!S_1))) && ((!H1_1)
                                                      || ((K_1 + (-1 * G1_1))
                                                          == -1)) && (H1_1
                                                                      || (K_1
                                                                          ==
                                                                          G1_1))
               && ((B2_1 == 10) == Z1_1)))
              abort ();
          state_0 = S1_1;
          state_1 = K2_1;
          state_2 = I1_1;
          state_3 = J1_1;
          state_4 = R1_1;
          state_5 = K1_1;
          state_6 = D1_1;
          state_7 = L1_1;
          state_8 = H1_1;
          state_9 = M1_1;
          state_10 = C1_1;
          state_11 = P1_1;
          state_12 = F1_1;
          state_13 = Q1_1;
          state_14 = F2_1;
          state_15 = O1_1;
          state_16 = D2_1;
          state_17 = N1_1;
          state_18 = B2_1;
          state_19 = Z1_1;
          state_20 = X1_1;
          state_21 = V1_1;
          state_22 = J2_1;
          state_23 = I2_1;
          state_24 = H2_1;
          state_25 = G2_1;
          state_26 = E2_1;
          state_27 = C2_1;
          state_28 = A2_1;
          state_29 = Y1_1;
          state_30 = W1_1;
          state_31 = U1_1;
          state_32 = E1_1;
          state_33 = T1_1;
          state_34 = B1_1;
          state_35 = G1_1;
          goto state_0;

      default:
          abort ();
      }

    // return expression

}

