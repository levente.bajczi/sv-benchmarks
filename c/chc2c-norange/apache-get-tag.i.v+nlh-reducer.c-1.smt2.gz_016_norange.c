// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: tricera-benchmarks/apache-get-tag.i.v+nlh-reducer.c-1.smt2.gz_016.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "apache-get-tag.i.v+nlh-reducer.c-1.smt2.gz_016_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int inv_main4_0;
    int inv_main4_1;
    int inv_main4_2;
    int inv_main4_3;
    int inv_main4_4;
    int inv_main4_5;
    int inv_main149_0;
    int inv_main149_1;
    int inv_main149_2;
    int inv_main149_3;
    int inv_main149_4;
    int inv_main149_5;
    int inv_main149_6;
    int inv_main149_7;
    int inv_main149_8;
    int inv_main149_9;
    int inv_main149_10;
    int inv_main188_0;
    int inv_main188_1;
    int inv_main188_2;
    int inv_main188_3;
    int inv_main188_4;
    int inv_main188_5;
    int inv_main188_6;
    int inv_main188_7;
    int inv_main188_8;
    int inv_main188_9;
    int inv_main188_10;
    int inv_main188_11;
    int inv_main188_12;
    int inv_main188_13;
    int inv_main188_14;
    int inv_main188_15;
    int inv_main188_16;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    int E_0;
    int F_0;
    int A_9;
    int B_9;
    int C_9;
    int D_9;
    int E_9;
    int F_9;
    int G_9;
    int H_9;
    int I_9;
    int J_9;
    int K_9;
    int L_9;
    int M_9;
    int N_9;
    int O_9;
    int P_9;
    int Q_9;
    int R_9;
    int S_9;
    int T_9;
    int U_9;
    int V_9;
    int W_9;
    int X_9;
    int Y_9;
    int Z_9;
    int A1_9;
    int B1_9;
    int C1_9;
    int D1_9;
    int E1_9;
    int F1_9;
    int G1_9;
    int H1_9;
    int I1_9;
    int J1_9;
    int K1_9;
    int L1_9;
    int M1_9;
    int N1_9;
    int O1_9;
    int P1_9;
    int v_42_9;
    int A_24;
    int B_24;
    int C_24;
    int D_24;
    int E_24;
    int F_24;
    int G_24;
    int H_24;
    int I_24;
    int J_24;
    int K_24;
    int L_24;
    int M_24;
    int N_24;
    int O_24;
    int P_24;
    int Q_24;
    int R_24;
    int S_24;
    int T_24;
    int U_24;
    int V_24;
    int W_24;
    int X_24;
    int Y_24;
    int Z_24;
    int A1_24;
    int B1_24;
    int C1_24;
    int D1_24;
    int E1_24;
    int F1_24;
    int G1_24;
    int H1_24;
    int I1_24;
    int J1_24;
    int K1_24;
    int L1_24;
    int M1_24;
    int N1_24;
    int O1_24;
    int P1_24;
    int A_25;
    int B_25;
    int C_25;
    int D_25;
    int E_25;
    int F_25;
    int G_25;
    int H_25;
    int I_25;
    int J_25;
    int K_25;
    int L_25;
    int M_25;
    int N_25;
    int O_25;
    int P_25;
    int Q_25;
    int R_25;
    int S_25;
    int T_25;
    int U_25;
    int V_25;
    int W_25;
    int X_25;
    int Y_25;
    int Z_25;
    int v_26_25;
    int v_27_25;
    int A_27;
    int B_27;
    int C_27;
    int D_27;
    int E_27;
    int F_27;
    int G_27;
    int H_27;
    int I_27;
    int J_27;
    int K_27;
    int L_27;
    int M_27;
    int N_27;
    int O_27;
    int P_27;
    int Q_27;



    // main logic
    goto main_init;

  main_init:
    if (!((E_0 == 0) && (C_0 == 0) && (B_0 == 0) && (F_0 == 0)))
        abort ();
    inv_main4_0 = E_0;
    inv_main4_1 = B_0;
    inv_main4_2 = F_0;
    inv_main4_3 = C_0;
    inv_main4_4 = A_0;
    inv_main4_5 = D_0;
    A_25 = __VERIFIER_nondet_int ();
    C_25 = __VERIFIER_nondet_int ();
    E_25 = __VERIFIER_nondet_int ();
    F_25 = __VERIFIER_nondet_int ();
    G_25 = __VERIFIER_nondet_int ();
    I_25 = __VERIFIER_nondet_int ();
    J_25 = __VERIFIER_nondet_int ();
    K_25 = __VERIFIER_nondet_int ();
    L_25 = __VERIFIER_nondet_int ();
    M_25 = __VERIFIER_nondet_int ();
    N_25 = __VERIFIER_nondet_int ();
    O_25 = __VERIFIER_nondet_int ();
    P_25 = __VERIFIER_nondet_int ();
    T_25 = __VERIFIER_nondet_int ();
    U_25 = __VERIFIER_nondet_int ();
    V_25 = __VERIFIER_nondet_int ();
    W_25 = __VERIFIER_nondet_int ();
    X_25 = __VERIFIER_nondet_int ();
    Y_25 = __VERIFIER_nondet_int ();
    v_27_25 = __VERIFIER_nondet_int ();
    Z_25 = __VERIFIER_nondet_int ();
    v_26_25 = __VERIFIER_nondet_int ();
    S_25 = inv_main4_0;
    H_25 = inv_main4_1;
    D_25 = inv_main4_2;
    R_25 = inv_main4_3;
    B_25 = inv_main4_4;
    Q_25 = inv_main4_5;
    if (!
        ((T_25 == (N_25 + -1)) && (P_25 == D_25) && (O_25 == 0)
         && (M_25 == V_25) && (L_25 == C_25) && (K_25 == 0) && (J_25 == K_25)
         && (I_25 == J_25) && (G_25 == 1) && (!(G_25 == 0)) && (F_25 == G_25)
         && (E_25 == O_25) && (C_25 == H_25) && (A_25 == G_25)
         && (Z_25 == U_25) && (Y_25 == P_25) && (!(X_25 == 0))
         && (W_25 == T_25) && (V_25 == R_25) && (U_25 == S_25) && (1 <= N_25)
         && (((0 <= (T_25 + (-1 * O_25))) && (X_25 == 1))
             || ((!(0 <= (T_25 + (-1 * O_25)))) && (X_25 == 0)))
         && (!(1 == N_25)) && (v_26_25 == I_25) && (v_27_25 == X_25)))
        abort ();
    inv_main149_0 = Z_25;
    inv_main149_1 = L_25;
    inv_main149_2 = Y_25;
    inv_main149_3 = I_25;
    inv_main149_4 = W_25;
    inv_main149_5 = E_25;
    inv_main149_6 = v_26_25;
    inv_main149_7 = F_25;
    inv_main149_8 = A_25;
    inv_main149_9 = X_25;
    inv_main149_10 = v_27_25;
    B_24 = __VERIFIER_nondet_int ();
    O1_24 = __VERIFIER_nondet_int ();
    C_24 = __VERIFIER_nondet_int ();
    D_24 = __VERIFIER_nondet_int ();
    M1_24 = __VERIFIER_nondet_int ();
    K1_24 = __VERIFIER_nondet_int ();
    G_24 = __VERIFIER_nondet_int ();
    H_24 = __VERIFIER_nondet_int ();
    I1_24 = __VERIFIER_nondet_int ();
    I_24 = __VERIFIER_nondet_int ();
    J_24 = __VERIFIER_nondet_int ();
    K_24 = __VERIFIER_nondet_int ();
    L_24 = __VERIFIER_nondet_int ();
    N_24 = __VERIFIER_nondet_int ();
    O_24 = __VERIFIER_nondet_int ();
    P_24 = __VERIFIER_nondet_int ();
    A1_24 = __VERIFIER_nondet_int ();
    Q_24 = __VERIFIER_nondet_int ();
    R_24 = __VERIFIER_nondet_int ();
    S_24 = __VERIFIER_nondet_int ();
    T_24 = __VERIFIER_nondet_int ();
    U_24 = __VERIFIER_nondet_int ();
    V_24 = __VERIFIER_nondet_int ();
    X_24 = __VERIFIER_nondet_int ();
    Z_24 = __VERIFIER_nondet_int ();
    P1_24 = __VERIFIER_nondet_int ();
    N1_24 = __VERIFIER_nondet_int ();
    L1_24 = __VERIFIER_nondet_int ();
    H1_24 = __VERIFIER_nondet_int ();
    F1_24 = __VERIFIER_nondet_int ();
    B1_24 = __VERIFIER_nondet_int ();
    Y_24 = inv_main149_0;
    E1_24 = inv_main149_1;
    F_24 = inv_main149_2;
    E_24 = inv_main149_3;
    G1_24 = inv_main149_4;
    M_24 = inv_main149_5;
    C1_24 = inv_main149_6;
    J1_24 = inv_main149_7;
    W_24 = inv_main149_8;
    D1_24 = inv_main149_9;
    A_24 = inv_main149_10;
    if (!
        ((L_24 == W_24) && (K_24 == F_24) && (J_24 == J1_24) && (I_24 == K_24)
         && (H_24 == P1_24) && (G_24 == E1_24) && (D_24 == Q_24)
         && (C_24 == L1_24) && (B_24 == G_24) && (P_24 == L1_24)
         && (O_24 == N1_24) && (N_24 == R_24) && (I1_24 == J_24)
         && (H1_24 == A1_24) && (!(F1_24 == 0)) && (B1_24 == M1_24)
         && (A1_24 == D1_24) && (Z_24 == S_24) && (X_24 == Y_24)
         && (V_24 == L_24) && (U_24 == H_24) && (T_24 == X_24)
         && (S_24 == (M_24 + 1)) && (R_24 == M_24) && (Q_24 == E_24)
         && (P1_24 == 0) && (O1_24 == A_24) && (N1_24 == E_24)
         && (M1_24 == G1_24) && (!(L1_24 == 0)) && (K1_24 == O1_24)
         && (((-1 <= M_24) && (L1_24 == 1))
             || ((!(-1 <= M_24)) && (L1_24 == 0)))
         && (((0 <= (M1_24 + (-1 * S_24))) && (F1_24 == 1))
             || ((!(0 <= (M1_24 + (-1 * S_24)))) && (F1_24 == 0)))
         && (!(M_24 == (G1_24 + -1)))))
        abort ();
    inv_main149_0 = T_24;
    inv_main149_1 = B_24;
    inv_main149_2 = I_24;
    inv_main149_3 = U_24;
    inv_main149_4 = B1_24;
    inv_main149_5 = Z_24;
    inv_main149_6 = D_24;
    inv_main149_7 = I1_24;
    inv_main149_8 = V_24;
    inv_main149_9 = H1_24;
    inv_main149_10 = K1_24;
    goto inv_main149_0;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  inv_main161:
    goto inv_main161;
  inv_main37:
    goto inv_main37;
  inv_main181:
    goto inv_main181;
  inv_main89:
    goto inv_main89;
  inv_main126:
    goto inv_main126;
  inv_main45:
    goto inv_main45;
  inv_main71:
    goto inv_main71;
  inv_main78:
    goto inv_main78;
  inv_main96:
    goto inv_main96;
  inv_main99:
    goto inv_main99;
  inv_main18:
    goto inv_main18;
  inv_main33:
    goto inv_main33;
  inv_main112:
    goto inv_main112;
  inv_main26:
    goto inv_main26;
  inv_main146:
    goto inv_main146;
  inv_main105:
    goto inv_main105;
  inv_main52:
    goto inv_main52;
  inv_main133:
    goto inv_main133;
  inv_main168:
    goto inv_main168;
  inv_main149_0:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          O1_9 = __VERIFIER_nondet_int ();
          C_9 = __VERIFIER_nondet_int ();
          D_9 = __VERIFIER_nondet_int ();
          E_9 = __VERIFIER_nondet_int ();
          F_9 = __VERIFIER_nondet_int ();
          K1_9 = __VERIFIER_nondet_int ();
          G_9 = __VERIFIER_nondet_int ();
          H_9 = __VERIFIER_nondet_int ();
          I1_9 = __VERIFIER_nondet_int ();
          I_9 = __VERIFIER_nondet_int ();
          J_9 = __VERIFIER_nondet_int ();
          L_9 = __VERIFIER_nondet_int ();
          E1_9 = __VERIFIER_nondet_int ();
          N_9 = __VERIFIER_nondet_int ();
          C1_9 = __VERIFIER_nondet_int ();
          O_9 = __VERIFIER_nondet_int ();
          P_9 = __VERIFIER_nondet_int ();
          A1_9 = __VERIFIER_nondet_int ();
          v_42_9 = __VERIFIER_nondet_int ();
          Q_9 = __VERIFIER_nondet_int ();
          R_9 = __VERIFIER_nondet_int ();
          S_9 = __VERIFIER_nondet_int ();
          T_9 = __VERIFIER_nondet_int ();
          U_9 = __VERIFIER_nondet_int ();
          W_9 = __VERIFIER_nondet_int ();
          Y_9 = __VERIFIER_nondet_int ();
          Z_9 = __VERIFIER_nondet_int ();
          N1_9 = __VERIFIER_nondet_int ();
          J1_9 = __VERIFIER_nondet_int ();
          H1_9 = __VERIFIER_nondet_int ();
          F1_9 = __VERIFIER_nondet_int ();
          B1_9 = __VERIFIER_nondet_int ();
          M1_9 = inv_main149_0;
          V_9 = inv_main149_1;
          L1_9 = inv_main149_2;
          G1_9 = inv_main149_3;
          P1_9 = inv_main149_4;
          B_9 = inv_main149_5;
          X_9 = inv_main149_6;
          M_9 = inv_main149_7;
          A_9 = inv_main149_8;
          K_9 = inv_main149_9;
          D1_9 = inv_main149_10;
          if (!
              ((J_9 == 0) && (I_9 == O_9) && (H_9 == C1_9) && (G_9 == I1_9)
               && (F_9 == G1_9) && (E_9 == F_9) && (D_9 == M1_9)
               && (C_9 == A_9) && (!(B_9 == (P1_9 + -1)))
               && (P_9 == (B_9 + 1)) && (O_9 == M_9) && (N_9 == D_9)
               && (J1_9 == L1_9) && (I1_9 == G1_9) && (H1_9 == B_9)
               && (!(F1_9 == 0)) && (E1_9 == T_9) && (C1_9 == K_9)
               && (B1_9 == J_9) && (A1_9 == F1_9) && (Z_9 == H1_9)
               && (Y_9 == F1_9) && (W_9 == P_9) && (U_9 == 0) && (T_9 == P1_9)
               && (S_9 == D1_9) && (R_9 == O1_9) && (Q_9 == B1_9)
               && (O1_9 == V_9) && (N1_9 == C_9) && (K1_9 == S_9)
               && (((-1 <= B_9) && (F1_9 == 1))
                   || ((!(-1 <= B_9)) && (F1_9 == 0)))
               && (((0 <= (T_9 + (-1 * P_9))) && (U_9 == 1))
                   || ((!(0 <= (T_9 + (-1 * P_9)))) && (U_9 == 0)))
               && (L_9 == J1_9) && (v_42_9 == U_9)))
              abort ();
          inv_main188_0 = N_9;
          inv_main188_1 = R_9;
          inv_main188_2 = L_9;
          inv_main188_3 = G_9;
          inv_main188_4 = E1_9;
          inv_main188_5 = W_9;
          inv_main188_6 = E_9;
          inv_main188_7 = I_9;
          inv_main188_8 = N1_9;
          inv_main188_9 = H_9;
          inv_main188_10 = K1_9;
          inv_main188_11 = Z_9;
          inv_main188_12 = Q_9;
          inv_main188_13 = A1_9;
          inv_main188_14 = Y_9;
          inv_main188_15 = U_9;
          inv_main188_16 = v_42_9;
          F_27 = inv_main188_0;
          C_27 = inv_main188_1;
          H_27 = inv_main188_2;
          Q_27 = inv_main188_3;
          M_27 = inv_main188_4;
          J_27 = inv_main188_5;
          E_27 = inv_main188_6;
          L_27 = inv_main188_7;
          I_27 = inv_main188_8;
          A_27 = inv_main188_9;
          G_27 = inv_main188_10;
          N_27 = inv_main188_11;
          D_27 = inv_main188_12;
          K_27 = inv_main188_13;
          P_27 = inv_main188_14;
          B_27 = inv_main188_15;
          O_27 = inv_main188_16;
          if (!1)
              abort ();
          goto main_error;

      case 1:
          B_24 = __VERIFIER_nondet_int ();
          O1_24 = __VERIFIER_nondet_int ();
          C_24 = __VERIFIER_nondet_int ();
          D_24 = __VERIFIER_nondet_int ();
          M1_24 = __VERIFIER_nondet_int ();
          K1_24 = __VERIFIER_nondet_int ();
          G_24 = __VERIFIER_nondet_int ();
          H_24 = __VERIFIER_nondet_int ();
          I1_24 = __VERIFIER_nondet_int ();
          I_24 = __VERIFIER_nondet_int ();
          J_24 = __VERIFIER_nondet_int ();
          K_24 = __VERIFIER_nondet_int ();
          L_24 = __VERIFIER_nondet_int ();
          N_24 = __VERIFIER_nondet_int ();
          O_24 = __VERIFIER_nondet_int ();
          P_24 = __VERIFIER_nondet_int ();
          A1_24 = __VERIFIER_nondet_int ();
          Q_24 = __VERIFIER_nondet_int ();
          R_24 = __VERIFIER_nondet_int ();
          S_24 = __VERIFIER_nondet_int ();
          T_24 = __VERIFIER_nondet_int ();
          U_24 = __VERIFIER_nondet_int ();
          V_24 = __VERIFIER_nondet_int ();
          X_24 = __VERIFIER_nondet_int ();
          Z_24 = __VERIFIER_nondet_int ();
          P1_24 = __VERIFIER_nondet_int ();
          N1_24 = __VERIFIER_nondet_int ();
          L1_24 = __VERIFIER_nondet_int ();
          H1_24 = __VERIFIER_nondet_int ();
          F1_24 = __VERIFIER_nondet_int ();
          B1_24 = __VERIFIER_nondet_int ();
          Y_24 = inv_main149_0;
          E1_24 = inv_main149_1;
          F_24 = inv_main149_2;
          E_24 = inv_main149_3;
          G1_24 = inv_main149_4;
          M_24 = inv_main149_5;
          C1_24 = inv_main149_6;
          J1_24 = inv_main149_7;
          W_24 = inv_main149_8;
          D1_24 = inv_main149_9;
          A_24 = inv_main149_10;
          if (!
              ((L_24 == W_24) && (K_24 == F_24) && (J_24 == J1_24)
               && (I_24 == K_24) && (H_24 == P1_24) && (G_24 == E1_24)
               && (D_24 == Q_24) && (C_24 == L1_24) && (B_24 == G_24)
               && (P_24 == L1_24) && (O_24 == N1_24) && (N_24 == R_24)
               && (I1_24 == J_24) && (H1_24 == A1_24) && (!(F1_24 == 0))
               && (B1_24 == M1_24) && (A1_24 == D1_24) && (Z_24 == S_24)
               && (X_24 == Y_24) && (V_24 == L_24) && (U_24 == H_24)
               && (T_24 == X_24) && (S_24 == (M_24 + 1)) && (R_24 == M_24)
               && (Q_24 == E_24) && (P1_24 == 0) && (O1_24 == A_24)
               && (N1_24 == E_24) && (M1_24 == G1_24) && (!(L1_24 == 0))
               && (K1_24 == O1_24) && (((-1 <= M_24) && (L1_24 == 1))
                                       || ((!(-1 <= M_24)) && (L1_24 == 0)))
               && (((0 <= (M1_24 + (-1 * S_24))) && (F1_24 == 1))
                   || ((!(0 <= (M1_24 + (-1 * S_24)))) && (F1_24 == 0)))
               && (!(M_24 == (G1_24 + -1)))))
              abort ();
          inv_main149_0 = T_24;
          inv_main149_1 = B_24;
          inv_main149_2 = I_24;
          inv_main149_3 = U_24;
          inv_main149_4 = B1_24;
          inv_main149_5 = Z_24;
          inv_main149_6 = D_24;
          inv_main149_7 = I1_24;
          inv_main149_8 = V_24;
          inv_main149_9 = H1_24;
          inv_main149_10 = K1_24;
          goto inv_main149_0;

      default:
          abort ();
      }

    // return expression

}

