// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: tricera-benchmarks/apache-get-tag.i.v+nlh-reducer.c-1.smt2.gz_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "apache-get-tag.i.v+nlh-reducer.c-1.smt2.gz_000_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int inv_main4_0;
    int inv_main4_1;
    int inv_main4_2;
    int inv_main4_3;
    int inv_main4_4;
    int inv_main4_5;
    int inv_main149_0;
    int inv_main149_1;
    int inv_main149_2;
    int inv_main149_3;
    int inv_main149_4;
    int inv_main149_5;
    int inv_main149_6;
    int inv_main149_7;
    int inv_main149_8;
    int inv_main149_9;
    int inv_main149_10;
    int inv_main18_0;
    int inv_main18_1;
    int inv_main18_2;
    int inv_main18_3;
    int inv_main18_4;
    int inv_main18_5;
    int inv_main18_6;
    int inv_main26_0;
    int inv_main26_1;
    int inv_main26_2;
    int inv_main26_3;
    int inv_main26_4;
    int inv_main26_5;
    int inv_main26_6;
    int inv_main26_7;
    int inv_main26_8;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    int E_0;
    int F_0;
    int A_5;
    int B_5;
    int C_5;
    int D_5;
    int E_5;
    int F_5;
    int G_5;
    int H_5;
    int I_5;
    int J_5;
    int K_5;
    int L_5;
    int M_5;
    int N_5;
    int O_5;
    int v_15_5;
    int A_11;
    int B_11;
    int C_11;
    int D_11;
    int E_11;
    int F_11;
    int G_11;
    int H_11;
    int I_11;
    int J_11;
    int K_11;
    int L_11;
    int M_11;
    int v_13_11;
    int A_12;
    int B_12;
    int C_12;
    int D_12;
    int E_12;
    int F_12;
    int G_12;
    int H_12;
    int I_12;
    int J_12;
    int v_10_12;
    int A_24;
    int B_24;
    int C_24;
    int D_24;
    int E_24;
    int F_24;
    int G_24;
    int H_24;
    int I_24;
    int J_24;
    int K_24;
    int L_24;
    int M_24;
    int N_24;
    int O_24;
    int P_24;
    int Q_24;
    int R_24;
    int S_24;
    int T_24;
    int U_24;
    int V_24;
    int W_24;
    int X_24;
    int Y_24;
    int Z_24;
    int A1_24;
    int B1_24;
    int C1_24;
    int D1_24;
    int E1_24;
    int F1_24;
    int G1_24;
    int H1_24;
    int I1_24;
    int J1_24;
    int K1_24;
    int L1_24;
    int M1_24;
    int N1_24;
    int O1_24;
    int P1_24;
    int A_25;
    int B_25;
    int C_25;
    int D_25;
    int E_25;
    int F_25;
    int G_25;
    int H_25;
    int I_25;
    int J_25;
    int K_25;
    int L_25;
    int M_25;
    int N_25;
    int O_25;
    int P_25;
    int Q_25;
    int R_25;
    int S_25;
    int T_25;
    int U_25;
    int V_25;
    int W_25;
    int X_25;
    int Y_25;
    int Z_25;
    int v_26_25;
    int v_27_25;
    int A_27;
    int B_27;
    int C_27;
    int D_27;
    int E_27;
    int F_27;
    int G_27;
    int H_27;
    int I_27;



    // main logic
    goto main_init;

  main_init:
    if (!((E_0 == 0) && (C_0 == 0) && (B_0 == 0) && (F_0 == 0)))
        abort ();
    inv_main4_0 = E_0;
    inv_main4_1 = B_0;
    inv_main4_2 = F_0;
    inv_main4_3 = C_0;
    inv_main4_4 = A_0;
    inv_main4_5 = D_0;
    goto inv_main4;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  inv_main161:
    goto inv_main161;
  inv_main37:
    goto inv_main37;
  inv_main181:
    goto inv_main181;
  inv_main89:
    goto inv_main89;
  inv_main126:
    goto inv_main126;
  inv_main45:
    goto inv_main45;
  inv_main71:
    goto inv_main71;
  inv_main4:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          A_25 = __VERIFIER_nondet_int ();
          C_25 = __VERIFIER_nondet_int ();
          E_25 = __VERIFIER_nondet_int ();
          F_25 = __VERIFIER_nondet_int ();
          G_25 = __VERIFIER_nondet_int ();
          I_25 = __VERIFIER_nondet_int ();
          J_25 = __VERIFIER_nondet_int ();
          K_25 = __VERIFIER_nondet_int ();
          L_25 = __VERIFIER_nondet_int ();
          M_25 = __VERIFIER_nondet_int ();
          N_25 = __VERIFIER_nondet_int ();
          O_25 = __VERIFIER_nondet_int ();
          P_25 = __VERIFIER_nondet_int ();
          T_25 = __VERIFIER_nondet_int ();
          U_25 = __VERIFIER_nondet_int ();
          V_25 = __VERIFIER_nondet_int ();
          W_25 = __VERIFIER_nondet_int ();
          X_25 = __VERIFIER_nondet_int ();
          Y_25 = __VERIFIER_nondet_int ();
          v_27_25 = __VERIFIER_nondet_int ();
          Z_25 = __VERIFIER_nondet_int ();
          v_26_25 = __VERIFIER_nondet_int ();
          S_25 = inv_main4_0;
          H_25 = inv_main4_1;
          D_25 = inv_main4_2;
          R_25 = inv_main4_3;
          B_25 = inv_main4_4;
          Q_25 = inv_main4_5;
          if (!
              ((T_25 == (N_25 + -1)) && (P_25 == D_25) && (O_25 == 0)
               && (M_25 == V_25) && (L_25 == C_25) && (K_25 == 0)
               && (J_25 == K_25) && (I_25 == J_25) && (G_25 == 1)
               && (!(G_25 == 0)) && (F_25 == G_25) && (E_25 == O_25)
               && (C_25 == H_25) && (A_25 == G_25) && (Z_25 == U_25)
               && (Y_25 == P_25) && (!(X_25 == 0)) && (W_25 == T_25)
               && (V_25 == R_25) && (U_25 == S_25) && (1 <= N_25)
               && (((0 <= (T_25 + (-1 * O_25))) && (X_25 == 1))
                   || ((!(0 <= (T_25 + (-1 * O_25)))) && (X_25 == 0)))
               && (!(1 == N_25)) && (v_26_25 == I_25) && (v_27_25 == X_25)))
              abort ();
          inv_main149_0 = Z_25;
          inv_main149_1 = L_25;
          inv_main149_2 = Y_25;
          inv_main149_3 = I_25;
          inv_main149_4 = W_25;
          inv_main149_5 = E_25;
          inv_main149_6 = v_26_25;
          inv_main149_7 = F_25;
          inv_main149_8 = A_25;
          inv_main149_9 = X_25;
          inv_main149_10 = v_27_25;
          B_24 = __VERIFIER_nondet_int ();
          O1_24 = __VERIFIER_nondet_int ();
          C_24 = __VERIFIER_nondet_int ();
          D_24 = __VERIFIER_nondet_int ();
          M1_24 = __VERIFIER_nondet_int ();
          K1_24 = __VERIFIER_nondet_int ();
          G_24 = __VERIFIER_nondet_int ();
          H_24 = __VERIFIER_nondet_int ();
          I1_24 = __VERIFIER_nondet_int ();
          I_24 = __VERIFIER_nondet_int ();
          J_24 = __VERIFIER_nondet_int ();
          K_24 = __VERIFIER_nondet_int ();
          L_24 = __VERIFIER_nondet_int ();
          N_24 = __VERIFIER_nondet_int ();
          O_24 = __VERIFIER_nondet_int ();
          P_24 = __VERIFIER_nondet_int ();
          A1_24 = __VERIFIER_nondet_int ();
          Q_24 = __VERIFIER_nondet_int ();
          R_24 = __VERIFIER_nondet_int ();
          S_24 = __VERIFIER_nondet_int ();
          T_24 = __VERIFIER_nondet_int ();
          U_24 = __VERIFIER_nondet_int ();
          V_24 = __VERIFIER_nondet_int ();
          X_24 = __VERIFIER_nondet_int ();
          Z_24 = __VERIFIER_nondet_int ();
          P1_24 = __VERIFIER_nondet_int ();
          N1_24 = __VERIFIER_nondet_int ();
          L1_24 = __VERIFIER_nondet_int ();
          H1_24 = __VERIFIER_nondet_int ();
          F1_24 = __VERIFIER_nondet_int ();
          B1_24 = __VERIFIER_nondet_int ();
          Y_24 = inv_main149_0;
          E1_24 = inv_main149_1;
          F_24 = inv_main149_2;
          E_24 = inv_main149_3;
          G1_24 = inv_main149_4;
          M_24 = inv_main149_5;
          C1_24 = inv_main149_6;
          J1_24 = inv_main149_7;
          W_24 = inv_main149_8;
          D1_24 = inv_main149_9;
          A_24 = inv_main149_10;
          if (!
              ((L_24 == W_24) && (K_24 == F_24) && (J_24 == J1_24)
               && (I_24 == K_24) && (H_24 == P1_24) && (G_24 == E1_24)
               && (D_24 == Q_24) && (C_24 == L1_24) && (B_24 == G_24)
               && (P_24 == L1_24) && (O_24 == N1_24) && (N_24 == R_24)
               && (I1_24 == J_24) && (H1_24 == A1_24) && (!(F1_24 == 0))
               && (B1_24 == M1_24) && (A1_24 == D1_24) && (Z_24 == S_24)
               && (X_24 == Y_24) && (V_24 == L_24) && (U_24 == H_24)
               && (T_24 == X_24) && (S_24 == (M_24 + 1)) && (R_24 == M_24)
               && (Q_24 == E_24) && (P1_24 == 0) && (O1_24 == A_24)
               && (N1_24 == E_24) && (M1_24 == G1_24) && (!(L1_24 == 0))
               && (K1_24 == O1_24) && (((-1 <= M_24) && (L1_24 == 1))
                                       || ((!(-1 <= M_24)) && (L1_24 == 0)))
               && (((0 <= (M1_24 + (-1 * S_24))) && (F1_24 == 1))
                   || ((!(0 <= (M1_24 + (-1 * S_24)))) && (F1_24 == 0)))
               && (!(M_24 == (G1_24 + -1)))))
              abort ();
          inv_main149_0 = T_24;
          inv_main149_1 = B_24;
          inv_main149_2 = I_24;
          inv_main149_3 = U_24;
          inv_main149_4 = B1_24;
          inv_main149_5 = Z_24;
          inv_main149_6 = D_24;
          inv_main149_7 = I1_24;
          inv_main149_8 = V_24;
          inv_main149_9 = H1_24;
          inv_main149_10 = K1_24;
          goto inv_main149_0;

      case 1:
          B_12 = __VERIFIER_nondet_int ();
          C_12 = __VERIFIER_nondet_int ();
          F_12 = __VERIFIER_nondet_int ();
          G_12 = __VERIFIER_nondet_int ();
          v_10_12 = __VERIFIER_nondet_int ();
          D_12 = inv_main4_0;
          E_12 = inv_main4_1;
          J_12 = inv_main4_2;
          A_12 = inv_main4_3;
          I_12 = inv_main4_4;
          H_12 = inv_main4_5;
          if (!
              ((B_12 == 0) && (G_12 == (C_12 + -1)) && (!(F_12 == 0))
               && (1 <= C_12) && (!(1 == C_12)) && (v_10_12 == F_12)))
              abort ();
          inv_main18_0 = D_12;
          inv_main18_1 = F_12;
          inv_main18_2 = J_12;
          inv_main18_3 = A_12;
          inv_main18_4 = G_12;
          inv_main18_5 = B_12;
          inv_main18_6 = v_10_12;
          A_5 = __VERIFIER_nondet_int ();
          C_5 = __VERIFIER_nondet_int ();
          E_5 = __VERIFIER_nondet_int ();
          I_5 = __VERIFIER_nondet_int ();
          J_5 = __VERIFIER_nondet_int ();
          K_5 = __VERIFIER_nondet_int ();
          N_5 = __VERIFIER_nondet_int ();
          O_5 = __VERIFIER_nondet_int ();
          v_15_5 = __VERIFIER_nondet_int ();
          M_5 = inv_main18_0;
          G_5 = inv_main18_1;
          H_5 = inv_main18_2;
          F_5 = inv_main18_3;
          B_5 = inv_main18_4;
          L_5 = inv_main18_5;
          D_5 = inv_main18_6;
          if (!
              ((E_5 == F_5) && (C_5 == M_5) && (A_5 == H_5) && (O_5 == G_5)
               && (N_5 == 0) && (K_5 == G_5) && (J_5 == B_5)
               && (((0 <= L_5) && (N_5 == 1))
                   || ((!(0 <= L_5)) && (N_5 == 0))) && (I_5 == L_5)
               && (v_15_5 == N_5)))
              abort ();
          inv_main26_0 = C_5;
          inv_main26_1 = K_5;
          inv_main26_2 = A_5;
          inv_main26_3 = E_5;
          inv_main26_4 = J_5;
          inv_main26_5 = I_5;
          inv_main26_6 = O_5;
          inv_main26_7 = N_5;
          inv_main26_8 = v_15_5;
          E_27 = inv_main26_0;
          B_27 = inv_main26_1;
          H_27 = inv_main26_2;
          A_27 = inv_main26_3;
          D_27 = inv_main26_4;
          G_27 = inv_main26_5;
          C_27 = inv_main26_6;
          I_27 = inv_main26_7;
          F_27 = inv_main26_8;
          if (!1)
              abort ();
          goto main_error;

      default:
          abort ();
      }
  inv_main78:
    goto inv_main78;
  inv_main96:
    goto inv_main96;
  inv_main99:
    goto inv_main99;
  inv_main188:
    goto inv_main188;
  inv_main33:
    goto inv_main33;
  inv_main112:
    goto inv_main112;
  inv_main146:
    goto inv_main146;
  inv_main105:
    goto inv_main105;
  inv_main52:
    goto inv_main52;
  inv_main133:
    goto inv_main133;
  inv_main168:
    goto inv_main168;
  inv_main149_0:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          B_24 = __VERIFIER_nondet_int ();
          O1_24 = __VERIFIER_nondet_int ();
          C_24 = __VERIFIER_nondet_int ();
          D_24 = __VERIFIER_nondet_int ();
          M1_24 = __VERIFIER_nondet_int ();
          K1_24 = __VERIFIER_nondet_int ();
          G_24 = __VERIFIER_nondet_int ();
          H_24 = __VERIFIER_nondet_int ();
          I1_24 = __VERIFIER_nondet_int ();
          I_24 = __VERIFIER_nondet_int ();
          J_24 = __VERIFIER_nondet_int ();
          K_24 = __VERIFIER_nondet_int ();
          L_24 = __VERIFIER_nondet_int ();
          N_24 = __VERIFIER_nondet_int ();
          O_24 = __VERIFIER_nondet_int ();
          P_24 = __VERIFIER_nondet_int ();
          A1_24 = __VERIFIER_nondet_int ();
          Q_24 = __VERIFIER_nondet_int ();
          R_24 = __VERIFIER_nondet_int ();
          S_24 = __VERIFIER_nondet_int ();
          T_24 = __VERIFIER_nondet_int ();
          U_24 = __VERIFIER_nondet_int ();
          V_24 = __VERIFIER_nondet_int ();
          X_24 = __VERIFIER_nondet_int ();
          Z_24 = __VERIFIER_nondet_int ();
          P1_24 = __VERIFIER_nondet_int ();
          N1_24 = __VERIFIER_nondet_int ();
          L1_24 = __VERIFIER_nondet_int ();
          H1_24 = __VERIFIER_nondet_int ();
          F1_24 = __VERIFIER_nondet_int ();
          B1_24 = __VERIFIER_nondet_int ();
          Y_24 = inv_main149_0;
          E1_24 = inv_main149_1;
          F_24 = inv_main149_2;
          E_24 = inv_main149_3;
          G1_24 = inv_main149_4;
          M_24 = inv_main149_5;
          C1_24 = inv_main149_6;
          J1_24 = inv_main149_7;
          W_24 = inv_main149_8;
          D1_24 = inv_main149_9;
          A_24 = inv_main149_10;
          if (!
              ((L_24 == W_24) && (K_24 == F_24) && (J_24 == J1_24)
               && (I_24 == K_24) && (H_24 == P1_24) && (G_24 == E1_24)
               && (D_24 == Q_24) && (C_24 == L1_24) && (B_24 == G_24)
               && (P_24 == L1_24) && (O_24 == N1_24) && (N_24 == R_24)
               && (I1_24 == J_24) && (H1_24 == A1_24) && (!(F1_24 == 0))
               && (B1_24 == M1_24) && (A1_24 == D1_24) && (Z_24 == S_24)
               && (X_24 == Y_24) && (V_24 == L_24) && (U_24 == H_24)
               && (T_24 == X_24) && (S_24 == (M_24 + 1)) && (R_24 == M_24)
               && (Q_24 == E_24) && (P1_24 == 0) && (O1_24 == A_24)
               && (N1_24 == E_24) && (M1_24 == G1_24) && (!(L1_24 == 0))
               && (K1_24 == O1_24) && (((-1 <= M_24) && (L1_24 == 1))
                                       || ((!(-1 <= M_24)) && (L1_24 == 0)))
               && (((0 <= (M1_24 + (-1 * S_24))) && (F1_24 == 1))
                   || ((!(0 <= (M1_24 + (-1 * S_24)))) && (F1_24 == 0)))
               && (!(M_24 == (G1_24 + -1)))))
              abort ();
          inv_main149_0 = T_24;
          inv_main149_1 = B_24;
          inv_main149_2 = I_24;
          inv_main149_3 = U_24;
          inv_main149_4 = B1_24;
          inv_main149_5 = Z_24;
          inv_main149_6 = D_24;
          inv_main149_7 = I1_24;
          inv_main149_8 = V_24;
          inv_main149_9 = H1_24;
          inv_main149_10 = K1_24;
          goto inv_main149_0;

      case 1:
          H_11 = __VERIFIER_nondet_int ();
          M_11 = __VERIFIER_nondet_int ();
          v_13_11 = __VERIFIER_nondet_int ();
          A_11 = inv_main149_0;
          K_11 = inv_main149_1;
          E_11 = inv_main149_2;
          F_11 = inv_main149_3;
          C_11 = inv_main149_4;
          L_11 = inv_main149_5;
          D_11 = inv_main149_6;
          G_11 = inv_main149_7;
          J_11 = inv_main149_8;
          B_11 = inv_main149_9;
          I_11 = inv_main149_10;
          if (!
              ((!(L_11 == (C_11 + -1))) && (H_11 == (L_11 + 1))
               && (!(M_11 == 0)) && (v_13_11 == F_11)))
              abort ();
          inv_main18_0 = A_11;
          inv_main18_1 = M_11;
          inv_main18_2 = E_11;
          inv_main18_3 = F_11;
          inv_main18_4 = C_11;
          inv_main18_5 = H_11;
          inv_main18_6 = v_13_11;
          A_5 = __VERIFIER_nondet_int ();
          C_5 = __VERIFIER_nondet_int ();
          E_5 = __VERIFIER_nondet_int ();
          I_5 = __VERIFIER_nondet_int ();
          J_5 = __VERIFIER_nondet_int ();
          K_5 = __VERIFIER_nondet_int ();
          N_5 = __VERIFIER_nondet_int ();
          O_5 = __VERIFIER_nondet_int ();
          v_15_5 = __VERIFIER_nondet_int ();
          M_5 = inv_main18_0;
          G_5 = inv_main18_1;
          H_5 = inv_main18_2;
          F_5 = inv_main18_3;
          B_5 = inv_main18_4;
          L_5 = inv_main18_5;
          D_5 = inv_main18_6;
          if (!
              ((E_5 == F_5) && (C_5 == M_5) && (A_5 == H_5) && (O_5 == G_5)
               && (N_5 == 0) && (K_5 == G_5) && (J_5 == B_5)
               && (((0 <= L_5) && (N_5 == 1))
                   || ((!(0 <= L_5)) && (N_5 == 0))) && (I_5 == L_5)
               && (v_15_5 == N_5)))
              abort ();
          inv_main26_0 = C_5;
          inv_main26_1 = K_5;
          inv_main26_2 = A_5;
          inv_main26_3 = E_5;
          inv_main26_4 = J_5;
          inv_main26_5 = I_5;
          inv_main26_6 = O_5;
          inv_main26_7 = N_5;
          inv_main26_8 = v_15_5;
          E_27 = inv_main26_0;
          B_27 = inv_main26_1;
          H_27 = inv_main26_2;
          A_27 = inv_main26_3;
          D_27 = inv_main26_4;
          G_27 = inv_main26_5;
          C_27 = inv_main26_6;
          I_27 = inv_main26_7;
          F_27 = inv_main26_8;
          if (!1)
              abort ();
          goto main_error;

      default:
          abort ();
      }

    // return expression

}

