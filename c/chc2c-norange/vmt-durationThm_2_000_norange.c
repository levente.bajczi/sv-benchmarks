// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: vmt-chc-benchmarks/vmt-durationThm_2_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "vmt-durationThm_2_000_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int state_0;
    int state_1;
    _Bool state_2;
    _Bool state_3;
    int state_4;
    int state_5;
    int state_6;
    int state_7;
    _Bool state_8;
    int state_9;
    int state_10;
    _Bool state_11;
    _Bool state_12;
    _Bool state_13;
    int state_14;
    _Bool state_15;
    _Bool state_16;
    int state_17;
    int state_18;
    int state_19;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    _Bool E_0;
    int F_0;
    int G_0;
    int H_0;
    int I_0;
    _Bool J_0;
    _Bool K_0;
    int L_0;
    _Bool M_0;
    _Bool N_0;
    _Bool O_0;
    int P_0;
    int Q_0;
    _Bool R_0;
    int S_0;
    _Bool T_0;
    int A_1;
    int B_1;
    int C_1;
    int D_1;
    _Bool E_1;
    int F_1;
    _Bool G_1;
    _Bool H_1;
    int I_1;
    int J_1;
    int K_1;
    int L_1;
    _Bool M_1;
    _Bool N_1;
    int O_1;
    _Bool P_1;
    int Q_1;
    _Bool R_1;
    int S_1;
    _Bool T_1;
    int U_1;
    int V_1;
    int W_1;
    int X_1;
    int Y_1;
    int Z_1;
    _Bool A1_1;
    int B1_1;
    _Bool C1_1;
    _Bool D1_1;
    int E1_1;
    _Bool F1_1;
    _Bool G1_1;
    int H1_1;
    _Bool I1_1;
    int J1_1;
    int K1_1;
    _Bool L1_1;
    int M1_1;
    _Bool N1_1;
    int A_2;
    int B_2;
    int C_2;
    int D_2;
    _Bool E_2;
    int F_2;
    int G_2;
    int H_2;
    int I_2;
    _Bool J_2;
    _Bool K_2;
    int L_2;
    _Bool M_2;
    _Bool N_2;
    _Bool O_2;
    int P_2;
    int Q_2;
    _Bool R_2;
    int S_2;
    _Bool T_2;



    // main logic
    goto main_init;

  main_init:
    if (!
        ((I_0 == L_0) && (G_0 == Q_0) && (G_0 == F_0) && (A_0 == 0)
         && (A_0 == S_0) && (H_0 == 0) && (H_0 == P_0)
         && (((R_0 && T_0) || (!K_0) || (!(Q_0 <= L_0))) == J_0)
         && (E_0 ==
             ((R_0 || (!(Q_0 <= P_0))) && (T_0 || (!(Q_0 <= S_0)))
              && (1 <= Q_0))) && (E_0 == M_0) && (M_0 == K_0) && (I_0 == 0)))
        abort ();
    state_0 = I_0;
    state_1 = L_0;
    state_2 = E_0;
    state_3 = M_0;
    state_4 = H_0;
    state_5 = P_0;
    state_6 = A_0;
    state_7 = S_0;
    state_8 = K_0;
    state_9 = G_0;
    state_10 = Q_0;
    state_11 = R_0;
    state_12 = T_0;
    state_13 = J_0;
    state_14 = F_0;
    state_15 = O_0;
    state_16 = N_0;
    state_17 = B_0;
    state_18 = C_0;
    state_19 = D_0;
    E_1 = __VERIFIER_nondet__Bool ();
    F_1 = __VERIFIER_nondet_int ();
    G_1 = __VERIFIER_nondet__Bool ();
    I1_1 = __VERIFIER_nondet__Bool ();
    G1_1 = __VERIFIER_nondet__Bool ();
    E1_1 = __VERIFIER_nondet_int ();
    C1_1 = __VERIFIER_nondet__Bool ();
    A1_1 = __VERIFIER_nondet__Bool ();
    Q_1 = __VERIFIER_nondet_int ();
    S_1 = __VERIFIER_nondet_int ();
    U_1 = __VERIFIER_nondet_int ();
    V_1 = __VERIFIER_nondet_int ();
    W_1 = __VERIFIER_nondet_int ();
    X_1 = __VERIFIER_nondet_int ();
    Y_1 = __VERIFIER_nondet_int ();
    Z_1 = __VERIFIER_nondet_int ();
    H1_1 = __VERIFIER_nondet_int ();
    F1_1 = __VERIFIER_nondet__Bool ();
    D1_1 = __VERIFIER_nondet__Bool ();
    B1_1 = __VERIFIER_nondet_int ();
    L_1 = state_0;
    O_1 = state_1;
    H_1 = state_2;
    P_1 = state_3;
    K_1 = state_4;
    J1_1 = state_5;
    A_1 = state_6;
    M1_1 = state_7;
    N_1 = state_8;
    J_1 = state_9;
    K1_1 = state_10;
    L1_1 = state_11;
    N1_1 = state_12;
    M_1 = state_13;
    I_1 = state_14;
    T_1 = state_15;
    R_1 = state_16;
    B_1 = state_17;
    C_1 = state_18;
    D_1 = state_19;
    if (!
        ((W_1 == Q_1) && (X_1 == V_1) && (Y_1 == X_1) && (Z_1 == U_1)
         && (B1_1 == W_1) && (E1_1 == S_1) && (L_1 == O_1) && (K_1 == J1_1)
         && (J_1 == K1_1) && (H1_1 == E1_1) && (A_1 == M1_1)
         && (((!N_1) || (!(K1_1 <= O_1)) || (L1_1 && N1_1)) == M_1)
         && (((!G1_1) || (!(Z_1 <= H1_1)) || (C1_1 && A1_1)) == F1_1)
         && (D1_1 ==
             (P_1 && (C1_1 || (!(Z_1 <= B1_1))) && (A1_1 || (!(Z_1 <= Y_1)))
              && (1 <= Z_1))) && (P_1 == N_1) && (H_1 == P_1)
         && (I1_1 == D1_1) && (I1_1 == G1_1) && ((!R_1) || (!T_1)
                                                 || ((O_1 + (-1 * S_1)) ==
                                                     -1)) && ((S_1 == 0)
                                                              || (R_1 && T_1))
         && ((!T_1) || ((J1_1 + (-1 * V_1)) == -1)) && (T_1 || (V_1 == 0))
         && ((!R_1) || ((M1_1 + (-1 * Q_1)) == -1)) && (R_1 || (Q_1 == 0))
         && (K1_1 == U_1)))
        abort ();
    state_0 = E1_1;
    state_1 = H1_1;
    state_2 = D1_1;
    state_3 = I1_1;
    state_4 = X_1;
    state_5 = Y_1;
    state_6 = W_1;
    state_7 = B1_1;
    state_8 = G1_1;
    state_9 = U_1;
    state_10 = Z_1;
    state_11 = A1_1;
    state_12 = C1_1;
    state_13 = F1_1;
    state_14 = F_1;
    state_15 = E_1;
    state_16 = G_1;
    state_17 = Q_1;
    state_18 = V_1;
    state_19 = S_1;
    goto state_0;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  state_0:
    switch (__VERIFIER_nondet__Bool ())
      {
      case 0:
          I_2 = state_0;
          L_2 = state_1;
          E_2 = state_2;
          M_2 = state_3;
          H_2 = state_4;
          P_2 = state_5;
          A_2 = state_6;
          S_2 = state_7;
          K_2 = state_8;
          G_2 = state_9;
          Q_2 = state_10;
          R_2 = state_11;
          T_2 = state_12;
          J_2 = state_13;
          F_2 = state_14;
          O_2 = state_15;
          N_2 = state_16;
          B_2 = state_17;
          C_2 = state_18;
          D_2 = state_19;
          if (!(!J_2))
              abort ();
          goto main_error;

      case 1:
          E_1 = __VERIFIER_nondet__Bool ();
          F_1 = __VERIFIER_nondet_int ();
          G_1 = __VERIFIER_nondet__Bool ();
          I1_1 = __VERIFIER_nondet__Bool ();
          G1_1 = __VERIFIER_nondet__Bool ();
          E1_1 = __VERIFIER_nondet_int ();
          C1_1 = __VERIFIER_nondet__Bool ();
          A1_1 = __VERIFIER_nondet__Bool ();
          Q_1 = __VERIFIER_nondet_int ();
          S_1 = __VERIFIER_nondet_int ();
          U_1 = __VERIFIER_nondet_int ();
          V_1 = __VERIFIER_nondet_int ();
          W_1 = __VERIFIER_nondet_int ();
          X_1 = __VERIFIER_nondet_int ();
          Y_1 = __VERIFIER_nondet_int ();
          Z_1 = __VERIFIER_nondet_int ();
          H1_1 = __VERIFIER_nondet_int ();
          F1_1 = __VERIFIER_nondet__Bool ();
          D1_1 = __VERIFIER_nondet__Bool ();
          B1_1 = __VERIFIER_nondet_int ();
          L_1 = state_0;
          O_1 = state_1;
          H_1 = state_2;
          P_1 = state_3;
          K_1 = state_4;
          J1_1 = state_5;
          A_1 = state_6;
          M1_1 = state_7;
          N_1 = state_8;
          J_1 = state_9;
          K1_1 = state_10;
          L1_1 = state_11;
          N1_1 = state_12;
          M_1 = state_13;
          I_1 = state_14;
          T_1 = state_15;
          R_1 = state_16;
          B_1 = state_17;
          C_1 = state_18;
          D_1 = state_19;
          if (!
              ((W_1 == Q_1) && (X_1 == V_1) && (Y_1 == X_1) && (Z_1 == U_1)
               && (B1_1 == W_1) && (E1_1 == S_1) && (L_1 == O_1)
               && (K_1 == J1_1) && (J_1 == K1_1) && (H1_1 == E1_1)
               && (A_1 == M1_1)
               && (((!N_1) || (!(K1_1 <= O_1)) || (L1_1 && N1_1)) == M_1)
               && (((!G1_1) || (!(Z_1 <= H1_1)) || (C1_1 && A1_1)) == F1_1)
               && (D1_1 ==
                   (P_1 && (C1_1 || (!(Z_1 <= B1_1)))
                    && (A1_1 || (!(Z_1 <= Y_1))) && (1 <= Z_1)))
               && (P_1 == N_1) && (H_1 == P_1) && (I1_1 == D1_1)
               && (I1_1 == G1_1) && ((!R_1) || (!T_1)
                                     || ((O_1 + (-1 * S_1)) == -1))
               && ((S_1 == 0) || (R_1 && T_1)) && ((!T_1)
                                                   || ((J1_1 + (-1 * V_1)) ==
                                                       -1)) && (T_1
                                                                || (V_1 == 0))
               && ((!R_1) || ((M1_1 + (-1 * Q_1)) == -1)) && (R_1
                                                              || (Q_1 == 0))
               && (K1_1 == U_1)))
              abort ();
          state_0 = E1_1;
          state_1 = H1_1;
          state_2 = D1_1;
          state_3 = I1_1;
          state_4 = X_1;
          state_5 = Y_1;
          state_6 = W_1;
          state_7 = B1_1;
          state_8 = G1_1;
          state_9 = U_1;
          state_10 = Z_1;
          state_11 = A1_1;
          state_12 = C1_1;
          state_13 = F1_1;
          state_14 = F_1;
          state_15 = E_1;
          state_16 = G_1;
          state_17 = Q_1;
          state_18 = V_1;
          state_19 = S_1;
          goto state_0;

      default:
          abort ();
      }

    // return expression

}

