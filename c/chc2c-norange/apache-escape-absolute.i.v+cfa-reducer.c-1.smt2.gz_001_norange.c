// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: tricera-benchmarks/apache-escape-absolute.i.v+cfa-reducer.c-1.smt2.gz_001.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "apache-escape-absolute.i.v+cfa-reducer.c-1.smt2.gz_001_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int inv_main48_0;
    int inv_main48_1;
    int inv_main48_2;
    int inv_main48_3;
    int inv_main48_4;
    int inv_main48_5;
    int inv_main48_6;
    int inv_main48_7;
    int inv_main48_8;
    int inv_main48_9;
    int inv_main48_10;
    int inv_main7_0;
    int inv_main7_1;
    int inv_main7_2;
    int inv_main7_3;
    int inv_main7_4;
    int inv_main7_5;
    int inv_main7_6;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    int E_0;
    int F_0;
    int G_0;
    int A_24;
    int B_24;
    int C_24;
    int D_24;
    int E_24;
    int F_24;
    int G_24;
    int H_24;
    int I_24;
    int J_24;
    int K_24;
    int L_24;
    int M_24;
    int N_24;
    int O_24;
    int P_24;
    int Q_24;
    int R_24;
    int S_24;
    int T_24;
    int U_24;
    int V_24;
    int W_24;
    int X_24;
    int Y_24;
    int Z_24;
    int A1_24;
    int B1_24;
    int v_28_24;
    int A_26;
    int B_26;
    int C_26;
    int D_26;
    int E_26;
    int F_26;
    int G_26;
    int H_26;
    int I_26;
    int J_26;
    int K_26;



    // main logic
    goto main_init;

  main_init:
    if (!((A_0 == 0) && (E_0 == 0)))
        abort ();
    inv_main7_0 = E_0;
    inv_main7_1 = A_0;
    inv_main7_2 = G_0;
    inv_main7_3 = F_0;
    inv_main7_4 = B_0;
    inv_main7_5 = D_0;
    inv_main7_6 = C_0;
    A_24 = __VERIFIER_nondet_int ();
    B_24 = __VERIFIER_nondet_int ();
    C_24 = __VERIFIER_nondet_int ();
    D_24 = __VERIFIER_nondet_int ();
    E_24 = __VERIFIER_nondet_int ();
    F_24 = __VERIFIER_nondet_int ();
    G_24 = __VERIFIER_nondet_int ();
    H_24 = __VERIFIER_nondet_int ();
    J_24 = __VERIFIER_nondet_int ();
    K_24 = __VERIFIER_nondet_int ();
    M_24 = __VERIFIER_nondet_int ();
    N_24 = __VERIFIER_nondet_int ();
    A1_24 = __VERIFIER_nondet_int ();
    Q_24 = __VERIFIER_nondet_int ();
    R_24 = __VERIFIER_nondet_int ();
    S_24 = __VERIFIER_nondet_int ();
    T_24 = __VERIFIER_nondet_int ();
    V_24 = __VERIFIER_nondet_int ();
    X_24 = __VERIFIER_nondet_int ();
    v_28_24 = __VERIFIER_nondet_int ();
    Y_24 = __VERIFIER_nondet_int ();
    B1_24 = __VERIFIER_nondet_int ();
    U_24 = inv_main7_0;
    Z_24 = inv_main7_1;
    P_24 = inv_main7_2;
    L_24 = inv_main7_3;
    I_24 = inv_main7_4;
    O_24 = inv_main7_5;
    W_24 = inv_main7_6;
    if (!
        ((S_24 == B_24) && (!(Q_24 == 0)) && (N_24 == S_24) && (M_24 == A1_24)
         && (K_24 == V_24) && (J_24 == Z_24) && (H_24 == W_24)
         && (G_24 == C_24) && (F_24 == 0) && (E_24 == D_24) && (D_24 == U_24)
         && (!(C_24 == 0)) && (A_24 == H_24) && (B1_24 == Y_24)
         && (A1_24 == R_24) && (Y_24 == Q_24) && (X_24 == C_24)
         && (V_24 == Q_24) && (-1000000 <= R_24) && (-1000000 <= Q_24)
         && (-1000000 <= B_24) && (1 <= R_24) && (1 <= B_24)
         && (!(0 <= (Q_24 + (-1 * B_24)))) && (0 <= Q_24) && (R_24 <= 1000000)
         && (Q_24 <= 1000000) && (B_24 <= 1000000)
         && (((1 <= V_24) && (F_24 == 1)) || ((!(1 <= V_24)) && (F_24 == 0)))
         && (((0 <= (B_24 + (-1 * Q_24))) && (C_24 == 1))
             || ((!(0 <= (B_24 + (-1 * Q_24)))) && (C_24 == 0)))
         && (T_24 == J_24) && (v_28_24 == F_24)))
        abort ();
    inv_main48_0 = E_24;
    inv_main48_1 = T_24;
    inv_main48_2 = B1_24;
    inv_main48_3 = N_24;
    inv_main48_4 = M_24;
    inv_main48_5 = K_24;
    inv_main48_6 = A_24;
    inv_main48_7 = X_24;
    inv_main48_8 = G_24;
    inv_main48_9 = F_24;
    inv_main48_10 = v_28_24;
    J_26 = inv_main48_0;
    G_26 = inv_main48_1;
    I_26 = inv_main48_2;
    C_26 = inv_main48_3;
    B_26 = inv_main48_4;
    F_26 = inv_main48_5;
    E_26 = inv_main48_6;
    A_26 = inv_main48_7;
    D_26 = inv_main48_8;
    H_26 = inv_main48_9;
    K_26 = inv_main48_10;
    if (!1)
        abort ();
    goto main_error;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  inv_main41:
    goto inv_main41;
  inv_main185:
    goto inv_main185;
  inv_main67:
    goto inv_main67;
  inv_main76:
    goto inv_main76;
  inv_main203:
    goto inv_main203;
  inv_main196:
    goto inv_main196;
  inv_main178:
    goto inv_main178;
  inv_main144:
    goto inv_main144;
  inv_main90:
    goto inv_main90;
  inv_main125:
    goto inv_main125;
  inv_main68:
    goto inv_main68;
  inv_main108:
    goto inv_main108;
  inv_main83:
    goto inv_main83;
  inv_main133:
    goto inv_main133;
  inv_main151:
    goto inv_main151;
  inv_main101:
    goto inv_main101;
  inv_main164:
    goto inv_main164;
  inv_main171:
    goto inv_main171;
  inv_main60:
    goto inv_main60;
  inv_main132:
    goto inv_main132;

    // return expression

}

