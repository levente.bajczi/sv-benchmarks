// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: tricera-benchmarks/apache-escape-absolute.i.v+cfa-reducer.c-1.smt2.gz_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "apache-escape-absolute.i.v+cfa-reducer.c-1.smt2.gz_000_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int inv_main41_0;
    int inv_main41_1;
    int inv_main41_2;
    int inv_main41_3;
    int inv_main41_4;
    int inv_main41_5;
    int inv_main41_6;
    int inv_main41_7;
    int inv_main41_8;
    int inv_main7_0;
    int inv_main7_1;
    int inv_main7_2;
    int inv_main7_3;
    int inv_main7_4;
    int inv_main7_5;
    int inv_main7_6;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    int E_0;
    int F_0;
    int G_0;
    int A_14;
    int B_14;
    int C_14;
    int D_14;
    int E_14;
    int F_14;
    int G_14;
    int H_14;
    int I_14;
    int J_14;
    int K_14;
    int L_14;
    int M_14;
    int N_14;
    int O_14;
    int P_14;
    int Q_14;
    int R_14;
    int v_18_14;
    int A_26;
    int B_26;
    int C_26;
    int D_26;
    int E_26;
    int F_26;
    int G_26;
    int H_26;
    int I_26;



    // main logic
    goto main_init;

  main_init:
    if (!((A_0 == 0) && (E_0 == 0)))
        abort ();
    inv_main7_0 = E_0;
    inv_main7_1 = A_0;
    inv_main7_2 = G_0;
    inv_main7_3 = F_0;
    inv_main7_4 = B_0;
    inv_main7_5 = D_0;
    inv_main7_6 = C_0;
    C_14 = __VERIFIER_nondet_int ();
    G_14 = __VERIFIER_nondet_int ();
    v_18_14 = __VERIFIER_nondet_int ();
    H_14 = __VERIFIER_nondet_int ();
    J_14 = __VERIFIER_nondet_int ();
    K_14 = __VERIFIER_nondet_int ();
    L_14 = __VERIFIER_nondet_int ();
    M_14 = __VERIFIER_nondet_int ();
    N_14 = __VERIFIER_nondet_int ();
    O_14 = __VERIFIER_nondet_int ();
    Q_14 = __VERIFIER_nondet_int ();
    R_14 = __VERIFIER_nondet_int ();
    D_14 = inv_main7_0;
    P_14 = inv_main7_1;
    I_14 = inv_main7_2;
    B_14 = inv_main7_3;
    F_14 = inv_main7_4;
    A_14 = inv_main7_5;
    E_14 = inv_main7_6;
    if (!
        ((J_14 == D_14) && (H_14 == Q_14) && (G_14 == O_14) && (R_14 == E_14)
         && (!(O_14 == 0)) && (N_14 == C_14) && (M_14 == 0) && (L_14 == P_14)
         && (-1000000 <= C_14) && (-1000000 <= Q_14) && (-1000000 <= O_14)
         && (1 <= C_14) && (1 <= Q_14) && (!(0 <= (O_14 + (-1 * C_14))))
         && (0 <= O_14) && (C_14 <= 1000000) && (Q_14 <= 1000000)
         && (O_14 <= 1000000) && (((0 <= (C_14 + (-1 * O_14))) && (M_14 == 1))
                                  || ((!(0 <= (C_14 + (-1 * O_14))))
                                      && (M_14 == 0))) && (K_14 == O_14)
         && (v_18_14 == M_14)))
        abort ();
    inv_main41_0 = J_14;
    inv_main41_1 = L_14;
    inv_main41_2 = G_14;
    inv_main41_3 = N_14;
    inv_main41_4 = H_14;
    inv_main41_5 = K_14;
    inv_main41_6 = R_14;
    inv_main41_7 = M_14;
    inv_main41_8 = v_18_14;
    I_26 = inv_main41_0;
    A_26 = inv_main41_1;
    B_26 = inv_main41_2;
    D_26 = inv_main41_3;
    E_26 = inv_main41_4;
    H_26 = inv_main41_5;
    G_26 = inv_main41_6;
    C_26 = inv_main41_7;
    F_26 = inv_main41_8;
    if (!1)
        abort ();
    goto main_error;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  inv_main185:
    goto inv_main185;
  inv_main67:
    goto inv_main67;
  inv_main76:
    goto inv_main76;
  inv_main203:
    goto inv_main203;
  inv_main196:
    goto inv_main196;
  inv_main178:
    goto inv_main178;
  inv_main144:
    goto inv_main144;
  inv_main90:
    goto inv_main90;
  inv_main125:
    goto inv_main125;
  inv_main68:
    goto inv_main68;
  inv_main48:
    goto inv_main48;
  inv_main108:
    goto inv_main108;
  inv_main83:
    goto inv_main83;
  inv_main133:
    goto inv_main133;
  inv_main151:
    goto inv_main151;
  inv_main101:
    goto inv_main101;
  inv_main164:
    goto inv_main164;
  inv_main171:
    goto inv_main171;
  inv_main60:
    goto inv_main60;
  inv_main132:
    goto inv_main132;

    // return expression

}

