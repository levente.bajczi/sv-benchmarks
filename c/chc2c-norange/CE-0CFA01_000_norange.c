// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: hopv/CE-0CFA01_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "CE-0CFA01_000_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int id_1030_unknown_29_0;
    int id_1030_unknown_29_1;
    int id_1030_unknown_29_2;
    int f_1034_unknown_23_0;
    int f_1034_unknown_23_1;
    int f_1034_unknown_23_2;
    int f_1034_unknown_23_3;
    int f_1034_unknown_23_4;
    int f_1034_unknown_23_5;
    int f_1034_unknown_23_6;
    int f_1034_unknown_23_7;
    int f_1034_unknown_23_8;
    int f_1034_unknown_23_9;
    int f_1034_unknown_23_10;
    int f_1034_unknown_23_11;
    int f_1034_unknown_23_12;
    int fail_unknown_25_0;
    int f_1034_unknown_19_0;
    int f_1034_unknown_19_1;
    int f_1034_unknown_19_2;
    int f_1034_unknown_19_3;
    int f_1034_unknown_19_4;
    int f_1034_unknown_19_5;
    int f_1034_unknown_19_6;
    int f_1034_unknown_19_7;
    int f_1034_unknown_19_8;
    int f_1034_unknown_19_9;
    int f_1034_unknown_19_10;
    int f_1034_unknown_19_11;
    int f_1034_unknown_19_12;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    int E_0;
    int F_0;
    int G_0;
    int H_0;
    int I_0;
    int J_0;
    int K_0;
    int L_0;
    int M_0;
    int N_0;
    int O_0;
    int P_0;
    int Q_0;
    int R_0;
    int S_0;
    int T_0;
    int U_0;
    int V_0;
    int W_0;
    int A_1;
    int B_1;
    int C_1;
    int D_1;
    int E_1;
    int F_1;
    int G_1;
    int H_1;
    int I_1;
    int J_1;
    int K_1;
    int L_1;
    int M_1;
    int A_2;
    int B_2;
    int C_2;
    int D_2;
    int E_2;
    int F_2;
    int G_2;
    int H_2;
    int I_2;
    int J_2;
    int K_2;
    int L_2;
    int M_2;
    int N_2;
    int O_2;
    int P_2;
    int Q_2;
    int R_2;
    int S_2;
    int T_2;
    int U_2;
    int V_2;
    int W_2;
    int X_2;
    int Y_2;
    int Z_2;
    int A_3;
    int B_3;
    int C_3;
    int D_3;
    int A_4;



    // main logic
    goto main_init;

  main_init:
    if (!
        ((M_0 == 0) && (L_0 == 0) && (K_0 == 0) && (J_0 == 0) && (I_0 == 0)
         && (H_0 == 0) && (G_0 == 0) && (F_0 == 0) && (E_0 == 0) && (D_0 == 0)
         && (C_0 == 0) && (B_0 == 0) && (A_0 == 0) && (S_0 == 0) && (R_0 == 0)
         && (Q_0 == 0) && (P_0 == 0) && (O_0 == 0) && (W_0 == 1) && (V_0 == 0)
         && (U_0 == 0) && (T_0 == 0) && (N_0 == 0)))
        abort ();
    f_1034_unknown_23_0 = W_0;
    f_1034_unknown_23_1 = V_0;
    f_1034_unknown_23_2 = U_0;
    f_1034_unknown_23_3 = T_0;
    f_1034_unknown_23_4 = S_0;
    f_1034_unknown_23_5 = A_0;
    f_1034_unknown_23_6 = R_0;
    f_1034_unknown_23_7 = Q_0;
    f_1034_unknown_23_8 = P_0;
    f_1034_unknown_23_9 = O_0;
    f_1034_unknown_23_10 = B_0;
    f_1034_unknown_23_11 = N_0;
    f_1034_unknown_23_12 = M_0;
    M_1 = f_1034_unknown_23_0;
    L_1 = f_1034_unknown_23_1;
    K_1 = f_1034_unknown_23_2;
    J_1 = f_1034_unknown_23_3;
    I_1 = f_1034_unknown_23_4;
    H_1 = f_1034_unknown_23_5;
    G_1 = f_1034_unknown_23_6;
    F_1 = f_1034_unknown_23_7;
    E_1 = f_1034_unknown_23_8;
    D_1 = f_1034_unknown_23_9;
    C_1 = f_1034_unknown_23_10;
    B_1 = f_1034_unknown_23_11;
    A_1 = f_1034_unknown_23_12;
    if (!1)
        abort ();
    f_1034_unknown_19_0 = M_1;
    f_1034_unknown_19_1 = L_1;
    f_1034_unknown_19_2 = K_1;
    f_1034_unknown_19_3 = J_1;
    f_1034_unknown_19_4 = I_1;
    f_1034_unknown_19_5 = H_1;
    f_1034_unknown_19_6 = G_1;
    f_1034_unknown_19_7 = F_1;
    f_1034_unknown_19_8 = E_1;
    f_1034_unknown_19_9 = D_1;
    f_1034_unknown_19_10 = C_1;
    f_1034_unknown_19_11 = B_1;
    f_1034_unknown_19_12 = A_1;
    E_2 = __VERIFIER_nondet_int ();
    F_2 = __VERIFIER_nondet_int ();
    G_2 = __VERIFIER_nondet_int ();
    H_2 = __VERIFIER_nondet_int ();
    I_2 = __VERIFIER_nondet_int ();
    J_2 = __VERIFIER_nondet_int ();
    K_2 = __VERIFIER_nondet_int ();
    L_2 = __VERIFIER_nondet_int ();
    M_2 = __VERIFIER_nondet_int ();
    N_2 = __VERIFIER_nondet_int ();
    W_2 = __VERIFIER_nondet_int ();
    X_2 = __VERIFIER_nondet_int ();
    Y_2 = __VERIFIER_nondet_int ();
    A_2 = f_1034_unknown_19_0;
    B_2 = f_1034_unknown_19_1;
    Z_2 = f_1034_unknown_19_2;
    V_2 = f_1034_unknown_19_3;
    U_2 = f_1034_unknown_19_4;
    C_2 = f_1034_unknown_19_5;
    T_2 = f_1034_unknown_19_6;
    S_2 = f_1034_unknown_19_7;
    R_2 = f_1034_unknown_19_8;
    Q_2 = f_1034_unknown_19_9;
    D_2 = f_1034_unknown_19_10;
    P_2 = f_1034_unknown_19_11;
    O_2 = f_1034_unknown_19_12;
    if (!
        ((P_2 == 0) && (O_2 == 0) && (N_2 == 0) && (M_2 == 0) && (L_2 == 0)
         && (K_2 == 0) && (J_2 == 0) && (I_2 == 0) && (H_2 == 0) && (G_2 == 0)
         && (F_2 == 0) && (E_2 == 0) && (D_2 == 0) && (C_2 == 0) && (V_2 == 0)
         && (U_2 == 0) && (T_2 == 0) && (S_2 == 0) && (R_2 == 0) && (Y_2 == 1)
         && (X_2 == 0) && (W_2 == 0) && (Q_2 == 0)))
        abort ();
    id_1030_unknown_29_0 = A_2;
    id_1030_unknown_29_1 = B_2;
    id_1030_unknown_29_2 = Z_2;
    D_3 = __VERIFIER_nondet_int ();
    A_3 = id_1030_unknown_29_0;
    C_3 = id_1030_unknown_29_1;
    B_3 = id_1030_unknown_29_2;
    if (!((D_3 == 1) && (!(0 == B_3))))
        abort ();
    fail_unknown_25_0 = D_3;
    A_4 = fail_unknown_25_0;
    if (!1)
        abort ();
    goto main_error;
  main_error:
    reach_error ();
  main_final:
    goto main_final;

    // return expression

}

