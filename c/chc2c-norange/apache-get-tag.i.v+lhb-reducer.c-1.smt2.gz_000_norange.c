// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: tricera-benchmarks/apache-get-tag.i.v+lhb-reducer.c-1.smt2.gz_000.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "apache-get-tag.i.v+lhb-reducer.c-1.smt2.gz_000_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int inv_main4_0;
    int inv_main4_1;
    int inv_main4_2;
    int inv_main4_3;
    int inv_main4_4;
    int inv_main4_5;
    int inv_main28_0;
    int inv_main28_1;
    int inv_main28_2;
    int inv_main28_3;
    int inv_main28_4;
    int inv_main28_5;
    int inv_main28_6;
    int inv_main28_7;
    int inv_main28_8;
    int inv_main28_9;
    int inv_main28_10;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    int E_0;
    int F_0;
    int A_22;
    int B_22;
    int C_22;
    int D_22;
    int E_22;
    int F_22;
    int G_22;
    int H_22;
    int I_22;
    int J_22;
    int K_22;
    int L_22;
    int M_22;
    int N_22;
    int O_22;
    int P_22;
    int Q_22;
    int R_22;
    int S_22;
    int T_22;
    int U_22;
    int V_22;
    int W_22;
    int X_22;
    int Y_22;
    int Z_22;
    int v_26_22;
    int A_27;
    int B_27;
    int C_27;
    int D_27;
    int E_27;
    int F_27;
    int G_27;
    int H_27;
    int I_27;
    int J_27;
    int K_27;



    // main logic
    goto main_init;

  main_init:
    if (!((E_0 == 0) && (D_0 == 0) && (A_0 == 0) && (F_0 == 0)))
        abort ();
    inv_main4_0 = F_0;
    inv_main4_1 = D_0;
    inv_main4_2 = A_0;
    inv_main4_3 = E_0;
    inv_main4_4 = B_0;
    inv_main4_5 = C_0;
    A_22 = __VERIFIER_nondet_int ();
    C_22 = __VERIFIER_nondet_int ();
    D_22 = __VERIFIER_nondet_int ();
    E_22 = __VERIFIER_nondet_int ();
    F_22 = __VERIFIER_nondet_int ();
    G_22 = __VERIFIER_nondet_int ();
    H_22 = __VERIFIER_nondet_int ();
    I_22 = __VERIFIER_nondet_int ();
    J_22 = __VERIFIER_nondet_int ();
    K_22 = __VERIFIER_nondet_int ();
    N_22 = __VERIFIER_nondet_int ();
    O_22 = __VERIFIER_nondet_int ();
    P_22 = __VERIFIER_nondet_int ();
    R_22 = __VERIFIER_nondet_int ();
    T_22 = __VERIFIER_nondet_int ();
    U_22 = __VERIFIER_nondet_int ();
    V_22 = __VERIFIER_nondet_int ();
    W_22 = __VERIFIER_nondet_int ();
    X_22 = __VERIFIER_nondet_int ();
    Y_22 = __VERIFIER_nondet_int ();
    v_26_22 = __VERIFIER_nondet_int ();
    Z_22 = inv_main4_0;
    M_22 = inv_main4_1;
    S_22 = inv_main4_2;
    Q_22 = inv_main4_3;
    B_22 = inv_main4_4;
    L_22 = inv_main4_5;
    if (!
        ((R_22 == 0) && (P_22 == D_22) && (O_22 == E_22) && (N_22 == U_22)
         && (!(K_22 == 0)) && (J_22 == (T_22 + -1)) && (I_22 == Y_22)
         && (H_22 == A_22) && (G_22 == M_22) && (F_22 == Z_22)
         && (E_22 == S_22) && (D_22 == 1) && (!(D_22 == 0)) && (C_22 == F_22)
         && (A_22 == Q_22) && (Y_22 == K_22) && (X_22 == G_22)
         && (W_22 == D_22) && (V_22 == J_22) && (U_22 == 0) && (1 <= T_22)
         && (((0 <= (J_22 + (-1 * U_22))) && (R_22 == 1))
             || ((!(0 <= (J_22 + (-1 * U_22)))) && (R_22 == 0)))
         && (!(1 == T_22)) && (v_26_22 == R_22)))
        abort ();
    inv_main28_0 = C_22;
    inv_main28_1 = X_22;
    inv_main28_2 = O_22;
    inv_main28_3 = H_22;
    inv_main28_4 = V_22;
    inv_main28_5 = N_22;
    inv_main28_6 = I_22;
    inv_main28_7 = P_22;
    inv_main28_8 = W_22;
    inv_main28_9 = R_22;
    inv_main28_10 = v_26_22;
    E_27 = inv_main28_0;
    H_27 = inv_main28_1;
    C_27 = inv_main28_2;
    F_27 = inv_main28_3;
    K_27 = inv_main28_4;
    G_27 = inv_main28_5;
    I_27 = inv_main28_6;
    A_27 = inv_main28_7;
    B_27 = inv_main28_8;
    D_27 = inv_main28_9;
    J_27 = inv_main28_10;
    if (!1)
        abort ();
    goto main_error;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  inv_main125:
    goto inv_main125;
  inv_main148:
    goto inv_main148;
  inv_main179:
    goto inv_main179;
  inv_main186:
    goto inv_main186;
  inv_main160:
    goto inv_main160;
  inv_main44:
    goto inv_main44;
  inv_main51:
    goto inv_main51;
  inv_main77:
    goto inv_main77;
  inv_main104:
    goto inv_main104;
  inv_main194:
    goto inv_main194;
  inv_main95:
    goto inv_main95;
  inv_main167:
    goto inv_main167;
  inv_main70:
    goto inv_main70;
  inv_main111:
    goto inv_main111;
  inv_main201:
    goto inv_main201;
  inv_main132:
    goto inv_main132;
  inv_main88:
    goto inv_main88;
  inv_main36:
    goto inv_main36;
  inv_main98:
    goto inv_main98;
  inv_main145:
    goto inv_main145;

    // return expression

}

