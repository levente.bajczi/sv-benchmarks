// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: tricera-benchmarks/apache-get-tag.i.v+lhb-reducer.c-1.smt2.gz_011.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "apache-get-tag.i.v+lhb-reducer.c-1.smt2.gz_011_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int inv_main4_0;
    int inv_main4_1;
    int inv_main4_2;
    int inv_main4_3;
    int inv_main4_4;
    int inv_main4_5;
    int inv_main145_0;
    int inv_main145_1;
    int inv_main145_2;
    int inv_main145_3;
    int inv_main145_4;
    int inv_main145_5;
    int inv_main145_6;
    int inv_main145_7;
    int inv_main145_8;
    int inv_main145_9;
    int inv_main145_10;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    int E_0;
    int F_0;
    int A_8;
    int B_8;
    int C_8;
    int D_8;
    int E_8;
    int F_8;
    int G_8;
    int H_8;
    int I_8;
    int J_8;
    int K_8;
    int L_8;
    int M_8;
    int N_8;
    int O_8;
    int P_8;
    int Q_8;
    int R_8;
    int S_8;
    int T_8;
    int U_8;
    int V_8;
    int W_8;
    int X_8;
    int Y_8;
    int Z_8;
    int v_26_8;
    int A_27;
    int B_27;
    int C_27;
    int D_27;
    int E_27;
    int F_27;
    int G_27;
    int H_27;
    int I_27;
    int J_27;
    int K_27;



    // main logic
    goto main_init;

  main_init:
    if (!((E_0 == 0) && (D_0 == 0) && (A_0 == 0) && (F_0 == 0)))
        abort ();
    inv_main4_0 = F_0;
    inv_main4_1 = D_0;
    inv_main4_2 = A_0;
    inv_main4_3 = E_0;
    inv_main4_4 = B_0;
    inv_main4_5 = C_0;
    B_8 = __VERIFIER_nondet_int ();
    C_8 = __VERIFIER_nondet_int ();
    D_8 = __VERIFIER_nondet_int ();
    F_8 = __VERIFIER_nondet_int ();
    G_8 = __VERIFIER_nondet_int ();
    H_8 = __VERIFIER_nondet_int ();
    I_8 = __VERIFIER_nondet_int ();
    J_8 = __VERIFIER_nondet_int ();
    K_8 = __VERIFIER_nondet_int ();
    L_8 = __VERIFIER_nondet_int ();
    O_8 = __VERIFIER_nondet_int ();
    P_8 = __VERIFIER_nondet_int ();
    Q_8 = __VERIFIER_nondet_int ();
    R_8 = __VERIFIER_nondet_int ();
    S_8 = __VERIFIER_nondet_int ();
    T_8 = __VERIFIER_nondet_int ();
    U_8 = __VERIFIER_nondet_int ();
    V_8 = __VERIFIER_nondet_int ();
    W_8 = __VERIFIER_nondet_int ();
    Z_8 = __VERIFIER_nondet_int ();
    v_26_8 = __VERIFIER_nondet_int ();
    Y_8 = inv_main4_0;
    E_8 = inv_main4_1;
    N_8 = inv_main4_2;
    M_8 = inv_main4_3;
    A_8 = inv_main4_4;
    X_8 = inv_main4_5;
    if (!
        ((T_8 == O_8) && (S_8 == M_8) && (R_8 == 1) && (!(R_8 == 0))
         && (Q_8 == R_8) && (P_8 == G_8) && (O_8 == 0) && (L_8 == R_8)
         && (K_8 == Y_8) && (I_8 == 0) && (H_8 == K_8) && (G_8 == N_8)
         && (F_8 == U_8) && (D_8 == W_8) && (C_8 == 0) && (B_8 == S_8)
         && (Z_8 == V_8) && (W_8 == E_8) && (V_8 == (J_8 + -1))
         && (U_8 == I_8) && (1 <= J_8)
         && (((!(0 <= (V_8 + (-1 * O_8)))) && (C_8 == 0))
             || ((0 <= (V_8 + (-1 * O_8))) && (C_8 == 1))) && (!(1 == J_8))
         && (v_26_8 == C_8)))
        abort ();
    inv_main145_0 = H_8;
    inv_main145_1 = D_8;
    inv_main145_2 = P_8;
    inv_main145_3 = B_8;
    inv_main145_4 = Z_8;
    inv_main145_5 = T_8;
    inv_main145_6 = F_8;
    inv_main145_7 = L_8;
    inv_main145_8 = Q_8;
    inv_main145_9 = C_8;
    inv_main145_10 = v_26_8;
    G_27 = inv_main145_0;
    I_27 = inv_main145_1;
    D_27 = inv_main145_2;
    F_27 = inv_main145_3;
    J_27 = inv_main145_4;
    A_27 = inv_main145_5;
    H_27 = inv_main145_6;
    K_27 = inv_main145_7;
    E_27 = inv_main145_8;
    B_27 = inv_main145_9;
    C_27 = inv_main145_10;
    if (!1)
        abort ();
    goto main_error;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  inv_main125:
    goto inv_main125;
  inv_main148:
    goto inv_main148;
  inv_main179:
    goto inv_main179;
  inv_main186:
    goto inv_main186;
  inv_main160:
    goto inv_main160;
  inv_main44:
    goto inv_main44;
  inv_main51:
    goto inv_main51;
  inv_main77:
    goto inv_main77;
  inv_main104:
    goto inv_main104;
  inv_main194:
    goto inv_main194;
  inv_main95:
    goto inv_main95;
  inv_main167:
    goto inv_main167;
  inv_main70:
    goto inv_main70;
  inv_main111:
    goto inv_main111;
  inv_main201:
    goto inv_main201;
  inv_main132:
    goto inv_main132;
  inv_main88:
    goto inv_main88;
  inv_main36:
    goto inv_main36;
  inv_main98:
    goto inv_main98;
  inv_main28:
    goto inv_main28;

    // return expression

}

