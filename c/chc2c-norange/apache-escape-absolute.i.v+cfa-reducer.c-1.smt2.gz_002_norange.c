// This file is part of the SV-Benchmarks collection of verification tasks:
// https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
//
// SPDX-FileCopyrightText: 2023 Levente Bajczi, Department of Measurement and Information Systems, Budapest University of Technology and Economics
//
// SPDX-License-Identifier: Apache-2.0

// Original CHC file: tricera-benchmarks/apache-escape-absolute.i.v+cfa-reducer.c-1.smt2.gz_002.smt2
extern void abort ();
extern int __VERIFIER_nondet_int ();
extern _Bool __VERIFIER_nondet__Bool ();
extern void __assert_fail(const char *, const char *, unsigned int, const char *) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__noreturn__));
void reach_error() { __assert_fail("0", "apache-escape-absolute.i.v+cfa-reducer.c-1.smt2.gz_002_norange.c", 13, "reach_error"); }



int main ();

int
main ()
{
    // return parameter


    // variables
    int inv_main7_0;
    int inv_main7_1;
    int inv_main7_2;
    int inv_main7_3;
    int inv_main7_4;
    int inv_main7_5;
    int inv_main7_6;
    int inv_main60_0;
    int inv_main60_1;
    int inv_main60_2;
    int inv_main60_3;
    int inv_main60_4;
    int inv_main60_5;
    int inv_main60_6;
    int inv_main60_7;
    int inv_main60_8;
    int inv_main60_9;
    int inv_main60_10;
    int inv_main60_11;
    int inv_main60_12;
    int inv_main60_13;
    int A_0;
    int B_0;
    int C_0;
    int D_0;
    int E_0;
    int F_0;
    int G_0;
    int A_13;
    int B_13;
    int C_13;
    int D_13;
    int E_13;
    int F_13;
    int G_13;
    int H_13;
    int I_13;
    int J_13;
    int K_13;
    int L_13;
    int M_13;
    int N_13;
    int O_13;
    int P_13;
    int Q_13;
    int R_13;
    int S_13;
    int T_13;
    int U_13;
    int V_13;
    int W_13;
    int X_13;
    int Y_13;
    int Z_13;
    int A1_13;
    int B1_13;
    int C1_13;
    int D1_13;
    int E1_13;
    int F1_13;
    int G1_13;
    int H1_13;
    int I1_13;
    int J1_13;
    int K1_13;
    int L1_13;
    int M1_13;
    int N1_13;
    int O1_13;
    int P1_13;
    int v_42_13;
    int A_26;
    int B_26;
    int C_26;
    int D_26;
    int E_26;
    int F_26;
    int G_26;
    int H_26;
    int I_26;
    int J_26;
    int K_26;
    int L_26;
    int M_26;
    int N_26;



    // main logic
    goto main_init;

  main_init:
    if (!((A_0 == 0) && (E_0 == 0)))
        abort ();
    inv_main7_0 = E_0;
    inv_main7_1 = A_0;
    inv_main7_2 = G_0;
    inv_main7_3 = F_0;
    inv_main7_4 = B_0;
    inv_main7_5 = D_0;
    inv_main7_6 = C_0;
    A_13 = __VERIFIER_nondet_int ();
    B_13 = __VERIFIER_nondet_int ();
    O1_13 = __VERIFIER_nondet_int ();
    D_13 = __VERIFIER_nondet_int ();
    M1_13 = __VERIFIER_nondet_int ();
    E_13 = __VERIFIER_nondet_int ();
    F_13 = __VERIFIER_nondet_int ();
    K1_13 = __VERIFIER_nondet_int ();
    G_13 = __VERIFIER_nondet_int ();
    H_13 = __VERIFIER_nondet_int ();
    I_13 = __VERIFIER_nondet_int ();
    J_13 = __VERIFIER_nondet_int ();
    G1_13 = __VERIFIER_nondet_int ();
    L_13 = __VERIFIER_nondet_int ();
    E1_13 = __VERIFIER_nondet_int ();
    N_13 = __VERIFIER_nondet_int ();
    P_13 = __VERIFIER_nondet_int ();
    A1_13 = __VERIFIER_nondet_int ();
    v_42_13 = __VERIFIER_nondet_int ();
    R_13 = __VERIFIER_nondet_int ();
    S_13 = __VERIFIER_nondet_int ();
    T_13 = __VERIFIER_nondet_int ();
    U_13 = __VERIFIER_nondet_int ();
    V_13 = __VERIFIER_nondet_int ();
    W_13 = __VERIFIER_nondet_int ();
    X_13 = __VERIFIER_nondet_int ();
    Y_13 = __VERIFIER_nondet_int ();
    Z_13 = __VERIFIER_nondet_int ();
    P1_13 = __VERIFIER_nondet_int ();
    N1_13 = __VERIFIER_nondet_int ();
    L1_13 = __VERIFIER_nondet_int ();
    J1_13 = __VERIFIER_nondet_int ();
    H1_13 = __VERIFIER_nondet_int ();
    F1_13 = __VERIFIER_nondet_int ();
    D1_13 = __VERIFIER_nondet_int ();
    B1_13 = __VERIFIER_nondet_int ();
    Q_13 = inv_main7_0;
    O_13 = inv_main7_1;
    M_13 = inv_main7_2;
    K_13 = inv_main7_3;
    C_13 = inv_main7_4;
    C1_13 = inv_main7_5;
    I1_13 = inv_main7_6;
    if (!
        ((G1_13 == E_13) && (F1_13 == O_13) && (E1_13 == I_13)
         && (D1_13 == J1_13) && (B1_13 == A_13) && (A1_13 == F1_13)
         && (Z_13 == 0) && (Y_13 == B1_13) && (X_13 == J_13) && (W_13 == Y_13)
         && (V_13 == J_13) && (U_13 == O1_13) && (T_13 == F_13)
         && (S_13 == N1_13) && (R_13 == N_13) && (P_13 == N1_13)
         && (!(L_13 == 0)) && (!(J_13 == 0)) && (I_13 == G_13)
         && (H_13 == I1_13) && (G_13 == Q_13) && (F_13 == S_13)
         && (E_13 == H_13) && (D_13 == L_13) && (B_13 == X_13)
         && (P1_13 == L_13) && (!(O1_13 == 0)) && (!(N1_13 == 0))
         && (M1_13 == K1_13) && (L1_13 == V_13) && (K1_13 == P_13)
         && (J1_13 == R_13) && (-1000000 <= N_13) && (-1000000 <= A_13)
         && (-1000000 <= N1_13) && (1 <= N_13) && (1 <= A_13)
         && (!(0 <= (N1_13 + (-1 * N_13)))) && (0 <= N1_13)
         && (N_13 <= 1000000) && (A_13 <= 1000000) && (N1_13 <= 1000000)
         && (((1 <= (J1_13 + (-1 * F_13))) && (Z_13 == 1))
             || ((!(1 <= (J1_13 + (-1 * F_13)))) && (Z_13 == 0)))
         && (((!(1 <= S_13)) && (L_13 == 0)) || ((1 <= S_13) && (L_13 == 1)))
         && (((!(0 <= (N_13 + (-1 * N1_13)))) && (J_13 == 0))
             || ((0 <= (N_13 + (-1 * N1_13))) && (J_13 == 1)))
         && (H1_13 == A1_13) && (v_42_13 == Z_13)))
        abort ();
    inv_main60_0 = E1_13;
    inv_main60_1 = H1_13;
    inv_main60_2 = M1_13;
    inv_main60_3 = D1_13;
    inv_main60_4 = W_13;
    inv_main60_5 = T_13;
    inv_main60_6 = G1_13;
    inv_main60_7 = B_13;
    inv_main60_8 = L1_13;
    inv_main60_9 = D_13;
    inv_main60_10 = P1_13;
    inv_main60_11 = U_13;
    inv_main60_12 = Z_13;
    inv_main60_13 = v_42_13;
    D_26 = inv_main60_0;
    G_26 = inv_main60_1;
    M_26 = inv_main60_2;
    H_26 = inv_main60_3;
    E_26 = inv_main60_4;
    N_26 = inv_main60_5;
    K_26 = inv_main60_6;
    L_26 = inv_main60_7;
    A_26 = inv_main60_8;
    J_26 = inv_main60_9;
    F_26 = inv_main60_10;
    C_26 = inv_main60_11;
    B_26 = inv_main60_12;
    I_26 = inv_main60_13;
    if (!1)
        abort ();
    goto main_error;
  main_error:
    reach_error ();
  main_final:
    goto main_final;
  inv_main41:
    goto inv_main41;
  inv_main185:
    goto inv_main185;
  inv_main67:
    goto inv_main67;
  inv_main76:
    goto inv_main76;
  inv_main203:
    goto inv_main203;
  inv_main196:
    goto inv_main196;
  inv_main178:
    goto inv_main178;
  inv_main144:
    goto inv_main144;
  inv_main90:
    goto inv_main90;
  inv_main125:
    goto inv_main125;
  inv_main68:
    goto inv_main68;
  inv_main48:
    goto inv_main48;
  inv_main108:
    goto inv_main108;
  inv_main83:
    goto inv_main83;
  inv_main133:
    goto inv_main133;
  inv_main151:
    goto inv_main151;
  inv_main101:
    goto inv_main101;
  inv_main164:
    goto inv_main164;
  inv_main171:
    goto inv_main171;
  inv_main132:
    goto inv_main132;

    // return expression

}

