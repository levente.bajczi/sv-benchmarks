<!--
This file is part of the SV-Benchmarks collection of verification tasks:
https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks

SPDX-FileCopyrightText: 2023 Dan Iorga <d.iorga17@imperial.ac.uk>

SPDX-License-Identifier: Apache-2.0
-->

The benchmarks in this directory were submitted by Dan Iorga, as part of a set of operational memory model tests.

